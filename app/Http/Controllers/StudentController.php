<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Studentcourse;
use App\DTO;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Coursesection;
use App\Lecture;
use App\Wallet;
use App\Paymentplatformlog;
use App\Course;
use App\Question;
use Illuminate\Support\Facades\DB;
use PDF;
use App\Studentcertificate;
use App\Studentlecture;
use App\Courseresource;
use Carbon\Carbon;
use App\Support;
use App\Feedback;
use App\Courserating;

class StudentController extends Controller
{
      private function RemainingDuration($dueDate)
        {
            $todayDate = Carbon::now();


            $d1 = $todayDate;
            $d2 = $dueDate;
            $interval = $d1->diff($d2);



           return $diffInMonths  = $interval->d; //4

        }


    public function lockedcourses()
    {
        $getDays;
        //dd($duration);
        if (Auth::check())
        {

            $user = Auth::user();
            $userId = $user->id;
            $studentCourses = DTO::getUserLockedCourses($userId);

            $countIsCompleted = 0;

        // student progress bar

        $countIsCompleted1 = Studentlecture::where('user_id', '=', $userId)->where('course_id', 50)->count();
        $countIsCompleted2 = Studentlecture::where('user_id', '=', $userId)->where('course_id', 50)->where('is_completed', 1)->count();


       if($countIsCompleted1 !=0)
       {
        $result = 100 / $countIsCompleted1;
        $countIsCompleted = round($result * $countIsCompleted2);
       }



            return view('students.lockedcourses', compact('studentCourses', 'countIsCompleted'));
        }
        else
        {
            return redirect()->route('login');

        }
    }


    public function completedcourses()
    {

        if (Auth::check())
        {

            $user = Auth::user();
            $userId = $user->id;
            $studentCourses = DTO::getUserCompletedCourses($userId);

            $countIsCompleted = 0;

        // student progress bar

        $countIsCompleted1 = Studentlecture::where('user_id', '=', $userId)->where('course_id', 50)->count();
        $countIsCompleted2 = Studentlecture::where('user_id', '=', $userId)->where('course_id', 50)->where('is_completed', 1)->count();


       if($countIsCompleted1 !=0)
       {
        $result = 100 / $countIsCompleted1;
        $countIsCompleted = round($result * $countIsCompleted2);
       }

            return view('students.completedcourses', compact('studentCourses', 'countIsCompleted'));
        }
        else
        {
            return redirect()->route('login');

        }

    }


    public function mycertificates()
    {
        //return view('students.mycertificates');
        if (Auth::check())
        {

            $user = Auth::user();
            $userId = $user->id;
            $studentCertificates = DTO::getUserCertificates($userId);
            return view('students.mycertificates', compact('studentCertificates'));
        }
        else
        {
            return redirect()->route('login');

        }
    }



    public function slug()
    {
        return view('students.slug');
    }

    public function coursesec(Request $request, $id)
    {
        if (Auth::check())
        {

            $user = Auth::user();
            $userId = $user->id;

           $studentCourseObj = Studentcourse::where('course_id', $id)->where('user_id', $userId)->first();


            $sections = DTO::getCourseSections($id);
            $courseObect = Course::find($id);


        // student progress bar
        $countIsCompleted1 = Lecture::where('course_id', $id)->count();
        $countIsCompleted2 = Studentlecture::where('user_id', '=', $userId)->where('course_id', $id)->where('is_completed', 1)->count();
        $firstLecture = Lecture::where('course_id', $id)->first();


        $countStudents = Studentcourse::where('course_id', $id)->count();

        $courseResources = Courseresource::where('course_id', $id)->get();

       if($countIsCompleted1 !=0)
       {
        $result = 100 / $countIsCompleted1;
        $countIsCompleted = round($result * $countIsCompleted2);
       }
       else
       {
          $countIsCompleted = 0;
       }

            $modules = Coursesection::where('course_id', $id)->get();
            $moduleArray = array();
            foreach ($modules as $key)
            {
              $lecture = new Lecture();
              $oneModule = array();
              $oneModule["name"] = $key->section_name .' '.$key->section_title;
              $oneModule["lectures"]  =  lecture::where('section_id', $key->id)->where('course_id', $courseObect->id)->get();
              // $lecture::join('lectures', 'lectures.id', '=', 'studentlectures.lecture_id')->select('studentlectures.id as studentlecture_id', 'studentlectures.is_completed', 'lectures.*')->where(['studentlectures.module_id' =>$key->id, 'studentlectures.course_id' =>$key->course_id, 'studentlectures.user_id' => $userId])->get();

              if(count( $oneModule["lectures"] ) > 0)
              {
                $moduleArray[] = $oneModule;
              }

            }




            return view('students.coursesec', compact('sections', 'id', 'moduleArray', 'courseObect', 'countIsCompleted', 'firstLecture', 'countStudents', 'courseResources', 'studentCourseObj'));
       // }
   }
        else
        {
            return redirect()->route('login');

        }







    }

    public function learning(Request $request, $id)
    {

        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;
            $lessons = Lecture::where('section_id', $id)->orderBy('id', 'asc')->get();
            $sectionObject = Coursesection::where('id', $id)->first();
            $courseId = $sectionObject->course_id;
            return view('students.lessons', compact('lessons', 'courseId'));
        }
        else
        {
            return redirect()->route('login');

        }

    }

    public function lesson(Request $request, $id)
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;
            $lesson = Lecture::where('id', $id)->first();
            $lessons = Lecture::where('section_id', $lesson->section_id)->orderBy('id', 'asc')->get();
            $courseId = $lesson->course_id;
            // update student course tracking

            // update lesson views
            $lessonObject = Lecture::find($id);
            $lessonObject->is_viewed = 0;
            $lessonObject->save();


           // $embed = \Embed::make('http://youtu.be/uifYHNyH-jA')->parseUrl();
            // Will return Embed class if provider is found. Otherwie will return false - not found. No fancy errors for now.
           // if ($embed) {
                // Set width of the embed.
              //  $embed->setAttribute(['width' => 600]);

                // Print html: '<iframe width="600" height="338" src="//www.youtube.com/embed/uifYHNyH-jA" frameborder="0" allowfullscreen></iframe>'.
                // Height will be set automatically based on provider width/height ratio.
                // Height could be set explicitly via setAttr() method.
               // echo $embed->getHtml();
          //  }

            return view('students.mainlesson', compact('lesson', 'lessons', 'courseId'));
        }
        else
        {
            return redirect()->route('login');

        }
    }





     //nsima
     public function fundwallet()
     {
       if (Auth::check())
       {

         $user = Auth::user();
         $userId = $user->id;
         return view('students.fundwallet', compact('userId'));
       }
       else
       {
           return redirect()->route('login');
       }
     }

     public function initiateWalletFunding(Request $request)
     {
       $user = Auth::user();
       $userId = $user->id;

       $Paymentplatformlog = new Paymentplatformlog();
       $Paymentplatformlog->user_id = $userId;
       $Paymentplatformlog->amount = $request->input('amount');
       $Paymentplatformlog->tempref = "L" . date("dmY"). substr(str_shuffle("ABCDEFGHIJKNPQRSTUVWXYZ"),-3);
       $Paymentplatformlog->purpose = "Funding of wallet";
       $Paymentplatformlog->name = $user->name;

       if($Paymentplatformlog->save())
       {
         return response()->json(['success'=>'nsima',   'amount' =>$Paymentplatformlog->amount, 'consumer_id'=>$Paymentplatformlog->id, 'username'=>$user->name, 'ref'=>$Paymentplatformlog->tempref, 'consumer_mac'=>$_SERVER['REMOTE_ADDR'], 'email'=>$user->email],200);
       }
       else{
           return response()->json(['error'=>'could not start'],200);
       }
     }



     public function verifyFundWalletReference(Request $request)
     {

       if (Auth::check())
       {
       $Paymentplatformlog = new Paymentplatformlog();
       $user = Auth::user();
       $userId = $user->id;
       $tx_ref = $request->input('tx_ref');
       $flw_ref = $request->input('flw_ref');
       $transaction_id = $request->input('transaction_id');



       $curl = curl_init();

       curl_setopt_array($curl, array(
       CURLOPT_URL => "https://api.flutterwave.com/v3/transactions/". $transaction_id. "/verify",
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_ENCODING => "",
       CURLOPT_MAXREDIRS => 10,
       CURLOPT_TIMEOUT => 0,
       CURLOPT_FOLLOWLOCATION => true,
       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
       CURLOPT_CUSTOMREQUEST => "GET",
       CURLOPT_HTTPHEADER => array(
         "Content-Type: application/json",
         "Authorization: Bearer FLWSECK_TEST-1f41c3dc4542b80e8df8bfb53fd2c568-X"
       ),
     ));

     $request = curl_exec($curl);
     curl_close($curl);
     // ends from fluer

         if ($request) {//if respose is gotten well
                $result = json_decode($request, true);
                if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['status'] === 'success'))
                {
                  echo "Transaction was successfull nsima";
                  $data = $result["data"];




                  //update Paymentplatformlog table
                  $Paymentplatformlog = new Paymentplatformlog();
                  $Paymentplatformlog = $Paymentplatformlog::where('tempref', $tx_ref)->first();
                  $Paymentplatformlog->verified = 1;
                  $Paymentplatformlog->paymentresponse = json_encode($data);
                  $Paymentplatformlog->transaction_id = $transaction_id;
                  $Paymentplatformlog->save();


                  //now add to wallet
                  $wallet =  new Wallet();
                  $wallet->user_id =  $userId;
                  $wallet->amount =  $Paymentplatformlog->amount;
                  $wallet->transaction_type =  'credit';
                  $wallet->transaction_id =  $Paymentplatformlog->transaction_id;
                  $wallet->date =  date("Y-m-d");
                  $wallet->Description =  "Credit using Flutter wave";
                  $wallet->save();

                }
                else{
                   echo "Transaction was not successfull";
                }
         }
         else{//if respose was not gotten

         }
     }

   }//end function


   public function viewwallet(Request $request)
   {

         if (Auth::check())
         {

           $user = Auth::user();
           $userId = $user->id;

           $wallet = new Wallet();
           $wallet  = $wallet::where('user_id', $userId)->get();
           $balance = 0;

           foreach ($wallet as $key )
           {
             if($key["transaction_type"] == 'debit')
             {
               $balance = $balance - $key["amount"];
             }
             if($key["transaction_type"] == 'credit')
             {
               $balance = $balance + $key["amount"];
             }
           }

           return view('students.viewwallet', compact('userId', 'wallet', 'balance'));
         }
         else
         {
             return redirect()->route('login');
         }

   }


   public function updateprofile()
   {
        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;
           $userObject = User::find($userId);
            return view('changeprofile', compact('userObject'));
        }
        else
        {
            return redirect()->route('login');

        }
       //return view('changeprofile');
   }

   public function postupdateprofile(Request $request)
   {
        $userId = $request['id'];

        $fullname = $request['fullname'];

        $address = $request['address'];
        $phone = $request['phone'];

        $user = User::find($userId);
        $user->name = $fullname;
        $user->address = $address;
        $user->phone = $phone;

        $user->save();
        return redirect('updateprofile')->with("success","Profile updated successfully");
   }


   public function postupdatepassword(Request $request)
   {

        if (!(\Hash::check($request->get('cpassword'), Auth::user()->password))) {

            return redirect('/updateprofile')->with('error', 'Your current password does not match with the password you provided. Please try again.');
        }

        if(strcmp($request->get('cpassword'), $request->get('npassword')) == 0){

            return redirect('/updateprofile')->with('error', 'New Password cannot be same as your current password. Please choose a different password.');
        }

        if(strcmp($request->get('password_confirmation'), $request->get('npassword')) != 0){

        return redirect('/updateprofile')->with('error', 'Confirmation password mismatch');
        }

        $user = Auth::user();
        $email = $user->email;
        $id = $user->id;

        $user = User::whereid($id)->FirstOrFail();

        $user->password = bcrypt($request->get('npassword'));
        $user->save();


        return redirect('updateprofile')->with("success","Password changed successfully !");
    }


    public function exam(Request $request, $id)
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;
            $userObject = User::find($userId);

            // check to see that students complete the course 100% before attempting exam.


            // student progress bar
        $countIsCompleted1 = Lecture::where('course_id', $id)->count();
        $countIsCompleted2 = Studentlecture::where('user_id', '=', $userId)->where('course_id', $id)->where('is_completed', 1)->count();


       if($countIsCompleted1 !=0)
       {
        $result = 100 / $countIsCompleted1;
        $countIsCompleted = round($result * $countIsCompleted2);
       }
       else
       {
          $countIsCompleted = 0;
       }


       if($countIsCompleted == 100)
       {
                $checkresult = Studentcertificate::where('user_id', $userId)->where('course_id', $id)->first();
                if($checkresult == null)
                {
                $questions = Question::where('course_id', $id)->OrderBy('id', 'asc')->get();
                return view('students.exam', compact('questions', 'id'));
                }
                else if($checkresult->score >= 70)
                {
                return redirect()->back()->with('error', 'You have ready passed this exam.');
                }
                else
                {
                    return redirect()->back()->with('error', 'Could not load exam.');

                }

       }
       else
       {
           return redirect()->back()->with('error', 'To attempt this exam, you must complete this course 100%. Your course progress is ' .$countIsCompleted . '%');
           dd("notification message");
       }

        }
        else
        {
            return redirect()->route('login');

        }

    }


    public Function submitExam(Request $request)
    {
        // Request contains the answers
        $answers = $request->all();

        $certNo = str_pad(mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);

        $points = 0;
        $percentage = 0;
       // $totalQuestion = 10;
        $mark;
        $userMark = 0;

        $course_id = $request->input('course_id');
        $user_name;

        if (Auth::check())
        {
            $id = Auth::user()->id;
        }
        else
        {
            return redirect()->route('login');

        }

        foreach ($answers as $questionId => $usersAnswer) {

            //if id is not a number then don't try to get an answer.
            if (is_numeric($questionId)) {

                $questionInfo = \DB::table('questions')->where('id', $questionId)->get();

                $correctAnswer = $questionInfo[0]->answer;
                $mark = $questionInfo[0]->mark;

                if ($correctAnswer == $usersAnswer) {
                    // give user a point
                   $userMark += $mark;

                }

            }


        }


        // calculate the final score
        if($userMark >= 70)
        {

            DB::table('studentcertificates')->insert([
                'user_id' => $id,
                'score' => $userMark,
                'course_id' => $course_id,
                'certificate_no' =>$certNo,
                'attempt' => 1,

            ]);

            return redirect()->route('mycertificates')->with('success', "Congratulations. You passed this exam");
        }
        else
        {

            $user = User::find($id);
            $user_name = $user->name;
            return view('students.examfailure', compact('course_id', 'user_name', 'userMark'));

        }


    }

    public function generatecertificate(Request $request)
    {
        $course_id = $request['course_id'];

        if (Auth::check())
        {

          $user = Auth::user();
          $userId = $user->id;


          $course = Course::find($course_id);
          $course_name = $course->course_title;

          $course_user_id = $course->user_id;
          $userObjectForSignature = User::find($course_user_id);
          $userObjectForSignatureAdmin = User::where('is_chairman', 1)->first();
          $studentCertificate = Studentcertificate::where('user_id', $userId)->first();
          $certNo = $studentCertificate->certificate_no;
          //$cert_date = $studentCertificate->created_at;

          $cert_date = Carbon::parse($studentCertificate->created_at)->format('d M Y');


         // dd($userObjectForSignature);

          $user = User::find($userId);
          $user_name = $user->certname;
          return view('students.certview', compact('course_name', 'user_name', 'userObjectForSignature', 'userObjectForSignatureAdmin', 'certNo', 'course', 'cert_date'));

        }
        else
        {
            return redirect()->route('login');
        }




    }


    public function examnotice()
    {
        return view('students.examnotice');
    }

    public function newlayout()
    {
        return view('students.studentcourses');
    }

    public function lecture($lecture, $courseId)
    {
        $lect_id = $lecture;



        if (Auth::check()) {

        $user = Auth::user();
        $userId = $user->id;

        $studentCourseObj = Studentcourse::where('course_id', $courseId)->where('user_id', $userId)->first();

        if($studentCourseObj->is_active == 0)
        {

             return redirect()->route('lockedcourses');


        }
        else
        {

        $modules = Coursesection::where('course_id', $courseId)->get();


        // GET lecture object
        $LectureObject = Lecture::where('course_id', $courseId)->where('id', $lecture)->first();



        // student progress bar
        $countIsCompleted1 = Lecture::where('course_id', $courseId)->count();
        //dd($countIsCompleted1);
        $countIsCompleted2 = Studentlecture::where('user_id', '=', $userId)->where('course_id', $courseId)->where('is_completed', 1)->count();


       if($countIsCompleted1 !=0)
       {
        $result = 100 / $countIsCompleted1;
        $countIsCompleted = round($result * $countIsCompleted2);
       }
       else
       {
          $countIsCompleted = 0;
       }


        $moduleArray = array();
        foreach ($modules as $key)
        {
           // $cc = DTO::getLectures($key->id, $courseId);

          $lecture = new Lecture();
          $oneModule = array();
          $oneModule["name"] = $key->section_name .' '.$key->section_title;
          $oneModule["lectures"]  = lecture::Leftjoin('studentlectures', 'lectures.id', '=', 'studentlectures.lecture_id')->select('lectures.*', 'studentlectures.is_completed')->where(['lectures.section_id' =>$key->id, 'lectures.course_id' =>$courseId])->orderby('id', 'asc')->get(); //lecture::where('section_id', $key->id)->where('course_id', $courseId)->get();

          //Lecture::where('section_id', $key->id)->where('course_id', $courseId)->get();


          /*\DB::table('lectures')
          ->join('studentlectures', 'lectures.id', '=', 'studentlectures.lecture_id')
          ->select('lectures.*', 'studentlectures.is_completed')
          ->where('lectures.section_id', $key->id)
          ->where('lectures.course_id', $courseId)
          ->get();*/

          //lecture::where('section_id', $key->id)->where('course_id', $courseId)->get();

          //lecture::Leftjoin('studentlectures', 'lectures.id', '=', 'studentlectures.lecture_id')->select('lectures.*', 'studentlectures.is_completed')->where(['lectures.section_id' =>$key->id, 'lectures.course_id' =>$courseId])->orderby('id', 'asc')->get(); //lecture::where('section_id', $key->id)->where('course_id', $courseId)->get(); //DTO::getLectures($key->id, $courseId);
         // $oneModule["is_completed"]  =  Studentlecture::where('user_id', $userId)->where('course_id', $courseId)->where('lecture_id', $lect_id)->get('is_completed');


          if(count( $oneModule["lectures"] ) > 0)
          {
            $moduleArray[] = $oneModule;
          }

        }


        return view('students.lecture', compact('moduleArray', 'LectureObject', 'countIsCompleted', 'courseId'));

    }
}
    else
    {
        return Redirect()->route('login');
    }
}





    public function completed($id, $course_id)
    {
        if (Auth::check()) {


            $user = Auth::user();
            $userId = $user->id;

            $checkStudentLectureEntry = Studentlecture::where('lecture_id', $id)->where('course_id', $course_id)->where('user_id', $userId)->first();
            if($checkStudentLectureEntry == null)
            {
                // Lecture not been added. Add it
                $studentlecture = new Studentlecture();
                $studentlecture->user_id = $userId;
                $studentlecture->course_id = $course_id;
                $studentlecture->lecture_id = $id;
                $studentlecture->is_completed = 1;
                $studentlecture->save();

                // Update student course and mark as completed if countiscompleted is 100
                         // student progress bar
        $countIsCompleted1 = Lecture::where('course_id', $course_id)->count();
        $countIsCompleted2 = Studentlecture::where('user_id', '=', $userId)->where('course_id', $course_id)->where('is_completed', 1)->count();

                if($countIsCompleted1 !=0)
                {
                 $result = 100 / $countIsCompleted1;
                 $countIsCompleted = round($result * $countIsCompleted2);
                }
                else
                {
                   $countIsCompleted = 0;
                }

                if($countIsCompleted == 100)
                {
                 // update student course and mark as complete.
                 $studentCourse = Studentcourse::where('user_id', '=', $userId)->where('course_id', $course_id)->first();
                 $studentCourse->is_completed = 1;
                 $studentCourse->save();

                 // Add student certificate data

                 $certNo = str_pad(mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);
                 DB::table('studentcertificates')->insert([
                                 'user_id' => $userId,
                                // 'score' => $userMark,
                                 'course_id' => $course_id,
                                 'certificate_no' =>$certNo,
                                // 'attempt' => 1,

                             ]);


                 // Send congratulations email

                 // Redirect a user to a congratulatory page after a course completion.

                 return redirect()->route('completion', $course_id);

                }




                return redirect()->back()->with('success', 'success');
            }
            else
            {

                return redirect()->back()->with('error', 'lecture already completed');

            }




        }
        else
        {
            return Redirect()->route('login');
        }



    }

    /*
    public function orderhistory(Request $request)
    {
        if (Auth::check()) {

            $user = Auth::user();
            $userId = $user->id;


            $selectStatment = \DB::getPdo()->prepare("SELECT c.course_title, p.created_at, p.amount_paid, p.tempref from courses as c inner join paymentplatformlogs as p
            on c.id = p.course_id where p.user_id =:user_id order by created_at desc");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$studentPayments = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
        return view('students.orderhistory', compact('studentPayments'));


        }
        else
        {
            return Redirect()->route('login');
        }

    }*/

    public function supports()
    {
        return view('students.support1');
    }

    public function submitsupport(Request $request)
    {

        $this->validate($request, [

            'subject' => 'required',
            'body' => 'required',

      ]);

      if (Auth::check()) {

        $user = Auth::user();
        $userId = $user->id;

      $subject = $request['subject'];
      $body = $request['body'];

      $support = new Support();
      $support->subject = $subject;
      $support->body = $body;
      $support->user_id = $userId;
      if($support->save())
      {
        // send email to Memdal admin
        return redirect()->back()->with('success', 'Thank you for your submission. Our team will response to your submission as soon as possible.');
      }
      else
      {
        return redirect()->back()->with('error', 'Sorry! Ypur request could not be submitted. Please try again later.');

      }



      }
      else
      {
        return Redirect()->route('login');
      }


    }





    public function submitfeedback(Request $request)
    {

        $this->validate($request, [

            'course_id' => 'required',
            'body' => 'required',
            'rating' => 'required'

      ]);

      if (Auth::check()) {

        $user = Auth::user();
        $userId = $user->id;

      $course_id = $request['course_id'];
      $body = $request['body'];
      $rating = $request['rating'];

      $feedback = new Courserating();
      $feedback->course_id = $course_id;
      $feedback->reviews = $body;
      $feedback->review_by = $userId;
      $feedback->star = $rating;
      if($feedback->save())
      {
        // send email to Memdal admin
        return redirect()->back()->with('success', 'Feedback received. Thank you for your feedback! Our team considers feedback to provide the best leaning experience.');
      }
      else
      {
        return redirect()->back()->with('error', 'Sorry! Your feedback could not be submitted. Please try again later.');

      }



      }
      else
      {
        return Redirect()->route('login');
      }


    }








    public function profile()
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;
            $userObject = User::find($userId);
            $certname = $userObject->name .' '. $userObject->lastname;
            return view('students.profile', compact('userObject', 'certname'));
        }
        else
        {
            return redirect()->route('login');

        }

    }

    public function saveprofile(Request $request)
    {

        $this->validate($request, [

            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'bio' => 'required',
            'userId'  => 'required',
        ]);

        $userId = $request['userId'];

        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $phone = $request['phone'];
        $bio = $request['bio'];
        $twitter = $request['twitter'];
        $instagram = $request['instagram'];
        $facebook = $request['facebook'];
        $linkedin = $request['linkedin'];
        $github = $request['github'];






        $user = User::find($userId);
        $user->name = $firstname;
        $user->lastname = $lastname;
        $user->phone = $phone;
        $user->bio = $bio;
        $user->twitter = $twitter;
        $user->instagram = $instagram;
        $user->facebook = $facebook;
        $user->linkedin = $linkedin;
        $user->github = $github;

        $user->save();
        return redirect('profile')->with("success","Great job! Profile updated successfully");

    }

    public function savecertname(Request $request)
    {

        $this->validate($request, [

            'certname' => 'required',
            'user_id'  => 'required',
        ]);

        $userId = $request['user_id'];
        $certname = $request['certname'];
        $user = User::find($userId);
        $user->certname = $certname;
        $user->save();
        return redirect('profile')->with("success","Great job! Certificate name set successfully");

    }

    public function changepassword()
    {
        return view('students.changepassword');
    }

   public function savepassword(Request $request)
   {

        if (!(\Hash::check($request->get('cpassword'), Auth::user()->password))) {

            return redirect('/changepassword')->with('error', 'Your current password does not match with the password you provided. Please try again.');
        }

        if(strcmp($request->get('cpassword'), $request->get('npassword')) == 0){

            return redirect('/changepassword')->with('error', 'New Password cannot be same as your current password. Please choose a different password.');
        }

        if(strcmp($request->get('password_confirmation'), $request->get('npassword')) != 0){

        return redirect('/changepassword')->with('error', 'Confirmation password mismatch');
        }

        $user = Auth::user();
        $email = $user->email;
        $id = $user->id;

        $user = User::whereid($id)->FirstOrFail();

        $user->password = bcrypt($request->get('npassword'));
        $user->save();


        return redirect('changepassword')->with("success","Password changed successfully !");
    }



    public function savephoto(Request $request)
    {

        $this->validate($request, [

            'user_id' => 'required',
            'photo' => 'required|image:jpeg,png,jpg,gif',
        ]);


            $image = $request->file('photo');
            if (isset($image)) {
                $name = $image->getClientOriginalName();
                $name = explode(".", $name);
                $count = count($name);
                $name = "student".str_shuffle("56789").time() . ".". $name[count($name) -1];
                $fileSize = $image->getSize();
                if (!$image->isValid()) {
                    return response()->json(['error'=>'Invalid file upload'], 401);
                } else {
                    $image->move('./storage/student', $name);
                }
            }

            $user_id = $request['user_id'];
            $user = User::findOrFail($user_id);
            $user->photo = 'storage/student/'.$name;
            $user->save();


            return redirect('profile')->with("success","Profile photo saved successfully !");


    }

    public function feedback($id)
    {

        $course = Course::find($id);
        if($course)
        {
            $selectCommand = \DB::getPdo()->prepare("select u.name, c.* from users as u inner join courseratings as c
            on u.id = c.review_by where c.course_id = :course_id order by c.id desc LIMIT 20");
            $selectCommand->bindValue(':course_id', $id, \PDO::PARAM_STR);
            $selectCommand->execute();
            $feedbacks = $selectCommand->fetchAll(\PDO::FETCH_OBJ);

            return view('students.feedback', compact('feedbacks', 'course'));

        }
        else
        {
            return redirect()->back()->with('error', 'Sorry! Invalid parameter given.');

        }
    }
























    public function classroom()
    {
      // return view('students.classroom');
      if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;


            $enrolledcourses = Studentcourse::where('user_id', $userId)->count();
            $completedcourses = Studentcourse::where('user_id', $userId)->where('is_completed', 1)->count();
            $activecourses = Studentcourse::where('is_active', 1)->where('user_id', $userId)->count();
            $lockedcourses = Studentcourse::where('is_active', 0)->where('user_id', $userId)->count();
            $certificatecount = Studentcertificate::where('user_id', $userId)->count();

            return view('students.classroom1', compact('activecourses', 'lockedcourses', 'certificatecount', 'enrolledcourses', 'completedcourses'));
        }
        else
        {
            return redirect()->route('login');

        }
    }



    public function mycourses()
    {
        $getDays;
        //dd($duration);
        if (Auth::check())
        {


            $user = Auth::user();
            $userId = $user->id;
            $studentCourses = DTO::getUserCourses($userId);



            $countIsCompleted = 0;

        // student progress bar

        $countIsCompleted1 = Studentlecture::where('user_id', '=', $userId)->where('course_id', 50)->count();
        $countIsCompleted2 = Studentlecture::where('user_id', '=', $userId)->where('course_id', 50)->where('is_completed', 1)->count();


       if($countIsCompleted1 !=0)
       {
        $result = 100 / $countIsCompleted1;
        $countIsCompleted = round($result * $countIsCompleted2);
       }



            return view('students.mycourses1', compact('studentCourses', 'countIsCompleted'));
        }
        else
        {
            return redirect()->route('login');

        }
    }



    public function orderhistory(Request $request)
    {
        if (Auth::check()) {

            $user = Auth::user();
            $userId = $user->id;


            $selectStatment = \DB::getPdo()->prepare("SELECT c.course_title, p.created_at, p.amount_paid, p.tempref from courses as c inner join paymentplatformlogs as p
            on c.id = p.course_id where p.user_id =:user_id order by created_at desc");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$studentPayments = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
        return view('students.orderhistory1', compact('studentPayments'));


        }
        else
        {
            return Redirect()->route('login');
        }

    }


    public function generatecertificate2()
    {
        //return view('students.certview');
        set_time_limit(300);

        $course_id = 50;//$request['course_id'];

        if (Auth::check())
        {

          $user = Auth::user();
          $userId = $user->id;


          $course = Course::find($course_id);
          $course_name = $course->course_title;

          $course_user_id = $course->user_id;
          $userObjectForSignature = User::find($course_user_id);
          $userObjectForSignatureAdmin = User::where('is_chairman', 1)->first();
          $studentCertificate = Studentcertificate::where('user_id', $userId)->first();
          $certNo = $studentCertificate->certificate_no;




        //$letter = Letter::findOrFail(1);

        $pdfPath = 'certificates/' .'Emmanuel-Bassey'. 434 . '.pdf';
       // $l = DTO::monitor_data(1);
        $view = View('students.certview', compact('certNo', 'course_name'));
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view->render())->save($pdfPath);

        //$letter->path = $pdfPath;
       // $letter->save();




         // dd($userObjectForSignature);
          $user = User::find($userId);
          $user_name = $user->name;
        }
        else
        {
            return redirect()->route('login');
        }

    }



    public function unlock(Request $request, $id)
    {

        $studentcourse = Studentcourse::find($id);
        $studentcourse->expire_date = Carbon::now()->addDays("30");
        $studentcourse->user_duration = "30";
        $studentcourse->is_active = 1;
        $studentcourse->save();

        return redirect()->back()->with('success', 'Course unlocked. You can now have access to course contents and resources. Happy learning');


    }

    public function completion($id)
    {
        if (Auth::check())
        {

          $user = Auth::user();
          $certname = $user->certname;
          $course = Course::find($id);
          return view('students.coursecompletion', compact('course', 'certname'));
        }
        else
        {
            return redirect()->route('login');
        }
    }


}
