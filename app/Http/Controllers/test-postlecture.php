public function postlecture(Request $request)
{

    $course_format = $request['lecture_format'];

 if($course_format == "pdf")
 {
    request()->validate([

        'file' => 'required|mimes:pdf,mp4,webm,ogv,MP4,mov,wmv,flv,avi,AVCHD,MKV',
        'course_id' => 'required',
        'course_section' => 'required',
        'lecture_title' => 'required',
        'lecture_format' => 'required',


        ]);

        //ini_set('memory_limit','256M');
        $course_id = $request['course_id'];
        $lecture_title = $request['lecture_title'];
        $course_section = $request['course_section'];
        $youtube_url = $request['youtube_url'];





        if($request->hasFile('file'))
        {

            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $name = explode(".", $name);
            $count = count($name);
            $name = "lecture".str_shuffle("56789").time() . ".". $name[count($name) -1];
            $fileSize = $file->getSize();
            if(!$file->isValid()) {
              return response()->json(['error'=>'Invalid file upload'], 201);
            }

            if($course_format =='pdf')
            {
                if($file->move('./lecturepdf', $name))
                {
                    $lecture = new Lecture();
                    $lecture->url = 'lecturepdf/'.$name;
                    $lecture->section_id = $course_section;
                    $lecture->course_id = $course_id;
                    $lecture->lecture_title = $lecture_title;
                    $lecture->format = $course_format;

                    $lecture->save();
                    $data = "Lecture uploaded successfully.";
                    return response()->json($data);
                }
            }
            else if($course_format =='video')
            {

              /*  $video = Youtube::upload($request->file('file')->getPathName(), [
                    'title'       => $lecture_title,
                    'description' => $lecture_title,
                    'status' => 'unlisted', // or 'private' or 'public'
                ]);*/
                // upload to YouTube

               // if($file->move('./lecturepdf', $name)
//$file->move('./lecturepdf', $name);
               /* $video = \Youtube::upload($name, [
                    'title'       => $lecture_title,
                    'description' => 'SAMPLE.',
                    'tags'	      => ['foo', 'bar', 'baz'],
                    'category_id' => 10
                ]);*/

              /*  \Youtube::upload(array(
                    'title' => $lecture_title,
                    'description' => 'This is what My video is about',
                    'status' => 'unlisted', // or 'private' or 'public'
                    'video' => $name, // Instance of Symfony\Component\HttpFoundation\File\UploadedFile see http://laravel.com/docs/requests#files
                ));*/

                //return $video->getVideoId();

                $lecture = new Lecture();
                //$lecture->url = $video->getVideoId(); // upload to YouTube
                $lecture->url = $youtube_url; // YouTube URL
                $lecture->section_id = $course_section;
                $lecture->course_id = $course_id;
                $lecture->lecture_title = $lecture_title;
                $lecture->format = $course_format;

                $lecture->save();
                $data = "Lecture uploaded successfully.";
                return response()->json($data);

            }
            else
            {
                $data = "Invalid course format selection. Please select either video or pdf";
                return response()->json($data);
            }


        }

        else
        {
            $data = "Please select file to upload";
            return response()->json($data);
        }
 }
 else if($course_format =='video')
 {
    request()->validate([

        'course_id' => 'required',
        'course_section' => 'required',
        'lecture_title' => 'required',
        'lecture_format' => 'required',
        'youtube_url' => 'required',


        ]);

       // ini_set('memory_limit','256M');
        $course_id = $request['course_id'];
        $lecture_title = $request['lecture_title'];
        $course_section = $request['course_section'];


        $youtube_url = $request['youtube_url'];

              /*  $video = Youtube::upload($request->file('file')->getPathName(), [
                    'title'       => $lecture_title,
                    'description' => $lecture_title,
                    'status' => 'unlisted', // or 'private' or 'public'
                ]);*/
                // upload to YouTube


                $lecture = new Lecture();
                //$lecture->url = $video->getVideoId(); // upload to YouTube
                $lecture->url = $youtube_url; // YouTube URL
                $lecture->section_id = $course_section;
                $lecture->course_id = $course_id;
                $lecture->lecture_title = $lecture_title;
                $lecture->format = $course_format;

                $lecture->save();
                $data = "Lecture uploaded successfully.";
                return response()->json($data);

            }
            else
            {
                $data = "Invalid course format selection. Please select either video or pdf";
                return response()->json($data);
            }
