<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Course;
use App\Coursesection;
use App\Lecture;
use Intervention\Image\Facades\Image as Image;
use App\Question;
use App\DTO;
use App\Studentcourse;
//use Youtube;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Project;
use Illuminate\Support\Str;
use App\Courseresource;
use App\Courseprojectimage;

class CourseController extends Controller
{

    public function courses()
    {

        if (Auth::check())
        {

            $user = Auth::user();
            $userId = $user->id;

            $courses = DTO::getMyCourses($userId);
            return view('courses.courselists', compact('courses'));
        }
        else
        {
            return redirect()->route('login');

        }

         /* $courses = Course::all();
        return view('courses.courselists', compact('courses'));*/


    }

    public function createcourse()
    {
        $programs = Program::orderby('program_name', 'ASC')->get();
        return view('courses.createcourse', compact('programs'));
    }

    public function postcourse(Request $request)
    {

        request()->validate([
            'course_objectives' => 'required',
            'course_full_description' => 'required',
            'program' => 'required',
            'duration' => 'required',
            'title'  => 'required',
            'course_type'  => 'required',
            'is_single'  => 'required',
            'course_brief_description' => 'required'
           /* 'lecture_title' => 'required',
            'lecture_format' => 'required',*/


            ]);

            if (Auth::check())
            {

                $user = Auth::user();
                $userId = $user->id;

                $slug = Str::slug($request['title'], '-');


        $course = new Course();
        $course->course_title = $request['title'];
        $course->duration = $request['duration'];

        $course->program_id = $request['program'];
       // $course->course_privacy = $request['privacy'];

        $course->course_price = $request['price'];
        $course->promo_price = $request['promo_price'];
        $course->course_type = $request['course_type'];
        $course->course_requirements = $request['course_requirements'];
        $course->course_objectives = $request['course_objectives'];
        $course->course_brief_description = $request['course_brief_description'];


       // $course->course_brief_description = $request['course_brief_description'];
        $course->course_full_description = $request['course_full_description'];
        $course->user_id = $userId;
        $course->is_single = $request['is_single'];
        $course->slug = $slug;
        $course->coupon_code = "STANDOUT101";
        $course->access_duration = 30; // pass this from a form field

       if($course->save())
       {
        //$courseObject = Course::find($course->id);
        $course_id = $course->id;
        return redirect()->route('coursesettings', $course_id);
       }
       else
       {
        return response()->json(['error'=>'error']);
       }

    }

    }


    public function coursesettings(Request $request, $id)
    {
        $courseObject = Course::find($id);
        $courseCount = Studentcourse::where('course_id', $id)->count();
        return view('courses.coursesettings', compact('courseObject', 'courseCount'));
    }



    public function postcontent(Request $request)
    {
     request()->validate([

            'file' => 'required|mimes:pdf',
            'course_id' => 'required',


        ]);


        $course = Course::find($request['course_id']);

        if($request->hasFile('file'))
        {

            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $name = explode(".", $name);
            $count = count($name);
            $name = "coursecontent".str_shuffle("56789").time() . ".". $name[count($name) -1];
            $fileSize = $file->getSize();
            if(!$file->isValid()) {
              return response()->json(['error'=>'Invalid file upload'], 200);
            }
            //if($file->move('./storage/coverimage', $name))
            if($file->move('./coursecontent', $name))
            {

                $course->content_url = 'coursecontent/'.$name;
                $course->save();
                $message = "done";
                return response()->json($message);
            }

        }

        else
        {
            $message = "Please select file to upload";
            return response()->json($message);
        }
    }

    public function postprojectimage(Request $request)
    {

        request()->validate([

            'file' => 'required|image|mimes:jpeg,png,jpg,gif',

        ]);


        $course = Course::find($request['course_id']);

        $course_id = $request['course_id'];

        if($request->hasFile('file'))
        {

            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $name = explode(".", $name);
            $count = count($name);
            $name = "courseprojectimages".str_shuffle("56789").time() . ".". $name[count($name) -1];
            $fileSize = $file->getSize();
            if(!$file->isValid()) {
              return response()->json(['error'=>'Invalid file upload'], 200);
            }
            //if($file->move('./storage/coverimage', $name))
            if($file->move('./courseprojectimages', $name))
            {

                $courseprojectimages = new Courseprojectimage();
                $courseprojectimages->images = 'courseprojectimages/'.$name;
                $courseprojectimages->course_id = $course_id;
                $courseprojectimages->save();
                $message = "done";
                return response()->json($message);
            }

        }

        else
        {
            $message = "Please select file to upload";
            return response()->json($message);
        }

    }


    public function postimage(Request $request)
    {
     request()->validate([

            'file' => 'required|image|mimes:jpeg,png,jpg,gif',

        ]);


        $course = Course::find($request['course_id']);

        if($request->hasFile('file'))
        {

            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $name = explode(".", $name);
            $count = count($name);
            $name = "coverimage".str_shuffle("56789").time() . ".". $name[count($name) -1];
            $fileSize = $file->getSize();
            if(!$file->isValid()) {
              return response()->json(['error'=>'Invalid file upload'], 200);
            }
            //if($file->move('./storage/coverimage', $name))
            if($file->move('./courseimages', $name))
            {

                $course->image_url = 'courseimages/'.$name;
                $course->save();
                $message = "done";
                return response()->json($message);
            }

        }

        else
        {
            $message = "Please select file to upload";
            return response()->json($message);
        }
    }


    public function managelectures(Request $request, $id)
    {
        $courseObject = Course::find($id);
        $countLectures = Lecture::where('course_id', $id)->count();
        return view('courses.managelectures', compact('courseObject', 'countLectures'));
    }

    public function load_sections(Request $request)
    {
        $course_id = $request['course_id'];
        $data = Coursesection::where('course_id', $course_id)->get();


        return response()->json($data);

    }

    public function load_lectures(Request $request)
    {
        $course_id = $request['course_id'];
        $data = Lecture::where('course_id', $course_id)->get();


        return response()->json($data);

    }

    public function loadlecturesbysectionid(Request $request)
    {
        $section_id = $request['section'];
        $data = Lecture::where('section_id', $section_id)->get();
        return response()->json($data);
    }

    public function Deletelectures(Request $request)
    {
        $id = $request['id'];
        $lecture = Lecture::find($id);

        if($lecture->delete())
        {
            $data = "done";
            return response()->json($data);
        }


    }

    public function postlecture(Request $request)
    {

            $course_id = $request['course_id'];
            $lecture_title = $request['lecture_title'];
            $course_section = $request['course_section'];
            $youtube_url = $request['youtube_url'];
            $file_url = $request['file_url'];
            $lecture_format = $request['lecture_format'];



            if($lecture_format == "pdf")
            {

                request()->validate([


                    'course_id' => 'required',
                    'course_section' => 'required',
                    'lecture_title' => 'required',
                    'lecture_format' => 'required',



                    ]);

                $lecture = new Lecture();
                $lecture->url = $file_url;
                $lecture->section_id = $course_section;
                $lecture->course_id = $course_id;
                $lecture->lecture_title = $lecture_title;
                $lecture->format = $lecture_format;

                $lecture->save();
                $data = "Lecture added successfully.";
                return response()->json($data);

            }
            elseif($lecture_format == "video")
            {

                request()->validate([

                    'course_id' => 'required',
                    'course_section' => 'required',
                    'lecture_title' => 'required',
                    'lecture_format' => 'required',
                    'youtube_url' => 'required',


                    ]);



            $course_id = $request['course_id'];
            $lecture_title = $request['lecture_title'];
            $course_section = $request['course_section'];
            $youtube_url = $request['youtube_url'];

            $lecture = new Lecture();
            $lecture->url = $youtube_url; // YouTube URL
            $lecture->section_id = $course_section;
            $lecture->course_id = $course_id;
            $lecture->lecture_title = $lecture_title;
            $lecture->format = $lecture_format;

             $lecture->save();
            $data = "Lecture added successfully.";
             return response()->json($data);



    }
    else
    {
        request()->validate([

            'course_id' => 'required',
            'course_section' => 'required',
            'lecture_title' => 'required',
            'lecture_format' => 'required',
            'text_lecture' => 'required',


            ]);



     $lnote = $request['text_lecture'];
     $formattedNote = str_replace('..', '', $lnote);

    $course_id = $request['course_id'];
    $lecture_title = $request['lecture_title'];
    $course_section = $request['course_section'];
    $text_lecture = $request['text_lecture'];

    $lecture = new Lecture();
    $lecture->url = $formattedNote; // Text lecture
    $lecture->section_id = $course_section;
    $lecture->course_id = $course_id;
    $lecture->lecture_title = $lecture_title;
    $lecture->format = $lecture_format;

     $lecture->save();
    $data = "Lecture added successfully.";
     return response()->json($data);

    }



    }






    public function managesections(Request $request, $id)
    {
        $courseObject = Course::find($id);
        //$countLectures = Lecture::where('course_id', $id)->count();
        return view('courses.managesections', compact('courseObject'));
    }


    public function DeleteSection(Request $request)
    {
        $id = $request['id'];
        $section = Coursesection::find($id);

        if($section->delete())
        {
            $data = "done";
            return response()->json($data);
        }


    }

    public function postsection(Request $request)
    {
        request()->validate([


            'section_name' => 'required',
            'section_title' => 'required',


            ]);

        $course_id = $request['course_id'];
        $section_name = $request['section_name'];
        $section_title = $request['section_title'];

        $section = new Coursesection();
        $section->course_id = $course_id;
        $section->section_name = $section_name;
        $section->section_title = $section_title;

        if($section->save())
        {
            $data = "Course section created successfully.";
            return response()->json($data);
        }
        else
        {
            $data = "Could not create Course section! Please try again.";
            return response()->json($data);
        }
    }

    public function getSectionById(Request $request)
    {
        $section_id = $request['id'];
        $data = Coursesection::find($section_id);
        return response()->json($data);
    }

    public function updatesection(Request $request)
    {
        request()->validate([


            'section_name' => 'required',
            'section_title' => 'required',


            ]);

        $section_id = $request['id'];
        $section_name = $request['section_name'];
        $section_title = $request['section_title'];

        $section = Coursesection::find($section_id);
        $section->section_name = $section_name;
        $section->section_title = $section_title;
        if($section->save())
        {
            $data = "Course section updated successfully.";
            return response()->json($data);
        }
        else
        {
            $data = "Could not update Course section! Please try again.";
            return response()->json($data);
        }

    }



    public function createexam($id)
    {
        $courseObject = Course::find($id);
        return view('courses.createexam', compact('courseObject'));
    }

    public function postexam(Request $request)
    {
        request()->validate([
            'question' => 'required',
            'option1' => 'required',
            'option2' => 'required',
            'option3' => 'required',
            'option4'  => 'required',
            'mark'  => 'required',
            'correct_option'  => 'required',


            ]);

            $answer = $request['correct_option'];


        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;




            $question = new Question();
            $question->question = $request['question'];
            $question->mark = $request['mark'];
            $question->option1 = $request['option1'];
            $question->option2 = $request['option2'];
            $question->option3 = $request['option3'];
            $question->option4 = $request['option4'];
            $question->user_id = $userId;
            $question->course_id = $request['courseId'];
            if($answer == 1)
            {
                $question->answer =  $request['option1'];
            }
            else if($answer == 2)
            {
                $question->answer = $request['option2'];
            }
            else if($answer == 3)
            {
                $question->answer = $request['option3'];
            }
            else
            {
                $question->answer = $request['option4'];
            }

            $question->save();
            return redirect()->back()->with('success', 'Question added successfully!');

        }
        else
        {
            return redirect()->route('login');

        }


    }


    public function editcourse(Request $request, $id)
    {
        $courseObject =DTO::getSingleCourse($id);
        $programs = Program::orderby('program_name', 'ASC')->get();

        return view('courses.editcourse', compact('courseObject', 'programs'));
    }
    public function posteditcourse(Request $request)
    {

        request()->validate([
            'course_objectives' => 'required',
            'course_full_description' => 'required',
            'program' => 'required',
            'duration' => 'required',
            'title'  => 'required',
            'course_type'  => 'required',
            'is_single'  => 'required',
            'course_brief_description' => 'required'

            ]);

        $course = Course::find($request['course_id']);
        $course->course_title = $request['title'];
        $course->duration = $request['duration'];
        $course->is_single = $request['is_single'];



        $course->program_id = $request['program'];
       // $course->course_privacy = $request['privacy'];

        $course->course_price = $request['price'];
        $course->promo_price = $request['promo_price'];
        $course->course_type = $request['course_type'];
        $course->course_requirements = $request['course_requirements'];
        $course->course_objectives = $request['course_objectives'];
        $course->course_brief_description = $request['course_brief_description'];

       // $course->course_brief_description = $request['course_brief_description'];
        $course->course_full_description = $request['course_full_description'];

       if($course->save())
       {
        return redirect()->route('courses')->with('success', 'Course updated successfully');

       }
       else
       {
        return redirect()->back()->with('error', 'There was error updating course. Please try again');
       }


    }

    public function disablecourse(Request $request, $id)
    {
        $course_id = $id;
        $courseObject = Course::find($course_id);
        $courseObject->course_status = 0;
        if($courseObject->save())
        {
            return redirect()->back()->with('success', 'Course disabled successfully');
            //return redirect()->route('courses')->with('success', 'Course disabled successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'There was error disabling course. Please try again');
        }
    }


    public function enablecourse(Request $request, $id)
    {
        $course_id = $id;
        $courseObject = Course::find($course_id);
        $courseObject->course_status = 1;
        if($courseObject->save())
        {
            return redirect()->route('courses')->with('success', 'Course enabled successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'There was error enabling course. Please try again');
        }
    }



    public function disableenrollment(Request $request, $id)
    {
        $course_id = $id;
        $courseObject = Course::find($course_id);
        $courseObject->course_status = 2;
        if($courseObject->save())
        {
            return redirect()->back()->with('success', 'Course Enrollment OFF successfully');
            //return redirect()->route('courses')->with('success', 'Course disabled successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'There was error disabling course. Please try again');
        }
    }


    public function acceptenrollment(Request $request, $id)
    {
        $course_id = $id;
        $courseObject = Course::find($course_id);
        $courseObject->course_status = 1;
        if($courseObject->save())
        {
            return redirect()->back()->with('success', 'Course Enrollment ON successfully');
            //return redirect()->route('courses')->with('success', 'Course enabled successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'There was error enabling course. Please try again');
        }
    }



    public function teachercourselists()
    {
        $courses  = DTO::getTeacherCourses(2);

        return view('courses.courselists', compact('courses'));

    }

    public function allcourses()
    {

        $courses  = DTO::getAllCourses();

        return view('courses.courselists', compact('courses'));
    }



    public function postproject(Request $request)
    {
         $this->validate($request, [

             'course_id'=>'required',
             'description'=>'required',
               'file' => 'required',


             	]);

                 if (Auth::check())
                {

                $user = Auth::user();




                 if($request->hasFile('file'))
                 {
                     $image_array = $request->file('file');
                     $array_len = count($image_array);

                    for($i=0; $i<$array_len; $i++)
                    {
                        $image_size = $image_array[$i]->getClientSize();
                        $image_ext = $image_array[$i]->getClientOriginalExtension();
                        $allowedfileExtension=['jpg', 'png', 'jpeg', 'gif'];
                        $check=in_array($image_ext,$allowedfileExtension);

                        if($check)
                        {
                        $new_image_name = rand(123456,999999).".".$image_ext;

                         $image_array[$i]->move('./eventpictures', $new_image_name);

                           $media = new Project();
                           $media->title = $request['title'];
                           $media->link = $request['link'];
                           $media->description = $request['description'];
                           $media->file_url = 'projectimage/'.$new_image_name;
                           $media->extension = $image_ext;
                           $media->added_by = $user->name;
                           $media->user_id = $user->id;
                           $media->media_type = 3;

                           $media->save();
                        }
                        else
                        {
                            return redirect('addnewevent')->with('error', 'Sorry Only JPG, JPEG, PNG and GIF file extensions are allowed');

                        }
                    }
                 }

                return redirect('allevents')->with('success', 'Picture added successfully');

                }
                else
                {
                    return redirect()->route('login');
                }
    }



    public function deleteproject($id)
    {
        $project = Project::whereid($id)->FirstOrFail();
        $project->delete();
        return redirect('allevents')->with('success','event deleted successfully');

    }

    public function allprojects()
    {
        $projetcs = Project::where('media_type', 3)->orderBy('created_at', 'desc')->get();
        return view('media.allevents', compact('projects'));
    }





    public function postresource(Request $request)
    {

        $this->validate($request, [

            'resource_name' => 'required',
            'course_id' => 'required',
            'resource_link' => 'required',
            ]);


            $c = new Courseresource();
            $c->course_id = $request['course_id'];
            $c->resource_name = $request['resource_name'];
            $c->resource_link = $request['resource_link'];

            if($c->save())
            {

               return redirect()->back()->with('success', 'Course resources added successfully');

            }
            else
            {
                return redirect()->back()->with('error', 'There was error adding course. Please try again');

            }

    }

    public function deletecourseresource(Request $request)
    {
        $c = Courseresource::find($request['resource_id']);

        if($c->delete())
        {

            return redirect()->back()->with('success', 'Course resource deleted successfully');

        }
        else
        {

            return redirect()->back()->with('error', 'Could not delete course resource. Please try again');

        }
    }

    public function posteditcourseresource(Request $request)
    {
        $this->validate($request, [

            'resource_name' => 'required',
            'resource_link' => 'required',
            'courseresource_id' => 'required',
            ]);

            $c = Courseresource::find($request['courseresource_id']);
            $c->resource_name = $request['resource_name'];
            $c->resource_link = $request['resource_link'];
        if($c->save())
        {
            return redirect()->back()->with('success', 'Changes saved successfully');


        }
        else
        {

            return redirect()->back()->with('error', 'Could not save changes. Please try again');

        }

    }


    public function courseresourcelists($id)
    {
        $courseresources = Courseresource::where('course_id', $id)->get();

        $courseId = $id;
        return view('courses.courseresourcelists', compact('courseresources', 'courseId'));
    }

    public function addcourseresource($id)
    {
        $courseObject =  $courseObject = Course::find($id);
        return view('courses.addcourseresources', compact('courseObject'));

    }

    public function editcourseresources($id)
    {
        $courseresource = Courseresource::find($id);
        return view('courses.editcourseresources', compact('courseresource'));
    }




    public function postlecturenote(Request $request)
    {

        $this->validate($request, [

            'course_id' => 'required',
            'lecture_id' => 'required',
            'note' => 'required',
            ]);


            $l = new Lecturenote();
            $l->course_id = $request['course_id'];
            $l->lecture_id = $request['lecture_id'];
            $l->note = $request['note'];

            if($l->save())
            {

               return redirect()->back()->with('success', 'Lecture note added successfully');

            }
            else
            {
                return redirect()->back()->with('error', 'There was error adding lecture note. Please try again');

      }

    }

  /*  public function upload(Request $request){
        $fileName=$request->file('file')->getClientOriginalName();
        $path=$request->file('file')->storeAs('uploads', $fileName, 'public');
        return response()->json(['location'=>"/storage/$path"]);



    }*/


    public function postededitlecture(Request $request)
    {


        $this->validate($request, [

            'lecturenote_id' => 'required',

            'title' => 'required',
            'lecture' => 'required',
            ]);




            $lecture = $request['lecture'];
            $formattedLecture = str_replace('..', '', $lecture);


            //$l->note = $formattedLecture;
            $l =  Lecture::find($request['lecturenote_id']);
            $l->url = $request['url'];
            $l->lecture_title = $request['title'];
            //$l->course_id = $request['course_id'];
            $l->section_id =  $request['module'];
            $l->note = $formattedLecture;

            if($l->save())
            {
               //return redirect()->back()->with('success', 'Changes saved successfully');
               return redirect()->route('courselectures', $l->course_id)->with('success', 'Changes saved successfully');
            }
            else
            {
                return redirect()->back()->with('error', 'There was error saving lecture note. Please try again');
            }

    }



    public function editlecturenote($id)
    {
        $lectureObject = Lecture::find($id);
        $modules = Coursesection::where('course_id', $lectureObject->course_id)->orderBy('id', "ASC")->get();
        return view('courses.editlecturenote', compact('lectureObject', 'modules'));
    }



    public function courselectures($id)
    {
        $lectures = Lecture::where('course_id', $id)->get();
        $courseObject =  $courseObject = Course::find($id);
        return view('courses.courselectures', compact('lectures', 'courseObject'));

    }



    public function upload(Request $request){

        $fileName=$request->file('file')->getClientOriginalName();
       // $path=$request->file('file')->move('./storage/courseimages', $fileName);
        $path=$request->file('file')->storeAs('courseimages', $fileName, 'public');



        return response()->json(['location'=>"/storage/$path"]);

        /*$imgpath = request()->file('file')->store('uploads', 'public');
        return response()->json(['location' => "/storage/$imgpath"]);*/

    }


    public function addlecture($id)
    {
        $modules = Coursesection::where('course_id', $id)->orderBy('id', "ASC")->get();
        $course = Course::find($id);
        return view('courses.addlecture', compact('modules', 'course', 'id'));

    }

    public function storelecture(Request $request)
    {
        $this->validate($request, [

            'course_id' => 'required',
            'title' => 'required',
            'lecture' => 'required',
            ]);



            $lecture = $request['lecture'];
            $formattedLecture = str_replace('..', '', $lecture);


            //$l->note = $formattedLecture;
            $l = new Lecture();
            $l->url = $request['url'];
            $l->lecture_title = $request['title'];
            $l->course_id = $request['course_id'];
            $l->section_id =  $request['module'];
            $l->note = $formattedLecture;

            if($l->save())
            {

               return redirect()->back()->with('success', 'Lecture added successfully');

            }
            else
            {
                return redirect()->back()->with('error', 'There was error adding lecture lecture note. Please try again');

            }

    }



    public function deletelecture($id)
    {
        $lectureObject = $lecture = Lecture::find($id);
        $course_id = $lectureObject->course_id;
        $lecture = Lecture::find($id)->delete();
        return redirect()->route('courselectures', $course_id)->with('success', 'Changes saved successfully');
    }




}
