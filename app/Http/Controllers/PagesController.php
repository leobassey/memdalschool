<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\DTO;
use Illuminate\Support\Facades\Auth;
use App\Paymentplatformlog;
use App\Wallet;
use App\Program;
use App\Studentcourse;
use App\Mail\ForgotPasswordEmail;
use App\User;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Application;
use App\Studentlecture;
use App\Lecture;
use App\Coursesection;
use App\Courseprojectimage;
use Carbon\Carbon;
use App\Usercoupon;




class PagesController extends Controller
{
    public function about()
    {
        return view('about');
    }


    public function onlinecourses()
    {

        //$courses = DTO::getOnlineCourses();

       // $courses = Course::where('course_type', 1)->orderby('is_single', 'DESC')->get();

        $selectStatment = \DB::getPdo()->prepare("SELECT c.*, sum(cr.star) / count(cr.star) as average_ratings, count(cr.reviews) as reviews from courses as c left join courseratings as cr on c.id = cr.course_id where c.course_type =  1 group by c.course_title");
        //$selectStatment->bindValue(':course_id', $course->id, \PDO::PARAM_STR);
		$selectStatment->execute();
		$courses = $selectStatment->fetchAll(\PDO::FETCH_OBJ);



        //dd($courses);

        $programs = new Program();
        $programs = $programs::all();

        $programArray = array();
        foreach ($programs as $key)
        {
          $course = new Course();
          $oneProgram = array();
          $oneProgram["name"] = $key->program_name;
          $oneProgram["courses"] = $course::where(['course_type' => 1, 'program_id' =>$key->id])->get();
          if(count( $oneProgram["courses"] ) > 0)
          {
            $programArray[] = $oneProgram;
          }


        }

        return view('onlinecourses', compact('programArray', 'courses'));
    }

   /* public function onlinecourses()
    {
        $courses = DTO::getOnlineCourses();
        return view('onlinecourses', compact('courses'));
    }*/

    public function welcome()
    {
        $courses = Course::where('course_type', 1)->orderby('is_single', 'DESC')->get();
        $onlineSingleCourses = Course::where('is_single', 1)->get();
        $full_courses = Course::where('is_single', 2)->get();

        return view('welcome', compact('courses', 'onlineSingleCourses', 'full_courses'));
    }

    public function welcome2()
    {
        $courses = Course::where('course_type', 1)->orderby('is_single', 'DESC')->get();
        $single_courses = Course::where('is_single', 1)->get();
        $full_courses = Course::where('is_single', 2)->get();

        return view('welcome', compact('courses', 'single_courses', 'full_courses'));
    }

    public function classroomcourses()
    {

        $programs = new Program();
        $programs = $programs::all();

        $programArray = array();
        foreach ($programs as $key)
        {
          $course = new Course();
          $oneProgram = array();
          $oneProgram["name"] = $key->program_name;
          $oneProgram["courses"] = $course::where(['course_type' => 2, 'program_id' =>$key->id])->get();

          if(count( $oneProgram["courses"] ) > 0)
          {
            $programArray[] = $oneProgram;
          }

        }

        return view('classroomcourses', compact('programArray'));
    }

  /*  public function classroomcourses()
    {
        return view('classroomcourses');
    }*/


    public function freecourses()
    {
      $programs = new Program();
      $programs = $programs::all();

      $programArray = array();
      foreach ($programs as $key)
      {
        $course = new Course();
        $oneProgram = array();
        $oneProgram["name"] = $key->program_name;
        $oneProgram["courses"] = $course::where(['course_type' => 3, 'program_id' =>$key->id])->get();

        if(count( $oneProgram["courses"] ) > 0)
        {
          $programArray[] = $oneProgram;
        }

      }


        return view('freecourses' , compact('programArray'));
    }

   /* public function freecourses()
    {
        return view('freecourses');
    }*/

    public function details(Request $request, $slug)
    {
        $course = Course::where('slug', $slug)->first();
        $courses = Course::all();
        $projectImages = Courseprojectimage::where('course_id', $course->id)->get();
        $firstCourseProjectImage = Courseprojectimage::where('course_id', $course->id)->first();

        $selectStatment = \DB::getPdo()->prepare("SELECT sum(star) / count(star) as average_ratings, count(reviews) as reviews from courseratings where course_id = :course_id");
        $selectStatment->bindValue(':course_id', $course->id, \PDO::PARAM_STR);
		$selectStatment->execute();
		$courseRatings = $selectStatment->fetch(\PDO::FETCH_OBJ);

            $modules = Coursesection::where('course_id', $course->id)->get();
            $moduleArray = array();
            foreach ($modules as $key)
            {
              $lecture = new Lecture();
              $oneModule = array();
              $oneModule["name"] = $key->section_name .' '.$key->section_title;
              $oneModule["id"] = $key->id;
              $oneModule["lectures"]  = lecture::where('section_id', $key->id)->where('course_id', $course->id)->get();
              //$lecture::join('lectures', 'lectures.id', '=', 'studentlectures.lecture_id')->select('studentlectures.id as studentlecture_id', 'studentlectures.is_completed', 'lectures.*')->where(['studentlectures.module_id' =>$key->id, 'studentlectures.course_id' =>$key->course_id, 'studentlectures.user_id' => $userId])->get();
              //$oneProgram["courses"] = $course::where(['course_type' => 2, 'program_id' =>$key->id])->get();
              if(count( $oneModule["lectures"] ) > 0)
              {
                $moduleArray[] = $oneModule;
              }

            }

        return view('details', compact('course', 'courses', 'moduleArray', 'projectImages', 'courseRatings', 'firstCourseProjectImage'));
    }

    public function onlinecoursedetails(Request $request, $id)
    {
        $course = Course::where('id', $id)->first();
        $courses = Course::all();
        return view('onlinecoursedetails', compact('course', 'courses'));
    }

    public function classroomcoursedetails(Request $request, $id)
    {
        $course = Course::where('id', $id)->first();
        $courses = Course::all();
        return view('classroomcoursedetails',  compact('course', 'courses'));
    }

    public function freecoursedetails(Request $request, $id)
    {
        $course = Course::where('id', $id)->first();
        $courses = Course::all();
        return view('freecoursedetails',  compact('course', 'courses'));
    }


    public function enrollment(Request $request, $id)
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;

        // get a user coupon object
        $usercoupon = Usercoupon::where('user_id', $userId)->where('course_id', $id)->first();

        if($usercoupon)
        {
            // there is entry for the user. Meaning a user is entitle to a coupon

            // check if a user coupon_status 0 or 1.

            if($usercoupon->coupon_status == 1) // coupon active
            {
                // coupon is IS READY FOR USE BY THIS USER AND FOR THIS COURSE.

                    $couponform = 1; // Hidd coupon form. It is believe a user coupon is applied already, there is no need for the coupon form again

                    $course = Course::where('id', $id)->first();

                    $course->course_type = 3;

                    $courses = Course::all();

                    $studentCourses =  new Studentcourse();
                    $studentcourse = $studentCourses::where(['user_id'=>$userId, 'course_id'=>$id])->first();//check if student has enrolled before

                    if($studentcourse == null){
                    $hasEnrolled = false;
                    }
                    else{
                    $hasEnrolled = true;
                    }


                    //calculate wallet

                    $wallet = new Wallet();
                    $wallet  = $wallet::where('user_id', $userId)->get();
                    $balance = 0;

                    foreach ($wallet as $key )
                    {
                    if($key["transaction_type"] == 'debit')
                    {
                        $balance = $balance - $key["amount"];
                    }
                    if($key["transaction_type"] == 'credit')
                    {
                        $balance = $balance + $key["amount"];
                    }
                    }

                    return view('enrollment',  compact('course', 'courses', 'hasEnrolled', 'user', 'balance', 'couponform'));
            }
            else
            {
                // Coupon is 0 for the user and the course meaning it HAS BEEN USED ALREADY. RETURN THE COURSE NORMAL THE WAY IT IS

                $couponform = 2;
                $course = Course::where('id', $id)->first();
                $courses = Course::all();


                $studentCourses =  new Studentcourse();
                $studentcourse = $studentCourses::where(['user_id'=>$userId, 'course_id'=>$id])->first();//check if student has enrolled before

                if($studentcourse == null){
                  $hasEnrolled = false;
                }
                else{
                  $hasEnrolled = true;
                }


                //calculate wallet

                $wallet = new Wallet();
                $wallet  = $wallet::where('user_id', $userId)->get();
                $balance = 0;

                foreach ($wallet as $key )
                {
                  if($key["transaction_type"] == 'debit')
                  {
                    $balance = $balance - $key["amount"];
                  }
                  if($key["transaction_type"] == 'credit')
                  {
                    $balance = $balance + $key["amount"];
                  }
                }

                return view('enrollment',  compact('course', 'courses', 'hasEnrolled', 'user', 'balance', 'couponform'));
            }


        }
        else
        {
            // No coupon

            $couponform = 3;
        $course = Course::where('id', $id)->first();
        $courses = Course::all();



        $studentCourses =  new Studentcourse();
        $studentcourse = $studentCourses::where(['user_id'=>$userId, 'course_id'=>$id])->first();//check if student has enrolled before

        if($studentcourse == null){
          $hasEnrolled = false;
        }
        else{
          $hasEnrolled = true;
        }


        //calculate wallet

        $wallet = new Wallet();
        $wallet  = $wallet::where('user_id', $userId)->get();
        $balance = 0;

        foreach ($wallet as $key )
        {
          if($key["transaction_type"] == 'debit')
          {
            $balance = $balance - $key["amount"];
          }
          if($key["transaction_type"] == 'credit')
          {
            $balance = $balance + $key["amount"];
          }
        }

        return view('enrollment',  compact('course', 'courses', 'hasEnrolled', 'user', 'balance', 'couponform'));

    }
        }
        else
        {
            return redirect()->route('login');
        }



    }


   /* public function enrollment(Request $request, $id)
    {
        if (Auth::check())
        {
        $course = Course::where('id', $id)->first();
        $courses = Course::all();
        return view('enrollment',  compact('course', 'courses'));
        }
        else
        {
            return redirect()->route('login');
        }
    }*/




     /*
      public function initiatepaywithflutter(Request $request)
      {

        dd("djkjkdjkd");


        if (Auth::check())
        {
       // $Paymentplatformlog = new Paymentplatformlog();

        $course = Course::where('id', $request->input('course_id'))->first();

        $user = Auth::user();
        $userId = $user->id;




        $Paymentplatformlog->user_id = $userId;
        $Paymentplatformlog->amount = $course->course_price;
        $Paymentplatformlog->tempref = "L" . date("dmY"). substr(str_shuffle("ABCDEFGHIJKNPQRSTUVWXYZ"),-3);
        $Paymentplatformlog->purpose = "Payment for course enrollment";
        $Paymentplatformlog->name = $user->name;


        if($Paymentplatformlog->save())
        {

          $studentcourses = new Studentcourse();
          $studentcourses->user_id = $userId;
          $studentcourses->course_id = $course->id;
          $studentcourses->save();



          $lectures = Lecture::where('course_id', $course->id)->get();


          foreach($lectures as $l)
          {
            $studentLecture = new Studentlecture();
            $studentLecture->user_id = $userId;
            $studentLecture->course_id = $course->id;
            $studentLecture->lecture_id = $l->id;
            $studentLecture->module_id = $l->section_id;
            $studentLecture->save();
          }

          return response()->json(['success'=>'nsima',   'amount' =>$course->course_price, 'consumer_id'=>1, 'username'=>$user->name, 'ref'=>'123', 'consumer_mac'=>$_SERVER['REMOTE_ADDR'], 'email'=>$user->email],200);
        }
        else{
            return response()->json(['error'=>'could not start'],200);
        }

        //return view('enrollment',  compact('course', 'courses', 'hasEnrolled', 'user'));

      }
    }

      public function verifyReference(Request $request)
      {
          dd($request->all());
            if (Auth::check())
            {
            $Paymentplatformlog = new Paymentplatformlog();
            $user = Auth::user();
            $userId = $user->id;
            $tx_ref = $request->input('tx_ref');
            $flw_ref = $request->input('flw_ref');
            $transaction_id = $request->input('transaction_id');
            $course_id = $request->input('course_id');
            $amount = $request->input('amount');


            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.flutterwave.com/v3/transactions/". $transaction_id. "/verify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "Content-Type: application/json",
              "Authorization: Bearer FLWSECK_TEST-1f41c3dc4542b80e8df8bfb53fd2c568-X"
            ),
          ));

          $request = curl_exec($curl);

          curl_close($curl);
          // ends from fluer

              if ($request) {

                //if respose is gotten well
                     $result = json_decode($request, true);
                     if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['status'] === 'success'))
                     {
                       //echo "Transaction was successfull nsima";
                       $data = $result["data"];


                       $course = Course::where('id', $request->input('course_id'))->first();

                       dd($course);

                      $user = Auth::user();
                      $userId = $user->id;



                       // Create student course
                       $studentcourses = new Studentcourse();
                       $studentcourses->user_id = $userId;
                       $studentcourses->course_id = $course->id;
                       $studentcourses->save();

                       $lectures = Lecture::where('course_id', $course->id)->get();
                       foreach($lectures as $l)
                       {
                         $studentLecture = new Studentlecture();
                         $studentLecture->user_id = $userId;
                         $studentLecture->course_id = $course->id;
                         $studentLecture->lecture_id = $l->id;
                         $studentLecture->module_id = $l->section_id;
                         $studentLecture->save();
                       }


                       //update Paymentplatformlog table
                       $Paymentplatformlog = new Paymentplatformlog();
                      // $Paymentplatformlog = $Paymentplatformlog::where('tempref', $tx_ref)->first();
                       $Paymentplatformlog->verified = 1;
                       $Paymentplatformlog->paymentresponse = json_encode($data);
                       $Paymentplatformlog->transaction_id = $transaction_id;
                       $Paymentplatformlog->course_id =  $course_id;
                       $Paymentplatformlog->user_id = $userId;
                       $Paymentplatformlog->amount = $amount;
                       $Paymentplatformlog->tempref = "L" . date("dmY"). substr(str_shuffle("ABCDEFGHIJKNPQRSTUVWXYZ"),-3);
                       $Paymentplatformlog->purpose = "Payment for course enrollment";
                       $Paymentplatformlog->name = $user->name;
                       $Paymentplatformlog->save();








                       return response()->json(['success'=>'Transaction was successful'],200);



                     }
                     else{
                        return response()->json(['error'=>'Transaction was not successful'],401);
                     }
              }
              else{//if respose was not gotten


                return response()->json(['error'=>'Transaction was not successful'],401);
              }
          }

    }//end function

*/











   public function enrollfree(Request $request)
   {
    if (Auth::check())
    {

     $user = Auth::user();
     $userId = $user->id;

     $course_id = $request->input('course_id');

     $courseObj = Course::find($course_id);

     $studentcourse =  new Studentcourse();
     $studentcourse->user_id =  $userId;
     $studentcourse->course_id =  $course_id;
     $studentcourse->start_date =   Carbon::now();
     $studentcourse->expire_date =    Carbon::now()->addDays($courseObj->access_duration);  // access by day. The day specified in the course table is assigned to the student course
     $studentcourse->user_duration = $courseObj->access_duration; // assign user access duration from access duration column in the course tabale

    $studentcourse->save();
    $usercoupon = Usercoupon::where('user_id', $userId)->where('course_id', $course_id)->first();
    if($usercoupon)
    {

        // if this user is given a coupon, check if is 1 or 0. If is 1 change to 0

        $usercoupon->coupon_status = 0;
        $usercoupon->save();

        echo "Enrollment was successfull done";

    }
    else
    {
        echo "Enrollment was successfull done";
    }
    }
    else
    {
        return redirect()->route('login');
    }

   }

 public function  payViaWallet(Request $request)
 {

   $user = Auth::user();
   $userId = $user->id;

    $course_id = $request->input('course_id');
    $course = Course::where('id',   $course_id)->first();
    $amount = $course->course_price;



    $wallet = new Wallet();
    $wallet  = $wallet::where('user_id', $userId)->get();
    $balance = 0;

    foreach ($wallet as $key )
    {
      if($key["transaction_type"] == 'debit')
      {
        $balance = $balance - $key["amount"];
      }
      if($key["transaction_type"] == 'credit')
      {
        $balance = $balance + $key["amount"];
      }
    }

    if($amount > $balance)
    {
       echo "Insufficient balance";
       die;
       return;
    }



    $courseObj = Course::find($course_id);

    $studentcourse =  new Studentcourse();
    $studentcourse->user_id =  $userId;
    $studentcourse->course_id =  $course_id;
    $studentcourse->start_date =   Carbon::now();
    $studentcourse->expire_date =    Carbon::now()->addDays($courseObj->access_duration);  // access by day. The day specified in the course table is assigned to the student course
    $studentcourse->user_duration = $courseObj->access_duration; // assign user access duration from access duration column in the course tabale



   //now add to wallet
   $wallet =  new Wallet();
   $wallet->user_id =  $userId;
   $wallet->amount =  $amount;
   $wallet->transaction_type =  'debit';
   $wallet->transaction_id =  substr(str_shuffle('1234567890'), -7);
   $wallet->date =  date("Y-m-d");
   $wallet->Description =  "Payment for course ".$course->course_title ;
   $wallet->save();
   echo "Transaction was successfull nsima";
 }


public function forgotpassword()
{
    return view('forgotpassword');
}

public function postforgotpassword(Request $request)
  {

    $email = $request['email'];
    $user = User::where('email', $email)->first();//\DB::select(\DB::raw("SELECT * FROM users WHERE email = :email"), array( 'email' => $email));

   if(empty($user))
   {

    return redirect('forgotpassword')->with('error', 'The email address you entered does not exist.');

   }
   else
   {



    $data = array(

        'user' => $user->name,
        'link' => \URL::route('resetpassword', $email),

     );

         \Mail::to($email)->send(New ForgotPasswordEmail($data));
         return redirect()->back()->with('success', 'An email with password reset instructions has been sent to your email address');
   }

  }


   public function resetpassword(Request $request, $email)
  {
     $userEmail = $email;

     return view('newpassword', compact('userEmail'));
  }


  public function saveresetpassword(Request $request)
  {


        if($request->get('cpassword') == ""){

            return \Redirect::back()->withErrors(['Pleas enter new password']);
        }
        if($request->get('npassword') == ""){

            return \Redirect::back()->withErrors(['Pleas confirm password']);
        }
        if($request->get('cpassword') !=$request->get('npassword')){

            return \Redirect::back()->withErrors(['Password confirmation mismatched']);
        }

      $email = $request['email'];

      $user = User::whereEmail($email)->FirstOrFail();

       $user->password = bcrypt($request->get('npassword'));
       $user->save();


      return redirect('login')->with("success","Password changed successfully. Please login");
  }


  public function certificate()
  {
      return view('certificate');
  }

  public function apply()
  {
      $courses = Course::all();
      return view('apply', compact('courses'));
  }

  public function postapplication(Request $request)
  {

    request()->validate([
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'course' => 'required',
        'experience' => 'required',


        ]);

        $application  = new Application();
        $application->firstname = $request['firstname'];
        $application->lastname = $request['lastname'];
        $application->email = $request['email'];
        $application->phone = $request['phone'];
        $application->course_id = $request['course'];
        $application->experience = $request['experience'];
        $application->save();

        return redirect()->back()->with('success', 'Application received. Thank, our representative will call you soon');





  }
  public function landingpage()
  {
    return view('landingpage');
  }

    public function details2(Request $request, $slug)
    {
        $course = Course::where('slug', $slug)->first();
        $courses = Course::all();
        return view('details2', compact('course', 'courses'));
    }




    public function initiatepaywithflutter(Request $request)
    {
      if (Auth::check())
      {
      $Paymentplatformlog = new Paymentplatformlog();

      $course = Course::where('id', $request->input('course_id'))->first();
      $amountForFrontend;

      $user = Auth::user();
      $userId = $user->id;

      $Paymentplatformlog->user_id = $userId;
      $Paymentplatformlog->tempref = "L" . date("dmY"). substr(str_shuffle("ABCDEFGHIJKNPQRSTUVWXYZ"),-3);
      $Paymentplatformlog->purpose = "Payment for course enrollment";
      $Paymentplatformlog->name = $user->name;


      if($course->promo_price == 0.00)
      {
        $amountForFrontend = $course->course_price; // use regular course fee which is $course->course_price
        $Paymentplatformlog->isPromo = 0;
        $Paymentplatformlog->amount_paid = $course->course_price;
        $Paymentplatformlog->course_amount = $course->course_price;

      }
      else
      {
        $amountForFrontend = $course->promo_price;  // use promo price course fee which is $course->promo_price
        $Paymentplatformlog->isPromo = 1;
        $Paymentplatformlog->amount_paid = $amountForFrontend;
        $Paymentplatformlog->course_amount = $course->course_price;
      }




      if($Paymentplatformlog->save())
      {
        return response()->json(['success'=>'nsima',   'amount' =>$amountForFrontend, 'consumer_id'=>$Paymentplatformlog->id, 'username'=>$user->name, 'ref'=>$Paymentplatformlog->tempref, 'consumer_mac'=>$_SERVER['REMOTE_ADDR'], 'email'=>$user->email],200);
      }
      else{
          return response()->json(['error'=>'could not start'],200);
      }


      }
      else
      {
          echo 419;
      }
    }



  public function verifyReference(Request $request)
  {
      //dd($request->all());
        if (Auth::check())
        {

        $user = Auth::user();
        $userId = $user->id;

        $tx_ref = $request->input('ref');
        $flw_ref = $request->input('flw_ref');
        $transaction_id = $request->input('transaction_id');

        $course_id = $request->input('course_id');
        $payment_id = $request->input('payment_id');
        $result = array();

        $PaymentplatformlogObject = Paymentplatformlog::where('id', $payment_id)->first();

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "'https://api.paystack.co/transaction/verify/".$PaymentplatformlogObject->tempref,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Content-Type: application/json",
          "Authorization: Bearer sk_test_6666d514716a180c1853f9d08cc99839e8ec611f"
        ),
      ));

      $request = curl_exec($curl);
      curl_close($curl);


                $PaymentplatformlogObject->verified = 1;
                $PaymentplatformlogObject->paymentresponse = "success"; //json_encode($data); //"success";//json_encode($data);
                $PaymentplatformlogObject->transaction_id = $PaymentplatformlogObject->tempref;
                $PaymentplatformlogObject->course_id =  $course_id;
                $PaymentplatformlogObject->save();


                /*$studentcourse =  new Studentcourse();
                $studentcourse->user_id =  $userId;
                $studentcourse->course_id =  $course_id;
                $studentcourse->save();*/

                // Get a course object to access ACCESS_DURATION

                $courseObj = Course::find($course_id);

                $studentcourse =  new Studentcourse();
                $studentcourse->user_id =  $userId;
                $studentcourse->course_id =  $course_id;
                $studentcourse->start_date =   Carbon::now();
                $studentcourse->expire_date =    Carbon::now()->addDays($courseObj->access_duration);  // access by day. The day specified in the course table is assigned to the student course
                $studentcourse->user_duration = $courseObj->access_duration; // assign user access duration from access duration column in the course tabale

                //$studentcourse->expire_date =  Carbon::now()->addMonths(1); // access by month
                //$studentcourse->expire_date =  Carbon::now()->addYears(1); // Access by year

                $studentcourse->save();


                return response()->json(['success'=>'nsima'],200);

}//end function
  }











/*
 PAYSTACK
      public function verifypaymentfrompaystack(Request $request)
      {


              $reference = $request->input('reference');//collect json data from client

              $result = array();
              //The parameter after verify/ is the transaction reference to be verified
              $url = 'https://api.paystack.co/transaction/verify/'. $reference;

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt(
              $ch, CURLOPT_HTTPHEADER, [
              'Authorization: Bearer sk_live_46a5412296bdad0740b55dd516e1fc201bc436a889']
              //'Authorization: sk_test_9837e6619c7bbe9a6a3f4ebf4b2d262b6d3d0d20']
              );
              $request = curl_exec($ch);
              curl_close($ch);

              if ($request) {
              $result = json_decode($request, true);
              }

              if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['data']['status'] === 'success')) {
              echo "Transaction was successfull";
              $amountJustPaid = ($result["data"]["amount"])/100;


              echo "Transaction was successfull nsima";
              $data = $result["data"];

              //var_dump($data);
              //now register the student course
              $studentcourse =  new Studentcourse();
              $studentcourse->user_id =  $userId;
              $studentcourse->course_id =  $course_id;
              $studentcourse->save();

              //update Paymentplatformlog table
              $Paymentplatformlog = new Paymentplatformlog();
              $Paymentplatformlog = $Paymentplatformlog::where('tempref', $tx_ref)->first();
              $Paymentplatformlog->verified = 1;
              $Paymentplatformlog->paymentresponse = json_encode($data);
              $Paymentplatformlog->transaction_id = $transaction_id;
              $Paymentplatformlog->course_id =  $course_id;
              $Paymentplatformlog->save();


             /* $order = new Order();
              $order = $order::where('ordercode', $reference)->first();
              $order->Paymentchannel = 'paystack';
              $order->save();


              $Paymentlogger =  new Paymentlogger();
              $Paymentlogger->ordercode = $reference;
              $Paymentlogger->amount =   $amountJustPaid;
              $Paymentlogger->ordercode = $reference;
              $Paymentlogger->payer =   $order->Fullname;
              $Paymentlogger->details = json_encode($result["data"]);
              $Paymentlogger->channel =   'paystack';
              $Paymentlogger->method =   'paystack';
              $Paymentlogger->save();

              $cart = new Cart();

              $cart::where('CartSession', session('cartsession'))
              ->update(['CheckOut' => 1]);

              }else{
              echo "Transaction error";
              }

      }*/

      /*



      public function verifyReference(Request $request)
  {
      //dd($request->all());
        if (Auth::check())
        {
        $Paymentplatformlog = new Paymentplatformlog();
        $user = Auth::user();
        $userId = $user->id;
        $tx_ref = $request->input('tx_ref');
        $flw_ref = $request->input('flw_ref');
        $transaction_id = $request->input('transaction_id');
        $course_id = $request->input('course_id');


        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.flutterwave.com/v3/transactions/". $transaction_id. "/verify",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Content-Type: application/json",
          "Authorization: Bearer FLWSECK_TEST-1f41c3dc4542b80e8df8bfb53fd2c568-X"
        ),
      ));

      $request = curl_exec($curl);
      curl_close($curl);
      // ends from fluer

          if ($request) {//if respose is gotten well
                 $result = json_decode($request, true);
                 if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['status'] === 'success'))
                 {
                   echo "Transaction was successfull nsima";
                   $data = $result["data"];

                   //var_dump($data);
                   //now register the student course
                   $studentcourse =  new Studentcourse();
                   $studentcourse->user_id =  $userId;
                   $studentcourse->course_id =  $course_id;
                   $studentcourse->save();

                   //update Paymentplatformlog table
                   $Paymentplatformlog = new Paymentplatformlog();
                   $Paymentplatformlog = $Paymentplatformlog::where('tempref', $tx_ref)->first();
                   $Paymentplatformlog->verified = 1;
                   $Paymentplatformlog->paymentresponse = json_encode($data);
                   $Paymentplatformlog->transaction_id = $transaction_id;
                   $Paymentplatformlog->course_id =  $course_id;
                   $Paymentplatformlog->save();

                 }
                 else{
                    echo "Transaction was not successfull";
                 }
          }
          else{//if respose was not gotten

          }
      }

}//end function


      */


      public function enrollmentsuccess($id)
      {
        $course = Course::where('id', $id)->first();
        $courseName = $course->course_title;
        return view('enrollmentsuccess', compact('courseName'));

      }



      public function addcoupon(Request $request)
      {

        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;

            $coupon = $request['coupon'];

            $courseId = $request['course'];
            $course = Course::find($courseId);

            if($course)
            {

                if($coupon == $course->coupon_code)
                {


                    if($course->coupon_active == 1) // general coupon code is enable for usage
                    {

                       /* $uc = new Usercoupon();
                        $uc->user_id = $userId;
                        $uc->coupon = $coupon;
                        $uc->coupon_status = 0;
                        $uc->course_id = $courseId;
                        $uc->save();*/

                            // coupon is enabled for use
                            $userscoupon = Usercoupon::where('coupon', $coupon)->where('course_id', $courseId)->first();

                            if($userscoupon)  // Coupon hass been added for users. NOT FOR SPECIFIC USER YET
                            {

                                $usercoupon = Usercoupon::where('user_id', $userId)->where('course_id', $courseId)->first();

                                if($usercoupon)
                                 {



                                    return redirect()->route('enrollment', $courseId)->with('message', "Coupon already used");

                                 }
                                 else
                                 {

                                    // a user has not added coupon to his or herself

                                    $userscoupon->user_id = $userId;
                                    $userscoupon->coupon = $coupon;
                                    $userscoupon->coupon_status = 1; // 1 means user can use the coupon
                                    $userscoupon->save();
                                    return redirect()->route('enrollment', $courseId)->with('success', "Coupon applied.");

                                }



                            }
                            else
                            {
                               // dd('there is no users coupon added for this course');//
                                return redirect()->back()->with('message', 'there is no users coupon added for this course');

                            }


                    }
                    else
                    {
                       // dd("General coupon code is not enabel for use");
                        return redirect()->back()->with('message', 'General coupon code is not enabled for use');
                    }

                }
                else
                {

                   // dd("Invalid coupon code");
                    return redirect()->back()->with('message', 'Invalid coupon code');

                }

            }
            else
            {
                //dd("course not found for coupon");
                return redirect()->back()->with('message', 'course not found for coupon');
            }
        }
        else
        {
            return redirect()->route('login');

        }

      }


      public function terms()
      {
        return view('terms');
      }


}




