<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Course;
use App\Coursesection;
use App\Lecture;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Question;
use App\DTO;
use App\Studentcourse;
use App\Assignment;

class TeacherController extends Controller
{
    public function teacherdashboard()
    {
        return view('teacher.dashboard');
    }


    public function teachercourses()
    {
        if (Auth::check())
        {

            $user = Auth::user();
            $userId = $user->id;

            $courses = Course::where('user_id', $userId)->get();
            return view('teacher.courselists', compact('courses'));
        }
        else
        {
            return redirect()->route('login');

        }


    }

    public function createteachercourse()
    {
        $programs = Program::orderby('program_name', 'ASC')->get();
        return view('teacher.createcourse', compact('programs'));
    }

    public function postteachercourse(Request $request)
    {

        request()->validate([
            'course_objectives' => 'required',
            'course_full_description' => 'required',
            'program' => 'required',
            'duration' => 'required',
            'title'  => 'required',
            'course_type'  => 'required',
           /* 'lecture_title' => 'required',
            'lecture_format' => 'required',*/


            ]);

            if (Auth::check())
            {

                $user = Auth::user();
                $userId = $user->id;

                $course = new Course();
        $course->course_title = $request['title'];
        $course->duration = $request['duration'];

        $course->program_id = $request['program'];
        $course->course_price = $request['price'];
        $course->promo_price = $request['promo_price'];
        $course->course_type = $request['course_type'];
        $course->course_requirements = $request['course_requirements'];
        $course->course_objectives = $request['course_objectives'];
        $course->course_full_description = $request['course_full_description'];
        $course->user_id = $userId;

       if($course->save())
       {
        //$courseObject = Course::find($course->id);
        $course_id = $course->id;
        return redirect()->route('teachercoursesettings', $course_id);
       }
       else
       {
        return response()->json(['error'=>'error']);
       }

            }
            else
            {
                return redirect()->route('login');

            }


    }


    public function teachercoursesettings(Request $request, $id)
    {
        $courseObject = Course::find($id);
        $teacherCourseCount = Studentcourse::where('course_id', $id)->count();
        return view('teacher.coursesettings', compact('courseObject', 'teacherCourseCount'));

    }


   /* public function postimage(Request $request)
    {
     request()->validate([

            'file' => 'required|image|mimes:jpeg,png,jpg,gif',

        ]);


        $course = Course::find($request['course_id']);

        if($request->hasFile('file'))
        {

            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $name = explode(".", $name);
            $count = count($name);
            $name = "coverimage".str_shuffle("56789").time() . ".". $name[count($name) -1];
            $fileSize = $file->getSize();
            if(!$file->isValid()) {
              return response()->json(['error'=>'Invalid file upload'], 200);
            }
            //if($file->move('./storage/coverimage', $name))
            if($file->move('./courseimages', $name))
            {

                $course->image_url = 'courseimages/'.$name;
                $course->save();
                $message = "done";
                return response()->json($message);
            }

        }

        else
        {
            $message = "Please select file to upload";
            return response()->json($message);
        }
    }*/


    public function manageteacherlectures(Request $request, $id)
    {
        $courseObject = Course::find($id);
        $countLectures = Lecture::where('course_id', $id)->count();
        return view('teacher.managelectures', compact('courseObject', 'countLectures'));
    }

  /*  public function load_sections(Request $request)
    {
        $course_id = $request['course_id'];
        $data = Coursesection::where('course_id', $course_id)->get();


        return response()->json($data);

    }*/

   /* public function load_lectures(Request $request)
    {
        $course_id = $request['course_id'];
        $data = Lecture::where('course_id', $course_id)->get();


        return response()->json($data);

    }*/

   /* public function loadlecturesbysectionid(Request $request)
    {
        $section_id = $request['section'];
        $data = Lecture::where('section_id', $section_id)->get();
        return response()->json($data);
    }*/

   /* public function Deletelectures(Request $request)
    {
        $id = $request['id'];
        $lecture = Lecture::find($id);

        if($lecture->delete())
        {
            $data = "done";
            return response()->json($data);
        }


    }*/



    public function postteacherlecture(Request $request)
    {

        $course_format = $request['lecture_format'];

     if($course_format == "pdf")
     {
        request()->validate([

            'file' => 'required|mimes:pdf,mp4,webm,ogv,MP4,mov,wmv,flv,avi,AVCHD,MKV',
            'course_id' => 'required',
            'course_section' => 'required',
            'lecture_title' => 'required',
            'lecture_format' => 'required',


            ]);

            //ini_set('memory_limit','256M');
            $course_id = $request['course_id'];
            $lecture_title = $request['lecture_title'];
            $course_section = $request['course_section'];


            $youtube_url = $request['youtube_url'];




            if($request->hasFile('file'))
            {

                $file = $request->file('file');
                $name = $file->getClientOriginalName();
                $name = explode(".", $name);
                $count = count($name);
                $name = "lecture".str_shuffle("56789").time() . ".". $name[count($name) -1];
                $fileSize = $file->getSize();
                if(!$file->isValid()) {
                  return response()->json(['error'=>'Invalid file upload'], 201);
                }
                //if($file->move('./storage/coverimage', $name))
                if($course_format =='pdf')
                {
                    if($file->move('./lecturepdf', $name))
                    {
                        $lecture = new Lecture();
                        $lecture->url = 'lecturepdf/'.$name;
                        $lecture->section_id = $course_section;
                        $lecture->course_id = $course_id;
                        $lecture->lecture_title = $lecture_title;
                        $lecture->format = $course_format;

                        $lecture->save();
                        $data = "Lecture uploaded successfully.";
                        return response()->json($data);
                    }
                }
                else if($course_format =='video')
                {

                  /*  $video = Youtube::upload($request->file('file')->getPathName(), [
                        'title'       => $lecture_title,
                        'description' => $lecture_title,
                        'status' => 'unlisted', // or 'private' or 'public'
                    ]);*/
                    // upload to YouTube

                   // if($file->move('./lecturepdf', $name)
    //$file->move('./lecturepdf', $name);
                   /* $video = \Youtube::upload($name, [
                        'title'       => $lecture_title,
                        'description' => 'SAMPLE.',
                        'tags'	      => ['foo', 'bar', 'baz'],
                        'category_id' => 10
                    ]);*/

                  /*  \Youtube::upload(array(
                        'title' => $lecture_title,
                        'description' => 'This is what My video is about',
                        'status' => 'unlisted', // or 'private' or 'public'
                        'video' => $name, // Instance of Symfony\Component\HttpFoundation\File\UploadedFile see http://laravel.com/docs/requests#files
                    ));*/

                    //return $video->getVideoId();

                    $lecture = new Lecture();
                    //$lecture->url = $video->getVideoId(); // upload to YouTube
                    $lecture->url = $youtube_url; // YouTube URL
                    $lecture->section_id = $course_section;
                    $lecture->course_id = $course_id;
                    $lecture->lecture_title = $lecture_title;
                    $lecture->format = $course_format;

                    $lecture->save();
                    $data = "Lecture uploaded successfully.";
                    return response()->json($data);

                }
                else
                {
                    $data = "Invalid course format selection. Please select either video or pdf";
                    return response()->json($data);
                }


            }

            else
            {
                $data = "Please select file to upload";
                return response()->json($data);
            }
     }
     else if($course_format =='video')
     {
        request()->validate([

            'course_id' => 'required',
            'course_section' => 'required',
            'lecture_title' => 'required',
            'lecture_format' => 'required',
            'youtube_url' => 'required',


            ]);

           // ini_set('memory_limit','256M');
            $course_id = $request['course_id'];
            $lecture_title = $request['lecture_title'];
            $course_section = $request['course_section'];


            $youtube_url = $request['youtube_url'];

                  /*  $video = Youtube::upload($request->file('file')->getPathName(), [
                        'title'       => $lecture_title,
                        'description' => $lecture_title,
                        'status' => 'unlisted', // or 'private' or 'public'
                    ]);*/
                    // upload to YouTube


                    $lecture = new Lecture();
                    //$lecture->url = $video->getVideoId(); // upload to YouTube
                    $lecture->url = $youtube_url; // YouTube URL
                    $lecture->section_id = $course_section;
                    $lecture->course_id = $course_id;
                    $lecture->lecture_title = $lecture_title;
                    $lecture->format = $course_format;

                    $lecture->save();
                    $data = "Lecture uploaded successfully.";
                    return response()->json($data);

                }
                else
                {
                    $data = "Invalid course format selection. Please select either video or pdf";
                    return response()->json($data);
                }

}




    public function manageteachersections(Request $request, $id)
    {
        $courseObject = Course::find($id);
        //$countLectures = Lecture::where('course_id', $id)->count();
        return view('teacher.managesections', compact('courseObject'));
    }


   /* public function DeleteSection(Request $request)
    {
        $id = $request['id'];
        $section = Coursesection::find($id);

        if($section->delete())
        {
            $data = "done";
            return response()->json($data);
        }


    }*/

   /* public function postsection(Request $request)
    {
        request()->validate([


            'section_name' => 'required',
            'section_title' => 'required',


            ]);

        $course_id = $request['course_id'];
        $section_name = $request['section_name'];
        $section_title = $request['section_title'];

        $section = new Coursesection();
        $section->course_id = $course_id;
        $section->section_name = $section_name;
        $section->section_title = $section_title;

        if($section->save())
        {
            $data = "Course section created successfully.";
            return response()->json($data);
        }
        else
        {
            $data = "Could not create Course section! Please try again.";
            return response()->json($data);
        }
    }*/

  /*  public function getSectionById(Request $request)
    {
        $section_id = $request['id'];
        $data = Coursesection::find($section_id);
        return response()->json($data);
    }*/

   /* public function updatesection(Request $request)
    {
        request()->validate([


            'section_name' => 'required',
            'section_title' => 'required',


            ]);

        $section_id = $request['id'];
        $section_name = $request['section_name'];
        $section_title = $request['section_title'];

        $section = Coursesection::find($section_id);
        $section->section_name = $section_name;
        $section->section_title = $section_title;
        if($section->save())
        {
            $data = "Course section updated successfully.";
            return response()->json($data);
        }
        else
        {
            $data = "Could not update Course section! Please try again.";
            return response()->json($data);
        }

    }*/



    public function updateteacherprofile()
   {
        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;
           $userObject = User::find($userId);
            return view('teacher.changeprofile', compact('userObject'));
        }
        else
        {
            return redirect()->route('login');

        }
       //return view('changeprofile');
   }

   public function postupdateteacherprofile(Request $request)
   {
        $userId = $request['id'];

        $fullname = $request['fullname'];

        $address = $request['address'];
        $phone = $request['phone'];

        $user = User::find($userId);
        $user->name = $fullname;
        $user->address = $address;
        $user->phone = $phone;

        $user->save();
        return redirect('updateteacherprofile')->with("success","Profile updated successfully");
   }


   public function postupdateteacherpassword(Request $request)
   {

        if (!(\Hash::check($request->get('cpassword'), Auth::user()->password))) {

            return redirect('/updateteacherprofile')->with('error', 'Your current password does not match with the password you provided. Please try again.');
        }

        if(strcmp($request->get('cpassword'), $request->get('npassword')) == 0){

            return redirect('/updateteacherprofile')->with('error', 'New Password cannot be same as your current password. Please choose a different password.');
        }

        if(strcmp($request->get('password_confirmation'), $request->get('npassword')) != 0){

        return redirect('/updateteacherprofile')->with('error', 'Confirmation password mismatch');
        }

        $user = Auth::user();
        $email = $user->email;
        $id = $user->id;

        $user = User::whereid($id)->FirstOrFail();

        $user->password = bcrypt($request->get('npassword'));
        $user->save();


        return redirect('updateteacherprofile')->with("success","Password changed successfully !");
    }



    public function createteacherexam($id)
    {
        $courseObject = Course::find($id);
        return view('teacher.createexam', compact('courseObject'));
    }

    public function postteacherexam(Request $request)
    {
        request()->validate([
            'question' => 'required',
            'option1' => 'required',
            'option2' => 'required',
            'option3' => 'required',
            'option4'  => 'required',
            'mark'  => 'required',
            'correct_option'  => 'required',


            ]);


        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;


            $question = new Question();
            $question->question = $request['question'];
            $question->mark = $request['mark'];
            $question->option1 = $request['option1'];
            $question->option2 = $request['option2'];
            $question->option3 = $request['option3'];
            $question->option4 = $request['option4'];
            $question->answer = $request['correct_option'];
            $question->user_id = $userId;
            $question->course_id = $request['courseId'];



            $question->save();
            //return view('courses.createexam');
            //return redirect('createexam')->with("success","Question added successfully.");
            return redirect()->back()->with('success', 'Question added successfully!');
        }
        else
        {
            return redirect()->route('login');

        }


    }





    public function uploadteachersignature()
    {
        return view('teacher.uploadteachersignature');
    }

    public function postteachersignature(Request $request)
    {

      $this->validate($request, [

         'file' => 'required|mimes:png,jpg,jpeg',

   ]);

    if (Auth::check())
    {
        $user = Auth::user();
        $userId = $user->id;

        if(!$request->hasFile('file')) {

            return redirect('uploadteachersignature')->with("error","Please select signature to upload");
            }

            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $name = explode(".", $name);
            $count = count($name);
            $name = "signature".str_shuffle("56789").time() . ".". $name[count($name) -1 ];
            $fileSize = $file->getSize();
            if(!$file->isValid()) {
                return redirect('uploadteachersignature')->with("error","Invalid file upload. Please select png, jpg or jpeg file to upload");
            }
            if($file->move('./signatures', $name))
            {


                $userObject = User::find($userId);

                $userObject->signature_url = 'signatures/'.$name;
                $userObject->save();

                 return redirect('uploadteachersignature')->with("success","Signature uploaded successfully");
            }
       // $userObject = User::find($userId);
        //return view('admin.changeprofile', compact('userObject'));
    }
    else
    {
        return redirect()->route('login');

    }


}





    public function editteachercourse(Request $request, $id)
    {
        $courseObject =DTO::getSingleCourse($id);
        $programs = Program::orderby('program_name', 'ASC')->get();

        return view('teacher.editcourse', compact('courseObject', 'programs'));
    }
    public function posteditteachercourse(Request $request)
    {

        request()->validate([
            'course_objectives' => 'required',
            'course_full_description' => 'required',
            'program' => 'required',
            'duration' => 'required',
            'title'  => 'required',
            'course_type'  => 'required',

            ]);

        $course = Course::find($request['course_id']);
        $course->course_title = $request['title'];
        $course->duration = $request['duration'];

        $course->program_id = $request['program'];
       // $course->course_privacy = $request['privacy'];

        $course->course_price = $request['price'];
        $course->promo_price = $request['promo_price'];
        $course->course_type = $request['course_type'];
        $course->course_requirements = $request['course_requirements'];
        $course->course_objectives = $request['course_objectives'];

       // $course->course_brief_description = $request['course_brief_description'];
        $course->course_full_description = $request['course_full_description'];

       if($course->save())
       {
        return redirect()->route('teachercourses')->with('success', 'Course updated successfully');
       }
       else
       {
        return redirect()->back()->with('error', 'There was error updating course. Please try again');
       }


    }

    public function disableteachercourse(Request $request, $id)
    {
        $course_id = $id;
        $courseObject = Course::find($course_id);
        $courseObject->course_status = 0;
        if($courseObject->save())
        {
            return redirect()->route('teachercourses')->with('success', 'Course disabled successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'There was error disabling course. Please try again');
        }
    }


    public function enableteachercourse(Request $request, $id)
    {
        $course_id = $id;
        $courseObject = Course::find($course_id);
        $courseObject->course_status = 1;
        if($courseObject->save())
        {
            return redirect()->route('teachercourses')->with('success', 'Course enabled successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'There was error enabling course. Please try again');
        }
    }





    public function teachercourseassignment(Request $request, $id)
    {
        $courseObject = Course::find($id);
        return view('teacher.addteacherassignment', compact('courseObject'));
    }

    public function postteacherassignment(Request $request)
    {

        $this->validate($request, [

            'assignment_name' => 'required',
            'course_id' => 'required',
            ]);


            $assignment = new Assignment();
            $assignment->assignment_name = $request['assignment_name'];
            $assignment->course_id = $request['course_id'];

            if($assignment->save())
            {

               return redirect()->back()->with('success', 'Assignment created successfully');
                //return redirect('assignment', $request['course_id'])->with("success","Assignment created successfully");

            }
            else
            {
                return redirect()->back()->with('error', 'There was error creating assignment. Please try again');

               // return redirect('assignment', $request['course_id'])->with("error","There was error creating assignment. Please try again");

            }


    }




    public function teacherassignmentlists($id)
    {
        $assignments = Assignment::orderby('assignment_name', 'ASC')->where('course_id', $id)->get();
        return view('teacher.teacherassignmentlists', compact('assignments'));
    }



    public function editteacherassignment(Request $request, $id)
    {
        $assignment = Assignment::find($id);
        return view('teacher.editteacherassignment', compact('assignment'));
    }

    public function posteditteacherassignment(Request $request)
    {
        $assignment = Assignment::find($request['assignment_id']);
        $assignment->assignment_name = $request['assignment_name'];
        if($assignment->save())
        {
            //return redirect('assignmentlists')->with("success","Changes saved successfully");
            return redirect()->back()->with('success', 'Changes saved successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'Could not save changes. Please try again');
            //return redirect('assignmentlists')->with("error","Could not save changes. Please try again");
        }

    }



    public function deleteteacherassignment(Request $request)
    {
        $assignment = Assignment::find($request['assignment_id']);

        if($assignment->delete())
        {

            return redirect()->back()->with('success', 'Assignment deleted successfully');
         // return redirect()->back()->with('success', 'Score deleted successfully');

        }
        else
        {
            return redirect()->back()->with('error', 'Could not delete assignment. Please try again');


        }
    }


}
