<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\DTO;
use App\Course;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Studentcertificate;
use App\Program;
use App\Assignment;


class AdminController extends Controller
{
    public function admindashboard()
    {
        return view('admin.dashboard');
    }

    public function updateadminprofile()
   {
        if (Auth::check())
        {
            $user = Auth::user();
            $userId = $user->id;
           $userObject = User::find($userId);
            return view('admin.changeprofile', compact('userObject'));
        }
        else
        {
            return redirect()->route('login');

        }
       //return view('changeprofile');
   }

   public function postupdateadminprofile(Request $request)
   {
        $userId = $request['id'];

        $fullname = $request['fullname'];

        $address = $request['address'];
        $phone = $request['phone'];

        $user = User::find($userId);
        $user->name = $fullname;
        $user->address = $address;
        $user->phone = $phone;

        $user->save();
        return redirect('updateadminprofile')->with("success","Profile updated successfully");
   }


   public function postupdateadminpassword(Request $request)
   {

        if (!(\Hash::check($request->get('cpassword'), Auth::user()->password))) {

            return redirect('/updateadminprofile')->with('error', 'Your current password does not match with the password you provided. Please try again.');
        }

        if(strcmp($request->get('cpassword'), $request->get('npassword')) == 0){

            return redirect('/updateadminprofile')->with('error', 'New Password cannot be same as your current password. Please choose a different password.');
        }

        if(strcmp($request->get('password_confirmation'), $request->get('npassword')) != 0){

        return redirect('/updateadminprofile')->with('error', 'Confirmation password mismatch');
        }

        $user = Auth::user();
        $email = $user->email;
        $id = $user->id;

        $user = User::whereid($id)->FirstOrFail();

        $user->password = bcrypt($request->get('npassword'));
        $user->save();


        return redirect('updateadminprofile')->with("success","Password changed successfully !");
    }



    public function uploadadminsignature()
    {
        return view('admin.uploadadminsignature');
    }

    public function postadminsignature(Request $request)
    {

      $this->validate($request, [

         'file' => 'required|mimes:png,jpg,jpeg',

   ]);

    if (Auth::check())
    {
        $user = Auth::user();
        $userId = $user->id;

        if(!$request->hasFile('file')) {

            return redirect('uploadadminsignature')->with("error","Please select signature to upload");
            }

            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $name = explode(".", $name);
            $count = count($name);
            $name = "signature".str_shuffle("56789").time() . ".". $name[count($name) -1 ];
            $fileSize = $file->getSize();
            if(!$file->isValid()) {
                return redirect('uploadadminsignature')->with("error","Invalid file upload. Please select png, jpg or jpeg file to upload");
            }
            if($file->move('./signatures', $name))
            {


                $userObject = User::find($userId);

                $userObject->signature_url = 'signatures/'.$name;
                $userObject->save();

                 return redirect('uploadadminsignature')->with("success","Signature uploaded successfully");
            }
       // $userObject = User::find($userId);
        //return view('admin.changeprofile', compact('userObject'));
    }
    else
    {
        return redirect()->route('login');

    }


}

    public function studentlists()
    {
        $students = User::where('user_type', 'student')->get();
        return view('admin.studentlists', compact('students'));
    }

    public function studentdetails(Request $request, $id)
    {
        // fetch student courses
        $user = User::find($id);
        $userName = $user->name;
        $studentCourese = DTO::getUserCourses($id);

        return view('admin.studentdetails', compact('studentCourese', 'userName'));

        // fetch payment details

    }

    public function addstudentscore()
    {
      $courses = Course::orderby('course_title', 'ASC')->get();
      $students = User::where('user_type', 'student')->orderby('name', 'ASC')->get();

      $studentCertificates = array();


      return view('admin.addstudentscore', compact('courses', 'students', 'studentCertificates'));
    }


    public function postscore(Request $request)
    {

            $this->validate($request, [

                'course' => 'required',
            'student' => 'required',
            'score' => 'required|integer',

          ]);

            $certNo = str_pad(mt_rand(0, 999999), 4, '0', STR_PAD_LEFT);

            $course = $request['course'];
            $student = $request['student'];
            $score = $request['score'];

            $studentcertificates = new Studentcertificate();
            $studentcertificates->user_id = $student;
            $studentcertificates->course_id = $course;
            $studentcertificates->score = $score;
            $studentcertificates->attempt = 1;
            if($score >= 70)
            {
                $studentcertificates->certificate_no = $certNo;
            }
            else
            {

            }


            $studentcertificates->save();

            $studentCertificates = DTO::getUserCertificatesAdmin($studentcertificates->id);


            $courses = Course::orderby('course_title', 'ASC')->get();
            $students = User::where('user_type', 'student')->orderby('name', 'ASC')->get();

            return view('admin.addstudentscore', compact('courses', 'students', 'studentCertificates'));


           /// return redirect()->back()->with('success', 'Score added successfully', compact('studentCertificates'));

    }



    public function generatecertificateadmin(Request $request)
    {
        $course_id = $request['course_id'];

          $course = Course::find($course_id);
          $course_name = $course->course_title;

          $course_user_id = $course->user_id;
          $userObjectForSignature = User::find($course_user_id);
          $userObjectForSignatureAdmin = User::where('is_chairman', 1)->first();
          $studentCertificateObject = Studentcertificate::where('course_id', $course_id)->first();
          $studentCertificate = Studentcertificate::where('user_id', $studentCertificateObject->user_id)->first();
          $certNo = $studentCertificate->certificate_no;


         // dd($userObjectForSignature);

          $user = User::find($studentCertificateObject->user_id);
          $user_name = $user->name;
          return view('admin.certificate', compact('course_name', 'user_name', 'userObjectForSignature', 'userObjectForSignatureAdmin', 'certNo'));

    }

    public function deleteScore(Request $request)
    {
        $studentCertificate = Studentcertificate::find($request['id']);

        if($studentCertificate->delete())
        {
            return redirect('addstudentscore')->with("success","Score deleted successfully");
         // return redirect()->back()->with('success', 'Score deleted successfully');

        }
        else
        {
            return redirect('addstudentscore')->with("error","There was error deleting score. Please try again");

        }
    }


    public function generatecertificate()
    {
        $students = User::where('user_type', 'student')->orderby('name', 'ASC')->get();

      $studentCertificates = array();


      return view('admin.generatecertificate', compact('students', 'studentCertificates'));
    }

    public function loadcertificate(Request $request)
    {

        request()->validate([

            'student' => 'required',

            ]);

        $students = User::where('user_type', 'student')->orderby('name', 'ASC')->get();
        $studentCertificates = DTO::getUserCertificates($request['student']);
        return view('admin.generatecertificate', compact('students', 'studentCertificates'));
    }





    public function stafflists()
    {
        $staffs = User::where('user_type', 'staff')->get();
        return view('admin.stafflists', compact('staffs'));
    }

    public function staffdetails(Request $request, $id)
    {
        // fetch student courses
        $user = User::find($id);
        $userName = $user->name;
        $studentCourese = DTO::getUserCourses($id);
        return view('admin.staffdetails', compact('studentCourese', 'userName'));

        // fetch payment details

    }



    public function addstaffscore()
    {
      $courses = Course::orderby('course_title', 'ASC')->get();
      $students = User::where('user_type', 'staff')->orderby('name', 'ASC')->get();

      $studentCertificates = array();


      return view('admin.addstaffscore', compact('courses', 'students', 'studentCertificates'));
    }



    public function staffscore()
    {
        $students = User::where('user_type', 'staff')->orderby('name', 'ASC')->get();

      $studentCertificates = array();


      return view('admin.staffscore', compact('students', 'studentCertificates'));
    }


    public function createcategory()
    {
        return view('admin.addcategory');
    }

    public function postcategory(Request $request)
    {

        $this->validate($request, [

            'category_name' => 'required',


      ]);

        $category = new Program();
        $category->program_name = $request['category_name'];

        if($category->save())
        {
            return redirect('createcategory')->with("success","Course category created successfully");
         // return redirect()->back()->with('success', 'Score deleted successfully');

        }
        else
        {
            return redirect('createcategory')->with("error","There was error creating course category. Please try again");

        }

    }


    public function coursecategorylists()
    {
        $programs = Program::orderby('program_name', 'ASC')->get();
        return view('admin.categorylists', compact('programs'));
    }

    public function editcoursecategory(Request $request, $id)
    {
        $category = Program::find($id);
        return view('admin.editcategory', compact('category'));
    }

    public function posteditcategory(Request $request)
    {
        $category = Program::find($request['category_id']);
        $category->program_name = $request['category_name'];
        if($category->save())
        {
            return redirect('coursecategorylists')->with("success","Changes saved successfully");
        }
        else
        {
            return redirect('coursecategorylists')->with("error","Could not save changes. Please try again");
        }

    }


    public function deletecategory(Request $request)
    {
        $category = Program::find($request['category_id']);

        if($category->delete())
        {
            return redirect('coursecategorylists')->with("success","Course category deleted successfully");
         // return redirect()->back()->with('success', 'Score deleted successfully');

        }
        else
        {
            return redirect('coursecategorylists')->with("error","Could not delete course category. Please try again");

        }
    }



    public function assignment(Request $request, $id)
    {
        $courseObject = Course::find($id);
        return view('admin.addassignment', compact('courseObject'));
    }

    public function postassignment(Request $request)
    {

        $this->validate($request, [

            'assignment_name' => 'required',
            'course_id' => 'required',
            ]);


            $assignment = new Assignment();
            $assignment->assignment_name = $request['assignment_name'];
            $assignment->course_id = $request['course_id'];

            if($assignment->save())
            {

               return redirect()->back()->with('success', 'Assignment created successfully');
                //return redirect('assignment', $request['course_id'])->with("success","Assignment created successfully");

            }
            else
            {
                return redirect()->back()->with('error', 'There was error creating assignment. Please try again');

               // return redirect('assignment', $request['course_id'])->with("error","There was error creating assignment. Please try again");

            }


    }




    public function assignmentlists()
    {
        $assignments = Assignment::orderby('assignment_name', 'ASC')->get();
        return view('admin.assignmentlists', compact('assignments'));
    }



    public function editassignment(Request $request, $id)
    {
        $assignment = Assignment::find($id);
        return view('admin.editassignment', compact('assignment'));
    }

    public function posteditassignment(Request $request)
    {
        $assignment = Assignment::find($request['assignment_id']);
        $assignment->assignment_name = $request['assignment_name'];
        if($assignment->save())
        {
            return redirect('assignmentlists')->with("success","Changes saved successfully");
        }
        else
        {
            return redirect('assignmentlists')->with("error","Could not save changes. Please try again");
        }

    }



    public function deleteassignment(Request $request)
    {
        $assignment = Assignment::find($request['assignment_id']);

        if($assignment->delete())
        {
            return redirect('assignmentlists')->with("success","Assignment deleted successfully");
         // return redirect()->back()->with('success', 'Score deleted successfully');

        }
        else
        {
            return redirect('assignmentlists')->with("error","Could not delete assignment. Please try again");

        }
    }






}
