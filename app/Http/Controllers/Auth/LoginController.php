<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo;

    public function redirectTo()
    {
     switch(Auth::user()->role_id)

    {
        case 3:
                $this->redirectTo = '/classroom';
                return $this->redirectTo;
                break;
         case 2:
                 $this->redirectTo = '/teacherdashboard';
                 return $this->redirectTo;
                 break;
        case 1:
                $this->redirectTo = '/admindashboard';
                return $this->redirectTo;
                break;

       default:
                 $this->redirectTo = '/login';
                 return $this->redirectTo;
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       /* if(Auth::check() && Auth::user()->role->id ==1)
        {
            $this->redirectTo = route('courses');
        }
        elseif(Auth::check() && Auth::user()->role->id ==2)
        {
            $this->redirectTo = route('teacher.dashboard');
        }
        else
        {
            $this->redirectTo = route('classroom');
        }*/




        //$this->redirectTo = url()->previous();
        $this->middleware('guest')->except('logout');
    }










}
