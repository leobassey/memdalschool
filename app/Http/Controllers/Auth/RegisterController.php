<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    //protected $redirectTo;

      // you don't need this here if you need to redirect a user to verification page


      public function redirectTo()
      {
       switch(Auth::user()->role_id)

      {
          case 3:
                  $this->redirectTo = '/';
                  return $this->redirectTo;
                  break;
           case 2:
                   $this->redirectTo = '/teacherdashboard';
                   return $this->redirectTo;
                   break;
          case 1:
                  $this->redirectTo = '/admindashboard';
                  return $this->redirectTo;
                  break;

         default:
                   $this->redirectTo = '/login';
                   return $this->redirectTo;
          }

      }



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*if(Auth::check() && Auth::user()->role->id ==1)
        {
            $this->redirectTo = route('admin.dashboard');
        }
        elseif(Auth::check() && Auth::user()->role->id ==2)
        {
            $this->redirectTo = route('teacher.dashboard');
        }
        else
        {
            $this->redirectTo = route('welcome');
        }*/
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role_id' => 3,
        ]);
    }
}
