<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\DTO;
use Illuminate\Support\Facades\Auth;
//use App\Mail\UserCreationEmail;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegistrationNotification;
use App\Mail\UserRegistration;
use Validator;
use Carbon\Carbon;
use App\Studentcourse;


class UserController extends Controller
{
    public function users()
    {
        $countUsers = User::count();
        return view('admin.users', compact('countUsers'));
    }

    public function fetchallusers()
    {
        $data = User::All();
        return response()->json($data);

    }

    public function load_user_by_name(Request $request)
    {
        $name = $request['name'];


        $data = DTO::UserSeach($name);
        return response()->json($data);


    }

    public function load_user_by_type(Request $request)
    {
        $user_type = $request['user_type'];


        if($user_type == "all"){
            $data = User::all();
            return response()->json($data);

        }
        else
        {
        $data = User::where('user_type', $user_type)->get();
        return response()->json($data);
        }


    }


   /* public function createaccount()
    {

      $roles = Role::all();
      return view('admins.createuser', compact('roles'));
    }*/

	public function createaccount(Request $request)
    {

            $this->validate($request, [

                'email' => 'required|string|max:255|email|unique:users',
                'password' => 'required|min:5',
                'name' =>'required|string|max:255',
                'user_type' => 'required',



        ]);

        $password = $request['password'];
        $user_type = $request['user_type'];
        $encriptedPassword = bcrypt($password);

        $user = new User();
        $user->email = $request['email'];
        $user->password = $encriptedPassword;
        $user->name =$request['name'];
        $user->user_type = $request['user_type'];
        if($user_type == "admin")
        {
            $user->role_id = 1;
        }
        else if($user_type == "teacher")
        {
            $user->role_id = 2;
        }
        else if($user_type == "student")
        {
            $user->role_id = 3;
        }
        else if($user_type == "staff")
        {
            $user->role_id = 3;
        }

        $user->save();

     /*   $to_user =$request['name'];
        $email =  $request['email'];
        $password = $request['password'];

        $data = array(
            'to_user' => $to_user,
            'email' => $email,
            'password' => $password,
        );


        try{
            Mail::to($email)->send(new UserCreationEmail($data));
        }
        catch(\Exception $e){

        }*/
        $data = "User created successfully";
        return response()->json($data);

}



public function getUserById(Request $request)
{
    $user_id = $request['id'];
    $data = User::find($user_id);
    return response()->json($data);
}

public function updateUser(Request $request)
{

    $this->validate($request, [

        'name' =>'required|string|max:255',
        'user_type' => 'required',
        'id' => 'required',

]);

    $user_id = $request['id'];
    $password = $request['password'];
    $name = $request['name'];
    $user_type = $request['user_type'];

    $user = User::whereid($user_id)->FirstOrFail();

    $user->name = $name;
    $user->user_type = $user_type;

      if($user_type == "admin")
        {
            $user->role_id = 1;
        }
        else if($user_type == "teacher")
        {
            $user->role_id = 2;
        }
        else if($user_type == "student")
        {
            $user->role_id = 3;
        }
        else if($user_type == "staff")
        {
            $user->role_id = 3;
        }


    if (!$request['password'] == '')
    {
        $user->password = bcrypt($request['password']);
    }

      $user->save();
      $data = "User updated successfully";
        return response()->json($data);
     // return redirect('users')->with('success', 'Changes successfully saved');

       /*if ($request->has('npassword'))
      $user->password = bcrypt($request->input('npassword'));*/

   /* $section = Coursesection::find($section_id);
    $section->section_name = $section_name;
    $section->section_title = $section_title;
    if($section->save())
    {
        $data = "Course section updated successfully.";
        return response()->json($data);
    }
    else
    {
        $data = "Could not update Course section! Please try again.";
        return response()->json($data);
    }*/

}



public function userregistration(Request $request)
    {

        // validate fields
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required |email|unique:users',
            'password' => 'required',
            'password_confirmation'=>'required|same:password',

          ]);


            // create a company login account before creating the company profile using

                $name = $request->input('name');
                $email =  $request->input('email');
                $password =  $request->input('password');
                $encriptedPassword = bcrypt($password);

                if($user = new User())
                {

                    $user->name = $name;
                    $user->email = $email;
                    $user->password = $encriptedPassword;
                    $user->verification_token = User::generateVerificationCode();
                    $user->role_id = 3;
                    $user->certname = $name;

                    $user->save();

                    $link = \Config::get('constants.frontend');
                     $data = array(
                        'user_name' => $name,
                        'user_email' => $email,
                    );

        try{
            Mail::to($user)->send(new UserRegistration($user, $link));
            Mail::to('leoemmanuel11@gmail.com')->send(new UserRegistrationNotification($data));

        }
                     catch(\Exception $e){

                     }


                   return redirect()->back()->with('success', 'Registration successful. Please check your email for verification');
                }


        }


        public function verifytoken($token)
        {
              $user  = User::where('verification_token', $token)->first();
              if(empty($user))
              {
                return redirect()->back()->with('error', 'Verification failed! Please try again');
              }
              else{
                  $user->verified = User::VERIFIED_USER;
                  $user->verification_token = null;
                  $link = \Config::get('constants.frontend').'login';
                  $user->save();
                 // return \Redirect::to($link);

                 return redirect()->route('login')->with('success', 'Verification successful. Please login');

              }

        }

        public function signin(Request $request)
        {

            $email = $request['email'];
            $password = $request['password'];

            if (Auth::attempt(['email'=>$email,'password'=>$password, 'is_active' => 1, 'verified' => 1])) {


                $studentCoursesForChecking = DTO::getUserCoursesForDurationChecking(Auth::user()->id);
                foreach($studentCoursesForChecking as $s)
                {


                    $getDays = $this->RemainingDuration($s->expire_date);
                    //dd($s->user_duration);
                   // dd($getDays);
                    if($getDays >= $s->user_duration)
                    {

                        $studentcourse = Studentcourse::find($s->student_course_id);
                        $studentcourse->is_active = 0;
                        $studentcourse->save();


                    }


                }




                switch(Auth::user()->role_id)

                {

                    case 3:
                        return redirect()->route('classroom');
                            break;
                     case 2:

                             return redirect()->route('teacherdashboard');
                             break;
                    case 1:

                            return redirect()->route('admindashboard');

                            break;

                   default:

                   return redirect()->back()->with('error', 'Inavalid email and or password!. Please try again');

                }

            }
               else{

                return redirect()->back()->with('error', 'Inavalid email and or password!. Please try again');
               }
        }


        public function RemainingDurationx()
        {
            $dueDate = '2022-08-05';
            $todayDate = Carbon::now();

            dd($days = $todayDate->diffInDays($dueDate));



          /* $to = \Carbon\Carbon::parse($request->to);
$from = \Carbon\Carbon::parse($request->from);

        $years = $to->diffInYears($from);
        $months = $to->diffInMonths($from);
        $weeks = $to->diffInWeeks($from);
        $days = $to->diffInDays($from);
        $hours = $to->diffInHours($from);
        $minutes = $to->diffInMinutes($from);
        $seconds = $to->diffInSeconds($from);*/

        }



        private function RemainingDuration($dueDate)
        {
            $todayDate = Carbon::now();


            $d1 = $todayDate;
            $d2 = $dueDate;
            return $days = $todayDate->diffInDays($dueDate);
            //$interval = $d1->diff($d2);
            //return $diffInMonths  = $interval->d; //4

        }







}
