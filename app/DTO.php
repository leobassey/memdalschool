<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DTO extends Model
{

    public static function getOnlineCourses()
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT c.*, p.program_name from programs as p inner join courses as c
        on p.id = c.program_id where c.course_type = 1");

		$selectStatment->execute();
		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }

    public static function getUserCoursesForDurationChecking($userId)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT sc.*, sc.id as student_course_id, c.*, c.id as courseId from studentcourses as sc inner join courses as c on sc.course_id = c.id where sc.user_id =:user_id AND sc.is_active = 1 order by c.course_type");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }

    public static function getUserCourses($userId)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT sc.*, sc.id as student_course_id, c.*, c.id as courseId from studentcourses as sc inner join courses as c on sc.course_id = c.id where sc.user_id =:user_id AND sc.is_active = 1 order by c.course_type");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }

    public static function getUserLockedCourses($userId)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT sc.*, sc.id as student_course_id, c.*, c.id as courseId from studentcourses as sc inner join courses as c on sc.course_id = c.id where sc.user_id =:user_id AND sc.is_active = 0 order by c.course_type");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }

    public static function getUserCompletedCourses($userId)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT sc.*, sc.id as student_course_id, c.*, c.id as courseId from studentcourses as sc inner join courses as c on sc.course_id = c.id where sc.user_id =:user_id AND sc.is_completed = 1 order by c.course_type");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }

    public static function getCourseSections($course_id)
    {

        $selectStatment = \DB::getPdo()->prepare("SELECT coursesections.*, COUNT(*) AS lecture_count
        FROM coursesections
        JOIN lectures ON lectures.section_id = coursesections.id
        where coursesections.course_id =:course_id GROUP BY 1");

        $selectStatment->bindValue(':course_id', $course_id, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }



    public static function getUserCertificates($userId)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT sc.*, c.*, c.id as courseId from studentcertificates as sc inner join courses as c on sc.course_id = c.id where sc.user_id =:user_id order by sc.id");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }



    public static function UserSeach($val)
    {

        $selectStatment = \DB::getPdo()->prepare("SELECT * from users where name like :val");

         $selectStatment->bindValue(':val', "%$val%", \PDO::PARAM_STR);


		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }


    public static function getSingleCourse($course_id)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT p.program_name, p.id as program_id, c.* from courses as c inner join programs as p
        on p.id = c.program_id where c.id = :course_id");

        $selectStatment->bindValue(':course_id', $course_id, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetch(\PDO::FETCH_OBJ);
		return $results;

    }

    public static function getTeacherCourses($role_id)
    {

        $selectStatment = \DB::getPdo()->prepare("SELECT u.name, c.* from users as u inner join courses as c
        on u.id = c.user_id where u.role_id =:role_id order by u.name");

        $selectStatment->bindValue(':role_id', $role_id, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;
    }

    public static function getAllCourses()
    {

        $selectStatment = \DB::getPdo()->prepare("SELECT u.name, c.* from users as u inner join courses as c
        on u.id = c.user_id order by u.name");

		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;
    }

    public static function getMyCourses($userId)
    {

        $selectStatment = \DB::getPdo()->prepare("SELECT u.name, c.* from users as u inner join courses as c
        on u.id = c.user_id where c.user_id = :user_id");

        $selectStatment->bindValue(':user_id', $userId, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;
    }


    public static function getUserCertificatesAdmin($id)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT sc.*, sc.id as scid, c.*, c.id as courseId from studentcertificates as sc inner join courses as c on sc.course_id = c.id where sc.id =:id order by sc.id");

        $selectStatment->bindValue(':id', $id, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }


    public static function getLectures($section_id, $course_id)
    {
        $selectStatment = \DB::getPdo()->prepare("SELECT l.*, sl.is_completed from lectures as l left join studentlectures as sl on l.id = sl.lecture_id where l.section_id = :section_id and l.course_id = :course_id");
        $selectStatment->bindValue(':section_id', $section_id, \PDO::PARAM_STR);
        $selectStatment->bindValue(':course_id', $course_id, \PDO::PARAM_STR);
		$selectStatment->execute();

		$results = $selectStatment->fetchAll(\PDO::FETCH_OBJ);
		return $results;

    }






}
