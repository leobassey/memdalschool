
(function(window, document, $) {
    'use strict';



    /************************************************
    *               Sparkline Charts                *
    ************************************************/

    var sparkLineDraw = function() {
        /******************
        *   Line Charts   *
        ******************/
        // Total Cost
        $("#sp-line-total-cost").sparkline([14,12,4,9,3,6,11,10,13,9,14,11,16,20,15], {
            type: 'line',
            width: '100%',
            height: '100px',
            lineColor: '#FF9149',
            fillColor: '#FF9149',
            spotColor: '',
            minSpotColor: '',
            maxSpotColor: '',
            highlightSpotColor: '',
            highlightLineColor: '',
            chartRangeMin: 0,
            chartRangeMax: 20,
        });

        // Total Sales
        $("#sp-line-total-sales").sparkline([14,12,4,9,3,6,11,10,13,9,14,11,16,20,15], {
            type: 'line',
            width: '100%',
            height: '100px',
            lineColor: '#28D094',
            fillColor: '#28D094',
            spotColor: '',
            minSpotColor: '',
            maxSpotColor: '',
            highlightSpotColor: '',
            highlightLineColor: '',
            chartRangeMin: 0,
            chartRangeMax: 20,
        });

        // Total Revenue
        $("#sp-line-total-revenue").sparkline([14,12,4,9,3,6,11,10,13,9,14,11,16,20,15], {
            type: 'line',
            width: '100%',
            height: '100px',
            lineColor: '#FF4961',
            fillColor: '#FF4961',
            spotColor: '',
            minSpotColor: '',
            maxSpotColor: '',
            highlightSpotColor: '',
            highlightLineColor: '',
            chartRangeMin: 0,
            chartRangeMax: 20,
        });

        /*****************
        *   Bar Charts   *
        *****************/
        $("#sp-bar-total-cost").sparkline([5,6,7,8,9,10,12,13,15,14,13,12,10,9,8,10,12,14,15,16,17,14,12,11,10,8], {
            type: 'bar',
            width: '100%',
            height: '30px',
            barWidth: 2,
            barSpacing: 4,
            barColor: '#FF9149'
        });

        $("#sp-bar-total-sales").sparkline([5,6,7,8,9,10,12,13,15,14,13,12,10,9,8,10,12,14,15,16,17,14,12,11,10,8], {
            type: 'bar',
            width: '100%',
            height: '30px',
            barWidth: 2,
            barSpacing: 4,
            barColor: '#28D094'
        });

        $("#sp-bar-total-revenue").sparkline([5,6,7,8,9,10,12,13,15,14,13,12,10,9,8,10,12,14,15,16,17,14,12,11,10,8], {
            type: 'bar',
            width: '100%',
            height: '30px',
            barWidth: 2,
            barSpacing: 4,
            barColor: '#FF4961'
        });

        /*************************
        *   Stacked Bar Charts   *
        *************************/
        $("#sp-stacked-bar-total-cost").sparkline([ [8,10], [12,8], [9,14], [8,11], [10,13], [7,11], [8,14], [9,12], [10,11], [12,14], [8,12], [9,12], [9,14] ], {
            type: 'bar',
            width: '100%',
            height: '30px',
            barWidth: 4,
            barSpacing: 6,
            stackedBarColor: ['#4CAF50', '#FFEB3B']
        });

        $("#sp-stacked-bar-total-sales").sparkline([ [8,10], [12,8], [9,14], [8,11], [10,13], [7,11], [8,14], [9,12], [10,11], [12,14], [8,12], [9,12], [9,14] ], {
            type: 'bar',
            width: '100%',
            height: '30px',
            barWidth: 4,
            barSpacing: 6,
            stackedBarColor: ['#FF5722', '#009688']
        });

        $("#sp-stacked-bar-total-revenue").sparkline([ [8,10], [12,8], [9,14], [8,11], [10,13], [7,11], [8,14], [9,12], [10,11], [12,14], [8,12], [9,12], [9,14] ], {
            type: 'bar',
            width: '100%',
            height: '30px',
            barWidth: 4,
            barSpacing: 6,
            stackedBarColor: ['#E91E63', '#00BCD4']
        });

        /**********************
        *   Tristate Charts   *
        **********************/
        $("#sp-tristate-bar-total-cost").sparkline([1,1,0,1,-1,-1,1,-1,0,0,1,1,0,-1,1,-1], {
            type: 'tristate',
            height: '30',
            posBarColor: '#ffeb3b',
            negBarColor: '#4caf50',
            barWidth: 4,
            barSpacing: 5,
            zeroAxis: false
        });

        $("#sp-tristate-bar-total-sales").sparkline([1,1,0,1,-1,-1,1,-1,0,0,1,1,0,-1,1,-1], {
            type: 'tristate',
            height: '30',
            posBarColor: '#009688',
            negBarColor: '#FF5722',
            barWidth: 4,
            barSpacing: 5,
            zeroAxis: false
        });

        $("#sp-tristate-bar-total-revenue").sparkline([1,1,0,1,-1,-1,1,-1,0,0,1,1,0,-1,1,-1], {
            type: 'tristate',
            height: '30',
            posBarColor: '#00BCD4',
            negBarColor: '#E91E63',
            barWidth: 4,
            barSpacing: 5,
            zeroAxis: false
        });

        // Total Revenue
        $("#sp-line-total-profit").sparkline([14,12,4,9,3,6,11,10,13,9,14,11,16,20,15], {
            type: 'line',
            width: '100%',
            height: '50px',
            lineColor: '#E91E63',
            fillColor: '',
            spotColor: '',
            minSpotColor: '',
            maxSpotColor: '',
            highlightSpotColor: '',
            highlightLineColor: '',
            chartRangeMin: 0,
            chartRangeMax: 20,
        });
    };

    var sparkResize;

    $(window).resize(function(e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparkLineDraw, 500);
    });
    sparkLineDraw();



})(window, document, jQuery);