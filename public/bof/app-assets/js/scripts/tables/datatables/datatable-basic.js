$(document).ready(function() {
    $('.scroll-horizontal').DataTable({
        'columnDefs': [
         {
            
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
        
        "scrollX": true,
        lengthMenu: [[25, 50, 100, 150, -1], [25, 50, 100, 150, "All"]],
        ordering:  false,

        dom: 'Blfrtip',
            buttons: [
                'csv', 'pdf', 'print'
            ]
    });

    $('.buttons-csv, .buttons-print, .buttons-pdf').addClass('btn btn-outline-primary btn-sm round mr-1');


});