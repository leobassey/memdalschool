/*=========================================================================================
    File Name: datatables-colvis.js
    Description: ColVis Datatable
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {
    var d = new Date();
    var year = d.getFullYear();

/************************************
*       Basic initialisation        *
************************************/

$('.dataex-colvis-basic').DataTable( {
    dom: 'C<"clear">lfrtip'
} );

/**********************************
*       Custom button text        *
**********************************/

$('.dataex-colvis-oagf-data').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' OAGF Revenue Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-nnpc-data').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' NNPC Revenue Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-dpr-data').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' DPR Revenue Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-mines-data').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' MINES & STEEL Revenue Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-ncs-data').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' NCS Revenue Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});

$('.dataex-colvis-firs-data').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' FIRS Revenue Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-cbn-data').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' CBN Revenue Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-historical').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : 'Revenue Historical Data',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-bof').DataTable( {
   
    dom: 'Bfrtip',

        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            // {
            //     extend: 'pdfHtml5',
            //     title : year +' Revenue Performance Reporting Template',
            //     orientation: 'landscape',
            //     pageSize: 'LEGAL',
            //     exportOptions: {
            //         columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,17, 18]
            //     }
            // },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-revenue').DataTable( {
    dom: 'Bfrtip',
        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +'  Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                title : year +'  Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,17]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-fgn').DataTable( {
    dom: 'Bfrtip',
        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,
        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +'  FGN Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                title : year +'  FGN Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,17]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-faac').DataTable( {
  
    dom: 'Bfrtip',
        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' FAAC Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                title : year +' FAAC Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,17]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});

//bieannual report
$('.dataex-colvis-b1').DataTable( {
    dom: 'Bfrtip',
        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' First Biannual Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                title : year +' First Biannual Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-b2').DataTable( {
    dom: 'Bfrtip',
        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Second Biannual Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                title : year +' Second Biannual Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});

//Quarterly Report
$('.dataex-colvis-q1').DataTable( {
    
    dom: 'Bfrtip',
        "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' First Quarter Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                title : year +' First Quarter Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});

$('.dataex-colvis-q2').DataTable( {
    
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Second Quarter Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                title : year +' Second Quarter Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-q3').DataTable( {
    
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Third Quarter Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Third Quarter Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-q4').DataTable( {
    
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Fourth Quarter Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            {
                extend: 'pdfHtml5',
                title : year +' Fourth Quarter Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});



$('.dataex-colvis-jan').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-feb').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-mar').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-apr').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-may').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-jun').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-jul').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-aug').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-sep').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-oct').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-nov').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
           
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});
$('.dataex-colvis-dec').DataTable( {
 
    dom: 'Bfrtip',
    "lengthMenu": [[25,50, 100, 150, -1], [25,50, 100, 150, "All"]],
        "pageLength":50,

        buttons: [
          
            {
                extend: 'excelHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                exportOptions: {
                    columns: ':visible'
                }
            },
            
            {
                extend: 'pdfHtml5',
                title : year +' Monthly Revenue Performance Reporting Template',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                }
            },
            'pageLength',
            'colvis',
        ],
        "ordering": false,

});





/*****************************************
*       Exclude columns from list        *
*****************************************/

$('.dataex-colvis-exclude').DataTable( {
    dom: 'C<"clear">lfrtip',
    colVis: {
        exclude: [ 0 ],
        //order : none,
    },

} );


/************************************
*       Mouseover activation        *
************************************/

$('.dataex-colvis-mouseover').DataTable( {
    dom: 'C<"clear">lfrtip',
    colVis: {
        activate: "mouseover"
    }
} );

/********************************************
*       Mouseover Restore / show all        *
********************************************/

$('.dataex-colvis-restore').DataTable( {
    dom: 'C<"clear">lfrtip',
    columnDefs: [
        { visible: false, targets: 2 }
    ],
    colVis: {
        restore: "Restore",
        showAll: "Show all",
        showNone: "Show none"
    }
} );


} );