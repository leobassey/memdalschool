$(document).ready(function(){ 
    $('.dataTables_filter input[type="search"]').
  attr('placeholder','Search ....').
  css({'width':'350px','display':'inline-block'});

    $('select[name="lga_id"]').attr('disabled', 'disabled');
    $('select[name="community_id"]').attr('disabled', 'disabled');
    $('#sectors').attr('disabled', 'disabled');
   $('select[name="primary_mdas"]').attr('disabled', 'disabled');
   $('select[name="project_status"]').attr('disabled', 'disabled');


    $('#senatorial_district').change(function(e) {

    var senatorial_district_id = e.target.value;
        $.get('/system-admin/local-government-areas?senatorial_district_id=' + senatorial_district_id, function(data){
            
            

            $('#lgas').empty();
            $('select[name="lga_id"]').removeAttr('disabled');
            $('select[name="lga_id"]').append('<option value="" disable="true" selected="true">--- Select Local Government Area ---</option>');


              $.each(data, function(index, value){
                    $('select[name="lga_id"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');

              })
        });
    });

    $('#lgas').on('change', function(e){
        //console.log(e);
        var lga_id = e.target.value;
        $.get('/system-admin/community?lga_id=' + lga_id,function(data) {
          //console.log(data);
            $('#communities').empty();
            $('select[name="community_id"]').removeAttr('disabled');
            $('select[name="community_id"]').append('<option value="0" disable="true" selected="true">--- Select Community ---</option>');
           
            $('#sectors').removeAttr('disabled');

            $.each(data, function(index, communitiesObj){
                $('select[name="community_id"]').append('<option value="'+ communitiesObj.id +'">'+ communitiesObj.name +'</option>');
            });
        });
    });



    $('#sectors').on('change', function(e){
        //console.log(e);
        var sector_id = e.target.value;
        $.get('/system-admin/primary_mda?sector_id=' + sector_id,function(data) {
          
            $('#primary_mdas').empty();
            $('#primary_mdas').removeAttr('disabled');
            $('#primary_mdas').append('<option value="0" disable="true" selected="true">--- Select MDA ---</option>');
           

            $.each(data, function(index, villagesObj){
                $('select[name="primary_mdas"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
            });
        });
    });
    $('#primary_mdas').on('change', function(e){
        //console.log(e);
        var sector_id = e.target.value;
        $.get('/system-admin/project_status?sector_id=' + sector_id,function(data) {
          
            $('#project_status').empty();
            $('#project_status').removeAttr('disabled');
            $('#project_status').append('<option value="0" disable="true" selected="true">--- Select Project Status ---</option>');
           

            $.each(data, function(index, villagesObj){
                $('select[name="project_status"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
            });
        });
    });
});