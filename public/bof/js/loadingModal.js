$(document).ready(function(){ 

    $('select[name="subsector_id"]').attr('disabled', 'disabled');
    $('#sectors').on('change', function(e){
        //console.log(e);
        var sector_id = e.target.value;
        $.get('/system-admin/subsector?sector_id=' + sector_id,function(data) {
          //console.log(data);
            $('#subsectors').empty();
            $('select[name="subsector_id"]').removeAttr('disabled');
            $('select[name="subsector_id"]').append('<option value="0" disable="true" selected="true">--- Select Sub-Sector ---</option>');

            $.each(data, function(index, villagesObj){
                $('select[name="subsector_id"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
            });
        });
    });
});