$(document).ready(function(){ 

    $('#senatorial_district').change(function(e) {

    var senatorial_district_id = e.target.value;
        $.get('/system-admin/local-government-areas?senatorial_district_id=' + senatorial_district_id, function(data){
            
            

            $('#lgas').empty();
            $('select[name="lga_id"]').removeAttr('disabled');
            $('select[name="lga_id"]').append('<option value="0" disable="true" selected="true">--- Select Local Government Area ---</option>');


              $.each(data, function(index, value){
                    $('select[name="lga_id"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');

              })
        });
    });

    $('#lgas').on('change', function(e){
        //console.log(e);
        var lga_id = e.target.value;
        $.get('/system-admin/community?lga_id=' + lga_id,function(data) {
          //console.log(data);
            $('#communities').empty();
            $('select[name="community_id"]').removeAttr('disabled');
            $('select[name="community_id"]').append('<option value="0" disable="true" selected="true">--- Select Community ---</option>');
           
            $('select[name="sector_id"]').removeAttr('disabled');

            $.each(data, function(index, communitiesObj){
                $('select[name="community_id"]').append('<option value="'+ communitiesObj.id +'">'+ communitiesObj.name +'</option>');
            });
        });
    });

    $('#sectors').on('change', function(e){
        //console.log(e);
        var sector_id = e.target.value;
        $.get('/system-admin/subsector?sector_id=' + sector_id,function(data) {
          //console.log(data);
            $('#subsectors').empty();
            $('select[name="subsector_id"]').removeAttr('disabled');
            $('select[name="subsector_id"]').append('<option value="0" disable="true" selected="true">--- Select Sub-Sector ---</option>');

            $.each(data, function(index, villagesObj){
                $('select[name="subsector_id"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
            });
        });
    });
});