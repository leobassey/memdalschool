new Vue({
        el:'#calculate',
        data : {
            adult_male :'',
            adult_female :'',
            youth_male :'',
            youth_female :'',
            special_need :''
         },
        computed: {
                total: function(){
                    return parseInt(this.adult_male) + parseInt(this.adult_female) + parseInt(this.youth_male) + parseInt(this.youth_female) + parseInt(this.special_need);
                }
            }
    });