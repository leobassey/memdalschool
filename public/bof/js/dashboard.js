$(document).ready(function(){
    PieChartLoader();
    MDAChartLoader();
    FGNChartLoader();
    OilProductionChartLoader();
    ExchangesChartLoader();
    RetainedChartLoader();

});


    function PieChartLoader(){
        let year = 2018;
        var title = '2018 Revenue by Sector';

         $.ajax({
            dataType: 'json',
            type : 'get',
            url : '{{URL::to('api/chart/piechart')}}',
            data: {
              'year': year
            },
            success:function(data)
            {
            	console.log(data);

                var chart_data = [];  
                $.each(data, function(i, v){
                     var name, y;
                     chart_data.push({name: v.revenue, y:parseFloat(v.total)});
                   });        


                    //charting starts here 
                Build_Pie_Chart(title, chart_data, 'containerFER');  
                                            
            }                
        });

    }
    function FGNChartLoader(){
        let year = 2018;
        var title = '2018 FGN Independent Revenue Performance * 1000';

         $.ajax({
            dataType: 'json',
            type : 'get',
            url : '{{URL::to('api/chart/fgn')}}',
            data: {
              'year': year
            },
            success:function(data)
            {

                var chart_data = [];  
                $.each(data, function(i, v){
                     var name, y;
                     chart_data.push({name: v.revenue, y:parseFloat(v.total)});
                   });        


                    //charting starts here 
                Build_Pie_ChartFGN(title, chart_data, 'containerFgn');  
                                            
            }                
        });

    }

    function MDAChartLoader(){
        var d = new Date();
        var year = d.getFullYear();
        var title = '2019 Revenue per MDA';
        
         $.ajax({
            dataType: 'json',
            type : 'get',
            url : '{{URL::to('api/chart/mdas')}}',
            data: {
            'year': year
            },
            success:function(data)
            {

                var chart_data = [];  
                $.each(data, function(i, v){
                     var name, y;
                     chart_data.push({name: v.agency, y:parseFloat(v.revenue)});
                   });        


                    //charting starts here 
                      //var barcolors_array=['#F2784B', '#F2784B', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
                        // '#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#113322','#4455aa','#a86622','#11aa33'];
                      
                      var xaxis_categories = chart_data.map(a => a.name);
                      var  data_array_values = chart_data.map(a => parseFloat(a.y));
                      
                Build_Bar_Chart(title, xaxis_categories, data_array_values, 'containerMDA');
                                            
            }                
        });

    }
    function OilProductionChartLoader(){
        var year = 2018;

        var title = '2018 Total Oil Production (mbpd)';
        
         $.ajax({
            dataType: 'json',
            type : 'get',
            url : '{{URL::to('api/chart/oilproduction')}}',
            data: {
            'year': year
            },
            success:function(data)
            {

                var chart_data = [];  
                $.each(data, function(i, v){
                     var name, y;
                     chart_data.push({name: v.item, y:parseFloat(v.total)});
                   });        


                    //charting starts here 
                      var barcolors_array=['#F2784B', '#F2784B', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
                        '#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#113322','#4455aa','#a86622','#11aa33'];
                      
                      var xaxis_categories = chart_data.map(a => a.name);
                      var  data_array_values = chart_data.map(a => parseFloat(a.y));
                      
                Build_Column_Chart(title, xaxis_categories, data_array_values, 'containerOilProduction');
                                            
            }                
        });

    }
    function ExchangesChartLoader(){
        var d = new Date();
        var year = d.getFullYear();

        var title = '2018 Exchange Gain';
        
         $.ajax({
            dataType: 'json',
            type : 'get',
            url : '{{URL::to('api/chart/exchanges')}}',
            data: {
            'year': year
            },
            success:function(data)
            {

                var chart_data = [];  
                $.each(data, function(i, v){
                     var name, y;
                     chart_data.push({name: v.item, y:parseFloat(v.total)});
                   });        


                    //charting starts here 
                      var barcolors_array=['#F2784B', '#F2784B', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
                        '#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#113322','#4455aa','#a86622','#11aa33'];
                      
                      var xaxis_categories = chart_data.map(a => a.name);
                      var  data_array_values = chart_data.map(a => parseFloat(a.y));
                      
                Build_Line_Chart(title, xaxis_categories, data_array_values, 'containerExchange');
                                            
            }                
        });

    }


    function RetainedChartLoader(){
        var d = new Date();
        var year = d.getFullYear();
        var title = "2018 FGN Retained Revenue"
        
         $.ajax({
            dataType: 'json',
            type : 'get',
            url : '{{URL::to('api/chart/retained')}}',
            data: {
            'year': year
            },
            success:function(data)
            {

                var chart_data = [];  
                $.each(data, function(i, v){
                     var name, y;
                     chart_data.push({name: v.item, y:parseFloat(v.total)});
                   });        


                    //charting starts here 
                      var barcolors_array=['#F2784B', '#F2784B', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
                        '#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#113322','#4455aa','#a86622','#11aa33'];
                      
                      var xaxis_categories = chart_data.map(a => a.name);
                      var  data_array_values = chart_data.map(a => parseFloat(a.y));
                      
                Build_Area_Chart(title, xaxis_categories, data_array_values, 'containerRE');
                                            
            }                
        });

    }

    
    function Build_Pie_ChartFGN(title, data_array,  container_id) { //id, title, data_array) {
        Highcharts.chart(container_id, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: false,
            title: {
                text: title,
                style: {
                    fontSize: '12px'
                }
            },
            tooltip: {
                pointFormat: '{point.y} <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.y:,.0f}</b> ({point.percentage:.1f} %)'
                    },
                    //showInLegend: true,
                }
            },        
            series: [{
                colorByPoint: true,
                data: data_array,
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 400
                    }
                }]
            },
            colors: ['#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4','#50B432',]
        });
    }
    function Build_Pie_Chart(title, data_array,  container_id) { //id, title, data_array) {
        Highcharts.chart(container_id, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: false,
            title: {
                text: title,
                style: {
                    fontSize: '12px'
                }
            },
            tooltip: {
                pointFormat: '{point.y} <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.y:,.0f}</b> ({point.percentage:.1f} %)'
                    },
                    //showInLegend: true,
                }
            },        
            series: [{
                colorByPoint: true,
                data: data_array,
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 400
                    }
                }]
            },
            colors: ['#003f5c', '#f95d6a', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4']
        });
    }

    function Build_Bar_Chart(title,xaxis_categories, data_array_values,  container_id) { //id, title, data_array) {
        //var colors = get_color_shades();
        //var colors=['#F2784B', '#F2784B', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
        //'#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#113322','#4455aa','#a86622','#11aa33'];
        Highcharts.chart(container_id, {
            chart: {
                type: 'bar',
                //height: height
            },
            title: {
                text: title,
                style: {
                    fontSize: '12px'
                }
            },
            labelFormatter: function () {
                    return '<span> NGN' + this.name + '</span>';
                },
                 yAxis: {            
                        labels: {
                            formatter: function () {
                                return 'NGN' + this.axis.defaultLabelFormatter.call(this);
                            }            
                        }
                    },            
            credits: false,
            xAxis: {
                categories: xaxis_categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                   text: 'Value (NGN)'

                },
                plotLines: [{
                    color: 'red',
                    //value: data.average_value,
                    width: '1',
                    zIndex: 2
                }]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y:, 1f}</b>',
                valuePrefix: 'NGN '
            },
            
           
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:, 1f}'
                    }
                }
            },
            
            colors: ['#2f4b7c', '#ffa600', '#003f5c','#bc5090','#ff6361','#FFC300','#79D815'],
            
            series: [{
                data: data_array_values,
               // colors: ['#825852', '#ffa600', '#003f5c','#bc5090','#ff6361','#FFC300','#79D815'],
            }]
        });
    }
    function Build_Column_Chart(title,xaxis_categories, data_array_values,  container_id) { //id, title, data_array) {
        var colors = get_color_shades();
        //var colors=['#F2784B', '#F2784B', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
        //'#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#113322','#4455aa','#a86622','#11aa33'];
        Highcharts.chart(container_id, {
            chart: {
                type: 'column',
                //height: height
            },
            title: {
                text: title,
                style: {
                    fontSize: '12px'
                }
            },
            labelFormatter: function () {
                    return '<span> NGN' + this.name + '</span>';
                },
                 yAxis: {            
                        labels: {
                            formatter: function () {
                                return 'NGN' + this.axis.defaultLabelFormatter.call(this);
                            }            
                        }
                    },            
            credits: false,
            xAxis: {
                categories: xaxis_categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                   text: 'Value (NGN)'

                },
                plotLines: [{
                    color: 'red',
                    //value: data.average_value,
                    width: '1',
                    zIndex: 2
                }]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y:, 1f}</b>',
                valuePrefix: 'NGN '
            },
            
           
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:, 1f}'
                    }
                }
            },
            series: [{
                data: data_array_values,
                colors: ['#2f4b7c', '#F27024', '#08bf0f','#003f5c','#a05195','#ffa600','#0099cc','#f95d6a','#008495','#112943','#d8ff2b']
            }]
        });
    }
    function Build_Line_Chart(title,xaxis_categories, data_array_values,  container_id) { //id, title, data_array) {
        
        Highcharts.chart(container_id, {
            chart: {
                type: 'line',
                //height: height
            },
            title: {
                text: title,
                style: {
                    fontSize: '12px'
                }
            },
            labelFormatter: function () {
                    return '<span> NGN' + this.name + '</span>';
                },
                 yAxis: {            
                        labels: {
                            formatter: function () {
                                return 'NGN' + this.axis.defaultLabelFormatter.call(this);
                            }            
                        }
                    },            
            credits: false,
            xAxis: {
                categories: xaxis_categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                   text: 'Value (NGN)'

                },
                plotLines: [{
                    color: 'red',
                    //value: data.average_value,
                    width: '1',
                    zIndex: 2
                }]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y:, 1f}</b>',
                valuePrefix: 'NGN '
            },
            
           
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:, 1f}'
                    }
                }
            },
            series: [{
                data: data_array_values,
                color: '#FF0000'
            }]
        });
    }
    function Build_Area_Chart(title,xaxis_categories, data_array_values,  container_id) { 
        Highcharts.chart(container_id, {
            chart: {
                type: 'areaspline',
            },
            title: {
                text: title,
                style: {
                    fontSize: '12px'
                }
            },
            labelFormatter: function () {
                    return '<span> NGN' + this.name + '</span>';
                },
                 yAxis: {            
                        labels: {
                            formatter: function () {
                                return 'NGN' + this.axis.defaultLabelFormatter.call(this);
                            }            
                        }
                    },            
            credits: false,
            xAxis: {
                categories: xaxis_categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                   text: 'Value (NGN)'

                },
                plotLines: [{
                    color: 'red',
                    //value: data.average_value,
                    width: '1',
                    zIndex: 2
                }]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y:, 1f}</b>',
                valuePrefix: 'NGN '
            },
            
           
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:, 1f}'
                    }
                }
            },
            series: [{
                data: data_array_values,
                colors: ['#79D815', '#F27024', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
                '#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#0099cc','#4455aa','#a86622','#884EA0'
        ]
            }]
        });
    }
    
    function get_color_shades() {
        var colors=['#F2784B', '#F2784B', '#08bf0f','#779c74','#caa94d','#097f09','#0099cc','#ffa500','#332943','#112943','#d7aa94',
        '#dedede','#2a2831','#a8daf9','#a8dadd','#341af9','#333451','#113322','#4455aa','#a86622','#11aa33'];
        {{-- var colors = [],
           base = Highcharts.getOptions().colors[start],
          i;

        for (i = 0; i < 100; i += 1) {
           colors.push(colors[i]);
        } --}}
        return colors;
    }

    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });
    Highcharts.setOptions({
        lang: {
            decimalPoint: '.',
            thousandsSep: ','
        },
        global: {
            useUTC: false
          },
          exporting: {
            buttons: {
                printButton: {
                    enabled: true
                },
              contextButton: {
                menuItems: [{
                  text: 'Download Chart PNG',
                  onclick: function() {
                    this.exportChart();
                  },
                  separator: false
                }]
              }
            }
          }

    });
    var pieColors = (function () {
        var colors = [],
            i;

        for (i = 0; i < 10; i += 1) {
            base = Highcharts.getOptions().colors[i];
            c = Highcharts.Color(base).get();
            if (c.stops[0][1] != 'rgb(0,0,0)' && c.stops[1][1] != 'rgb(0,0,0)')
                colors.push(c);
        }
        return colors;
    }());

    var barColors = (function () {
        var colors = [],
            i;

        for (i = 0; i < 9; i += 1) {
            base = Highcharts.getOptions().colors[i];
            c = Highcharts.Color(base).get();
            if (c.stops[0][1] != 'rgb(0,0,0)' && c.stops[1][1] != 'rgb(0,0,0)')
                colors.push(c);
        }
        return colors;
    }());