$(document).ready(function() {
    // Checkbox & Radio 1
    $('.input-chk').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
    });
    // Check All checkbox start
    $('#check-all').on('ifChecked', function (event) {
        $('.check').iCheck('check');
        triggeredByChild = false;
        $('.checked').parents('tr').addClass("selected");
    });

    $('#check-all').on('ifChecked', function (event) {
        $('.check').iCheck('check');
        triggeredByChild = false;
        $('.checked').parents('tr').addClass("selected");
    });

    $('#check-all').on('ifUnchecked', function (event) {
        if (!triggeredByChild) {
            $('.check').iCheck('uncheck');
            $('.check').parents('tr').removeClass("selected");
        }
        triggeredByChild = false;
    });

    // Removed the checked state from "All" if any checkbox is unchecked
    $('.check').on('ifUnchecked', function (event) {
        triggeredByChild = true;
        $('#check-all').iCheck('uncheck');
        $(this).parents('tr').removeClass("selected");
    });

    $('.check').on('ifChecked', function (event) {
        if ($('.check').filter(':checked').length == $('.check').length) {
            $('#check-all').iCheck('check');
        }
        $(this).parents('tr').addClass("selected");
    });
});



