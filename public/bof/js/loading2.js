$(document).ready(function(){ 

    $('#primary_mda').attr('disabled', 'disabled');
    $('#mda_id').attr('disabled', 'disabled');
    $('#secondary_mda').attr('disabled', 'disabled');

    $('#sectorss').on('change', function(e){
        //console.log(e);
        var sector_id = e.target.value;
        $.get('/system-admin/primary_mda?sector_id=' + sector_id,function(data) {
          //console.log(data);
            $('#primary_mda').empty();
            $('#mda_id').empty();
            $('#primary_mda').removeAttr('disabled');
            $('#mda_id').removeAttr('disabled');
            $('#primary_mda').append('<option value="0" disable="true" selected="true">--- Select MDA ---</option>');
            $('#mda_id').append('<option value="0" disable="true" selected="true">--- Select MDA ---</option>');
            

            $.each(data, function(index, villagesObj){
                $('select[name="primary_mda"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
                $('select[name="mda_id"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
                $('#secondary_mda').removeAttr('disabled');
            });
        });
    });
});