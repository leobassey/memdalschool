$(document).ready(function(){ 

    $('#primary').attr('disabled', 'disabled');
    $('#secondary_mda').attr('disabled', 'disabled');

    $('#sector_id').on('change', function(e){
        //console.log(e);
        var sector_id = e.target.value;
        $.get('/system-admin/primary?sector_id=' + sector_id,function(data) {
          //console.log(data);
            $('#primary').empty();
            $('#primary').removeAttr('disabled');
            $('#primary').append('<option value="0" disable="true" selected="true">--- Select MDA ---</option>');
          

            $.each(data, function(index, villagesObj){
                $('select[name="primary"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
                //$('select[name="mda_id"]').append('<option value="'+ villagesObj.id +'">'+ villagesObj.name +'</option>');
                $('#secondary_mda').removeAttr('disabled');
            });
        });
    });
});