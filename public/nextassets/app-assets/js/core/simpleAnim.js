$(document).ready(function() {
    $('[class="dropdown dropdown-notification nav-item"] a').mouseenter(function() {
        $(this).addClass("animated tada");
    });

    $('[class="dropdown dropdown-notification nav-item"] a').mouseleave(function() {
        $(this).removeClass("animated tada");
    });

    $('.heading-elements [class="list-inline mb-0"] li a').mouseenter(function() {
        $(this).animate({paddingLeft: "-=2px", paddingRight: "-=2px"});
        $(this).children("i").animate({fontSize : "+=5px"});
    });

    $('.heading-elements [class="list-inline mb-0"] li a').mouseleave(function() {
        $(this).animate({paddingLeft: "+=2px", paddingRight: "+=2px"});
        $(this).children("i").animate({fontSize : "-=5px"});
    });
})