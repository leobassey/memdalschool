<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Course;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', 'PagesController@about')->name('about');
Route::get('/online-courses', 'PagesController@onlinecourses')->name('onlinecourses');
Route::get('/', 'PagesController@welcome')->name('welcome');
Route::get('/classroom-courses', 'PagesController@classroomcourses')->name('classroomcourses');
Route::get('/free-courses', 'PagesController@freecourses')->name('freecourses');

Route::get('/onlinecourse-details/{id?}/', 'PagesController@onlinecoursedetails')->name('onlinecoursedetails');
Route::get('/classroomcourse-details/{id?}/', 'PagesController@classroomcoursedetails')->name('classroomcoursedetails');
Route::get('/freecourse-details/{id?}/', 'PagesController@freecoursedetails')->name('freecoursedetails');
Route::get('/details/{slug?}/', 'PagesController@details')->name('details');
Route::get('/forgotpassword', 'PagesController@forgotpassword')->name('forgotpassword');
Route::post('/postforgotpassword', 'PagesController@postforgotpassword')->name('postforgotpassword');
Route::get('/resetpassword/{email?}/reset', 'PagesController@resetpassword')->name('resetpassword');
Route::post('/saveresetpassword', 'PagesController@saveresetpassword')->name('saveresetpassword');

Route::get('/terms', 'PagesController@terms')->name('terms');

Route::post('/userregistration', 'UserController@userregistration')->name('userregistration');
Route::get('/verifytoken/{token}', 'UserController@verifytoken')->name('verifytoken');
Route::post('/signin', 'UserController@signin')->name('signin');
Route::get('/fetchallusers', 'UserController@fetchallusers')->name('fetchallusers');
Route::get('/users', 'UserController@users')->name('users');
Route::post('/load_user_by_name', 'UserController@load_user_by_name')->name('load_user_by_name');
Route::post('/load_user_by_type', 'UserController@load_user_by_type')->name('load_user_by_type');
Route::post('/createaccount', 'UserController@createaccount')->name('createaccount');
Route::post('/getUserById', 'UserController@getUserById')->name('getUserById');
Route::post('/updateUser', 'UserController@updateUser')->name('updateUser');



Route::group(['middleware' => ['auth','admin']], function() {
   // ADMIN ROUTE
Route::get('/admindashboard', 'AdminController@admindashboard')->name('admindashboard');
Route::get('/updateadminprofile', 'AdminController@updateadminprofile')->name('updateadminprofile');
Route::post('/postupdateadminprofile', 'AdminController@postupdateadminprofile')->name('postupdateadminprofile');
Route::post('/postupdateadminpassword', 'AdminController@postupdateadminpassword')->name('postupdateadminpassword');
Route::get('/uploadadminsignature', 'AdminController@uploadadminsignature')->name('uploadadminsignature');
Route::post('/postadminsignature', 'AdminController@postadminsignature')->name('postadminsignature');


Route::post('/upload', 'CourseController@upload')->name('upload');
Route::get('/studentlists', 'AdminController@studentlists')->name('studentlists');
Route::get('/studentdetails/{id}', 'AdminController@studentdetails')->name('studentdetails');
Route::get('/addstudentscore', 'AdminController@addstudentscore')->name('addstudentscore');
Route::post('/postscore', 'AdminController@postscore')->name('postscore');
Route::post('/generatecertificateadmin', 'AdminController@generatecertificateadmin')->name('generatecertificateadmin');
Route::post('/deleteScore', 'AdminController@deleteScore')->name('deleteScore');

/*Route::get('/generatecertificate', 'AdminController@generatecertificate')->name('generatecertificate');*/
Route::post('/loadcertificate', 'AdminController@loadcertificate')->name('loadcertificate');

Route::get('/stafflists', 'AdminController@stafflists')->name('stafflists');
Route::get('/staffdetails/{id}', 'AdminController@staffdetails')->name('staffdetails');


Route::get('/addstaffscore', 'AdminController@addstaffscore')->name('addstaffscore');

Route::get('/staffscore', 'AdminController@staffscore')->name('staffscore');
Route::post('/postcategory', 'AdminController@postcategory')->name('postcategory');
Route::get('/createcategory', 'AdminController@createcategory')->name('createcategory');
Route::get('/editcoursecategory/{id}', 'AdminController@editcoursecategory')->name('editcoursecategory');
Route::get('/coursecategorylists', 'AdminController@coursecategorylists')->name('coursecategorylists');
Route::post('/posteditcategory', 'AdminController@posteditcategory')->name('posteditcategory');
Route::post('/deletecategory', 'AdminController@deletecategory')->name('deletecategory');


Route::post('/postassignment', 'AdminController@postassignment')->name('postassignment');
Route::get('/assignment/{id}', 'AdminController@assignment')->name('assignment');

Route::get('/assignmentlists', 'AdminController@assignmentlists')->name('assignmentlists');
Route::post('/posteditassignment', 'AdminController@posteditassignment')->name('posteditassignment');
Route::post('/deleteassignment', 'AdminController@deleteassignment')->name('deleteassignment');
Route::get('/editassignment/{id}', 'AdminController@editassignment')->name('editassignment');




// Course route

Route::post('/postcontent', 'CourseController@postcontent')->name('postcontent');

Route::get('/courses', 'CourseController@courses')->name('courses');
Route::get('/loadprograms', 'Courses\CoursesController@loadprograms')->name('loadprograms');
Route::post('/postcourse', 'CourseController@postcourse')->name('postcourse');

Route::get('/create-course', 'CourseController@createcourse')->name('createcourse');
Route::get('/coursesettings/{id?}/', 'CourseController@coursesettings')->name('coursesettings');
Route::post('/postimage', 'CourseController@postimage')->name('postimage');

Route::get('/manage-lectures/{id?}/', 'CourseController@managelectures')->name('managelectures');
Route::post('/load_sections', 'CourseController@load_sections')->name('load_sections');
Route::post('/load_lectures', 'CourseController@load_lectures')->name('load_lectures');
Route::post('/loadlecturesbysectionid', 'CourseController@loadlecturesbysectionid')->name('loadlecturesbysectionid');
Route::post('/Deletelectures', 'CourseController@Deletelectures')->name('Deletelectures');
Route::post('/postlecture', 'CourseController@postlecture')->name('postlecture');

Route::get('/manage-sections/{id?}/', 'CourseController@managesections')->name('managesections');
Route::post('/DeleteSection', 'CourseController@DeleteSection')->name('DeleteSection');
Route::post('/postsection', 'CourseController@postsection')->name('postsection');
Route::post('/getSectionById', 'CourseController@getSectionById')->name('getSectionById');
Route::post('/updatesection', 'CourseController@updatesection')->name('updatesection');
Route::get('/createexam/{id}', 'CourseController@createexam')->name('createexam');
Route::post('/postexam', 'CourseController@postexam')->name('postexam');

Route::post('/posteditcourse', 'CourseController@posteditcourse')->name('posteditcourse');
Route::get('/editcourse/{id}', 'CourseController@editcourse')->name('editcourse');

Route::get('/disablecourse/{id}', 'CourseController@disablecourse')->name('disablecourse');
Route::get('/enablecourse/{id}', 'CourseController@enablecourse')->name('enablecourse');
Route::get('/teachercourselists', 'CourseController@teachercourselists')->name('teachercourselists');
Route::get('/allcourses', 'CourseController@allcourses')->name('allcourses');


Route::get('/disableenrollment/{id}', 'CourseController@disableenrollment')->name('disableenrollment');

Route::get('/acceptenrollment/{id}', 'CourseController@acceptenrollment')->name('acceptenrollment');


Route::get('/editcourseresources/{id}', 'CourseController@editcourseresources')->name('editcourseresources');
Route::get('/addcourseresource/{id}', 'CourseController@addcourseresource')->name('addcourseresource');
Route::get('/courseresourcelists/{id}', 'CourseController@courseresourcelists')->name('courseresourcelists');
Route::post('/postresource', 'CourseController@postresource')->name('postresource');
Route::post('/posteditcourseresource', 'CourseController@posteditcourseresource')->name('posteditcourseresource');
Route::post('/deletecourseresource', 'CourseController@deletecourseresource')->name('deletecourseresource');


Route::get('/courselectures/{id}', 'CourseController@courselectures')->name('courselectures');
Route::get('/editlecturenote/{id}', 'CourseController@editlecturenote')->name('editlecturenote');
Route::post('/postededitlecture', 'CourseController@postededitlecture')->name('postededitlecture');

Route::post('/postprojectimage', 'CourseController@postprojectimage')->name('postprojectimage');
Route::get('/addlecture/{id}', 'CourseController@addlecture')->name('addlecture');
Route::post('/storelecture', 'CourseController@storelecture')->name('storelecture');
Route::get('/deletelecture/{id}', 'CourseController@deletelecture')->name('deletelecture');









});

Route::group(['middleware' => ['auth','student']], function() {

    // Students route

Route::get('/classroom', 'StudentController@classroom')->name('classroo1');
Route::get('/mycourses', 'StudentController@mycourses')->name('mycourses');
Route::get('/orderhistory', 'StudentController@orderhistory')->name('orderhistory');
Route::get('/profile', 'StudentController@profile')->name('profile');
Route::post('/saveprofile', 'StudentController@saveprofile')->name('saveprofile');
Route::post('/savecertname', 'StudentController@savecertname')->name('savecertname');
Route::post('/savephoto', 'StudentController@savephoto')->name('savephoto');
Route::get('/changepassword', 'StudentController@changepassword')->name('changepassword');
Route::post('/savepassword', 'StudentController@savepassword')->name('savepassword');
Route::get('/feedback/{id}', 'StudentController@feedback')->name('feedback');

Route::get('/mycertificates', 'StudentController@mycertificates')->name('mycertificates');
Route::get('/classroom', 'StudentController@classroom')->name('classroom');
Route::get('/slug', 'StudentController@slug')->name('slug');
Route::get('/coursesec/{id?}', 'StudentController@coursesec')->name('coursesec');
Route::get('/learning/{id?}', 'StudentController@learning')->name('learning');
Route::get('/lesson/{id?}', 'StudentController@lesson')->name('lesson');
Route::get('/updateprofile', 'StudentController@updateprofile')->name('updateprofile');
Route::post('/postupdateprofile', 'StudentController@postupdateprofile')->name('postupdateprofile');
Route::post('/postupdatepassword', 'StudentController@postupdatepassword')->name('postupdatepassword');
Route::get('/exam/{id?}', 'StudentController@exam')->name('exam');
Route::post('/submitExam', 'StudentController@submitExam')->name('submitExam');
Route::post('/generatecertificate', 'StudentController@generatecertificate')->name('generatecertificate');

Route::get('/examsuccess', 'StudentController@examsuccess')->name('examsuccess');
Route::get('/examnotice', 'StudentController@examnotice')->name('examnotice');

Route::get('/newlayout', 'StudentController@newlayout')->name('newlayout');
Route::get('/supports', 'StudentController@supports')->name('supports');
Route::post('/submitsupport', 'StudentController@submitsupport')->name('submitsupport');
Route::post('/submitfeedback', 'StudentController@submitfeedback')->name('submitfeedback');
Route::get('/fetchfeedbacks', 'StudentController@fetchfeedbacks')->name('fetchfeedbacks');
Route::get('/feedbacks/{id}', 'StudentController@feedbacks')->name('feedbacks');

Route::get('/lockedcourses', 'StudentController@lockedcourses')->name('lockedcourses');
Route::get('/completedcourses', 'StudentController@completedcourses')->name('completedcourses');
Route::get('/generatecertificate2', 'StudentController@generatecertificate2')->name('generatecertificate2');
Route::get('/unlock/{id}', 'StudentController@unlock')->name('unlock');
Route::get('/completion/{id}', 'StudentController@completion')->name('completion');




});

Route::group(['middleware' => ['auth','teacher']], function() {

    // TEACHER ROUTE
Route::get('/teacherdashboard', 'TeacherController@teacherdashboard')->name('teacherdashboard');
Route::get('/teachercourses', 'TeacherController@teachercourses')->name('teachercourses');
Route::get('/createteachercourse', 'TeacherController@createteachercourse')->name('createteachercourse');
Route::post('/postteachercourse', 'TeacherController@postteachercourse')->name('postteachercourse');
Route::get('/teachercoursesettings/{id?}/', 'TeacherController@teachercoursesettings')->name('teachercoursesettings');
Route::get('/manageteachersections/{id?}/', 'TeacherController@manageteachersections')->name('manageteachersections');
Route::post('/postteacherlecture', 'TeacherController@postteacherlecture')->name('postteacherlecture');
Route::get('/manageteacherlectures/{id?}/', 'TeacherController@manageteacherlectures')->name('manageteacherlectures');

Route::get('/updateteacherprofile', 'TeacherController@updateteacherprofile')->name('updateteacherprofile');
Route::post('/postupdateteacherprofile', 'TeacherController@postupdateteacherprofile')->name('postupdateteacherprofile');
Route::post('/postupdateteacherpassword', 'TeacherController@postupdateteacherpassword')->name('postupdateteacherpassword');

Route::get('/createteacherexam/{id}', 'TeacherController@createteacherexam')->name('createteacherexam');
Route::post('/postteacherexam', 'TeacherController@postteacherexam')->name('postteacherexam');
Route::get('/uploadteachersignature', 'TeacherController@uploadteachersignature')->name('uploadteachersignature');
Route::post('/postteachersignature', 'TeacherController@postteachersignature')->name('postteachersignature');

Route::post('/posteditteachercourse', 'TeacherController@posteditteachercourse')->name('posteditteachercourse');
Route::get('/editteachercourse/{id}', 'TeacherController@editteachercourse')->name('editteachercourse');

Route::get('/disableteachercourse/{id}', 'TeacherController@disableteachercourse')->name('disableteachercourse');
Route::get('/enableteachercourse/{id}', 'TeacherController@enableteachercourse')->name('enableteachercourse');


Route::get('/teachercourseassignment/{id}', 'TeacherController@teachercourseassignment')->name('teachercourseassignment');
Route::post('/postteacherassignment', 'TeacherController@postteacherassignment')->name('postteacherassignment');
Route::get('/teacherassignmentlists/{id}', 'TeacherController@teacherassignmentlists')->name('teacherassignmentlists');
Route::post('/posteditteacherassignment', 'TeacherController@posteditteacherassignment')->name('posteditteacherassignment');
Route::post('/deleteteacherassignment', 'TeacherController@deleteteacherassignment')->name('deleteteacherassignment');
Route::get('/editteacherassignment/{id}', 'TeacherController@editteacherassignment')->name('editteacherassignment');





});

Auth::routes();



Route::get('/lecture/{id}/{courseId}', 'StudentController@lecture')->name('lecture');
Route::get('/completed/{id}/{courseId}', 'StudentController@completed')->name('completed');

// Transaction route
Route::post('/initiatepaywithflutter', 'PagesController@initiatepaywithflutter')->name('initiatepaywithflutter');//by Nsima
Route::post('/verifyReference', 'PagesController@verifyReference')->name('verifyReference');//by Nsima
Route::get('/fundwallet', 'StudentController@fundwallet')->name('fundwallet');//by Nsima
Route::post('/initiateWalletFunding', 'StudentController@initiateWalletFunding')->name('initiateWalletFunding');//by Nsima
Route::post('/verifyFundWalletReference', 'StudentController@verifyFundWalletReference')->name('verifyFundWalletReference');//by Nsima
Route::get('/viewwallet', 'StudentController@viewwallet')->name('viewwallet');//by nsima
Route::post('/enrollfree', 'PagesController@enrollfree')->name('enrollfree'); //by nsima
Route::post('/payViaWallet', 'PagesController@payViaWallet')->name(' payViaWallet');// by nsima
Route::get('/enrollment/{id?}/', 'PagesController@enrollment')->name('enrollment');

Route::get('/details2/{slug?}/', 'PagesController@details2')->name('details2');
Route::get('/enrollmentsuccess/{id}/', 'PagesController@enrollmentsuccess')->name('enrollmentsuccess');
Route::post('/addcoupon', 'PagesController@addcoupon')->name('addcoupon');
Route::get('/RemainingDuration', 'UserController@RemainingDuration')->name('RemainingDuration');
Route::get('/welcome2', 'PagesController@welcome2')->name('welcome2');
Route::get('/landingpage', 'PagesController@landingpage')->name('landingpage');

Route::get('/certificate', 'PagesController@certificate')->name('certificate');
Route::get('/apply', 'PagesController@apply')->name('apply');
Route::post('/postapplication', 'PagesController@postapplication')->name('postapplication');

Route::get('/home', 'HomeController@index')->name('home');


