@extends('layouts.frontlayout')

@section('content')

   <!-- Page Banner Section Start -->
   <div class="page-banner bg-color-05">
    <div class="page-banner__wrapper">
        <div class="container">


        </div>
    </div>
</div>
<!-- Page Banner Section End -->




           <!-- <div class="row">
              <div class="col my-2">
                <button class="btn btn-block btn-facebook">
                 <i class="ti-facebook mr-1"></i>
                 <span>Facebook Sign in</span>
               </button>
              </div>
              <div class="col my-2">
                <button class="btn btn-block btn-google-plus">
                 <i class="ti-google mr-1"></i>
                 <span>Google Sign in</span>
               </button>
              </div>
            </div>-->
          <!--  <p class="text-center my-4">
              OR
            </p>-->

            <!-- Log In Modal Start -->
<div>

    <div class="modal-dialog modal-dialog-centered modal-login">

    <!-- Modal Wrapper Start -->
    <div class="modal-wrapper">

        @if ($message = Session::get('success'))
<div class="alert alert-success alert-block mb-3" style="width: 70%; margin-left: 10px;">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif

@if(count($errors) >0)
<ul style="color:red; font-weight:bold; list-style:none">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif


        <button class="modal-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>

        <!-- Modal Content Start -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-size: 22px">Password Reset</h5>
                <br/>
                <p class="modal-description">Enter your new password for your student account.</p>
            </div>
            <div class="modal-body">

                <form action="{{route('saveresetpassword')}}" method="post">
                    @csrf

                    <div class="input-group input-group--focus mb-3">


                        <input id="email" type="email" class="form-control border-left-0 pl-0 @error('email') is-invalid @enderror" name="email"  required value="{{$userEmail}}" readonly>

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
                      </div>


                <div class="input-group input-group--focus mb-3">


                    <input id="password" type="password" class="form-control border-left-0 pl-0" name="cpassword" placeholder="New password" required>

    @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
                  </div>


                  <div class="input-group input-group--focus mb-3">


                    <input id="password" type="password" class="form-control border-left-0 pl-0" name="npassword" Placeholder="Confirm Password" required>

    @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
                  </div>




                <div class="modal-form">
                    <button class="btn btn-primary btn-hover-secondary w-100">Send Email</button>
                </div>
            </form>


            <!--
            <form method="POST" action="{{ route('login') }}" class="px-lg-4">
                        @csrf

                        @if ($message = Session::get('success'))
<div class="alert alert-success alert-block mb-3" style="width: 70%; margin-left: 10px;">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif

@if(count($errors) >0)
<ul style="color:red; font-weight:bold; list-style:none">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif
              <div class="input-group input-group--focus mb-3">


                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter email address" >

     </div>
              <div class="input-group input-group--focus mb-3">


                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter password">

@error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
              </div>
              <div class="d-md-flex justify-content-between my-4">
                <label class="ec-checkbox check-sm my-2 clearfix">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  <span class="ec-checkbox__control"></span>
                  <span class="ec-checkbox__lebel">Remember Me</span>
                </label>
                <a href="{{route('forgotpassword')}}" class=" my-2 d-block" style="color:#02b159">Forgot password?</a>
              </div>
              <button class="btn btn-block btn-primary">Log In</button>

              <div class="row mt-3">
              <div class="col-md-6">
              <p class="">
              Don't an account ?<a href="{{route('register')}}" class="" style="color:#02b159"> Register</a>
              </p>
              </div>
              <div class="col-md-6">


              </div>
              </div>



            </form>-->
          </div>
        </div>
      </div>
    </div> <!-- END row-->
  </div> <!-- END container-->
</section>

<br/><br/><br/><br/><br/>


<!--
<section class="padding-y-100 bg-light">
<div class="container">
    <div class="row">
      <div class="col-lg-6 mx-auto">
        <div class="card shadow-v2">
         <div class="card-header border-bottom">
          <h4 class="mt-4">
            Log In to Your EchoTheme Account!
          </h4>
         </div>
                <div class="card-body">
                <div class="row">
              <div class="col my-2">
                <button class="btn btn-block btn-facebook">
                 <i class="ti-facebook mr-1"></i>
                 <span>Facebook Sign in</span>
               </button>
              </div>
              <div class="col my-2">
                <button class="btn btn-block btn-google-plus">
                 <i class="ti-google mr-1"></i>
                 <span>Google Sign in</span>
               </button>
              </div>
            </div>
            <p class="text-center my-4">
              OR
            </p>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->


@endsection
