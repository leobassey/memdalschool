<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate</title>
    <link rel="stylesheet" href="{{asset('bootstrap.css')}}">
    <style>
        @font-face {
            src: url('ALS-Script.ttf.woff');
            font-family: TheALS;
        }

        * {
            font-family: 'Work Sans';
        }

        #holder-name {
            font-family: Arial, Helvetica, sans-serif !important;
        }
    </style>

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
</head>

<body>

<div class="row mt-2 ml-2" id="closediv">
<div class="col-md-6 mb-3"></div>
<div class="col-md-6 mb-3">
<span><a href="#" onclick="printcertificate()" >Print Certificate</a></span> <span class="ml-2"><a href="{{route('mycertificates')}}">Go to My Certificate page</a></span>

</div>
</div>


    <div style="width:1000px; height:700px; padding:13px; text-align:center; border: 10px solid #08193e; background-color: #08193e;">
        <div style="width:950px; height:650px; padding:20px; text-align:center; border: 5px solid #fff; background-image: url('bg.png'); background-size: cover;">
            <div class="row">
                <div class="text-center">
                    <img style="width: 30%; margin-top:13px" src="{{asset('img/Memdalschool.png')}}" alt="">
                </div>
            </div>
            <!--<div class="row">
                <div class="col-md-5 text-left" style="padding-left: 0;
        padding-top: 10px;
        position: relative;
        right: 30px;">
                    <span style="font-weight: 600; font-size: 14px;">
               MEMDAL DIGITAL
                <br>
                SCHOOL OF TECHNOLOGY
            </span>
                </div>
                <div class="col-md-5 text-center" style="padding-right: 15px; padding-left: 0;">
llll
                </div>
            </div>-->
            <div class="row" style="height: 180px;">
                <div class="col-md-12 text-left" style="margin-top: 30px;">
                    <span style="font-size: 26px;
             font-family: 'Poppins', sans-serif; font-weight: bolder; color:#08193e;">{{ strtoupper($course_name) }}</span>
                    <br>

                </div>
            </div>
            <div class="row" style="margin-top: -70px">
                <div class="col-md-7 text-left">
                    <span style="color: #333;
            font-size: 20px;
            font-family: 'Poppins', sans-serif;">Certifcate is presented to:</span>
                </div>

                <div class="col-md-5 text-left">
                    <ul style="list-style: none; font-size: 18px; font-family: 'Poppins', sans-serif;">
                        <li>Data analysis fundamental</li>
                        <li>Data Transformation with Power Query</li>
                        <li>Data Manipulation with DAX</li>
                        <li>Data Visualization with Charts</li>
                        <li>Advanced Power BI Concept</li>
                        <li>Power BI Service</li>
                        <li>Dasgboard design with PowerPoint</li>
                       </ul>
                </div>

            </div>
            <div class="row" style="margin-top: -190px">
                <div class="col-md-7 text-left mt-4" style="margin-top: 0px">
                    <span id="holder-name" style="width: 455px; display: block; font-size: 40px;
            padding-bottom: 0px; color:#08193e; font-weight: bold;
           ">{{$user_name}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-left" style="padding-top: 20px;">
                    <span style="color: #333;
            font-size: 20px;
            font-family: serif; display: block; margin-top:20px">Having completed course requirements on June 03, 2022 </span>
                    <span style="display: block;
            padding-top: 10px;
            font-size: 21px;
            font-weight: 700;  font-family: 'Poppins', sans-serif;"></span>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-4 text-center" style="padding-top: 70px;">
                    <span style="display: block;
            font-size: 20px;
            font-family: 'Poppins', sans-serif;

            height: auto;
            margin: 0px 50px;

            font-family: 'Poppins', sans-serif;">
                Certificate ID:

            </span>
                    <span style="display: block;
            font-size: 20px;
            font-family: 'Poppins', sans-serif;
            padding-bottom: 0px;
            border-bottom: 2px solid #08193e  ;
            height: auto;
            margin: 0px 50px;">{{$certNo}}</span>
                    <span style="display: block;
            padding-top: 0;
            color: #08193e  ;
            font-family: serif;
            font-size: 19px;">www.memdalschool.com/verify</span>
                </div>

                <div class="col-md-4 text-center" style="padding-top: 60px;">
                    <span style="display: block;
            font-size: 20px;
            font-family: 'Poppins', sans-serif;
            padding-bottom: 0px;
            height: auto;
            margin: 0px 50px;
            padding-bottom: 15px;
            font-family: serif;">
                &nbsp;
            </span>
                    <span style="display: block;
            font-size: 20px;
            font-family: 'Poppins', sans-serif;
            padding-bottom: 0px;
            border-bottom: 2px solid #08193e;
            height: auto;
            margin: 0px 50px; margin-top:-60px"><img src="{{$userObjectForSignature->signature_url}}"  width="178px;" height="86px"/></span>

              <span style="color:#08193e">   Chairman - Memdal School</span><br/>

                  <!--Microsft Certified Professional-->
                </div>

                <!--<div class="col-md-4 text-center" style="padding-top: 70px;">
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;

            height: auto;
            margin: 0px 50px;

            font-family: serif;">
                &nbsp;
            </span>
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;
            padding-bottom: 0px;
            border-bottom: 2px solid #007b10;
            height: auto;
            margin: 0px 50px; margin-top:-60px"><img src="{{$userObjectForSignatureAdmin->signature_url}}" width="178px;" height="86px"/></span>
                    <span style="display: block;
            padding-top: 0;
            color: #007b10;
            font-family: serif;
            font-size: 19px;">Chairman</span>
                </div>-->

            </div>
        </div>


    </div>

    <script src="{{asset('bootstrap.js')}}"></script>
</body>


<script>
function printcertificate() {
  document.getElementById("closediv").style.display = "none";
  window.print();
  window.location.href="mycourses";
}
</script>

</html>
