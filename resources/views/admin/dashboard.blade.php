@extends('layouts.adminlayout')
@section('title', 'dashboard')

@section('content')

@php
use App\User;
use Carbon\Carbon;
use App\Course;

$now = Carbon::now();
        $year = $now->year;
        $month = $now->month;
        $day = $now->day;

        $monthName = $now->format('F');
        $coursecount = Course::count();
        $teachercount = User::where('role_id',2)->count();
        $studentcount = User::where('role_id',3)->count();


@endphp


<!-- BEGIN: Content-->
<div id="app2">

        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">

            <!-- Hospital Info cards -->
            <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-center">
                                            <i class="la la-ship font-large-2 success"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h5 class="text-muted text-bold-500">Students</h5>
                                            <h3 class="text-bold-600" style="color:#02b159">{{$studentcount}}</h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-center">
                                            <i class="la la-street-view font-large-2 warning"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h5 class="text-muted text-bold-500">Courses</h5>
                                             <h3 class="text-bold-600" style="color:#02b159">{{$coursecount}}</h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-center">
                                            <i class="la la-database font-large-2 info"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h5 class="text-muted text-bold-500">Teacher</h5>
                                              <h3 class="text-bold-600" style="color:#02b159">{{$teachercount}}</h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Hospital Info cards Ends -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"></h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                              <div class="card-content collapse show">
                                <div class="card-body" style="height:500px">
                                    <div id="chartContainer" class="ml-5 text-center" >
                                       <!--<div id="containerC" style="width:100%; height:10px;" style="margin-top: 10px;" class="ml-2 mt-5" >
                                        </div>-->
                                        <img src="{{asset('assets/img/flat-graphic-4.png')}}" width="57%" />
                                    </div>
</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>



    <!-- END: Content-->


</div>

<!--
<script>
    var vm = new Vue({
        el:"#app2",
        data:{

            results:[],
        },

        created:function(){

            this.loadChart();
        },

        methods:{
            loadChart:function(){
                axios.post('loadopchartadmin').then(function(response){

                    document.getElementById("chartContainer").innerHTML = '&nbsp;';
                    document.getElementById("chartContainer").innerHTML = '<div id="containerC" style="width:100%; height:400px;" style="margin-top: 30px;"></div>';
                    var chart = document.getElementById('myChart');
                    var document_title = [];

                    var duration = [];
                    response.data.data.forEach(function (data) {
                        duration.push(data.qty);
                        document_title.push(data.name);
                    });
                    var title = 'Terminal Analysis Insight '+response.data.year;

                    var b=[];
                    for (var i=0,l=duration.length;i<l;i++) b.push(+duration[i]);
                    b[3]
                    typeof(b[3])
                    columnChart(title, document_title, b);

                });
            },

        }
    });
    function columnChart(title, xaxis_categories, /*yaxis_categories,*/ data_array_values){
            var container_id = document.getElementById('containerC');
            Highcharts.chart(container_id, {
                chart: {
                      type: 'column'
                },

                title: {
                    text: title,
                    style: {
                        fontWeight: 'bold'
                    }
                },
                legend: {
                      enabled: false
                },
                credits: false,
                xAxis: {
                      categories: xaxis_categories,
                      crosshair: true,
                      title: {
                        text: '<p style="margin-top:100px; font-size:16px"></p>'
                    },
                },
                yAxis: {
                      min: 0,
                     // categories: yaxis_categories,
                    title: {
                        text: 'NET QUANTITY (BARRELS)',
                    },

                },
                tooltip: {
                    /* pointFormat:'<b> {point.y} Days </b>'*/

                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                        colorByPoint: true

                    },
                    colors: [
                        '#ff0000',
                        '#00ff00',
                        '#0000ff'
                    ],
                    series:{
                        borderWidth:0,
                        pointWidth: 40,
                        dataLabels:{
                            //  enabled:true,
                              /*format:' {point.y} Days'*/

                        }
                    }
                },
                series: [{
                    name: 'NET BARRELS',
                    data: data_array_values,

                }]
            });
    }
</script>
-->
@endsection
