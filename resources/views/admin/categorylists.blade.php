@extends('layouts.adminlayout')
@section('title', 'Course category lists')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">


                        <div class="card-header">
                                <h4 class="card-title">Course category lists</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                       <!-- <li><a data-action="" href="{{route('createcourse')}}">+ Enroll students</a></li>
-->
                                        <li><a data-action="" href="{{route('createcategory')}}">+ Create course category</a></li>


                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">

                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show">
                    <div class="card-body card-dashboard">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered patients-list dataex-html5-export">
                            <thead>
                            <tr>
                            <th class="wd-15p">Name</th>

                            <th class="wd-25p">Edit</th>

                            <th class="wd-25p">Delete</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($programs as $p)
                <tr>
                <td >{{$p->program_name}}</td>

                  <td>
                  <!--<a href="{{route('editcoursecategory', $p->id)}}" title="Edit course category">
                      <i class="la la-cog" ></i></a>-->
                      <a href="{{route('editcoursecategory', $p->id)}}" title="Edit course category"><i class="ft-edit text-success ml-1"></i></a>


                    </td>

                    <td> <form method="post" action="{{route('deletecategory')}}">
                                    @csrf

                                    <input type="hidden" name="category_id" value="{{$p->id}}"/>
                                    <button class="btn btn-info mg-r-5 ml-3" style="cursor:pointer; background-color:#02b159; border-color:#02b159">Delete</button>

                                    </form></td>


                </tr>
                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
