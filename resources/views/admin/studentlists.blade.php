@extends('layouts.adminlayout')
@section('title', 'Student lists')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">


                        <div class="card-header">
                                <h4 class="card-title">Student lists</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                       <!-- <li><a data-action="" href="{{route('createcourse')}}">+ Enroll students</a></li>
-->
                                        <li><a data-action="" href="{{route('generatecertificate')}}">+ Generate Certificate</a></li>


                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">

                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show">
                    <div class="card-body card-dashboard">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered patients-list dataex-html5-export">
                            <thead>
                            <tr>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Email</th>
                            <th class="wd-25p">Phone</th>
                            <th class="wd-25p">Date</th>
                            <th class="wd-25p">View Courses</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($students as $s)
                <tr>
                <td >{{$s->name}}</td>
                  <td >{{$s->email}}</td>
                  <td>{{$s->phone}}</td>
                  <td>{{$s->created_at}}</td>

                  <td><a href="{{route('studentdetails', $s->id)}}" title="View course settings">
                      <i class="la la-cog" ></i></a>
                     <!-- <a href="{{route('editcourse', $s->id)}}" title="Edit course"><i class="ft-edit text-success ml-1"></i>
                      <a href="{{route('disablecourse', $s->id)}}" title="Disable this course"><i class="fa fa-lock ml-1 text-warning"></i>
                      <a href="{{route('enablecourse', $s->id)}}" title="Enable this course"><i class="fa fa-unlock-alt ml-1 text-warning"></i>

-->
                    </td>


                </tr>
                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
