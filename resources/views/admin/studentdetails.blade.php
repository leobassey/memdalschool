@extends('layouts.adminlayout')
@section('title', 'Student lists')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">


                        <div class="card-header">
                                <h4 class="card-title"><h3>{{$userName}}</h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="" href="{{route('studentlists')}}">All Students</a></li>



                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">

                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif



                        @if(empty($studentCourese))
<p>No course found!</p>
@else

                                <section id="patients-list">
    <div class="row">


        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show">
                    <div class="card-body card-dashboard">
                        <div class="row">
                            <div class="col-md-10">  <h4>Courses Enrolled</h4></div>
                            <div class="col-md-2">  <h4>Add course</h4></div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered patients-list">
                            <thead>
                            <tr>
                                <th class="wd-15p">Details</th>
                            <th class="wd-15p">Course</th>
                            <th class="wd-15p">Course Type</th>
                            <th class="wd-15p">Duration</th>
                            <th class="wd-15p">Date enrolled</th>
                            <th class="wd-15p">Course Price</th>
                            <!--<th class="wd-15p">Email</th>
                            <th class="wd-25p">Phone</th>
                            <th class="wd-25p">Date</th>
                            <th class="wd-25p">View Details</th>-->

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($studentCourese as $sc)
                <tr>
                    <td ><a href="#">Details</a></td>
                <td >{{$sc->course_title}}</td>
                <td >

                @if($sc->course_type == 1)
                Online
                @elseif($sc->course_type == 2)
                Offline
                @else
                Free
                @endif


                </td>
                <td>{{$sc->duration}}</td>
                <td>{{$sc->created_at}}</td>
                <td>{{$sc->course_price}}</td>




                </tr>
                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif


                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
