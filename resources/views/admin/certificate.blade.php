<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate</title>
    <link rel="stylesheet" href="{{asset('bootstrap.css')}}">
    <style>
        @font-face {
            src: url('ALS-Script.ttf.woff');
            font-family: TheALS;
        }

        * {
            font-family: 'Work Sans';
        }

        #holder-name {
            font-family: TheALS !important;
        }
    </style>
</head>

<body>

<div class="row mt-2 ml-2" id="closediv">
<div class="col-md-6 mb-3"></div>
<div class="col-md-6 mb-3">
<span><a href="#" onclick="printcertificate()" >Print Certificate</a></span> <span class="ml-2"><a href="{{route('admindashboard')}}">Back</a></span>

</div>
</div>


    <div style="width:1000px; height:700px; padding:13px; text-align:center; border: 10px solid #007b10; background-color: #007b10;">
        <div style="width:950px; height:650px; padding:20px; text-align:center; border: 5px solid #fff; background-image: url('bg.png'); background-size: cover;">
            <div class="row">
                <div class="col-md-2 text-center" style="padding-right: 15px; padding-left: 0;">
                    <img style="width: 60%;" src="{{asset('FIDAS LOGO.png')}}" alt="">
                </div>
                <div class="col-md-5 text-left" style="padding-left: 0;
        padding-top: 10px;
        position: relative;
        right: 30px;">
                    <span style="font-weight: 600; font-size: 14px;">
                FARMKONNECT INSTITUTE FOR DATA
                <br>
                AND AGRIBUSINESS STUDIES
            </span>
                </div>
                <div class="col-md-5 text-center" style="padding-right: 15px; padding-left: 0;">
                    <img style="width: 60%;
            position: absolute;
            bottom: -300px;
            right: 50px;" src="badge stuff.png" alt="">
                </div>
            </div>
            <div class="row" style="height: 180px;">
                <div class="col-md-12 text-left" style="padding-top: 20px;">
                    <span style="font-size: 75px;
            font-family: serif;">CERTIFICATE</span>
                    <br>
                    <span style="position: relative;
            font-size: 35px;
            font-family: serif;
            bottom: 25px;">OF ACHIEVEMENT</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-left" style="padding-top: 0px;">
                    <span style="color: #007b10;
            font-size: 18px;
            font-family: serif;">This is to certify that</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 text-left" style="padding-top: 20px;">
                    <span id="holder-name" style="width: 455px; display: block; font-size: 40px;
            padding-bottom: 0px;
            border-bottom: 2px solid #000; height: 50px;">{{$user_name}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-left" style="padding-top: 20px;">
                    <span style="color: #007b10;
            font-size: 18px;
            font-family: serif; display: block;">has successfully completed our 3 months training on</span>
                    <span style="display: block;
            padding-top: 10px;
            font-size: 21px;
            font-weight: 700; font-family: serif;">{{ strtoupper($course_name) }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center" style="padding-top: 20px;">
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;
            padding-bottom: 0px;
            height: auto;
            margin: 0px 50px;
            padding-bottom: 15px;
            font-family: serif;">
                Certificate ID:

            </span>
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;
            padding-bottom: 0px;
            border-bottom: 2px solid #007b10;
            height: auto;
            margin: 0px 50px;">{{$certNo}}</span>
                    <span style="display: block;
            padding-top: 0;
            color: #007b10;
            font-family: serif;
            font-size: 19px;">www.fidasedu.com/verify</span>
                </div>

                <div class="col-md-4 text-center" style="padding-top: 20px;">
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;
            padding-bottom: 0px;
            height: auto;
            margin: 0px 50px;
            padding-bottom: 15px;
            font-family: serif;">
                &nbsp;
            </span>
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;
            padding-bottom: 0px;
            border-bottom: 2px solid #007b10;
            height: auto;
            margin: 0px 50px; margin-top:-60px"><img src="{{$userObjectForSignature->signature_url}}"  width="178px;" height="86px"/></span>
                    <span style="display: block;
            padding-top: 0;
            color: #007b10;
            font-family: serif;
            font-size: 19px;">Chief Instructor</span>
                </div>

                <div class="col-md-4 text-center" style="padding-top: 20px;">
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;
            padding-bottom: 0px;
            height: auto;
            margin: 0px 50px;
            padding-bottom: 15px;
            font-family: serif;">
                &nbsp;
            </span>
                    <span style="display: block;
            font-size: 20px;
            font-family: Roboto;
            padding-bottom: 0px;
            border-bottom: 2px solid #007b10;
            height: auto;
            margin: 0px 50px; margin-top:-60px"><img src="{{$userObjectForSignatureAdmin->signature_url}}" width="178px;" height="86px"/></span>
                    <span style="display: block;
            padding-top: 0;
            color: #007b10;
            font-family: serif;
            font-size: 19px;">Chairman</span>
                </div>

            </div>
        </div>


    </div>

    <script src="{{asset('bootstrap.js')}}"></script>
</body>


<script>
function printcertificate() {
  document.getElementById("closediv").style.display = "none";
  window.print();
  window.location.href="admindashboard";
}
</script>

</html>
