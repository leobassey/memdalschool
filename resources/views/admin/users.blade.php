
@extends('layouts.adminlayout')
@section('title', 'Users')

<style>

.has-search .form-control {
    padding-left: 1.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;

}

.text-small{
    height:32px;
    font-size:13px;
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.label-small
{
    font-size: 12px;
}



.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #6b6f82;
}
.table th, .table td {
    padding: 0.50rem;
    font-size:12px;
}
.table th
{
    background-color:#02b159;
    color:#fff;
}
#app2 {
    font-family: TheRoboto;
}

.btn-info {
    border-color: #069 !important;
    background-color: #069 !important;
    border-radius: 0.21rem;
}

.btn-info:hover {
    background-color: #0481c0 !important;
    border-color: #0481c0 !important;
}

#overlay
{
			position: fixed;
			top: 40;
			bottom: 1003;
			left: 100;
			right: 2;
			background:rgba(0,0,0,0, 0.6);
}




</style>


@section('content')

            <div id="app2">
<template>
            <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header" style="">


                                <div class="row">
                                <div class="col-md-10">
                                <h4 class="" style=""><i class="la la-street-view"></i>Registered Users <span style="background-color:#333; color:#fff; font-weight:bolder" class="btn">{{$countUsers}}</span></h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="col-md-2">

                                <a class="btn " style="background-color:#02b159; color:#fff" a href="#" id="btnAdd" data-toggle="modal" data-target=".bd-example-modal-lg">Create User</a>
                                </div>
                                </div>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">


                                <form>

                                <div class="row">
                                   <!-- <div class="col-lg-3 col-md-6">
                                    <div class="form-group">
                                            <label for="gender">Year:</label>


										 <select name="year" id="year" class="form-control text-small" v-model='form.year'>
                                            <option value="0" selected="" disabled="">Select Year</option>
                        <option v-for='y in years' :value='y.year'>@{{ y.year }}</option>
                                            </select>

                                        </div>
                                    </div>-->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group" style="margin-top:-5px">
                                            <label for="gender">Filter By User type:</label>
                                            <select name="user_type" id="user_type" class="form-control text-small" @change='fetchUsersByType()' v-model='form.user_type'>
												  <option value="0" selected="" disabled="">Select user type</option>
                                                <option value="admin">Admin</option>
                                                <option value="teacher">Teachers</option>
                                                <option value="student">Students</option>
                                                <option value="staff">Staffs</option>
                                                <option value="all">Load all</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                    <label for="gender">Search:</label>
                                    <div class="input-group" style="">


    <input type="text" class="form-control" id="search" placeholder="Search By Name" name="search" v-model="form.name" style="margin-top:-5px"/>
   <div class="">
      <button class="btn btn-secondary" type="button" style="height:42px; padding:10px; margin-top:-6px; z-index: 100;" @click.prevent="fetchUserByName()">
Search
      </button>
    </div>
  </div>
                                    </div>

                                </div>



                            </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div class = "container">
            <div class="row align-items-center mt-4">
                    <div class="col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">

                                <div class="" style="margin-top:-15px">

               <table class="table table-bordered">
       <tr>
            <th>Name</th>
            <th>Email</th>
		    <th>User Type</th>
           <!-- <th>Status</th>-->
            <th>Registered Date</th>
            <th>Action</th>

     </tr>

     <tr v-for="u in results">

    <td>@{{u.name}}</td>
    <td>@{{u.email}}</td>
    <td>@{{u.user_type}}</td>
    <!--<td>
    <span v-if="u.is_active == 1">Active</span>

	<span v-else><p>InActive</p></span>


    </td>-->
    <td>@{{u.created_at}}</td>
<td><a href="#" @click="showEditModal=true; SelectUser(u.id)"><img src="{{asset('img/edit.png')}}"/></a></td>

     </tr>



   </table>
               </div>




                            </div>

                        </div>
                    </div>


           </div>

           </div>


                <!--  Large Modal -->
     <div class="modal fade bd-example-modal-lg mt-5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="margin-top:-10px">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">

                           <h5 class="modal-title ml-3" id="exampleModalCenterTitle" style="margin-left:-10px">Create a user with login access to FIDAS.</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                            <div class="row" style="margin:25px; margin-top:-5px">

<div class="col-md-12">


<img src="{{asset('img/loading.gif')}}" width="50px" v-show="loader"/><span v-show="loader">Creating user please wait...</span>



<div class="alert alert-success alert-dismissible fade show" role="alert"  v-show="success" style="height:40px">

<p v-show="success" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>

<div class="alert alert-danger alert-dismissible fade show" role="alert"  v-show="error" style="height:40px">

<p v-show="error" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>



</div>



</div>

                            <form enctype="multipart/form-data" id="menuform">
                            <div class="row" style="margin:25px">

<div class="col-md-6" >
    <div class="form-group">


    </div>
</div>

<div class="col-md-6">
    <div class="form-group">

    </div>
</div>


</div>

<div class="row" style="margin:25px; margin-top:-48px">

<div class="col-md-12">
<div class="form-group">
        <label>Fullname</label>
<input type="text" class="form-control" v-model="form.name" style="height:30px">

</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Email address</label>
<input type="text" class="form-control"  v-model="form.email" style="height:30px">

</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Password</label>
<input type="password" class="form-control"  v-model="form.password" style="height:30px">

</div>
</div>

<div class="col-md-12">
<div class="form-group">
        <label>Confirm Password</label>
<input type="password" class="form-control"  v-model="form.confirmpassword" style="height:30px">

</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Role</label>
        <select name="user_type" id="user_type" class="form-control text-small" v-model='form.user_type'>
												  <option value="0" selected="" disabled="">Select user type</option>
                                                <option value="admin">Admin</option>
                                                <option value="teacher">Teacher</option>
                                                <option value="student">Student</option>
                                                <option value="staff">Staff</option>


                                            </select>

</div>
</div>

</div>

</div>
                                <button class="btn" type="button"  style="background-color:#02b159; color:#fff; cursor:pointer" @click.prevent() = "createuser" id="btnpost">Submit</button>
                            </div>

                            </form>
                        </div>
                    </div>












                    <!-- EDIT-->
                    <!--  Large Modal -->
    <div id="overlay" class="overlay" v-if="showEditModal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">

                                <h5 class="modal-title ml-5" id="exampleModalCenterTitle">Edit User</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.prevent="showEditModal=false"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">

                            <div class="d-flex justify-content-center">




                            </div>

                            <form enctype="multipart/form-data" id="menuform">
                            <div class="row" style="margin:25px">

<div class="col-md-6" >
    <div class="form-group">


    </div>
</div>

<div class="col-md-6">
    <div class="form-group">

    </div>
</div>


</div>


<div class="row" style="margin:25px; margin-top:-48px">

<div class="col-md-12">
<div class="form-group">
        <label>Fullname</label>
<input type="text" class="form-control" v-model="SectionObject.name" style="height:30px">
<input type="hidden" class="form-control text-small"  v-model="SectionObject.id">

</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Email address</label>
<input type="text" class="form-control"  v-model="SectionObject.email" style="height:30px" readonly>

</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Password</label>
<input type="password" class="form-control"  v-model="SectionObject.password" style="height:30px">

</div>
</div>

<!--<div class="col-md-12">
<div class="form-group">
        <label>Confirm Password</label>
<input type="password" class="form-control"  v-model="SectionObject.confirmpassword" style="height:30px">

</div>
</div>-->


<div class="col-md-12">
<div class="form-group">
        <label>Role</label>
        <select name="user_type" id="user_type" class="form-control text-small"  v-model='SectionObject.user_type'>
												  <option value="0" selected="" disabled="">Select user type</option>
                                                <option value="admin">Admin</option>
                                                <option value="teacher">Teacher</option>
                                                <option value="student">Student</option>
                                                <option value="staff">Staff</option>


                                            </select>

</div>
</div>

</div>

<!--<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">
        <label>Section Name</label>
<input type="text" class="form-control"  placeholder="Example: Section 2" v-model="SectionObject.section_name" style="height:50px">
<input type="hidden" class="form-control text-small"  v-model="SectionObject.id">
</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Section Title</label>
<input type="text" class="form-control"  placeholder="Example: Business Model Canvas" v-model="SectionObject.section_title" style="height:50px">

</div>
</div>

</div>-->




<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">


<img src="{{asset('img/loading.gif')}}" width="50px" v-show="loader"/><span v-show="loader">updating user please wait...</span>



<div class="alert alert-success alert-dismissible fade show" role="alert"  v-show="success" style="height:40px">

<p v-show="success" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>

<div class="alert alert-danger alert-dismissible fade show" role="alert"  v-show="error" style="height:40px">

<p v-show="error" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>



</div>



</div>


</div>
    <button class="btn" type="button"  style="background-color:#02b159; color:#fff; cursor:pointer" @click.prevent() = "updatesection" id="btnupdate">Save Changes</button>
    </div>

</form>

                        </div>
                    </div>




















           </template>
</div>



<script>
				var data;
				var vm = new Vue({
                    el:"#app2",
                    data:{

                        results:[],

                        operation_no:'',
                        form:{ 'name':'', 'email':'', 'password':'', 'confirmpassword':'', 'role':'0', 'address':'', 'phone':'', 'user_type':0},

                         SectionObject:{'id':'', 'name':'', 'email':'', 'password':'', 'confirmpassword':'', 'role':'0', 'address':'', 'phone':'', 'user_type':0},


                        loader:false,
                        years:[],
                        year:0,


                        showEditModal:false,
                        loader:false,
                        success:false,
                        error:false,
                        message:'',
                    },



                    created:function(){
                        this.loadYears();
                        this.loadusers();
                    },

                    methods:
                    {

                    loadYears:function()
                   {

                     var _this = this;

                     axios.get('load_year').then(function(response){
                    _this.years = response.data;

                  });


                   },



                    loadusers:function()
                   {

                    var _this = this;

                    axios.get('fetchallusers').then(function(response){
                    _this.results = response.data;


                  });

                   },



                    fetchUserByName:function()
                    {

						 var _this = this;
                        this.form.name =  this.form.name;

                        var input =  this.form;

                        axios.post('load_user_by_name', input).then(function(response){
                       _this.results = response.data;

                    });

                    },

                    fetchUsersByType:function()
                    {
                        var _this = this;
                        this.form.user_type =  this.form.user_type;

                        var input =  this.form;

                        axios.post('load_user_by_type', input).then(function(response){
                       _this.results = response.data;
                    });


                    },




                createuser:function()
                 {

                    var  _this = this;

                    if(this.form.name == "")
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please enter user fullnamee";
                        return;
                    }
                    else if(this.form.email == "")
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please enter user's email address";
                        return;
                    }

                    else if(this.form.password == "")
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please enter user's password";
                        return;
                    }

                    else if(this.form.password != this.form.confirmpassword)
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Password confirmation mismatch";
                        return;
                    }

                    else if(this.form.user_type ==0)
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please select a user type";
                        return;
                    }

                    else
                    {


                var _this = this;
                let currentObj = this;

                var input =  this.form;
             document.getElementById('btnpost').disabled = true
            _this.loader = true;
            _this.error = false;
            _this.success = false;

            axios.post('./createaccount', input).then(function(response){

                _this.loadusers();
                _this.error = false;
                _this.success = true;
                 _this.message = response.data;
              _this. form = { 'name':'', 'email':'', 'password':'', 'confirmpassword':'', 'role':'0', 'address':'', 'phone':'', 'user_type':0},

                _this.loader = false;
                document.getElementById('btnpost').disabled = false;

            }).catch(function(error){
                document.getElementById('btnpost').disabled = false;

                _this.success = false;
                _this.loader = false;
                _this.error = true;
                _this.message = error;

                document.getElementById('btnpost').disabled = false;

            })


        }

  },




                    SelectUser:function(id)
                   {

                        var _this = this;
                        this.SectionObject.id =  id;
                        var input = this.SectionObject;
                        axios.post('./getUserById', input).then(function(response){
                        _this.SectionObject = response.data;

                    }).catch(function(error){
                        alert('Could not load record. Please try again');

                    })

                 },

                 updatesection:function()
                 {


                       var _this = this;
                        var input = this.SectionObject;

                        document.getElementById('btnupdate').disabled = true
                        _this.loader = true;
                        _this.error = false;
                        _this.success = false;
                        axios.post('./updateUser', input).then(function(response){


                _this.error = false;
                _this.success = true;
               // _this.message = "Lecture uploaded successfully";
                 _this.message = response.data;

                _this.loader = false;
                document.getElementById('btnupdate').disabled = false

                        _this.loadusers();
                        _this.showEditModal = false;

                    }).catch(function(error){
                        alert('Could not update record. Please try again');

                        _this.success = false;
                _this.loader = false;
                _this.error = true;
                _this.message = error;

                document.getElementById('btnupdate').disabled = false;

                    })
                 },










                    }

                });

    </script>

@endsection
