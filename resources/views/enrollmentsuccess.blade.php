@extends('layouts.frontlayout')
@section('title', 'Course Details')


@section('content')


  <!-- Slider Section Start -->
  <div class="slider-section slider-section-07 scene">

    <div class="slider-wrapper">
        <div class="container" style="margin-top: -60px">

            <!-- Slider Caption Start -->
            <div class="slider-caption-07 text-center" data-aos="fade-up" data-aos-duration="1000">
                <h2 class="">Course enrollment successful!</h2>
                <p class="lead">Thank you for enrolling in {{$courseName}} course. Happy learning.</p>

                <div class="slider-caption-07__btn" style="margin-top: -40px">
                    <a class="btn btn-primary btn-hover-secondary" href="{{route('mycourses')}}" style="margin-bottom: 50px;">Start Course</a>
                </div>


            </div>




            <!-- Slider Caption End -->

            <!-- Slider Images Start -->


        </div>
    </div>

</div>



@endsection
