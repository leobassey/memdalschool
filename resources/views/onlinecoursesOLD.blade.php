@extends('layouts.frontlayout')
@section('title', 'Online Courses')
@section('content')



   <!-- Course Section Start -->
   <div class="courses-section section-padding-01">
    <div class="container custom-container">

        <div class="row align-items-center">
            <div class="col-lg-6">

                <!-- Section Title Start -->
                <div class="section-title">
                    <h2 class="section-title__title-03">Online Courses for Anyone, Anywhere</h2>
                    <p>You don't have to struggle alone, you've got our assistance and help.</p>
                </div>
                <!-- Section Title End -->

            </div>
            <div class="col-lg-6">

                <!-- Section button Start -->
                <div class="section-btn-02 text-lg-end" data-aos="fade-up" data-aos-duration="1000">
                    <a class="btn btn-light btn-hover-primary" href="course-grid-left-sidebar.html">View all courses</a>
                </div>
                <!-- Section button End -->

            </div>
        </div>

        <div class="row gy-6">
            <div class="col-lg-4 col-sm-6">

                <!-- Course Start -->
                <div class="course-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="course-header">
                        <div class="course-header__thumbnail ">
                            <a href="course-single-layout-01.html"><img src="assets/images/courses/courses-1.jpg" alt="courses" width="370" height="247"></a>
                        </div>
                    </div>
                    <div class="course-info">
                        <span class="course-info__badge-text badge-all">All Levels</span>
                        <h3 class="course-info__title"><a href="course-single-layout-01.html">Successful Negotiation: Master Your Negotiating Skills</a></h3>
                        <a href="#" class="course-info__instructor">parra</a>
                        <div class="course-info__price">
                            <span class="sale-price">$39.<small class="separator">00</small></span>
                        </div>
                        <div class="course-info__rating">

                            <div class="rating-star">
                                <div class="rating-label" style="width: 80%;"></div>
                            </div>

                            <span>(2)</span>
                        </div>
                    </div>
                </div>
                <!-- Course End -->

            </div>
            <div class="col-lg-4 col-sm-6">

                <!-- Course Start -->
                <div class="course-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="course-header">
                        <div class="course-header__thumbnail ">
                            <a href="course-single-layout-01.html"><img src="assets/images/courses/courses-2.jpg" alt="courses" width="370" height="247"></a>
                        </div>
                    </div>
                    <div class="course-info">
                        <span class="course-info__badge-text badge-all">All Levels</span>
                        <h3 class="course-info__title"><a href="course-single-layout-01.html">Time Management Mastery: Do More, Stress Less</a></h3>
                        <a href="#" class="course-info__instructor">parra</a>
                        <div class="course-info__price">
                            <span class="sale-price">$29.<small class="separator">99</small></span>
                        </div>
                        <div class="course-info__rating">

                            <div class="rating-star">
                                <div class="rating-label" style="width: 80%;"></div>
                            </div>

                            <span>(2)</span>
                        </div>
                    </div>
                </div>
                <!-- Course End -->

            </div>
            <div class="col-lg-4 col-sm-6">

                <!-- Course Start -->
                <div class="course-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="course-header">
                        <div class="course-header__thumbnail ">
                            <a href="course-single-layout-01.html"><img src="assets/images/courses/courses-3.jpg" alt="courses" width="370" height="247"></a>
                        </div>
                    </div>
                    <div class="course-info">
                        <span class="course-info__badge-text badge-beginner">Beginner</span>
                        <h3 class="course-info__title"><a href="course-single-layout-01.html">Angular – The Complete Guide Master Courses (2020 Edition)</a></h3>
                        <a href="#" class="course-info__instructor">parra</a>
                        <div class="course-info__price">
                            <span class="sale-price">$49.<small class="separator">99</small></span>
                        </div>
                        <div class="course-info__rating">

                            <div class="rating-star">
                                <div class="rating-label" style="width: 80%;"></div>
                            </div>

                            <span>(2)</span>
                        </div>
                    </div>
                </div>
                <!-- Course End -->

            </div>
            <div class="col-lg-4 col-sm-6">

                <!-- Course Start -->
                <div class="course-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="course-header">
                        <div class="course-header__thumbnail ">
                            <a href="course-single-layout-01.html"><img src="assets/images/courses/courses-4.jpg" alt="courses" width="370" height="247"></a>
                        </div>
                        <div class="course-header__badge">
                            <span class="free">Free</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <span class="course-info__badge-text badge-beginner">All Levels</span>
                        <h3 class="course-info__title"><a href="course-single-layout-01.html">Consulting Approach to Problem Solving Master Courses</a></h3>
                        <a href="#" class="course-info__instructor">parra</a>
                        <div class="course-info__price">
                            <span class="free">Free</span>
                        </div>
                        <div class="course-info__rating">

                            <div class="rating-star">
                                <div class="rating-label" style="width: 80%;"></div>
                            </div>

                            <span>(2)</span>
                        </div>
                    </div>
                </div>
                <!-- Course End -->

            </div>
            <div class="col-lg-4 col-sm-6">

                <!-- Course Start -->
                <div class="course-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="course-header">
                        <div class="course-header__thumbnail ">
                            <a href="course-single-layout-01.html"><img src="assets/images/courses/courses-5.jpg" alt="courses" width="370" height="247"></a>
                        </div>
                        <div class="course-header__badge">
                            <span class="free">Free</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <span class="course-info__badge-text badge-all">All Levels</span>
                        <h3 class="course-info__title"><a href="course-single-layout-01.html">The Business Intelligence Analyst Course (2020 Edition)</a></h3>
                        <a href="#" class="course-info__instructor">parra</a>
                        <div class="course-info__price">
                            <span class="free">Free</span>
                        </div>
                    </div>
                </div>
                <!-- Course End -->

            </div>
            <div class="col-lg-4 col-sm-6">

                <!-- Course Start -->
                <div class="course-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="course-header">
                        <div class="course-header__thumbnail ">
                            <a href="course-single-layout-01.html"><img src="assets/images/courses/courses-6.jpg" alt="courses" width="370" height="247"></a>
                        </div>
                        <div class="course-header__badge">
                            <span class="free">Free</span>
                        </div>
                    </div>
                    <div class="course-info">
                        <span class="course-info__badge-text badge-all">All Levels</span>
                        <h3 class="course-info__title"><a href="course-single-layout-01.html">Become a Product Manager | Learn the Skills & Get the Job</a></h3>
                        <a href="#" class="course-info__instructor">parra</a>
                        <div class="course-info__price">
                            <span class="free">Free</span>
                        </div>
                        <div class="course-info__rating">

                            <div class="rating-star">
                                <div class="rating-label" style="width: 80%;"></div>
                            </div>

                            <span>(2)</span>
                        </div>
                    </div>
                </div>
                <!-- Course End -->

            </div>

        </div>

    </div>
</div>
<!-- Course Section End -->



<!--
<style>

.card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: cover;
}

</style>



<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url(assets/img/1920/background.jpg) no-repeat">

            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mx-auto text-white my-5 text-center" style="margin-bottom: 0px !important;">
                        <h1 style="font-weight: bolder; font-size: 35px;">
                          Online courses
                        </h1>

                        <p style="font-size: 19px;">Learn online where ever you are and get your certificate of course completion </p>

                    </div>
                    <div class="col-lg-6 col-md-5 my-3">
                        <div class="owl-carousel dots-white-outline" data-dots="true" data-smartspeed="300" data-items-tablet="3" data-items-mobile="2" data-space="15">
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/1.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/2.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/3.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/4.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/5.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>




<?php foreach ($programArray as $key) {?>
    <section class="padding-y-100" style="background-color: #f8f9fa! important;">


        <div class="container" style="max-width:1140px">


            <div class="row">

                <div class="line col-md-6 col-sm-4 d-flex flex-row bd-highlight" style="margin-top:-30px">
                    <h3 class="bd-highlight"><?php echo $key["name"] ?></h3>
                </div>
                <div class="line col-md-6 col-sm-8 d-flex flex-row-reverse bd-highlight " style="margin-top:-30px">
                    <h5 class="bd-highlight"><?php echo count($key["courses"]) ?> Course(s)</h5>
                </div>
                <hr style="margin-top:-6px">

              <?php foreach ($key["courses"] as $one) {?>
                <div class="col-lg-4 mt-1">
                    <div class="card">
                    @if($one['image_url'] == "")
                        <img class="rounded card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
                        @else
							<img class="rounded card-img-top" src="{{$one['image_url']}}" alt="course image"/>
                        @endif
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                <?php echo $one["course_title"] ?>
                            </a>
                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                            </p>
                            <div class="card-text d-flex bd-highlight mt-2">
                            <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($one['course_type']  == 1)
                                    Type: Online
                                    @elseif($one['course_type']  == 2)
                                    Type: Classroom-based
                                    @else
                                    Type: Free course
                                    @endif
                                     </p>
                                <h4 class="h5 text-right ml-5">
                                    <span class="" style="font-weight: bold; margin-left:-10px">N<?php echo $one["course_price"] ?></span>
                                </h4>
                            </div>

                            <a href="{{route('onlinecoursedetails', $one["id"]) }}" class="btn btn-primary mt-3" style="color:#fff">View course details</a>
                        </div>
                    </div>
                </div>
              <?php } ?>



            </div>


        </div>


    <br><br>
    </section>
<?php } ?>


-->
@endsection
