@extends('layouts.teacherlayout')
@section('title', 'Course settings')

<style>
.user-tab {
    position: relative;
    cursor: pointer;
    display: block;
    border-radius: 8px;
    padding: 15px;
    margin: -5px;
    margin-bottom: 20px;
    height: 150px;
    /* background: #2980AC; */
    background: #d9dfe2;
}

.user-tab div {
    position: absolute;
    bottom: 10px;
    left: 0;
    width: 100%;
    padding: 5px;
    background-color: #fff;
    text-align: center;
}

h2 {
    font-size: 16px;
    line-height: 16px;
    font-weight: 400;
}

h1 {
    font-size: 24px;
    line-height: 24px;
    font-weight: 400;
}



        .choose::-webkit-file-upload-button {
  color: white;
  display: inline-block;
  background: #1CB6E0;
  border: none;
  padding: 7px 15px;
  font-weight: 700;
  border-radius: 3px;
  white-space: nowrap;
  cursor: pointer;
  font-size: 10pt;
}







.has-search .form-control {
    padding-left: 1.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;

}

.text-small{
    height:32px;
    font-size:13px;
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.label-small
{
    font-size: 12px;
}



.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #6b6f82;
}
.table th, .table td {
    padding: 0.50rem;
    font-size:12px;
}
.table th
{
    background-color:#02b159;
    color:#fff;
}
#app2 {
    font-family: TheRoboto;
}

.btn-info {
    border-color: #02b159 !important;
    background-color: #02b159 !important;
    border-radius: 0.21rem;
}

.btn-info:hover {
    background-color: #02b159 !important;
    border-color: #02b159 !important;
}

#overlay
		{
			position: fixed;
			top: 40;
			bottom: 1003;
			left: 100;
			right: 2;
			background:rgba(0,0,0,0, 0.6);
		}

        .row {
    display: flex;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
</style>

@section('content')

<div id="app2">
</template>


<div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">

        <h5 class="ml-4" style="color:#333">Add Lectures to Course: {{$courseObject->course_title}}</h5>
      </nav>



<div class="container">


      <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header" style="">


                                <div class="row">
                                <div class="col-md-8">
                                <h5 class="" style=""><i class="la la-street-view"></i>Added Lectures <span style="background-color:#333; color:#fff; border-radius:5px; font-weight:bolder" class="btn">{{$countLectures}}</span></h5>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="col-md-4">
                               <a class="btn" style="background-color:#02b159; color:#fff" a href="#" id="btnAdd" data-toggle="modal" data-target=".bd-example-modal-lg">Create Lecture</a>
                                <a class="btn" style="background-color:#02b159; color:#fff" a href="{{route('teachercoursesettings', $courseObject->id)}}"><< Course Settings</a>
                                </div>
                                </div>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">


                                <form>

                                <div class="row">
                                    <div class="col-lg-3 col-md-6">
                                    <div class="form-group">
                                            <label for="gender">Filter lectures by sections:</label>
                                           <!-- <select name="year" id="year" class="form-control text-small" @change='fetchFiscalizationRegister()' v-model='year'>
                                                <option value="2020">2020</option>

                                            </select>-->

										 <select name="section" id="section" class="form-control text-small" v-model='form.section'>
                                            <option value="0" selected="" disabled="">Select Section</option>
                        <option v-for='s in sections' :value='s.id'>@{{ s.section_title }}</option>
                                            </select>

                                        </div>

                                        <div class="input-group-append">
      <button class="btn btn-secondary" type="button" style="height:32px; padding: 8px 10px 14px 10px; margin-top:-5; cursor:pointer" @click.prevent="loadlecturesbysectionid()" >
      Load Lectures
      </button>
    </div>


                                    </div>


                                </div>



                            </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row align-items-center ">
                    <div class="col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">

                                <div class="" style="margin-top:-15px">
                                <p class="mt-1 mb-1" style="color:#333">The table below lists all the lectures added to the course: <strong>{{$courseObject->course_title}}.</strong> You can click on the bin icon to delete a lecture. Please note that you can remove a lecture and add again if there is any mistake in the added lecture.</p>

            <table class="table table-bordered">
       <tr>

            <th>Title</th>
		    <th>Format</th>
		    <!--<th>URL</th>-->

            <th>Created Date</th>

            <th>Remove Lecture</th>

     </tr>

     <tr v-for="l in lectures">

    <td style="color:#333">@{{l.lecture_title}}</td>
    <td style="color:#333">@{{l.format}}</td>
    <!--<td style="color:#333">@{{l.url}}</td>-->
    <td style="color:#333">@{{l.created_at}}</td>

    <td><!--<a href="#" @click="showEditModal=true; SelectMenu(l.id)"><img src="{{asset('img/edit.png')}}" width="20px"/></a>
    |-->
    <a href="#" @click="Deletelectures(l.id)" ><img src="{{asset('img/delete.png')}}" width="20px"/></a>
    </td>


     </tr>



   </table>
               </div>




                            </div>

                        </div>
                    </div>



     <!--  Large Modal -->
     <div class="modal fade bd-example-modal-lg mt-5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">

                            <span>  <i class="fa fa-upload fa-1x" aria-hidden="true"></i></span> <h5 class="modal-title ml-2" id="exampleModalCenterTitle" style="margin-left:-10px">Create Lecture</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                            <div class="container ml-2" style="color:#333">See a course as a book, a section as chapters in a book and lectures as contents grouped by chapter <br/>(section in this case). Please select the section to which this lecture belongs.</div>
                            <div class="d-flex justify-content-center">


                            </div>
                            <form enctype="multipart/form-data" id="menuform">
                            <div class="row" style="margin:25px">

<div class="col-md-6" >
    <div class="form-group">


    </div>
</div>

<div class="col-md-6">
    <div class="form-group">

    </div>
</div>


</div>

<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">
        <label>Lecture Title</label>
<input type="text" class="form-control"  placeholder="Example: Business model for Agric business" v-model="form.lecture_title" style="height:50px">

</div>
</div>

</div>


<div class="row" style="margin:25px; margin-top:-30px">

<div class="col-md-6" >
    <div class="form-group">
        <label>Course Section</label>
        <select name="course_section" id="section" class="form-control text-small" v-model='form.course_section'>
                                            <option value="0" selected="" disabled="">Select course section</option>
                        <option v-for='s in sections' :value='s.id'>@{{ s.section_title }}</option>
                                            </select>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label>Lecture Format (Video or PDF for now)</label>
        <select class="form-control" v-model="form.lecture_format" name="lecture_format" @change="CheckFormat()">
                                                <option value="0">Select course type</option>
                                                <option value="video">Video</option>
                                                <option value="pdf">PDF</option>

                                            </select>
    </div>
</div>


</div>

<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group" v-show="is_pdf">
        <label>File</label>
<input type="file" name="filename" id="inputFileUpload"
v-on:change="onFileChange" v-model="form.file_url">

</div>

<div class="form-group" v-show="is_video">
<label>Paste YouTube Video URL</label>
<input type="text" class="form-control"  placeholder="Paste YouTube video URL" v-model="form.youtube_url" style="height:50px">


</div>

<img src="{{asset('img/loading.gif')}}" width="50px" v-show="loader"/><span v-show="loader">uploading lecture please wait...</span>



<div class="alert alert-success alert-dismissible fade show" role="alert"  v-show="success" style="height:40px">

<p v-show="success" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>

<div class="alert alert-danger alert-dismissible fade show" role="alert"  v-show="error" style="height:40px">

<p v-show="error" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>



</div>



</div>


</div>
                                <button class="btn" type="button" style="background-color:#02b159; color:#fff; cursor:pointer"  @click.prevent() = "postlecture" id="btnUpload">Submit</button>
                            </div>

                            </form>
                        </div>
                    </div>



                    <!-- EDIT-->
                    <!--  Large Modal -->
     <!--<div id="overlay" class="overlay" v-if="showEditModal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">

                                <h5 class="modal-title ml-5" id="exampleModalCenterTitle">Edit Menu</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.prevent="showEditModal=false"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">

                            <div class="d-flex justify-content-center">


                            <img src="{{asset('images/loading.gif')}}" width="50px" v-show="loader"/>

                            </div>
                            <form  enctype="multipart/form-data" id="menuform">
                            <div class="row" style="margin:25px">

<div class="col-md-6" >
    <div class="form-group">
        <label>Select Restaurant</label>
        <select name="restaurant_id" id="restaurant_id" class="form-control text-small" v-model='MenuObject.restaurant_id'>
        <option value="0" selected="" disabled="">Select Restaurant</option>
                        <option v-for='r in restaurants' :value='r.id'>@{{ r.restaurant_name }}</option>
                                            </select>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label>Select Category</label>
        <select name="category_id" id="category_id" class="form-control text-small" v-model='MenuObject.category_id'>
                                            <option value="0" selected="" disabled="">Select Category</option>
                        <option v-for='c in categories' :value='c.id'>@{{ c.category_name }}</option>
                                            </select>
    </div>
</div>


</div>

<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">
        <label>Sub Heading Title</label>
<input type="text" class="form-control"  placeholder="Enter sub heading title" v-model="MenuObject.sub_heading_title" style="height:50px">

</div>
</div>

</div>


<div class="row" style="margin:25px; margin-top:-30px">

<div class="col-md-6" >
    <div class="form-group">
        <label>Item Name</label>
        <input type="text" class="form-control text-small"  placeholder="Enter item name" v-model="MenuObject.food_name">
        <input type="hidden" class="form-control text-small"  v-model="MenuObject.id">
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label>Price (per item)</label>
        <input type="number" class="form-control text-small"  placeholder="Enter price" v-model="MenuObject.price">
    </div>
</div>


</div>


<div class="row" style="margin:25px; margin-top:-30px">

<div class="col-md-6" >
    <div class="form-group">
        <label>Quantity</label>
        <input type="number" class="form-control text-small"  placeholder="Enter quantity" v-model="MenuObject.quantity">
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label>Menu Type</label>
        <select class="form-control" v-model="MenuObject.menu_type" name="menu_type">

                                                <option value="Breakfast">Breakfast</option>
                                                <option value="All day">All day</option>

                                            </select>
    </div>
</div>


</div>


<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">
        <label>Description (Optional)</label>
<input type="text" class="form-control"  placeholder="Enter description" v-model="MenuObject.description" style="height:50px">

</div>
</div>

</div>



<button class="btn btn-primary ml-5" type="button" style="cursor:pointer" @click.prevent() = "updateMenu" style="margin-top:-30px">Save Changes</button>
</div>

                            </div>

                            </form>
                        </div>
                    </div>-->



                </div>





</div>



</div>




</div>

</template>
</div>

      <script>

var vm = new Vue({

    el:"#app2",
    data:{

    programs:[],
    file:'',
    loader:false,
    success:false,
    error:false,
    message:'',
    image: '',

    courses:[],
    sections:[],
    lectures:[],

    showEditModal:false,

    form:{'id':'', 'lecture_format':0, 'course_id':{{$courseObject->id}}, 'section':0, 'course_section':0, 'lecture_title':'', 'file_url':'', 'youtube_url':''},



    showAddCourseImageModal:true
    //showEditCurrencyModal:false,

    },
    methods:{
        onFileChange(e)
        {
         this.file = e.target.files[0];

        },

        Deletelectures:function(id)
        {

            var c = confirm("Are you sure you want to delete this record ?");
            if(c)
            {

                this.form.id = id;
                var _this = this;
                var input = this.form;

                axios.post('../Deletelectures', input).then(function(response){
                    alert('Lecture deleted successfully');
                    _this.loadlectures();

                }).catch(function(error){
                    alert('There was error deleting record. Please try again');

                })

        }

        },




        createcourse:function()
        {

            var _this = this;
            var input = this.form;
            document.getElementById('btnAdd').disabled = true;
            axios.post('/c', input).then(function(response){
                alert('Approval created successfully');

                document.getElementById('btnAdd').disabled = false
                _this.form = {'title':'','program':'', 'privacy':'', 'price':'', 'promo_price':'', 'course_type':'', 'course_requirements':'', 'course_objectives':'', 'course_brief_description':'', 'course_full_description':''};
            }).catch(function(error){
                alert('There was error creating record. Please try again');
                document.getElementById('btnAdd').disabled = false;
            })

        },


                loadprograms:function()
                   {

                     var _this = this;

                     axios.get('loadprograms').then(function(response){
                    _this.programs = response.data;

                    console.log(_this.programs)

                  });

           },

           AddImage:function(id)
           {
               this.form.course_id = id;
           },

           loadlectures:function()
                   {

                    var _this = this;
                    var input = this.form;

                    axios.post('../load_lectures', input).then(function(response){
                    _this.lectures = response.data;




                  });

         },

         loadcourses:function()
                   {

                     var _this = this;

                     axios.get('../load_courses').then(function(response){
                    _this.courses = response.data;

                  });


                   },

                   loadsections:function()
                   {

                     var _this = this;
                     var input =  this.form;



                     axios.post('../load_sections', input).then(function(response){
                    _this.sections = response.data;



                  });


                   },

                   loadlecturesbysectionid:function()
                    {

		            	var _this = this;
                        var input =  this.form;

                        axios.post('../loadlecturesbysectionid', input).then(function(response){
                       _this.lectures = response.data;

                    });

                    },




                    postlecture:function()
                 {




                    var  _this = this;

                    if(this.form.lecture_title == "")
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please enter lecture title";
                        return;
                    }
                    else if(this.form.course_section == 0)
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please select course section";
                        return;
                    }
                    else if(this.form.lecture_format == 0)
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please select lecture format - pdf or video";
                        return;
                    }

                    else
                    {


                var _this = this;
                let currentObj = this;

                let formData = new FormData();
                formData.append('file', this.file);
                formData.append('course_id', this.form.course_id);
                formData.append('course_section', this.form.course_section);
                formData.append('lecture_title', this.form.lecture_title);
                formData.append('lecture_format', this.form.lecture_format);

                formData.append('youtube_url', this.form.youtube_url);

               // formData.append('restaurant_id', this.RestaurantObject.id);

             document.getElementById('btnUpload').disabled = true
            _this.loader = true;
            _this.error = false;
                _this.success = false;
            axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
         //   axios.defaults.xsrfCookieName = 'csrftoken'
          //  axios.defaults.xsrfHeaderName = 'X-CSRFToken'

            //'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            axios.post('../postlecture', formData).then(function(response){

                location.reload();

                _this.error = false;
                _this.success = true;
               // _this.message = "Lecture uploaded successfully";
                 _this.message = response.data;
                _this.form = {'lecture_format':0, 'course_id':{{$courseObject->id}}, 'section':0, 'course_section':0, 'lecture_title':'', 'file_url':'', 'youtube_url':''},

                _this.loader = false;
                document.getElementById('btnUpload').disabled = false

            }).catch(function(error){
                document.getElementById('btnUpload').disabled = false

                _this.success = false;
                _this.loader = false;
                _this.error = true;
                _this.message = error;


             /*   _this.$Notice.error({
                title: 'Error',

              desc: currentObj.output = error,

                duration: 7,

            });*/

                document.getElementById('btnUpload').disabled = false;

            })


        }



                 },


                 CheckFormat:function()
                 {
                     if(this.form.lecture_format =="video")
                     {
                         this.is_pdf = false;
                         this.is_video = true;
                     }
                     else if(this.form.lecture_format =="pdf")
                     {

                         this.is_video = false;
                         this.is_pdf = true;
                     }
                     else
                     {
                        this.is_video = false;
                         this.is_pdf = false;
                         alert('Please select either video or pdf');
                     }
                 }


    },

    created:function()
    {

        this.loadsections();
        this.loadlectures();

        //this.form.course_id = {{$courseObject->id}}
    }

})

</script>


@endsection
