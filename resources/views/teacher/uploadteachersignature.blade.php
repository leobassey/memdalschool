@extends('layouts.Teacherlayout')
@section('title', 'Upload Signature')

@section('content')

<style>
	.btn-danger-outline, .btn-primary-outline, .btn-infoz {
	background-color: #069 !important;
    color: #fff !important;
	border-color: #069 !important;
}

.btn-danger-outline:hover, .btn-primary-outline:hover, .btn-infoz:hover {
	background-color: #0481c0 !important;
	}

	b {
color: #069;
	}

	fieldset:nth-child(1) {
		border: none;
	}

	fieldset:nth-child(2) {
		display: none;
	}
</style>

<div class="app-content container center-layout mt-2">
<section id="configuration">
<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header d-flex justify-content-center">
					<h4 class="">Upload Signature</h4>
					<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

				</div>
				<div class="card-content collapse show">
					<div class="card-body">

                    @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif
                        @if (session('error'))
                            <div class="alert alert-danger mb-3">
                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif
                                                <form class=""  action="{{route('postteachersignature')}}" enctype="multipart/form-data" method="POST" style="margin-top:-25px">
                                                @csrf
                                                <div class="row justify-content-md-center">

                              <div class="col-md-6">
                                  <div class="form-body">




                                      <div class="form-group">
                                          <label for="eventInput1">Signature</label>


                                          <input type="file" class="form-control" id="eventInput1"
                                             placeholder="file" name="file" required value="{{ old('file') }}">
                                      </div>



                                  </div>
                              </div>
                          </div>

                          <div class="form-actions center">


                          <button type="submit" class="btn btn-info mg-r-5 ml-3" style="cursor:pointer; background-color:#02b159"> <i class="la la-save"></i> Submit</button>
                <a class="btn btn-secondary" href="{{route('teacherdashboard')}}">Cancel</a>


                             <!-- <button type="submit" class="btn btn-infoz col-md-3">
                                 Upload
                              </button>


                              <a  href="{{route('admindashboard')}}" class="btn mr-1 col-md-3" style="background-color: orange; color: #fff; border-color: orange;">
                                 << Back</a>-->
                          </div>
                      </form>

                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


@endsection
