@extends('layouts.teacherlayout')
@section('title', 'Course settings')

<style>
.user-tab {
    position: relative;
    cursor: pointer;
    display: block;
    border-radius: 8px;
    padding: 15px;
    margin: -5px;
    margin-bottom: 20px;
    height: 150px;
    /* background: #2980AC; */
    background: #d9dfe2;
}

.user-tab div {
    position: absolute;
    bottom: 10px;
    left: 0;
    width: 100%;
    padding: 5px;
    background-color: #fff;
    text-align: center;
}

h2 {
    font-size: 16px;
    line-height: 16px;
    font-weight: 400;
}

h1 {
    font-size: 24px;
    line-height: 24px;
    font-weight: 400;
}
#overlay
		{
			position: fixed;
			top: 0;
			bottom: 0;
			left: 130;
			right: 0;
			background:rgba(0,0,0,0, 0.6);
        }


        .choose::-webkit-file-upload-button {
  color: white;
  display: inline-block;
  background: #1CB6E0;
  border: none;
  padding: 7px 15px;
  font-weight: 700;
  border-radius: 3px;
  white-space: nowrap;
  cursor: pointer;
  font-size: 10pt;
}


</style>

@section('content')



    <div class="content-wrapper" style="margin-top:-25px">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic">INSTRUCTION</h4>
                    </div>
                    <div class="card-content collapse show" style="padding: 20px; margin-top:-25px">
                        <div class="card-content">

                        <p>The settings below are for the course <b>{{$courseObject->course_title}}</b>. Add settings to the course by clicking on the setting icon.</p>
                    </div>
                    </div>

                </div>
            </div>
        </div>
</div>


        <div class="content-wrapper" style="margin-top:-30px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Course settings for: {{$courseObject->course_title}}</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="" href="{{route('createteachercourse')}}">+ Create new course</a></li>
                                        <li><a data-action="" href="{{route('teachercourses')}}">Course lists</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div id="app2">
                                    <template>

                                    <div class="card pd-20 pd-sm-40">

<div class = "row mt-2">



<div class="col-md-2 col-sm-12 form-group-sm">
<a  class="user-tab center text-center" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" @click="AddImage({{$courseObject->id}})">

<i class="fa fa-ioxhost fa-4x"></i>
<div>
  <small>Add Course Image</small>
</div>
</a>
</div>

<div class="col-md-2 col-sm-12 form-group-sm">
<a id="ContentPlaceHolder1_lbCurrency" class="user-tab center text-center" href="{{route('manageteachersections', $courseObject->id)}}">
<i class="fa fa-money fa-4x"></i>
<div>
  <small>Create course sections</small>
</div>
</a>
</div>

<div class="col-md-2 col-sm-12 form-group-sm">
<a class="user-tab center text-center" href="{{route('manageteacherlectures', $courseObject->id)}}" >
<i class="fa fa-adjust fa-5x"></i>
<div>
  <small>Add Lectures</small>
</div>
</a>
</div>


<!--<div class="col-md-2 col-sm-12 form-group-sm">
<a id="ContentPlaceHolder1_lbCurrency" class="user-tab center text-center" href="">
<i class="fa fa-money fa-4x"></i>
<div>
  <small>Course Prerequisites</small>
</div>
</a>
</div>-->
<div class="col-md-2 col-sm-12 form-group-sm">
<a id="ContentPlaceHolder1_lbPackaging" class="user-tab center text-center" href="{{route('createteacherexam', $courseObject->id)}}">
<i class="fa fa-paperclip fa-4x"></i>
<div>
  <small>Add Exam</small>
</div>
</a>
</div>




<div class="col-md-2 col-sm-12 form-group-sm">
<a id="ContentPlaceHolder1_lbUserRole" class="user-tab center text-center" href="{{route('teachercourseassignment', $courseObject->id)}}">
<i class="fa fa-users fa-4x"></i>
<div>
  <small>Add Assigment</small>
</div>
</a>
</div>


<div class="col-md-2 col-sm-12 form-group-sm">
                  <a id="ContentPlaceHolder1_lbDemurrageStatus" class="user-tab center text-center" href="">
                      <i class="fa fa-users fa-4x"></i>
                      <div>
                          <small>Enrollments: {{$teacherCourseCount}}</small>
                      </div>
                  </a>
              </div>



</div><!-- end of row div-->


<div class="row">

<!--<div class="col-md-2 col-sm-12 form-group-sm">
<a id="ContentPlaceHolder1_lbParcelNumber" class="user-tab center text-center" href="">
<i class="fa fa-cubes fa-4x"></i>
<div>
  <small>Course Prerequisites</small>
</div>
</a>
</div>-->

              <!--<div class="col-md-2 col-sm-12 form-group-sm">
                  <a id="ContentPlaceHolder1_lbBasisOfSale" class="user-tab center text-center" href="">
                      <i class="fa fa-users fa-4x"></i>
                      <div>
                          <small>Add Students to course</small>
                      </div>
                  </a>
              </div>-->
              <div class="col-md-2 col-sm-12 form-group-sm">
                  <a id="ContentPlaceHolder1_lbMethodOfPayment" class="user-tab center text-center" href="">
                      <i class="fa fa-credit-card fa-4x"></i>
                      <div>
                          <small>Course Status:
                          @if($courseObject->course_status == 0)
                         Inactive
                         @else
                         Active
                         @endif

                          </small>
                      </div>
                  </a>
              </div>
              <div class="col-md-2 col-sm-12 form-group-sm">
                  <a id="ContentPlaceHolder1_lbApprovalStatus" class="user-tab center text-center" href="">
                      <i class="fa fa-check-square fa-4x"></i>
                      <div>
                          <small>Course Type:
                          @if($courseObject->course_type == 1)
                         Online
                         @elseif($courseObject->course_type == 2)
                         Campus
                         @else
                         Free
                         @endif
                          </small>
                      </div>
                  </a>
              </div>

             <!-- <div class="col-md-2 col-sm-12 form-group-sm">
                  <a id="ContentPlaceHolder1_lbApprovalStatus" class="user-tab center text-center" href="">
                      <i class="fa fa-check-square fa-4x"></i>
                      <div>
                          <small>Remove Course</small>
                      </div>
                  </a>
              </div>-->

            <!--  <div class="col-md-2 col-sm-12 form-group-sm">
                  <a id="ContentPlaceHolder1_lbApprovalStatus" class="user-tab center text-center" href="">
                      <i class="fa fa-check-square fa-4x"></i>
                      <div>
                          <small>Course Prerequisites</small>
                      </div>
                  </a>
              </div>-->

          </div>
          </div>

  </div> <!-- end of second row div-->




  <!-- Add Crude type Modal starts  -->
  <!--
<div id="overlay" class="overlay" v-if="showAddCourseImageModal" style="margin-top: 55px;">
<div class="modal-dialog">
<div class="" role="document">
<div class="modal-content" style="">
  <div class="modal-header" style="background-color:#fff; border:solid 1px #f4f5fa; border-radius:3px">
      <h5 class="modal-title ml-2" style="color:blue; font-size:bold; font-size:18px"><b>Create Currency</b></h5>
      <button type= "button" class="close" @click="showAddCourseImageModal=false">
          <span aria-hidden= "true">&times;</span>
      </button>

  </div>

  <div class="modal-body p-4" style="background-color:#f4f5fa;">




<fieldset>
                                      <div class="row" style="margin-top:-20px">

                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Acronym</label>
                                                  <input type="text" class="form-control text-small"  placeholder="Acronym" v-model="form.acronym">
                                              </div>
                                          </div>

                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Name</label>
                                                  <input type="text" class="form-control text-small"  placeholder="Name" v-model="form.name">
                                              </div>
                                          </div>








                                      </div>



                                  </fieldset>

<fieldset>






<div class="row" style="margin-top: -11px;">

</div>

                                  </fieldset>

                                  <hr class="mt-2"/>

          <div class="form-group d-flex justify-content-center">
              <button class="btn btn-info"  style="background-color: #84C225; border-color: #84C225;" id="btnAdd">Save Currency</button>
              <button class="btn  ml-2" @click="showAddCourseImageModal=false;" style="background-color: orange; border-color: orange; color:#fff"> x Cancel</button>
          </div>
      </form>
  </div>
</div>
</div>
</div>
</div>-->
<!-- Modal ends -->
</div>


























</div>







<!--  Large Modal -->
<div class="modal fade bd-example-modal-lg mt-5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
          <div class="modal-dialog modal-lg" style="width:700px">
              <div class="modal-content">
                  <div class="modal-header">
                  <i class="fa fa-upload fa-1x" aria-hidden="true"></i>
                      <h6 class="modal-title ml-1" id="exampleModalCenterTitle"> Add Course Cover Image</h6>
                      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">


                  <div class="row" style="margin:25px">



</div>

<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">
<label style="color:#333">Upload quality course cover image here. Important guides: 500 width, 260 height in pixels. accepted format; .jpg, .jpeg, .png or .gif extension only </label>
</div>
</div>

</div>


<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">

<div class="row">
<!--<div class="col-md-4"> <img :src="image" class="img-responsive" width="150px"></div>-->

<div class="col-md-8">
<input type="file" name="filename" id="inputFileUpload"
v-on:change="onFileChange" v-model="form.coverimage" class="choose">

<input type="hidden" name="course_id" v-model="form.course_id">

</div>
</div>


<br/>
<button class="btn btn-primary mt-3" type="button" style="cursor:pointer; border-radius:4px; background-color:#333" @click.prevent() ="uploadimage" id="btnUpload">Upload</button>

</div>

<img src="{{asset('img/loading.gif')}}" width="50px" v-show="loader"/><span v-show="loader">loading image please wait...</span>

<p v-show="success" class="lead">@{{message}}</p>
<p v-show="error" class="lead">@{{message}}</p>
</div>

</div>





</div>
                      <div class="btn btn-primary" style="background-color:#02b159; border:solid 1px #02b159; border-radius:none"></div>
                  </div>

                  </form>
              </div>
          </div>







</template>
</div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>

      <script>

var vm = new Vue({

    el:"#app2",
    data:{

    programs:[],
    file:'',
    loader:false,
    success:false,
    error:false,
    message:'',
    image: '',

    form:{'title':'','program':'', 'privacy':'', 'price':'', 'promo_price':'', 'course_type':'', 'course_requirements':'', 'course_objectives':'', 'course_brief_description':'', 'course_full_description':'', 'coverimage':'', 'course_id':''},



    showAddCourseImageModal:true
    //showEditCurrencyModal:false,

    },
    methods:{
        onFileChange(e){
                            this.file = e.target.files[0];

                            },


                        /*    onFileChange(e) {
                let files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                this.createImage(files[0]);
            },*/
            createImage(file) {
                let reader = new FileReader();
                let vm = this;
                reader.onload = (e) => {
                    vm.image = e.target.result;
                };
                reader.readAsDataURL(file);
            },
       /* deleteApproval:function(id)
        {

            var c = confirm("Are you sure you want to delete this record ?");
            if(c)
            {

                this.form.id = id;
                var _this = this;
                var input = this.form;

                axios.post('DeleteApproval', input).then(function(response){
                    alert('Approval deleted successfully');
                    _this.loadApproval();

                }).catch(function(error){
                    alert('There was error deleting approval. Please try again');

                })

        }

        },*/


       /* selectApproval:function(id)
        {

                var _this = this;
				this.ApprovalObject.id =  id;
				var input = this.ApprovalObject;

            axios.post('getApproval', input).then(function(response){
                _this.ApprovalObject = response.data;

            }).catch(function(error){
                alert('There was error getting this record. Please try again');

            })

        },*/



        createcourse:function()
        {

            var _this = this;
            var input = this.form;
            document.getElementById('btnAdd').disabled = true;
            axios.post('/c', input).then(function(response){
                alert('Approval created successfully');

                document.getElementById('btnAdd').disabled = false
                _this.form = {'title':'','program':'', 'privacy':'', 'price':'', 'promo_price':'', 'course_type':'', 'course_requirements':'', 'course_objectives':'', 'course_brief_description':'', 'course_full_description':''};
            }).catch(function(error){
                alert('There was error creating record. Please try again');
                document.getElementById('btnAdd').disabled = false;
            })

        },

       /* updateApproval:function()
        {

            var _this = this;
            var input = this.ApprovalObject;
            document.getElementById('btnUpdate').disabled = true;
            axios.post('UpdateApproval', input).then(function(response){

                alert('Changes saved successfully');
                _this.loadApproval();
                document.getElementById('btnUpdate').disabled = false
                _this.showEditApprovalModal = false;
            }).catch(function(error){
                alert('There was error updating approval. Please try again');
                document.getElementById('btnUpdate').disabled = false;
            })

        },*/

                loadprograms:function()
                   {

                     var _this = this;

                     axios.get('loadprograms').then(function(response){
                    _this.programs = response.data;

                    console.log(_this.programs)

                  });

           },

           AddImage:function(id)
           {
               this.form.course_id = id;
           },




           uploadimage:function()
                 {


                    var  _this = this;
                   /* if(this.form.coverimage == "")
                    {

                        _this.error = true;
                        _this.message = "Please select a cover image to upload";


                    return;

                    }
                    else
                    {*/


                var _this = this;
                let currentObj = this;

                let formData = new FormData();
                formData.append('file', this.file);
                formData.append('coverimage', document.getElementById("coverimage"));
                formData.append('course_id', this.form.course_id);



               // formData.append('restaurant_id', this.RestaurantObject.id);



             document.getElementById('btnUpload').disabled = true
            _this.loader = true;
            axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
            axios.post('../postimage', formData).then(function(response){

                _this.error = false;
                _this.success = true;
                _this.message = "Cover Image added successfully";
                _this.form = {'cover_image':''};

             /*  _this.$Notice.success({
                title: 'success',
                title: 'Cover Image added successfully',
                duration: 10,

            });*/


                _this.loader = false;
                document.getElementById('btnUpload').disabled = false

            }).catch(function(error){
                document.getElementById('btnUpload').disabled = false

                _this.success = false;
                _this.loader = false;
                _this.error = true;
                _this.message = error;


             /*   _this.$Notice.error({
                title: 'Error',

              desc: currentObj.output = error,

                duration: 7,

            });*/

                document.getElementById('btnUpload').disabled = false;

            })


        //}



                 },



    },

    created:function()
    {

        this.loadprograms();
    }

})

</script>


@endsection
