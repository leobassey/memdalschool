@extends('layouts.weblayouts')

@section('title', 'Online Courses')

@section('content')


<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url(assets/img/breadcrumb-bg.jpg) no-repeat">

            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mr-auto text-white my-3" style="margin-bottom: 0px !important;">
                        <h1 style="font-weight: bolder; ">
                           Online Courses
                        </h1>

                        <p>Join our small batch size and learn from experienced and professional trainers who are also pracadmics</p>

                    </div>
                    <div class="col-lg-6 col-md-5 my-3">
                        <div class="owl-carousel dots-white-outline" data-dots="true" data-smartspeed="300" data-items-tablet="3" data-items-mobile="2" data-space="15">
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/1.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/2.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/3.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/4.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/5.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END row-->
            </div>
        </div>
        <!-- END container-->
    </div>






    <section class="padding-y-100" style="background-color: #f8f9fa! important;">
        <div class="container">
            <div class="row">


                <!--<div class="col-12" style="margin-top:-60px">
                    <div class="d-md-flex justify-content-between bg-white rounded shadow-v1 p-4">
                        <ul class="nav nav-pills nav-isotop-filter align-items-center my-2">

                            <a class="nav-link " href="online-courses.html" data-filter=".free-courses">Online Courses</a>
                            <a class="nav-link active" href="classroom-courses.html" data-filter=".start-soon" style="background-color: #bb2129;">In-Person Courses</a>
                            <a class="nav-link" href="topic-courses.html" data-filter=".free-courses">Topic-Based Courses</a>

                            <a class="nav-link" href="#" data-filter=".free-courses">Career Courses</a>
                            <a class="nav-link" href="#" data-filter=".free-courses">Free Courses</a>
                        </ul>

                    </div>
                </div>-->

                <!-- END row-->
                @foreach($courses as $c)
               <!--<div class="line col-md-6 col-sm-4 d-flex flex-row bd-highlight" style="margin-top:-30px">
                    <h3 class="bd-highlight">{{$c->program_name}} Courses</h3>
                </div>
                <div class="line col-md-6 col-sm-8 d-flex flex-row-reverse bd-highlight " style="margin-top:-30px">
                    <h5 class="bd-highlight">5 Courses</h5>
                </div>
                <hr style="margin-top:-6px">
                <div class="col-md-12" style="margin-top:-10px">
                    <p>
                        Analize business data to gain insight and enhance value chain and operations </p>
                </div>-->
                <div class="col-lg-4 mt-1">



                    <div class="card">
                        <img class="rounded" src="assets/img/Business-Financing.jpg" alt="">
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                               {{$c->course_title}}
                            </a>

                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>


                            </p>


                            <div class="card-text d-flex bd-highlight mt-2">
                                <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                <i class="fas fa-stopwatch"></i>  Program: {{$c->program_name}}</p>


                            </div>
                            <span class="" style="font-weight: bold; font-size:18px">N{{$c->course_price}}</span>
<br/>

                            <a href="{{route('onlinecoursedetails', $c->id)}}" class="btn mt-2" style="background-color:orange; color:#fff">View course details</a>
                        </div>


                    </div>
                </div>

@endforeach
            </div>




           <!-- <div class="container mt-3">
                <div class="row">


                    <div class="line col-md-6 col-sm-4 d-flex flex-row bd-highlight mt-3">
                        <h3 class="bd-highlight">Web Development Courses</h3>
                    </div>
                    <div class="line col-md-6 col-sm-8 d-flex flex-row-reverse bd-highlight mt-3">
                        <h5 class="bd-highlight">5 Courses</h5>
                    </div>
                    <hr style="margin-top:-6px">
                    <div class="col-md-12" style="margin-top:-10px">
                        <p>
                            Analize business data to gain insight and enhance value chain and operations</p>
                    </div>
                    <div class="col-lg-4 mt-3">



                        <div class="card">
                            <img class="rounded" src="../assets/img/Business-Financing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                        HTML, CSS and Javascript
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>


                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N15,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="../assets/img/strategic-management-and-operations.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                        Bootstrap 4+ Framework
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>


                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold;margin-left:-40px">N10,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                       Core PHP Programming
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N15,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>
                    </div>




                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                        App Development with Laravel 8 Framework
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N30,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                        Laravel API & Microservices Development
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N20,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                       Core Vue js Frontend App Development
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N30,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>

                    </div>





                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                      UI Design with Adobe XD
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N30,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>

                    </div>




                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                       Version control management with github/bitbucket
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 4hrs / classroom-based</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N10,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>

                    </div>




                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                    Full-Stack Development
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 16days / 80hrs</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N70,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>

                    </div>



                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                    Front-End Development
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 16days / 80hrs</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N70,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>

                    </div>



                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                    Back-End Development
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 16days / 80hrs</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N70,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>

                    </div>


                    <div class="col-lg-4 mt-3">
                        <div class="card">
                            <img class="rounded" src="assets/img/marketing.jpg" alt="">
                            <div class="card-body px-0 pl-3">

                                <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                   Web Development
                                    </a>

                                <p class="mb-0">
                                    <span class="text-dark ml-1">5.8</span>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>
                                    <i class="fas fa-star text-warning"></i>



                                </p>


                                <div class="card-text d-flex bd-highlight mt-2">
                                    <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                        <i class="fas fa-stopwatch"></i> 16days / 80hrs</p>
                                    <h4 class="h5 text-right ml-5">
                                        <span class="" style="font-weight: bold; margin-left:-40px">N70,000</span>

                                    </h4>

                                </div>


                            </div>
                        </div>

                    </div>-->






                </div>




    </section>


@endsection
