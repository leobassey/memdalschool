@extends('layouts.weblayouts')
@section('title', 'Free Courses')


@section('content')

<style>

.card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: cover;
}

</style>


<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url(assets/img/1920/background.jpg) no-repeat">

            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mx-auto text-white my-5 text-center" style="margin-bottom: 0px !important;">
                        <h1 style="font-weight: bolder; font-size: 35px;">
                          Free courses
                        </h1>

                        <p style="font-size: 19px;">Learn relavance skills in Agriculture with our free courses taught by experts in the field </p>

                    </div>
                    <div class="col-lg-6 col-md-5 my-3">
                        <div class="owl-carousel dots-white-outline" data-dots="true" data-smartspeed="300" data-items-tablet="3" data-items-mobile="2" data-space="15">
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/1.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/2.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/3.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/4.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/5.png" alt="">
                            </div>
                            <div class="card height-6rem justify-content-center p-4">
                                <img src="assets/img/logos/1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END row-->
            </div>
        </div>
        <!-- END container-->
    </div>




<?php foreach ($programArray as $key) {?>
    <section class="padding-y-100" style="background-color: #f8f9fa! important;">


        <div class="container" style="max-width:1140px">


            <div class="row">

                <div class="line col-md-6 col-sm-4 d-flex flex-row bd-highlight" style="margin-top:-30px">
                    <h3 class="bd-highlight"><?php echo $key["name"] ?></h3>
                </div>
                <div class="line col-md-6 col-sm-8 d-flex flex-row-reverse bd-highlight " style="margin-top:-30px">
                    <h5 class="bd-highlight"><?php echo count($key["courses"]) ?> Course(s)</h5>
                </div>
                <hr style="margin-top:-6px">

              <?php foreach ($key["courses"] as $one) {?>
                <div class="col-lg-4 mt-1">
                    <div class="card">
                    @if($one['image_url'] == "")
                        <img class="rounded card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
                        @else
							<img class="rounded card-img-top" src="{{$one['image_url']}}" alt="course image"/>
                        @endif
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 18px; font-weight: bold;">
                                <?php echo $one["course_title"] ?>
                            </a>
                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                            </p>
                            <div class="card-text d-flex bd-highlight mt-2">
                            <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($one['course_type']  == 1)
                                    Type: Online
                                    @elseif($one['course_type']  == 2)
                                    Type: Classroom-based
                                    @else
                                    Type: Free course
                                    @endif
                                     </p>
                              <!-- <h4 class="h5 text-right ml-5">
                                    <span class="" style="font-weight: bold; margin-left:-10px">N<?php echo $one["course_price"] ?></span>
                                </h4>-->
                            </div>
                            <a href="{{route('freecoursedetails', $one["id"]) }}" class="btn btn-primary mt-3" style="color:#fff">View course details</a>
                        </div>
                    </div>
                </div>
              <?php } ?>



            </div>


        </div>


    <br><br>
    </section>
<?php } ?>




@endsection
