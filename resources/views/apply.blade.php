@extends('layouts.weblayouts')
@section('title', 'Course Details')


@section('content')



<div class="jumbotron" style="border-radius: 0px; height: 300px; background-size: cover; background-image: url({{asset('webassets/img/1920x800/jumbo.jpg')}})">
            <h2 class="text-white text-center mt-10" style="font-weight: bold;">Apply Here</h2>

        </div>
<!--
<section class="course-lists ">
            <div class="container mb-5">


            <div class="container space-2">


            <div class="row">
<div class="col-md-10"> @foreach($errors->all() as $error)
<p class="alert alert-danger">{{$error}}
<button type="button" class="close" data-dismiss="alert">×</button>
</p>
@endforeach

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong style="color:green"> {{ session()->get('error') }}</strong>
</div>
@endif
</div>

  <form action="{{route('postapplication')}}" method="post">
  @csrf
    <div class="row">
      <div class="col-md-6 mb-3 mb-md-5">


        <div class="js-form-message">
          <label class="input-label" for="fullNameLabel">First name</label>
          <div class="input-group">
            <input type="text" class="form-control" name="firstname" id="fullNameLabel" placeholder="First Name" aria-label="First Name" required
                   data-msg="Please enter your frist name.">
          </div>
        </div>

      </div>

      <div class="col-md-6 mb-md-5">

        <div class="js-form-message">
          <label class="input-label" for="lastNameLabel">Last name</label>
          <div class="input-group">
            <input type="text" class="form-control" name="lastname" id="lastNameLabel" placeholder="Last Name" aria-label="Last Name" required
                   data-msg="Please enter your last name.">
          </div>
        </div>

      </div>

      <div class="col-md-6 mb-3 mb-md-5" style="margin-top: -30px;">

        <div class="js-form-message">
          <label class="input-label" for="emailAddressLabel">Email</label>
          <div class="input-group">
            <input type="email" class="form-control" name="email" id="emailAddressLabel" placeholder="Email" aria-label="Email" required
                   data-msg="Please enter a valid email address.">
          </div>
        </div>

      </div>

      <div class="col-md-6 mb-3 mb-md-5" style="margin-top: -30px;">

        <div class="js-form-message">
          <label class="input-label" for="urlLabel">Phone </label>
          <div class="input-group">
            <input type="text" class="form-control" name="phone" id="urlLabel" placeholder="Enter contact number">
          </div>
        </div>

      </div>



      <div class="col-md-6 mb-3 mb-md-5" style="margin-top: -30px;">

        <div class="js-form-message">
          <label class="input-label" for="emailAddressLabel">Course</label>
          <div class="input-group">
          <select class="form-control custom-select" required data-msg="Please course" name="course">
          <option value="" selected>Select course</option>
          @foreach($courses as $c)
          <option value="{{$c->id}}">{{$c->course_title}}</option>
         @endforeach
        </select>
          </div>
        </div>

      </div>

      <div class="col-md-6 mb-3 mb-md-5" style="margin-top: -30px;">

        <div class="js-form-message">
          <label class="input-label" for="urlLabel">Experience Level </label>
          <div class="input-group">
          <select class="form-control custom-select" required data-msg="Please select your budget." name="experience">
          <option value="" selected>Select Experience</option>
          <option value="No Experience">No Experience</option>
          <option value="Biginner">Biginner</option>
          <option value="Intermediate">Intermediate</option>
        </select>
          </div>
        </div>

      </div>
    </div>


    <div class="text-center">
      <button type="submit" class="btn btn-primary transition-3d-hover" style="background-color:  rgba(254,254, 2); border-color:  rgba(254,254, 2); color:#333">Submit Application</button>
    </div>
  </form>

</div>


                    </div>



</section>

-->



<!-- Hero Section -->
<div class="container mb-5">
  <div class="row justify-content-lg-center align-items-lg-center">


    <div class="col-lg-6">
      <!-- Form -->
      <div class="col-md-12"> @foreach($errors->all() as $error)
<p class="alert alert-danger">{{$error}}
<button type="button" class="close" data-dismiss="alert">×</button>
</p>
@endforeach

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong style="color:green"> {{ session()->get('error') }}</strong>
</div>
@endif
</div>


      <form action="{{route('postapplication')}}" method="post" class="js-validate card shadow-lg">
  @csrf
        <div class="card-header bg-light text-center py-4 px-5 px-md-6" style="height: 62px;">
          <h2 class="h4 mb-0">Join our next cohort</h2>
        </div>

        <div class="card-body p-4 p-md-6">
          <div class="row">
            <div class="col-sm-6 mb-1">
              <!-- Form Group -->
              <div class="js-form-message form-group">
                <label for="firstName" class="input-label">First name</label>
                <input type="text" class="form-control" name="firstname" id="firstName" placeholder="Firstname" aria-label="Firstname" required
                       data-msg="Please enter first your name">
              </div>
              <!-- End Form Group -->
            </div>

            <div class="col-sm-6 mb-1">
              <!-- Form Group -->
              <div class="js-form-message form-group">
                <label for="lastName" class="input-label">Last name</label>
                <input type="text" class="form-control" name="lastname" id="lastName" placeholder="Lastname" aria-label="Lastname" required
                       data-msg="Please enter last your name">
              </div>
              <!-- End Form Group -->
            </div>

            <div class="col-sm-12 mb-1">
              <!-- Form Group -->
              <div class="js-form-message form-group">
                <label for="emailAddress" class="input-label">Email address</label>
                <input type="email" class="form-control" name="email" id="emailAddress" placeholder="Email" aria-label="Email" required
                       data-msg="Please enter a valid email address">
              </div>
              <!-- End Form Group -->
            </div>


            <div class="col-sm-12 mb-1">
              <!-- Form Group -->
              <div class="js-form-message form-group">
                <label for="emailAddress" class="input-label">Phone</label>
                <input type="text" class="form-control" name="phone" id="emailAddress" placeholder="Phone" aria-label="Phone" required
                       data-msg="Please enter a valid email address">
              </div>
              <!-- End Form Group -->
            </div>

            <div class="col-sm-6 mb-1">
              <!-- Form Group -->
              <div class="js-form-message form-group">
                <label for="password" class="input-label">Course</label>
                <select class="form-control custom-select" required data-msg="Please course" name="course">
          <option value="" selected>Select course</option>
          @foreach($courses as $c)
          <option value="{{$c->id}}">{{$c->course_title}}</option>
         @endforeach
        </select>
              </div>
              <!-- End Form Group -->
            </div>

            <div class="col-sm-6 mb-1">
              <!-- Form Group -->
              <div class="js-form-message form-group">
                <label for="confirmPassword" class="input-label">Experience Level</label>
                <select class="form-control custom-select" required data-msg="Please select your budget." name="experience">
          <option value="" selected>Select Experience</option>
          <option value="No Experience">No Experience</option>
          <option value="Biginner">Biginner</option>
          <option value="Intermediate">Intermediate</option>
        </select>
              </div>
              <!-- End Form Group -->
            </div>
          </div>



          <button type="submit" class="btn btn-block btn-primary transition-3d-hover" style="background-color:  rgba(254,254, 2); border-color:  rgba(254,254, 2); color:#333">Submit Application</button>

        </div>
      </form>
      <!-- End Form -->
    </div>
  </div>
</div>
<!-- End Hero Section -->




@endsection
