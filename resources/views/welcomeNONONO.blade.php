@extends('layouts.weblayouts')
@section('title', 'Memdal Digital School of Technology')
@section('content')
<style>

.card-img-top {
    width: 100%;
    height: 10vw;
    object-fit: cover;
}

</style>
<main id="content" role="main">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top:-45px">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block slideMobile w-100" src="{{asset('webassets/img/1920x800/img1.jpg')}}" alt="First slide">
                    <div class="carousel-caption d-md-block d-sm-block text-left">
                        <h1 class="text-white mb-8" style="
                    font-size: 2.50rem;
                    font-weight: 800;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: 3.55rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff;">Learn Digital Skills Faster</h1>


                        <p class="" style="font-size: 1.10rem;


                    font-style: normal;
                    font-stretch: normal;
                    line-height: 2rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff;
                    margin-top:-30px">
                            Get Trained in Digital Technologies that Reinvent Industries
                            <!--Expirical Digital Skills Training that reinvent industries-->
                        </p>

                        <a href="{{route('register')}}" class="btn btn-md headBtn mt-8" style="padding-left:40px;padding-right:40px; padding-top:10px;  padding-bottom:10px">Get Started</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block slideMobile w-100" src="{{asset('webassets/img/1920x800/img2.jpg')}}" alt="Second slide">
                    <div class="carousel-caption d-md-block d-sm-block text-left">
                        <h1 class="text-white mb-8" style="
                    font-size: 2.50rem;
                    font-weight: 800;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: 3.55rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff;">Accelerate Your Career Faster</h1>


                        <p class="" style="font-size: 1.10rem;


                    font-style: normal;
                    font-stretch: normal;
                    line-height: 2rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff;
                    margin-top:-30px
                   ">
                            Learn Data Analytics, Software Development and others from experts
                        </p>

                        <a href="{{route('register')}}" class="btn btn-md headBtn mt-8" style="padding-left:40px;padding-right:40px; padding-top:10px;  padding-bottom:10px">Get Started</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block slideMobile w-100" src="{{asset('webassets/img/1920x800/img3.jpg')}}" alt="Third slide">
                    <div class="carousel-caption d-md-block d-sm-block text-left">
                        <h1 class="text-white mb-8" style="
                    font-size: 2.50rem;
                    font-weight: 800;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: 3.55rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff;">Land a New Job Faster</h1>


                        <p class="" style="font-size: 1.10rem;


                    font-style: normal;
                    font-stretch: normal;
                    line-height: 2rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff;
                    margin-top:-30px
                   ">
                            Get expert mentorship and 6 months virtual internship
                        </p>

                        <a href="{{route('register')}}" class="btn btn-md headBtn mt-8" style="padding-left:40px;padding-right:40px; padding-top:10px;  padding-bottom:10px">Get Started</a>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- End Hero Section -->

        <section class="educators section section--accent">
            <div class="container">

                <div class="row">
                    <div class="col-md-2 col-6">
                        Hands-on Projects
                    </div>
                    <div class="col-md-2 col-6">
                        Lab Practical
                    </div>

                    <div class="col-md-2 col-6">
                        Mentorship
                    </div>

                    <div class="col-md-2 col-6">
                     Internship
                    </div>
                    <div class="col-md-2 col-6">
                        Job Preparation
                    </div>

                    <div class="col-md-2 col-6">
                        Reading Materials
                    </div>
                </div>
                <!--<div class="educators__item">

                </div>-->







                <!--<svg class="educators__logo--google" width="78" height="27" aria-label="Google" focusable="false">
            <use xlink:href="#styleable-logos.google"></use>
            </svg>-->

            </div>

        </section>


        <section class="course-lists mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="mt-5" style="font-family: AvenirNext,Helvetica,Arial,sans-serif;
                        font-weight: 600;
                        font-style: normal;
                        font-stretch: normal;
                        letter-spacing: normal; font-size: 1.857rem;
        line-height: 2.711rem; color:#1e2022;">Practical courses taught by industry experts</h3>
                        <h5 class="mb-5" style="color:#000">Our courses are designed with projects, labs and case studies</h5>
                    </div>


                    <div id="courses" class="owl-carousel owl-theme">
                    @foreach($courses as $course)
                        <div class="courses" data-aos="fade-up" data-aos-once="true" data-aos-delay="100">
                            <div class="courses-pack">

                                <div>
                                    <div class="mainOverlay">


                                        @if($course->image_url == "")
            <img class="card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image" class="img-responsive"/>
             @else
				<img class="card-img-top" src="./{{$course->image_url}}" alt="course image" class="img-responsive"/>
           @endif

                                    </div>
                                </div>
                                <div class="content">
                                    <a href="{{route('details', $course->id)}}">
                                        <h6 style="font-size: 13px; font-weight:300; color:#000">{{$course->course_title}}</h6>
                                    </a>
                                   <!-- <p>
                                        <span><i class="fas fa-chevron-right"></i></i></span> Course Certificate <br>
                                        <span><i class="fas fa-chevron-right"></i></i></span> 3 weeks<br>
                                        <span><i class="fas fa-chevron-right"></i></i></span> 100% online
                                    </p>-->
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </div>


                    <div class="col-md-12 mt-3">
                        <a href="{{route('register')}}" class="courses-all headBtn btn">All Courses</a>
                    </div>
                </div>
            </div>
        </section>



   <!-- <section class="course-lists mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mt-5" style="font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-weight: 600;
                    font-style: normal;
                    font-stretch: normal;
                    letter-spacing: normal; font-size: 1.857rem;
    line-height: 2.711rem;">Empirical training from FIDAS</h3>
                    <h5 class="mb-5" style="font-size:16px">Choose from more than 20 self-pace courses with case studies and course certificate</h5>
                </div>
                @foreach($courses as $i => $c)
                <div id="courses" class="owl-carousel owl-theme">
                    <div class="courses" data-aos="fade-up" data-aos-once="true" data-aos-delay="{{ $loop->iteration*100 }}">
                        <div class="courses-pack">

                            <div>
                                <div class="mainOverlay">

                                @if($c->image_url == "")
                        <img class="img-responsive" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
                        @else
							<img class="img-responsive" src="{{$c->image_url}}" alt="course image"/>
                        @endif


                                </div>
                            </div>
                            <div class="content">
                                <a href="{{route('onlinecoursedetails', $c->id)}}">
                                    <h6>{{$c->course_title}}</h6>
                                </a>
                                <p>
                                    <span><i class="fas fa-chevron-right"></i></i></span> Course Certificate <br>
                                    <span><i class="fas fa-chevron-right"></i></i></span> 100% online
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

                @endforeach
                <div class="col-md-12 mt-3">
                    <a href="{{route('onlinecourses')}}" class="courses-all btn-primary btn" style="color:#fff">All Courses</a>
                </div>
            </div>
        </div>
    </section>-->




    <section class="padding-y-100 ">
            <div class="container">

                <div class="row">

                    <div class="col-12  mb-2 mt-3">
                    <h4 class="" style="color:#1e2022; font-weight: 600;">
                           Why Memdal Training
                        </h4>

                    </div>

                    <div class="col-md-4 mt-2 text-center" data-aos="fade-right" data-aos-once="true">
                        <div class="card">
                            <img class="rounded" src="{{asset('webassets/img/labimg.jpg')}}" alt="">
                        </div>
                        <h5 class="my-4" style="margin-bottom: 1.14285714rem;
                        text-align: center;
                        font-size: 19px;
                        line-height: 1.2; font-family: AvenirNext,Helvetica,Arial,sans-serif;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal; color:#1e2022;">
                            Hands-On Projects & Labs
                        </h5>
                        <p style="color:#000">
                            Build more than 5 real-world projects in each course that you can showcase in your portfolio.
                        </p>
                    </div>
                    <div class="col-md-4 mt-2 text-center" data-aos="fade-up" data-aos-once="true">
                        <div class="card">
                            <img class="rounded" src="{{asset('webassets/img/climg.jpg')}}" alt="">
                        </div>
                        <h5 class="my-4" style="margin-bottom: 1.14285714rem;
                        text-align: center;
                        font-size: 19px;
                        line-height: 1.2; font-family: AvenirNext,Helvetica,Arial,sans-serif;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal; color:#1e2022;">
                            Comprehensive Curriculum
                        </h5>
                        <p style="color:#000">
                            Our curriculum is curated after years of research on the right skill needed in the industry.
                        </p>
                    </div>
                    <div class="col-md-4 mt-2 text-center" data-aos="fade-left" data-aos-once="true">
                        <div class="card">
                            <img class="rounded" src="{{asset('webassets/img/instructorimg.jpg')}}" alt="">
                        </div>
                        <h5 class="my-4" style="margin-bottom: 1.14285714rem;
                        text-align: center;
                        font-size: 19px;
                        line-height: 1.2; font-family: AvenirNext,Helvetica,Arial,sans-serif;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal; color:#1e2022;">
                            Experienced Trainer
                        </h5>
                        <p style="color:#000">
                            Our trainers are Professionals with 10+ years of experience in both training and solution implementation.
                        </p>
                    </div>
                </div>
                <!-- END row-->
            </div>
            <!-- END container-->
        </section>






        <section class="padding-y-100">


<!-- Features Section -->
<div class="container space-2">
    <!-- Title -->
    <h4 class="" style="color:#1e2022; font-weight: 600;">
       Our Courses
    </h4>
    <!-- End Title -->

    <!-- Features -->
    <div class="mb-3 mt-3">


        <div class="row mx-n2">
        @foreach($courses as $course)
            <div class="col-sm-4 px-2 mb-3 mb-sm-0 mt-1" data-aos="fade-left" data-aos-once="true" data-aos-delay="100">
                <!-- Icon Block -->
                <a href="{{route('details', $course->id)}}">
                    <div class="text-center rounded p-4 magicCategory" style="padding:42px 34px 42px 38px; box-shadow: 0 2px 18px 0 rgba(0,0,0,.08); background-color: #fff; color:#333;">

                        <h2 class="h3 mb-0" style="flex-grow: 2;
                    margin: 0 9px 0 18px;
                    font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-size: 16px;
                    font-weight: 600;


                    letter-spacing: normal;
                ">{{$course->course_title}}


                </h2>

                    </div>
                </a>
                <!-- End Icon Block -->
            </div>

            @endforeach






        </div>







    </div>
    <!-- End Features -->



</div>
</section>



    <!-- Testimonials Section-->
<div class="container">
<h4 class="" style="color:#1e2022; font-weight: 600;">
                Testimonials from our Students
            </h4>
        </div>


        <div class="bg-light rounded  container-fluid">



            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item">
                        <div class="container space-1 space-md-2">
                            <div class="card bg-transparent shadow-none">
                                <div class="row">
                                    <div class="col-lg-3 d-none d-lg-block">
                                        <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll bg-light" data-options="{direction: &quot;reverse&quot;}" style="overflow: visible;">
                                            <div data-parallaxanimation="[{property: &quot;transform&quot;, value:&quot; translate3d(0, rem,0)&quot;, initial:&quot;4&quot;, mid:&quot;0&quot;, final:&quot;-4&quot;}]">
                                                <img class="img-fluid rounded shadow-lg" src="{{asset('webassets/img/400x500/img31.jpg')}}" alt="Image Description">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="card-body h-100 rounded p-0 p-md-4">
                                            <figure class="mb-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="36" height="36" viewBox="0 0 8 8">
                          <path fill="#005eb8" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
                            C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
                            c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
                            C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z"></path>
                        </svg>
                                            </figure>

                                            <div class="row">
                                                <div class="col-lg-12 mb-3 mb-lg-0">
                                                    <div class="px-2">
                                                        <blockquote class="h3 font-weight-normal mb-4" style="color:#000">I'm absolutely happy with the training I received from Memdal, it was practical with hands-on project.</blockquote>
                                                        <div class="media">
                                                            <div class="avatar avatar-xs avatar-circle d-lg-none mr-2 mt-3">
                                                                <img class="avatar-img" src="assets/img/100x100/img19.jpg" alt="Image Description">
                                                            </div>
                                                            <div class="media-body">
                                                                <span class="text-dark font-weight-bold" style="color:#000">Divine</span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="container space-1 space-md-2">
                            <div class="card bg-transparent shadow-none">
                                <div class="row">
                                    <div class="col-lg-3 d-none d-lg-block">
                                        <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll bg-light" data-options="{direction: &quot;reverse&quot;}" style="overflow: visible;">
                                            <div data-parallaxanimation="[{property: &quot;transform&quot;, value:&quot; translate3d(0,rem,0)&quot;, initial:&quot;4&quot;, mid:&quot;0&quot;, final:&quot;-4&quot;}]">
                                                <img class="img-fluid rounded shadow-lg" src="{{asset('webassets/img/400x500/img31.jpg')}}" alt="Image Description">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="card-body h-100 rounded p-0 p-md-4">
                                            <figure class="mb-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="36" height="36" viewBox="0 0 8 8">
                          <path fill="#005eb8" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
                            C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
                            c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
                            C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z"></path>
                        </svg>
                                            </figure>

                                            <div class="row">
                                                <div class="col-lg-12 mb-3 mb-lg-0">
                                                    <div class="px-2">
                                                        <blockquote class="h3 font-weight-normal mb-4" style="color:#000">I'm absolutely happy with the training I received from Memdal, it was practical with hands-on project.</blockquote>
                                                        <div class="media">
                                                            <div class="avatar avatar-xs avatar-circle d-lg-none mr-2">
                                                                <img class="avatar-img" src="assets/img/100x100/img19.jpg" alt="Image Description">
                                                            </div>
                                                            <div class="media-body">
                                                                <span class="text-dark font-weight-bold" style="color:#000">Divine</span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item active">
                        <div class="container space-1 space-md-2">
                            <div class="card bg-transparent shadow-none">
                                <div class="row">
                                    <div class="col-lg-3 d-none d-lg-block">
                                        <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll bg-light" data-options="{direction: &quot;reverse&quot;}" style="overflow: visible;">
                                            <div data-parallaxanimation="[{property: &quot;transform&quot;, value:&quot; translate3d(0,rem,0)&quot;, initial:&quot;4&quot;, mid:&quot;0&quot;, final:&quot;-4&quot;}]">
                                                <img class="img-fluid rounded shadow-lg" src="{{asset('webassets/img/400x500/img31.jpg')}}" alt="Image Description">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="card-body h-100 rounded p-0 p-md-4">
                                            <figure class="mb-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="36" height="36" viewBox="0 0 8 8">
                                            <path fill="#005eb8" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
                                                C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
                                                c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
                                                C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z"></path>
                                        </svg>
                                            </figure>

                                            <div class="row">
                                                <div class="col-lg-12 mb-3 mb-lg-0">
                                                    <div class="px-2">
                                                        <blockquote class="h3 font-weight-normal mb-4" style="color:#000">I'm absolutely happy with the training I received from Memdal, it was practical with hands-on project.</blockquote>
                                                        <div class="media">
                                                            <div class="avatar avatar-xs avatar-circle d-lg-none mr-2">
                                                                <img class="avatar-img" src="assets/img/100x100/img19.jpg" alt="Image Description">
                                                            </div>
                                                            <div class="media-body">
                                                                <span class="text-dark font-weight-bold" style="color:#000">Divine</span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- End Testimonials Section -->





        <section class="accomplishment" style="height: 530px;">
            <div class="fill-screen fixed-attachment">
                <div class="container">

                    <div class="row">
                        <div class="col-md-12">
                            <h2 style="color:#1e2022; font-weight: 600;" class="mt-3">Start your learning experience today</h2>
                            <p style="color:#000">Join thousands of students who have already enhanced their career with Memdal training</p>
                        </div>
                        <div class="col-md-4 digits">
                            <div class="col-md-12" style="color:#000"><span class="number" style="color:#005eb8">1000+</span> <br> <span></span>Trained Students</div>
                        </div>
                        <div class="col-md-4 digits">
                            <div class="col-md-12" style="color:#000"><span class="number" style="color:#005eb8">20+</span> <br> Certified Trainers</div>
                        </div>

                        <div class="col-md-4 digits">
                            <div class="col-md-12" style="color:#000"><span class="number" style="color:#005eb8">15+</span> <br> Hiring Companies</div>
                        </div>
                        <!-- <div class="col-md-3 digits">
                            <div class="col-md-12" style="color:#333"><span class="number" style="color:#005eb8">94%</span> <br> Satisfied Leraners</div>
                        </div>-->

                    </div>

                    <div class="col-md-12 mt-5">
                        <a href="{{route('register')}}" class="courses-all btn" style="background-color:#fdd76e; color:#333; padding-left:40px;padding-right:40px; padding-top:10px;  padding-bottom:10px">Get Started</a>


                    </div>
                </div>
            </div>
        </section>




        </pre>
        </div>
        </div>

        </div>

        </div>





   <!-- <section class="padding-y-100" style="background-color: #f8f9fa! important;">
        <div class="container" style="max-width:1140px">
            <div class="row">
                <div class="col-12 text-center" style="margin-top:-40px;">
                    <h3 class="text-left">
                        Our Courses
                        <a href="{{route('onlinecourses')}}" style="float: right; font-size: initial; margin-top: 13px;">All Courses</a>
                    </h3>

                </div>


                @foreach($courses as $course)
                <div class="col-lg-4 mt-3" data-aos="fade-left" data-aos-once="true" data-aos-delay="{{$loop->index*100}}">

                <div class="card  mb-5">
                        <a href="#">
                            <div class="rounded mainOverlay">
                            @if($course->image_url == "")
                        <img class="rounded card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
                        @else
							<img class="rounded card-img-top" src="{{$course->image_url}}" alt="course image"/>
                        @endif

                            </div>
                        </a>
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 20px; font-weight: bold;">
                                   {{$course->course_title}}
                                </a>

                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>



                            </p>


                            <div class="card-text d-flex bd-highlight mt-2">
                                <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($course->course_type == 1)
                                    Type: Online
                                    @elseif($course->course_type == 2)
                                    Type: Classroom-based
                                    @else
                                    Type: Free course
                                    @endif
                                     </p>
                                <h4 class="h5 text-right ml-5">
                                    <span class="" style="font-weight: bold; margin-left:-40px">N{{$course->course_price}}</span>

                                </h4>

                            </div>
                            <a href="{{route('onlinecoursedetails', $course->id)}}" class="btn btn-primary mt-2" style="color:#fff">View course details</a>

                        </div>
                    </div>

                </div>

@endforeach

            </div>
        </div>
    </section>-->







@endsection
