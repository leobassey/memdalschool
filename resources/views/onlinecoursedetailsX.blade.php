@extends('layouts.weblayouts')
@section('title', 'Online Course Details')


@section('content')
<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url({{asset('assets/img/breadcrumb-bg.jpg')}}) no-repeat">

<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-8 my-2 text-white">
            <ol class="breadcrumb breadcrumb-double-angle bg-transparent p-0">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Courses</a></li>
                <li class="breadcrumb-item">Details</li>
            </ol>
            <h2 class="h4" style="font-size:30px">
                {{$course->course_title}} course
            </h2>
        </div>

    </div>
</div>

</div>



<section class="courseinfo mt-5">
<div class="container">
    <div class="row">

        <div class="col-md-8 details">
            <h3 style="font-weight: bolder;font-size:30px"> {{$course->course_title}}</h3>
            <p class="mb-4">

                <i class="fas fa-star text-warning"></i>
                <i class="fas fa-star text-warning"></i>
                <i class="fas fa-star text-warning"></i>
                <i class="fas fa-star text-warning"></i>
                <span class="text-dark ml-1">(4.5 user rating)</span>


            </p>
            <p class="mb-4">


            {!! $course->course_full_description !!}
            </p>
            <h4>Skills you will learn</h4>
            <p>
                <!--<i class="fas fa-check"></i> Basics of leadership & team development. <br />
                <i class="fas fa-check"></i> Strategic decision making & negotiation skills. <br />
                <i class="fas fa-check"></i> Essentials of project, crisis and time management. <br />-->



                {!! $course->course_objectives !!}
            </p>
           <!-- <h4>About This Course</h4>
            <p class="mb-5">
                In today’s business world, management and leadership skills are essential for long-term success. This course teaches you how to manage organisations and empower people. You will learn various leadership models, discover different leadership styles and
                understand how teams develop.
                <br /> You will also learn the basics of project management, change management and time management. Furthermore, this course will teach you how to handle crises, how to make better decisions and how to become a superior, more effective
                negotiator. <br /><br /> You will also receive short case studies of Apple, Microsoft and the University of Oxford that summarize the key takeaways of this course.
            </p>-->



            <!--<div class="card padding-30 shadow-v3">
          <h4>
            Course Features Include:
          </h4>
          <ul class="list-inline mb-0 mt-2">
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">246 lectures</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">27.5 Hours</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">98,250 students entrolled</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">Lifetime access</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">Certificate of Completion</span>
            </li>

          </ul>
        </div>-->



        <!--<div class="col-12 mt-4">
         <ul class="nav tab-line tab-line tab-line--3x border-bottom mb-5" role="tablist">
           <li class="nav-item">
            <a class="nav-link active show" data-toggle="tab" href="#tabDescription" role="tab" aria-selected="true">
              Description
            </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabCurriculum" role="tab" aria-selected="true">
              Curriculum
            </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabInstructors" role="tab" aria-selected="true">
              Instructors
            </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabReviews" role="tab" aria-selected="true">
              Reviews
            </a>
           </li>
         </ul>
          <div class="tab-content">
            <div class="tab-pane fade  active show" id="tabDescription" role="tabpanel">
              <h4>
                Course Description
              </h4>
              <p>
                Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius claritas est conctetur adip sicing. Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standad dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it make type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting remaining essentially unchanged.
              </p>
              <p>
                Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimen tum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi.
              </p>

              <div class="row mt-5">
               <div class="col-12">
                 <h4>
                  What Will I Learn?
                </h4>
               </div>
                <div class="col-md-12 my-2">
                  <ul class="list-unstyled list-style-icon list-icon-check">
                    <li>Learn how to captivate your audience</li>
                    <li>Get rid of negative self talk that brings you down before your presentations</li>
                    <li>Take your business / job to the next level</li>
                    <li>Overcome the fear of public speaking</li>
                  </ul>
                </div>


                <div class="col-12">
                 <h4>
                 Course Requirements
                </h4>
               </div>
                <div class="col-md-12 my-2">
                  <ul class="list-unstyled list-style-icon list-icon-check">
                    <li>Learn how to captivate your audience</li>
                    <li>Get rid of negative self talk that brings you down before your presentations</li>
                    <li>Take your business / job to the next level</li>
                    <li>Overcome the fear of public speaking</li>
                  </ul>
                </div>

                <div class="col-12">
                 <h4>
                 Target Audience
                </h4>
               </div>
                <div class="col-md-12 my-2">
                  <ul class="list-unstyled list-style-icon list-icon-check">
                    <li>Learn how to captivate your audience</li>
                    <li>Get rid of negative self talk that brings you down before your presentations</li>
                    <li>Take your business / job to the next level</li>
                    <li>Overcome the fear of public speaking</li>
                  </ul>
                </div>





              </div>
            </div> -->

            <!--<div class="tab-pane fade" id="tabCurriculum" role="tabpanel">
            <div id="accordionCurriculum">

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_1" data-toggle="collapse" aria-expanded="false">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                    <span class="h6 d-inline">Getting Started</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     6 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     20:20
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_1" class="collapse" data-parent="#accordionCurriculum" style="">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_2" data-toggle="collapse" aria-expanded="true">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                    <span class="h6 d-inline">HTML 5</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     19 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     2:37:10
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_2" class="collapse" data-parent="#accordionCurriculum">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_3" data-toggle="collapse" aria-expanded="true">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                    <span class="h6 d-inline">Bootstrap 4</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     8 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     40:40
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_3" class="collapse" data-parent="#accordionCurriculum">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_4" data-toggle="collapse" aria-expanded="true">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                      <span class="h6 d-inline">JavaScript: The Tricky Stuff</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     12 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     1:20:40
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_4" class="collapse" data-parent="#accordionCurriculum">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>


              </div>
            </div>-->


           <!-- <div class="tab-pane fade" id="tabInstructors" role="tabpanel">
              <h4 class="mb-4">
                About Instructors
              </h4>

              <div class="border-bottom mb-4 pb-4">
                <div class="d-md-flex mb-4">
                  <a href="#">
                    <img class="iconbox iconbox-xxxl" src="assets/img/262x230/5.jpg" alt="">
                  </a>
                  <div class="media-body ml-md-4 mt-4 mt-md-0">
                    <h6>
                      Hasan Jubayer
                    </h6>
                    <p class="mb-2">
                      <i class="ti-world mr-2"></i> Web Developer and Instructor
                    </p>
                    <ul class="list-inline">
                      <li class="list-inline-item mb-2">
                        <i class="ti-user mr-1"></i>
                        147570 studends
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-book mr-1"></i>
                        20 Courses
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-star text-warning mr-1"></i>
                        4.9 Average Rating (4578)
                      </li>
                    </ul>
                    <a href="#" class="btn btn-outline-primary btn-pill btn-sm">View Profile</a>
                  </div>
                </div>
                <h6>
                  Experience as Web Developer
                </h6>
                <p>
                  Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius claritas est conctetur adip sicing. Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standad dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it make type specimen book. It has survived not only five centuries.
                </p>
              </div>

              <div class="border-bottom mb-4 pb-5">
                <div class="d-md-flex mb-4">
                  <a href="#">
                    <img class="iconbox iconbox-xxxl" src="assets/img/262x230/6.jpg" alt="">
                  </a>
                  <div class="media-body ml-md-4 mt-4 mt-md-0">
                    <h4>
                      Hasan Jubayer
                    </h4>
                    <p class="mb-2">
                      <i class="ti-world mr-2"></i> Web Developer and Instructor
                    </p>
                    <ul class="list-inline">
                      <li class="list-inline-item mb-2">
                        <i class="ti-user mr-1"></i>
                        147570 studends
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-book mr-1"></i>
                        20 Courses
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-star text-warning mr-1"></i>
                        4.7 Average Rating (4578)
                      </li>
                    </ul>
                    <a href="#" class="btn btn-outline-primary btn-pill btn-sm">View Profile</a>
                  </div>
                </div>
              </div>

            </div>-->

           <!-- <div class="tab-pane fade" id="tabReviews" role="tabpanel">
             <h4 class="mb-4">
               Students Feedback
             </h4>

              <div class="row px-0 align-items-center border p-4">
               <div class="col-md-4 text-center">
                  <h1 class="display-4 text-primary mb-0">
                    4.70
                  </h1>
                  <p class="my-2">
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="mb-0">
                    Average Rating
                  </p>
               </div>
               <div class="col-md-8">
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:100%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>90%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:80%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>87%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:60%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>34%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:40%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>12%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:10%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>2%</span>
                    </div>
               </div>
              </div>

              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <img class="iconbox iconbox-xl" src="assets/img/avatar/4.jpg" alt="">
                  <div class="media-body ml-4">
                   <small class="text-gray">7 min ago</small>
                   <h6>
                     Anthony Forsey
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="font-size-18">
                    Awesome course
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                  </p>
                </div>
              </div>

              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <img class="iconbox iconbox-xl" src="assets/img/avatar/5.jpg" alt="">
                  <div class="media-body ml-4">
                   <small class="text-gray">1 mon ago</small>
                   <h6>
                     Justin Nam
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p class="text-light">
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                  </p>
                  <p class="font-size-18">
                    Test review lol
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty.
                  </p>
                </div>
              </div>
              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <div class="iconbox iconbox-xl border">
                    MD
                  </div>
                  <div class="media-body ml-4">
                   <small class="text-gray">3 Mon ago</small>
                   <h6>
                     Murir Dokan
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="font-size-18">
                    This is a title of review. the developer suffer from lot's of vitamin. what about you?
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                  </p>
                </div>
              </div>


              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <img class="iconbox iconbox-xl" src="assets/img/avatar/6.jpg" alt="">
                  <div class="media-body ml-4">
                   <small class="text-gray">1 year ago</small>
                   <h6>
                     John Doe
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="font-size-18">
                    Best course ever
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                  </p>
                </div>
              </div>
              <div class="text-center mt-5">
                <a href="#" class="btn btn-primary btn-icon">
                  <i class="ti-reload mr-2"></i>
                  Load More
                </a>
              </div>
            </div>
          </div>
      </div>-->
        </div>

        <div class="col-md-4">
            <div class="card mb-3" style="border: 1px solid rgba(0,0,0,.125) !important;">
                <img src="{{asset('assets/img/Business-Financing.jpg')}}" class="card-img-top" alt="..." />
                <div class="card-body">
                    <div class="cards bd-highlight">
                        <h5 class="card-title flex-fill bd-highlight">
                            <!--<span class="former">
                                &#8358;70,000.00
                            </span>-->
                            <br />
                            <span class="current flex-fill bd-highlight"> &#8358;{{$course->course_price}}</span>
                            <a class="btn  ml-1" href="#" style="background-color: orange; color:#ffff">Enroll Now</a>
                        </h5>

                    </div>
                    <div class="card-text d-flex bd-highlight">
                        <p class="flex-fill bd-highlight mb-2">
                            <i class="fas fa-laptop mr-2"></i> Type: <br />
                            <i class="fas fa-stopwatch mr-2"></i> Duration: <br />
                            <i class="fas fa-star mr-2"></i> Award: <br />


                        </p>
                        <div class="flex-fill bd-highlight pull-right pb-2">
                            <span class="ml-3 mt-2"> Online</span> <br />
                            <span class="ml-3 mt-2"> 4 Days, 5hrs/day </span> <br />
                            <span class="ml-3"> Certificate </span> <br />


                        </div>


                    </div>

                </div>
                <a href="#" class="ml-5 mb-4" style="color:orange; text-decoration: none; font-weight: bold;">Download course contents</a>

            </div>
        </div>
    </div>
</div>
</section>



<section class="padding-y-100" style="background-color: #f8f9fa! important;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center" style="margin-top:-40px;">
                    <h2 class="">
                        Students all enrolled
                    </h2>

                </div>

                <!--  <div class="col-12 mt-3">
                    <div class="d-md-flex justify-content-between bg-white rounded shadow-v1 p-4">
                        <ul class="nav nav-pills nav-isotop-filter align-items-center my-2">

                            <a class="nav-link" href="online-courses.html" data-filter=".free-courses">Online Courses</a>
                            <a class="nav-link" href="classroom-courses.html" data-filter=".start-soon">In-Person Courses</a>
                            <a class="nav-link" href="topic-courses.html" data-filter=".free-courses">Topic-Based Courses</a>

                            <a class="nav-link" href="#" data-filter=".free-courses">Career Courses</a>
                            <a class="nav-link" href="#" data-filter=".free-courses">Free Courses</a>
                        </ul>


                    </div>
                </div>-->

                <!-- END row-->

                @foreach($courses as $c)
                <div class="col-lg-4 mt-3">

                <div class="card">
                        <a href="#">
                            <div class="rounded mainOverlay">
                                <img class="rounded" src="{{asset('assets/img/marketing.jpg')}}" alt="">
                              <!--<p class="text-center overlayText">View Details</p>-->
                            </div>
                        </a>
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 20px; font-weight: bold;">
                                   {{$c->course_title}}
                                </a>

                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>



                            </p>


                            <div class="card-text d-flex bd-highlight mt-2">
                                <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($c->course_type == 1)
                                    Type: Online
                                    @elseif($c->course_type == 2)
                                    Type: Classroom-based
                                    @else
                                    Type: Free course
                                    @endif
                                     </p>
                                <h4 class="h5 text-right ml-5">
                                    <span class="" style="font-weight: bold; margin-left:-40px">N{{$c->course_price}}</span>

                                </h4>

                            </div>
                            <a href="{{route('onlinecoursedetails', $c->id)}}" class="btn mt-2" style="background-color:orange; color:#fff">View course details</a>

                        </div>
                    </div>

                </div>

@endforeach


            </div>
        </div>
    </section>

@endsection
