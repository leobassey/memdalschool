@extends('layouts.frontlayout')
@section('title', 'Online Course Details')


@section('content')

<style>

.card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: cover;
}

</style>
<!--<div class="jumbotron" style="border-radius: 0px; height: 300px; background-size: cover; background-image: url({{asset('webassets/img/1920x800/jumbo.jpg')}})">
            <h1 class="text-white text-center mt-10"> Course enrollment</h1>
        </div>-->


          <!-- Page Banner Section Start -->
          <div class="page-banner bg-color-05">
            <div class="page-banner__wrapper">
                <div class="container">

                    <!-- Page Breadcrumb Start -->
                    <div class="page-breadcrumb">
                        <ul class="breadcrumb">

                        </ul>
                    </div>
                    <!-- Page Breadcrumb End -->

                    <!-- Page Banner Caption Start -->
                    <div class="page-banner__caption text-center">
                        <h2 class="page-banner__main-title">Enrollment</h2>
                    </div>
                    <!-- Page Banner Caption End -->

                </div>
            </div>
        </div>
        <!-- Page Banner Section End -->


        <section class="course-lists mt-10">
            <div class="container mb-5">
                <div class="row container">
                    <div class="col-lg-7 col-12 mb-4">
                        <div class="row">
                            <div class="text-left col-9 col-lg-9">
                                <h5 class="text-left" style="font-weight: 600;">Course</h5>
                            </div>
                            <div class="col-3 col-lg-3">
                                <h5 class="text-left" style="font-weight: 600;">Price</h5>
                            </div>
                            <hr>
                        </div>
                        <div class="row pb-3" style="border-bottom: 1px solid #ccc;">
                            <div class="col-lg-3 col-3">
                                <div class="row">
                                    <div class="col-lg-1 col-1" style="align-self: center;">
                                        <i class="far fa-times-circle"></i>
                                    </div>
                                    <div class="col-lg-9 col-9">
                                    @if($course->image_url == "")
            <img class="w-100" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="w-100" src="../{{$course->image_url}}" alt="course image"/>
           @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-6" style="align-self: center;">
                                <span>{{$course->course_title}}</span>
                            </div>
                            <div class="col-lg-3 col-3" style="align-self: center; font-weight: 600;">
                                @if($course->course_type == 1)

                                @if($course->promo_price == 0.00)
                                &#x20A6;{{number_format($course->course_price, 2)}}
                                @else
                                &#x20A6;{{number_format($course->promo_price,2)}}
                                @endif

                                @elseif($course->course_type == 2)

                                @if($course->promo_price == 0.00)
                                &#x20A6;{{number_format($course->course_price,2)}}
                                @else
                                &#x20A6;{{number_format($course->promo_price,2)}}
                                @endif

                                @else
                                Free
                                @endif
                            </div>


                        </div>

                       @if($hasEnrolled == true)

                       @else

@if($course->coupon_active == 0)

@else
                        @if($couponform == 1)

                        @else

                        <form action="{{route('addcoupon')}}" method="post">
                            @csrf
<div class="row">


    <div class="col-md-7">

        <input type="text" name="coupon" class="form form-control mt-3" placeholder="Enter coupon here" style="border: solid 1px; width:100%" required/>
        <input type="hidden" name="course" class="form form-control mt-3" value="{{$course->id}}" />
    </div>

    <div class="col-md-5 mt-3">
        <button class="btn btn-primary btn-hover-secondary" style="text-align: center; margin-left:-28px">Apply coupon</button>
    </div>

    <br/>
    <br/>
    <br/>
    <br/>


    @if ($message = Session::get('message'))
    <div class="alert alert-success alert-block mb-3" style="width: 100%; margin-left: 10px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong style="color:#333"> {{ session()->get('message') }}</strong>
    </div>
    @endif

    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block mb-3" style="width: 100%; margin-left: 10px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong style="color:green"> {{ session()->get('success') }}</strong>
    </div>
    @endif

    @if(count($errors) >0)
    <ul style="color:red; font-weight:bold; list-style:none">
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </ul>
    @endif
</div>
</form>
@endif
@endif

@endif

                 </div>

                    <div class="col-lg-4 offset-lg-1 col-12">
                        <div class="row">
                            <div class="text-left col-6 col-lg-6">
                                <h5 style="font-weight: 600;" style="font-size: 12px">Cart Total</h5>
                            </div>
                            <div class="text-right col-6 col-lg-6"></div>
                            <hr>
                            <div class="text-left col-6 col-lg-6">
                                <span>Total</span>
                            </div>
                            <div class="text-right col-6 col-lg-6" style="font-weight: 600;">
                            @if($course->course_type == 1)

                            @if($course->promo_price == 0.00)
                            &#x20A6;{{number_format($course->course_price, 2)}}
                            @else
                            &#x20A6;{{number_format($course->promo_price,2)}}
                            @endif

                            @elseif($course->course_type == 2)

                            @if($course->promo_price == 0.00)
                            &#x20A6;{{number_format($course->course_price,2)}}
                            @else
                            &#x20A6;{{number_format($course->promo_price,2)}}
                            @endif

                            @else
                            Free
                            @endif
                            </div>


                        </div>


                    <div class="mt-5">
                   <!-- <a href="" class=" headBtn btn">-->

                   @if($course->course_status == 2)
                    <a href="#" class="w-100 btn-lg courses-all headBtn btn" style="font-size: 12px">Enrollment CLOSED</a>

                    @else

                    <?php if($hasEnrolled == false){?>

                        <?php if($course->course_type == 3){?>
                        <a href="#" onclick="EnrollFree('<?php echo $course->id?>')" class="btn btn-primary btn-hover-secondary w-100" style="" id="freebutton">Enroll for Free</a>
                        <?php } else {?>
                        <p class="lead"></p>
           <!--             @if($course->course_price == 0.00)

                        <a href="#" onclick="EnrollFree('<?php echo $course->id?>')" class="btn btn-primary btn-hover-secondary w-100" style="" id="freebutton">Enroll for Free</a>
                        @else
           -->
                        <a href="#" onclick="paywithPaystack('<?php echo $course->id?>')" class="btn btn-primary btn-hover-secondary w-100" style="">Pay

                           <!--{{number_format($course->course_price, 2)}}-->


                            @if($course->course_type == 1)

                                @if($course->promo_price == 0.00)
                                &#x20A6;{{number_format($course->course_price, 2)}}
                                @else
                                &#x20A6;{{number_format($course->promo_price,2)}}
                                @endif

                                @elseif($course->course_type == 2)

                                @if($course->promo_price == 0.00)
                                &#x20A6;{{number_format($course->course_price,2)}}
                                @else
                                &#x20A6;{{number_format($course->promo_price,2)}}
                                @endif


                                @endif

                            </a>

                          <!--@endif-->


                        <?php } ?>
                        <?php } else{

                        echo '<h6>You are enrolled in this course thank you.</h6>';
                        echo '<br/>';



                       /* echo "<a href='{{route('mycourses')}}' class='btn btn-primary btn-hover-secondary'>View Course</a>";*/
                        }
                        ?>

                    @endif
                    <!--</a>-->

                    </div>

                    </div>

                   <!-- <div class="">
                        <div class="col-lg-12 col-12">
                            <a href="dashboard.html" class="courses-all headBtn btn">To Checkout</a>
                        </div>
                         </div>-->



                </div>

            </div>
        </section>



<!--<section class="paddingTop-50 paddingBottom-100 bg-light-v2">
  <div class="container" style="max-width:1140px">
    <div class="list-card align-items-center shadow-v1 marginTop-30">
      <div class="col-lg-4 px-lg-4 my-4">


        @if($course->image_url == "")
            <img class="w-100" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="w-100" src="../{{$course->image_url}}" alt="course image"/>
           @endif
      </div>
      <div class="col-lg-8 paddingRight-30 my-4">
       <div class="media justify-content-between">
         <div class="group">
          <a href="#" class="h4">
          {{$course->course_title}}
          </a>
          <ul class="list-inline mt-3">
            <li class="list-inline-item mr-2">
              <i class="ti-user mr-2"></i>
             Course ratings
            </li>
            <li class="list-inline-item mr-2">
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <span class="text-dark">5</span>

            </li>
          </ul>
         </div>

       </div>
       <p>
         You can pay for this course using your credit card or pay via your FIDAS wallet
       </p>
       <div class="d-md-flex justify-content-between align-items-center">
         <ul class="list-inline mb-md-0">
         @if($course->course_type == 1)
         <li class="list-inline-item mr-3">
             <span class="h4 d-inline ">N{{$course->course_price}}</span>

           </li>
                                    @elseif($course->course_type == 2)
                                    <li class="list-inline-item mr-3">
             <span class="h4 d-inline ">N{{$course->course_price}}</span>

           </li>
                                    @else
                                    <li class="list-inline-item mr-3">
             <span class="h4 d-inline ">Free</span>

           </li>
                                    @endif

           <li class="list-inline-item mr-3">
             <i class="ti-headphone small mr-2"></i>
             Well curated contents
           </li>
           <li class="list-inline-item mr-3">
             <i class="ti-time small mr-2"></i>
             Learn with case studies
           </li>
         </ul>

       </div>
      </div>

      <hr>



<div class="col-md-7 mb-5 animate__animated animate__pulse">
  <?php if($hasEnrolled == false){?>

  <?php if($course->course_type == 3){?>
    <a href="#" onclick="EnrollFree('<?php echo $course->id?>')" class="btn btn-primary" style="background-color:orange; color:#fff">Enroll Free</a>
  <?php } else {?>
  <p class="lead">Pay via card</p>
  <a href="#" onclick="paywithFlutter('<?php echo $course->id?>')" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N{{number_format($course->course_price)}}</a>
   <?php } ?>
<?php } else{

  echo '<h6>You are enrolled in this course thank you.</h6>';
}
?>
</div>




<div class="col-md-5 animate__animated animate__pulse mb-5">
  <?php if($hasEnrolled == false)
  {?>

              <?php if($course->course_type == 3){?>
                    <a href="javascript:void(0)" id="freebutton" onclick="EnrollFree('<?php echo $course->id?>')" class="btn btn-primary" style="background-color:orange; color:#fff">Enroll Free</a>
              <?php } else {?>

                           <p class="lead">Pay via your FIDAS Wallet (Balance: N<?php echo $balance ?>)</p>
                           <?php if($balance > $course->course_price){?>
                          <a href="javascript:void(0)" onclick="payViaWallet('<?php echo $course->id?>')" id="walletbutton" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N <?php echo $course->course_price ?></a>
                        <?php }  else { echo "Insufficient Balance"; }?>

            <?php } ?>

  <?php } ?>
</div>

</div>




  </div>
</section>-->




<!--<section class="courseinfo mt-5 " >
<div class="container" style="max-width:1140px">
    <div class="row">

        <div class="col-md-6 details">
          <p class="lead">Pay via card</p>
          <a href="#" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N{{$course->course_price}}</a>
        </div>

        <div class="col-md-6 animate__animated animate__pulse">
            <p class="lead">Pay via your FIDAS Wallet (Balance: N45000)</p>
            <a href="#" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N{{$course->course_price}}</a>
        </div>
    </div>
</div>
</section>-->



@endsection

<input  type="hidden" id="t_" value="{{ csrf_token()}}">
<script>
var site = "<?php echo url('/');?>"
</script>
  <!--<script src="https://checkout.flutterwave.com/v3.js"></script>-->

    <script src="https://js.paystack.co/v1/inline.js"></script>
<script>

/*
    function paywithFlutter(course_id)
    {

        var url = site + "/initiatepaywithflutter";
        var xml = new XMLHttpRequest();
        var t = document.getElementById('t_').value;
        var xml = new XMLHttpRequest();
        xml.open("POST", url, true);
        fd = new FormData();

        fd.append("course_id", course_id);


             xml.setRequestHeader("X-CSRF-TOKEN", t);
                xml.onreadystatechange = function()
                {
                     if(xml.readyState == 4 && xml.status == 200)
                     {
                             if(xml.responseText.includes("nsima"))
                            {
                  var resdata =   JSON.parse(xml.responseText);
                              callFlutter(resdata.amount, resdata.consumer_id, resdata.consumer_mac, resdata.username, resdata.ref, resdata.email, course_id)
                            }
                            else{
                  console.log(xml.responseText)
                            //	ReportError(xml.responseText,'errorzone') ; return;
                            }
                     }
                     if(xml.status == 419)
                     {
                   location.reload();
                     }
                }
            xml.send(fd);
    }*/


    /*
    function callFlutter(amount, consumer_id, consumer_mac, username, ref, email, course_id)
    {
        FlutterwaveCheckout({
          public_key: "FLWPUBK_TEST-d4d87be296ca20a5bef4cfe860322639-X",
          tx_ref: ref,
          amount: amount,
          currency: "NGN",
          country: "NG",
          payment_options: "card",
          meta: {
            consumer_id: consumer_id,
            consumer_mac: consumer_mac,
          },
          customer: {
            email: email,
            phone_number: "",
            name: username,
          },
          callback: function (data) {
            console.log(data);
                verifyReference(data.tx_ref, data.amount,data.flw_ref, course_id, data.transaction_id);
          },
          onclose: function() {
          location.reload();
          },
          customizations: {
            title: "FIDAS",
            description: "Payment for course",
            logo: "{{asset('img/fidaslogo.png')}}",
          },
        });
      }*/

/*
      function verifyReference(tx_ref, amount, flw_ref, course_id, transaction_id)
      {
        var url = site + "/verifyReference";
        var xml = new XMLHttpRequest();
        var t = document.getElementById('t_').value;
        var xml = new XMLHttpRequest();
        xml.open("POST", url, true);
        fd = new FormData();

        fd.append("tx_ref", tx_ref);
        fd.append("amount", amount);
        fd.append("flw_ref", flw_ref);
        fd.append("course_id", course_id);
        fd.append("transaction_id", transaction_id);

               xml.setRequestHeader("X-CSRF-TOKEN", t);
                  xml.onreadystatechange = function()
                  {
                       if(xml.readyState == 4 && xml.status == 200)
                       {
                               if(xml.responseText.includes("nsima"))
                              {
                    //var resdata =   JSON.parse(xml.responseText);

                   location.reload();
                   console.log(xml.responseText)
                              }
                              else{
                    console.log(xml.responseText)
                              //	ReportError(xml.responseText,'errorzone') ; return;
                              }
                       }
                       if(xml.status == 419)
                       {
                     location.reload();
                       }
                  }
              xml.send(fd);
      }*/


    function EnrollFree(course_id)
    {
      var url = site + "/enrollfree";
      var xml = new XMLHttpRequest();
      var t = document.getElementById('t_').value;
      var xml = new XMLHttpRequest();
      xml.open("POST", url, true);
      fd = new FormData();
      document.getElementById("freebutton").style.display = "none";


      fd.append("course_id", course_id);


         xml.setRequestHeader("X-CSRF-TOKEN", t);
          xml.onreadystatechange = function()
          {
             if(xml.readyState == 4 && xml.status == 200)
             {
                if(xml.responseText.includes("done"))
                {
                  //var resdata =   JSON.parse(xml.responseText);

                 //location.reload();
                 //console.log(xml.responseText)

                 window.location.href = site + '/enrollmentsuccess/' + course_id;
                }
                else{
                  console.log(xml.responseText)
                //	ReportError(xml.responseText,'errorzone') ; return;
                }
             }
             if(xml.status == 419)
             {
                location.reload();
             }
          }
        xml.send(fd);
    }

    function payViaWallet(course_id)
    {
      var url = site + "/payViaWallet";
      var xml = new XMLHttpRequest();
      var t = document.getElementById('t_').value;
      var xml = new XMLHttpRequest();
      xml.open("POST", url, true);
      fd = new FormData();

    document.getElementById("walletbutton").style.display = "none";

      fd.append("course_id", course_id);


         xml.setRequestHeader("X-CSRF-TOKEN", t);
          xml.onreadystatechange = function()
          {
             if(xml.readyState == 4 && xml.status == 200)
             {
                if(xml.responseText.includes("nsima"))
                {
                  //var resdata =   JSON.parse(xml.responseText);

                 location.reload();
                 console.log(xml.responseText)
                }
                else{
                  console.log(xml.responseText)
                //	ReportError(xml.responseText,'errorzone') ; return;
                }
             }
             if(xml.status == 419)
             {
                location.reload();
             }
          }
        xml.send(fd);
    }



function paywithPaystack(course_id) {

    var url = site + "/initiatepaywithflutter";
        var xml = new XMLHttpRequest();
        var t = document.getElementById('t_').value;
        var xml = new XMLHttpRequest();
        xml.open("POST", url, true);
        fd = new FormData();

        fd.append("course_id", course_id);

             xml.setRequestHeader("X-CSRF-TOKEN", t);
                xml.onreadystatechange = function()
                {
                     if(xml.readyState == 4 && xml.status == 200)
                     {
                             if(xml.responseText.includes("nsima"))
                            {
                  var resdata =   JSON.parse(xml.responseText);
                  CallPaystack(resdata.amount, resdata.consumer_id, resdata.consumer_mac, resdata.username, resdata.ref, resdata.email, course_id)
                            }
                            else{
                  console.log(xml.responseText)
                            //	ReportError(xml.responseText,'errorzone') ; return;
                            }
                     }
                     if(xml.status == 419)
                     {
                   location.reload();
                     }
                }
            xml.send(fd);

}


function CallPaystack(amount, consumer_id, consumer_mac, username, ref, email, course_id) {
var handler = PaystackPop.setup({
  //key: 'pk_live_5879e8b7143a5edf7228b2d8f2a7fc108c5c4433',
  key: 'pk_test_3e931b7a3ad4936fbba52dcb8c98be7a529eadf5',
  email: email,
  amount: parseInt(amount) * 100,
  ref: ref, //''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
  metadata: {
    custom_fields: [{
      display_name: "Mobile Number",
      variable_name: "mobile_number",
      //value: phone
    }]
  },

  /*customer: {
            email: email,
            phone_number: "09078787876",
            name: username,
          },*/

  callback: function(data) {
    verifyReference(data.ref, data.amount,data.flw_ref, course_id, data.transaction_id, consumer_id); //normally, ordercode and response.reference should be the same
    //alert("blur");
  },

  onClose: function() {
    //window.location.href = site + '/orderplaced/' + ordercode;

    location.reload();
  },
  customizations: {
            title: "FIDAS",
            description: "Payment for course",
            logo: "{{asset('img/fidaslogo.png')}}",
          },
});
handler.openIframe();
}



function verifyReference(ref, amount, flw_ref, course_id, transaction_id, consumer_id) {

    console.log(ref);

    var url = site + "/verifyReference";
        var xml = new XMLHttpRequest();
        var t = document.getElementById('t_').value;
        var xml = new XMLHttpRequest();
        xml.open("POST", url, true);
        fd = new FormData();



        fd.append("amount", amount);

        fd.append("ref", ref);
        fd.append("flw_ref", flw_ref);
        fd.append("transaction_id", transaction_id);

        fd.append("course_id", course_id);
        fd.append("payment_id", consumer_id);


        xml.setRequestHeader("X-CSRF-TOKEN", t);
                  xml.onreadystatechange = function()
                  {
                       if(xml.readyState == 4 && xml.status == 200)
                       {
                               if(xml.responseText.includes("nsima"))
                              {



                                window.location.href = site + '/enrollmentsuccess/' + course_id;
                    //var resdata =   JSON.parse(xml.responseText);

                    //console.log(resdata);

                  // location.reload();
                  // console.log(xml.responseText)
                              }
                              else{
                    console.log(xml.responseText)
                              //	ReportError(xml.responseText,'errorzone') ; return;
                              }
                       }
                       if(xml.status == 419)
                       {
                     location.reload();
                       }
                  }
              xml.send(fd);


        /*

xml.setRequestHeader("X-CSRF-TOKEN", t);
xml.onreadystatechange = function() {

  if (xml.readyState == 4 && xml.status == 200) {

    if (xml.responseText.indexOf('successfull') > -1) {
      console.log(xml.responseText);
      document.getElementById("warning").innerHTML = "Payment successful........Redirecting";
      window.location.href = site + '/orderplaced/' + reference;

    }

  }
}
xml.send(fd);*/
}
    </script>


