<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/vendors/css/vendors.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/css/components.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/css/pages/hospital.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('loginassets/css1/main.css')}}">
    <!-- END: Custom CSS-->


    <link href="{{asset('nextassets/lib/medium-editor/medium-editor.css')}}" rel="stylesheet">
    <link href="{{asset('nextassets/lib/medium-editor/default.css')}}" rel="stylesheet">
    <link href="{{asset('nextassets/lib/summernote/summernote-bs4.css')}}" rel="stylesheet">

    <script src="{{asset('vue-files/vue.js')}}"></script>
	<script src="{{asset('vue-files/axios.js')}}"></script>



    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('nextassets/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}">

</head>


<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav style="height:10px" class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item">
                   <!-- <a class="navbar-brand" href="#"><img class="brand-logo" alt="" src="{{asset('img/fidaslogo.png')}}">-->

                            <h3 class="brand-text" style="margin-top: 12px;">Memdal</h3>
                        </a></li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>



                    </ul>
                    <ul class="nav navbar-nav float-right">

                    <li class="dropdown dropdown-user nav-item" ><a class="dropdown-toggle nav-link dropdown-user-link" style="color:#fff" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700">

                    @if(!Auth::user())
                                {
                                    <script>
                                    window.location.href = "{{url('login')}}";
                                    </script>
                                }
                                @else

                                    {{Auth::User()->name}}

                                @endif
                    </span><span class="avatar avatar-online"><img src="{{asset('nextassets/app-assets/images/portrait/small/avatar-s-19.png')}}" alt="avatar"><i></i></span></a>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{route('updateadminprofile')}}"><i class="ft-user"></i> Edit Profile</a>
                            <!--<a class="dropdown-item" href="#"><i class="ft-mail"></i> My Inbox</a><a class="dropdown-item" href="#"><i class="ft-check-square"></i> Task</a><a class="dropdown-item" href="#"><i class="ft-message-square"></i> Chats</a>-->
                                <div class="dropdown-divider"></div>
                               <a href="{{ route('logout') }}" class="dropdown-item"  onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                                <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                <i class="ft-power"></i> Logout
                            </a>
                            </div>
                        </li>

                       <!--<li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i><span class="badge badge-pill badge-danger badge-up badge-glow">5</span></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6><span class="notification-tag badge badge-danger float-right m-0">5 New</span>
                                </li>
                                <li class="scrollable-container media-list w-100"><a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan mr-0"></i></div>
                                            <div class="media-body">
                                                <h6 class="media-heading">You have new order!</h6>
                                                <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">30 minutes ago</time></small>
                                            </div>
                                        </div>
                                    </a><a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left align-self-center"><i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1 mr-0"></i></div>
                                            <div class="media-body">
                                                <h6 class="media-heading red darken-1">99% Server load</h6>
                                                <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Five hour ago</time></small>
                                            </div>
                                        </div>
                                    </a><a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left align-self-center"><i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3 mr-0"></i></div>
                                            <div class="media-body">
                                                <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                                                <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Today</time></small>
                                            </div>
                                        </div>
                                    </a><a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left align-self-center"><i class="ft-check-circle icon-bg-circle bg-cyan mr-0"></i></div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Complete the task</h6><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last week</time></small>
                                            </div>
                                        </div>
                                    </a><a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left align-self-center"><i class="ft-file icon-bg-circle bg-teal mr-0"></i></div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Generate monthly report</h6><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last month</time></small>
                                            </div>
                                        </div>
                                    </a></li>
                                <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Read all notifications</a></li>
                            </ul>
                        </li>-->

                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->

     <!-- BEGIN: Main Menu-->

     <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="active"><a href="{{route('admindashboard')}}"><i class="la la-home"></i><span class="menu-title" data-i18n="Dashboard Hospital">Admin Dashboard</span></a>
                </li>

                <li class=" nav-item"><a href="#"><i class="la la-users"></i><span class="menu-title" data-i18n="Appointment">User Management</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('users')}}"><i></i><span>User lists</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="la la-file-text"></i><span class="menu-title" data-i18n="Doctors">Course Management</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('courses')}}"><i></i><span>Course lists</span></a>
                        </li>

                        <li><a class="menu-item" href="{{route('coursecategorylists')}}"><i></i><span>Course categories</span></a>
                        </li>
                        <li><a class="menu-item" href="{{route('createcourse')}}"><i></i><span>Create course</span></a>
                        </li>



                    </ul>
                </li>
               <!-- <li class=" nav-item"><a href="#"><i class="la la-users"></i><span class="menu-title" data-i18n="Patients">Teacher management</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="#"><i></i><span>All Teachers</span></a>

                        </li>
                        <li><a class="menu-item" href="{{route('teachercourselists')}}"><i></i><span>Teacher courses</span></a>
                        </li>
                    </ul>
                </li>-->
               <!-- <li class=" nav-item"><a href="hospital-payment-reports.html"><i class="la la-bar-chart"></i><span class="menu-title" data-i18n="Report">Report</span></a>
                </li>-->
                <li class=" nav-item"><a href="#"><i class="la la-users"></i><span class="menu-title" data-i18n="Payments">Student management</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('studentlists')}}"><i></i><span>All students</span></a>
                        </li>
                       <!-- <li><a class="menu-item" href="#"><i></i><span>Enroll students</span></a>
                        </li>-->
                        <!--<li><a class="menu-item" href="#"><i></i><span>Payment logs</span></a>
                        </li>-->

                        <li><a class="menu-item" href="{{route('addstudentscore')}}"><i></i><span>Add student score</span></a>
                        </li>

                        <li><a class="menu-item" href="{{route('generatecertificate')}}"><i></i><span>Generate Certificate</span></a>
                        </li>
                    </ul>
                </li>

                <li class=" nav-item"><a href="#"><i class="la la-users"></i><span class="menu-title" data-i18n="Payments">Staff management</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('stafflists')}}"><i></i><span>All staffs</span></a>
                        </li>
                        <li><a class="menu-item" href="{{route('addstaffscore')}}"><i></i><span>Add staff score</span></a>
                        </li>
                        <li><a class="menu-item" href="{{route('staffscore')}}"><i></i><span>View Exam Scores</span></a>
                        </li>

                       <!-- <li><a class="menu-item" href="#"><i></i><span>View staff courses</span></a>
                        </li>-->


                    </ul>
                </li>

                <li class=" nav-item"><a href="#"><i class="la la-dollar"></i><span class="menu-title" data-i18n="Payments">Payments</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="#"><i></i><span>Payment lists</span></a>
                        </li>

                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="la la-clipboard"></i><span class="menu-title" data-i18n="Invoice">Settings</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('uploadadminsignature')}}"><i></i><span data-i18n="Invoice Summary">Upload signature</span></a>
                        </li>
                        <li><a class="menu-item" href="#"><i></i><span data-i18n="Invoice Template">Send zoom invite</span></a>
                        </li>
                        <!--<li><a class="menu-item" href="#"><i></i><span data-i18n="Invoice List">Send Notifications</span></a>
                        </li>-->
                    </ul>
                </li>

                </li>





            </ul>
        </div>
    </div>

    <!-- END: Main Menu-->


    <div class="app-content content">
            @yield('content')
    </div>

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-center d-none d-lg-block">Memdal Training Copyright &copy; 2021.<span id="scroll-top"></span></span></p>
    </footer>
    <!-- END: Footer-->



     <!-- BEGIN: Vendor JS-->
     <script src="{{asset('nextassets/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('nextassets/app-assets/vendors/js/charts/chart.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('nextassets/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('nextassets/app-assets/js/scripts/pages/appointment.js')}}"></script>
    <!-- END: Page JS-->




    <!--<script src="{{asset('nextassets/lib/jquery/jquery.js')}}"></script>
    <script src="{{asset('nextassets/lib/popper.js/popper.js')}}"></script>
    <script src="{{asset('nextassets/lib/bootstrap/bootstrap.js')}}"></script>-->

    <script src="{{asset('nextassets/lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('nextassets/lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('nextassets/lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('nextassets/lib/select2/js/select2.min.js')}}"></script>

    <script src="{{asset('nextassets/js/starlight.js')}}"></script>


    <script src="{{asset('nextassets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('nextassets/lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('nextassets/lib/medium-editor/medium-editor.js')}}"></script>
    <script src="{{asset('nextassets/lib/summernote/summernote-bs4.min.js')}}"></script>



    <script src="{{asset('nextassets/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/jszip.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/pdfmake.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/vfs_fonts.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/buttons.print.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/vendors/js/tables/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/js/core/app.js')}}"></script>
    <script src="{{asset('nextassets/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js')}}"></script>


    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>

<script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#summernote').summernote({
          height: 150,
          tooltip: false
        })
      });
    </script>

<script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#summernote-desc').summernote({
          height: 150,
          tooltip: false
        })
      });
    </script>

<script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#summernote-welcome-message').summernote({
          height: 150,
          tooltip: false
        })
      });
    </script>

<script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#summernote-congratulation-message').summernote({
          height: 150,
          tooltip: false
        })
      });
    </script>
