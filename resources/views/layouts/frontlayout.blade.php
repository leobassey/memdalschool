<!DOCTYPE html>
<html class="no-js" lang="en">


<!-- Mirrored from htmldemo.net/edumall/edumall/index-course-hub.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Apr 2022 15:28:40 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Memdal Digital School of Technology</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/memdalemblem.jpg">

    <!-- CSS (Font, Vendor, Icon, Plugins & Style CSS files) -->

    <!-- Font CSS -->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&amp;display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <!-- Vendor CSS (Bootstrap & Icon Font) -->
    <link rel="stylesheet" href="{{asset('newassets/assets/css/vendor/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/vendor/edumall-icon.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/vendor/bootstrap.min.css')}}">

    <!-- Plugins CSS (All Plugins Files) -->
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/aos.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/swiper-bundle.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/jquery.powertip.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/glightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/flatpickr.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/ion.rangeSlider.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/select2.min.css')}}">

    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('newassets/assets/css/style.css')}}">

</head>

@php

use App\Course;
$courses = Course::where('course_type', 1)->orderby('is_single', 'DESC')->get();
$single_courses = Course::where('is_single', 1)->where('course_type', 1)->get();
$full_courses = Course::where('is_single', 2)->where('course_type', 1)->get();

@endphp

<body>

    <main class="main-wrapper">

        <!-- Header start -->
        <div class="header-section header-sticky">

            <!-- Header Top Start -->
            <div class="header-top d-none d-sm-block">
                <div class="container">

                    <!-- Header Top Bar Wrapper Start -->
                    <div class="header-top-bar-wrap d-sm-flex justify-content-between">

                        <div class="header-top-bar-wrap__info">
                            <ul class="header-top-bar-wrap__info-list">
                                <li><a href="tel:+8819906886"><i class="fas fa-phone"></i> <span class="info-text">+234 (0) 7062274671</span></a></li>
                                <li><a href="mailto:agency@example.com"><i class="far fa-envelope"></i> <span class="info-text">support@memdalschool.com</span></a></li>
                            </ul>
                        </div>

                        <div class="header-top-bar-wrap__info d-sm-flex">




                            @if (Auth::check() && Auth::user()->role->id ==3)

                            <ul class="header-top-bar-wrap__info-list d-none d-lg-flex">
                                <li><a href="{{route('classroom')}}">My classroom</a></li>

                            </ul>



                            @elseif(Auth::check() && Auth::user()->role->id ==1)

                            <ul class="header-top-bar-wrap__info-list d-none d-lg-flex">
                                <li> <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                                    <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    <i class="ft-power"></i> Logout
                                </a></li>

                            </ul>




                                @elseif(Auth::check() && Auth::user()->role->id ==2)

                                <ul class="header-top-bar-wrap__info-list d-none d-lg-flex">
                                    <li> <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                                        <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                        <i class="ft-power"></i> Logout
                                    </a></li>

                                </ul>

                                @else

                                 <ul class="header-top-bar-wrap__info-list d-none d-lg-flex">
                                <li><a href="{{route('login')}}">Log in</a></li>
                                <li><a href="{{route('register')}}">Register</button></li>
                            </ul>

                            @endif







                            <ul class="header-top-bar-wrap__info-social">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="https://www.linkedin.com/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>

                    </div>
                    <!-- Header Top Bar Wrapper End -->

                </div>
            </div>
            <!-- Header Top End -->

            <!-- Header Main Start -->
            <div class="header-main">
                <div class="container">
                    <!-- Header Main Wrapper Start -->
                    <div class="header-main-wrapper">

                        <!-- Header Logo Start -->
                        <div class="header-logo">
                            <a class="header-logo__logo" href="{{route('welcome')}}">
                               <!-- <h6 style="font-family: 'Poppins', sans-serif; font-size:25px">Memdal</h6>-->
                               <img src="{{asset('img/logo.png')}}" alt="Logo" width="140px">
                            </a>
                        </div>
                        <!-- Header Logo End -->



                        <!-- Header Inner Start -->
                        <div class="header-inner">



                            <!-- Header Navigation Start -->
                            <div class="header-navigation d-none d-xl-block">
                                <nav class="menu-primary">
                                    <ul class="menu-primary__container">
                                        <li><a class="active" href="{{route('welcome')}}"><span>Home</span></a>


                                        <li>
                                            <a href="#"><span>Courses</span></a>
                                            <ul class="sub-menu">

                                                <li><a href="{{route('onlinecourses')}}"><span>Online courses</span></a></li>
                                                <!--@foreach($courses as $course)
                                                                 <li><a href="{{route('details', $course->slug)}}"><span>{{$course->course_title}}</span></a></li>

                                                         @endforeach-->




                                            </ul>
                                        </li>

                                       <!-- <li>
                                            <a href="#"><span> Bootcamps</span></a>
                                        </li>-->

                                       <!-- <li>
                                            <a href="#"><span> For Companies</span></a>
                                        </li>-->

                                       <!-- <li>
                                            <a href="#"><span> For Students</span></a>
                                        </li>-->

                                    </ul>
                                </nav>
                            </div>
                            <!-- Header Navigation End -->





                                </div>
                                <!-- Header Mini Cart End -->

                            </div>
                            <!-- Header Mini Cart End -->

                            <!-- Header Mobile Toggle Start -->
                            <div class="header-toggle">
                                <button class="header-toggle__btn d-xl-none" data-bs-toggle="offcanvas" data-bs-target="#offcanvasMobileMenu">
                                    <span class="line"></span>
                                    <span class="line"></span>
                                    <span class="line"></span>
                                </button>
                                <button class="header-toggle__btn search-open d-flex d-md-none">
                                    <span class="dots"></span>
                                    <span class="dots"></span>
                                    <span class="dots"></span>
                                </button>
                            </div>
                            <!-- Header Mobile Toggle End -->

                        </div>
                        <!-- Header Inner End -->

                    </div>
                    <!-- Header Main Wrapper End -->
                </div>
            </div>
            <!-- Header Main End -->

        </div>
        <!-- Header End -->


    @yield('content')



        <!-- Offcanvas Start -->
        <div class="offcanvas offcanvas-end offcanvas-mobile" id="offcanvasMobileMenu" style="background-image: url(newassets/assets/images/mobile-bg.jpg);">
            <div class="offcanvas-header bg-white">
                <div class="offcanvas-logo">
                    <a class="offcanvas-logo__logo" href="{{route('welcome')}}"><img src="{{asset('img/logo.png')}}" alt="Logo"></a>
                </div>
                <button type="button" class="offcanvas-close" data-bs-dismiss="offcanvas"><i class="fal fa-times"></i></button>
            </div>

            <div class="offcanvas-body">
                <nav class="canvas-menu">
                    <ul class="offcanvas-menu">

                            <li><a class="active" href="{{route('welcome')}}"><span>Home</span></a>


                            <li>
                                <a href="#"><span>Courses</span></a>
                                <ul class="sub-menu">

                                    <li><a href="{{route('onlinecourses')}}"><span>Online courses</span></a></li>
                                    <!--@foreach($courses as $course)
                                                     <li><a href="{{route('details', $course->slug)}}"><span>{{$course->course_title}}</span></a></li>

                                             @endforeach-->




                                </ul>
                            </li>

                            <li>
                                <a href="#"><span> Bootcamps</span></a>
                            </li>

                            <li>
                                <a href="#"><span> For Companies</span></a>
                            </li>

                            <li>
                                <a href="#"><span> For Students</span></a>
                            </li>

                        </ul>

                        @if (Auth::check() && Auth::user()->role->id ==3)

                        <ul class="offcanvas-menu">
                            <li><a href="{{route('classroom')}}">My classroom</a></li>

                        </ul>



                        @elseif(Auth::check() && Auth::user()->role->id ==1)

                        <ul class="offcanvas-menu">
                            <li> <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                                <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                <i class="ft-power"></i> Logout
                            </a></li>

                        </ul>




                            @elseif(Auth::check() && Auth::user()->role->id ==2)

                            <ul class="offcanvas-menu">
                                <li> <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                                    <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    <i class="ft-power"></i> Logout
                                </a></li>

                            </ul>

                            @else

                             <ul class="offcanvas-menu">
                                <div class="offcanvas-user d-lg-none">
                                    <div class="offcanvas-user__button">
                                        <a class="offcanvas-user__login btn btn-secondary btn-hover-secondarys" href="{{route('login')}}">Log In</a>
                                    </div>
                                    <div class="offcanvas-user__button">
                                        <a class="offcanvas-user__signup btn btn-primary btn-hover-primary" href="{{route('register')}}">Sign Up</a>
                                    </div>
                                </div>
                        </ul>

                        @endif



                    </nav>
                </nav>
            </div>








            <!-- Header User Button End -->

        </div>
        <!-- Offcanvas End -->
     <!-- Footer Start -->
     <div class="footer-section footer-section footer-bg-1">

<!-- Footer Widget Area Start -->
<div class="footer-widget-area section-padding-01">
    <div class="container">
        <div class="row gy-6">

            <div class="col-lg-4">
                <!-- Footer Widget Start -->
                <div class="footer-widget">

                    <div class="footer-widget__info" style="margin-top: 70px">
                        <a href="#" class="footer-widget__logo"><img src="{{asset('img/logo.png')}}" alt="Logo" width="140px" style="width: 140px"></a>




                    </div>



                </div>
                <!-- Footer Widget End -->
            </div>

            <div class="col-lg-8">
                <div class="row gy-6">


                    <div class="col-sm-4">
                        <!-- Footer Widget Start -->
                        <div class="footer-widget">
                            <h4 class="footer-widget__title">What's New</h4>

                            <ul class="footer-widget__link">

                                <!--
                                <li><a href="#">Gallery</a></li>
                                <li><a href="#">Career</a></li>-->
                            </ul>
                        </div>
                        <!-- Footer Widget End -->
                    </div>

                    <div class="col-sm-4">
                        <!-- Footer Widget Start -->
                        <div class="footer-widget">
                            <h4 class="footer-widget__title">Quick Links</h4>

                            <ul class="footer-widget__link">
                                <li><a href="{{route('welcome')}}">Home</a></li>
                               <!-- <li><a href="#">Internship</a></li>-->
                               <!-- <li><a href="#">Hire</a></li>-->

                            </ul>
                        </div>
                        <!-- Footer Widget End -->
                    </div>

                    <div class="col-sm-4">
                        <!-- Footer Widget Start -->
                        <div class="footer-widget">
                            <h4 class="footer-widget__title">Courses</h4>

                            <ul class="footer-widget__link">

                                <li><a href="{{route('onlinecourses')}}"><span>Online courses</span></a></li>
                           <!--@foreach($courses as $course)
                                            <li><a href="{{route('details', $course->slug)}}"><span>{{$course->course_title}}</span></a></li>

                                    @endforeach-->
                            </ul>
                        </div>
                        <!-- Footer Widget End -->
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<!-- Footer Widget Area End -->

<!-- Footer Copyright Area End -->
<div class="footer-copyright">
    <div class="container">
        <div class="copyright-wrapper text-center">
            <ul class="footer-widget__link flex-row gap-8 justify-content-center">
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Privacy Policy</a></li>
            </ul>
            <p class="footer-widget__copyright mt-0">&copy; 2022 (c) <span>All right reserve</span></p>
        </div>
    </div>
</div>
<!-- Footer Copyright Area End -->

</div>
<!-- Footer End -->

<!--Back To Start-->
<button id="backBtn" class="back-to-top backBtn">
<i class="arrow-top fal fa-long-arrow-up"></i>
<i class="arrow-bottom fal fa-long-arrow-up"></i>
</button>
<!--Back To End-->


</main>

<!-- Log In Modal Start -->
<div class="modal fade" id="loginModal">
<div class="modal-dialog modal-dialog-centered modal-login">

<!-- Modal Wrapper Start -->
<div class="modal-wrapper">
    <button class="modal-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>

    <!-- Modal Content Start -->
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Login</h5>
            <p class="modal-description">Don't have an account yet? <button data-bs-toggle="modal" data-bs-target="#registerModal">Sign up for free</button></p>
        </div>
        <div class="modal-body">
            <form action="#">
                <div class="modal-form">
                    <label class="form-label">Username or email</label>
                    <input type="text" class="form-control" placeholder="Your username or email">
                </div>
                <div class="modal-form">
                    <label class="form-label">Password</label>
                    <input type="password" class="form-control" placeholder="Password">
                </div>
                <div class="modal-form d-flex justify-content-between flex-wrap gap-2">
                    <div class="form-check">
                        <input type="checkbox" id="rememberme">
                        <label for="rememberme">Remember me</label>
                    </div>
                    <div class="text-end">
                        <a class="modal-form__link" href="#">Forgot your password?</a>
                    </div>
                </div>
                <div class="modal-form">
                    <button class="btn btn-primary btn-hover-secondary w-100">Log In</button>
                </div>
            </form>

            <div class="modal-social-option">
                <p>or Log-in with</p>

                <ul class="modal-social-btn">
                    <li><a href="#" class="btn facebook"><i class="fab fa-facebook-square"></i> Gacebook</a></li>
                    <li><a href="#" class="btn google"><i class="fab fa-google"></i> Google</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Modal Content End -->

</div>
<!-- Modal Wrapper End -->

</div>
</div>
<!-- Log In Modal End -->

<!-- Log In Modal Start -->
<div class="modal fade" id="registerModal">
<div class="modal-dialog modal-dialog-centered modal-register">

<!-- Modal Wrapper Start -->
<div class="modal-wrapper">
    <button class="modal-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>

    <!-- Modal Content Start -->
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Sign Up</h5>
            <p class="modal-description">Already have an account? <button data-bs-toggle="modal" data-bs-target="#loginModal">Log in</button></p>
        </div>
        <div class="modal-body">

            <form action="#">
                <div class="row gy-5">
                    <div class="col-md-6">
                        <div class="modal-form">
                            <label class="form-label">First Name</label>
                            <input type="text" class="form-control" placeholder="First Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="modal-form">
                            <label class="form-label">Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="modal-form">
                            <label class="form-label">Username</label>
                            <input type="text" class="form-control" placeholder="username">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="modal-form">
                            <label class="form-label">Email</label>
                            <input type="text" class="form-control" placeholder="Your Email">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="modal-form">
                            <label class="form-label">Password</label>
                            <input type="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="modal-form">
                            <label class="form-label">Re-Enter Password</label>
                            <input type="password" class="form-control" placeholder="Re-Enter Password">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="modal-form form-check">
                            <input type="checkbox" id="privacy">
                            <label for="privacy">Accept the Terms and Privacy Policy</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="modal-form">
                            <button class="btn btn-primary btn-hover-secondary w-100">Register</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- Modal Content End -->

</div>
<!-- Modal Wrapper End -->

</div>
</div>
<!-- Log In Modal End -->

<!-- Edumall Demo Option Start -->
<div class="edumall-demo-option">


<div class="edumall-demo-option__panel">



<div class="edumall-demo-option__body">
    <!-- <div class="edumall-demo-option-item">
    <a href="" data-bs-tooltip="tooltip" data-bs-placement="top" title="Landing Page">
        <img src="assets/images/demo/landing.jpg" alt="">
    </a>
</div> -->
    <div class="edumall-demo-option-item">
        <a href="index-main.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Main Demo">
            <img src="assets/images/demo/home-01.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-course-hub.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Course Hub">
            <img src="assets/images/demo/home-02.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-online-academy.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Online Academy">
            <img src="assets/images/demo/home-03.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-education-center.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Education Center">
            <img src="assets/images/demo/home-04.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-university.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="University">
            <img src="assets/images/demo/home-05.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-language-academic.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Language Academic">
            <img src="assets/images/demo/home-06.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-single-instructor.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Single Instructor">
            <img src="assets/images/demo/home-07.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-dev.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Dev">
            <img src="assets/images/demo/home-08.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
    <div class="edumall-demo-option-item">
        <a href="index-online-art.html" data-bs-tooltip="tooltip" data-bs-placement="top" title="Online Art">
            <img src="assets/images/demo/home-09.jpg" alt="Home" width="130" height="158">
        </a>
    </div>
</div>

</div>

</div>






    <!-- JS Vendor, Plugins & Activation Script Files -->

   <!-- Vendors JS -->
   <script src="{{asset('newassets/assets/js/vendor/modernizr-3.11.7.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/vendor/jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/vendor/jquery-migrate-3.3.2.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/vendor/bootstrap.bundle.min.js')}}"></script>

    <!-- Plugins JS -->
    <script src="{{asset('newassets/assets/js/plugins/aos.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/parallax.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/swiper-bundle.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/jquery.powertip.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/nice-select.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/glightbox.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/jquery.sticky-kit.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/masonry.pkgd.min.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/flatpickr.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/range-slider.js')}}"></script>
    <script src="{{asset('newassets/assets/js/plugins/select2.min.js')}}"></script>


    <!-- Activation JS -->
    <script src="{{asset('newassets/assets/js/main.js')}}"></script>


        <script>
        $("#pdo").submit(function() {

            $(".submit").attr("disabled", true);
            $(".submit").text("Please wait processing ...");
        });
        $("#pdo2").submit(function() {

        $(".submit2").attr("disabled", true);
        $(".submit2").text("Please wait processing ...");
        });

        $("#pdo3").submit(function() {

        $(".submit3").attr("disabled", true);
        $(".submit3").text("Please wait processing ...");
        });

        </script>

</body>

</html>
