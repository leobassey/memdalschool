<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> @yield('title')</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/memdalemblem.jpg">

    <!-- CSS (Font, Vendor, Icon, Plugins & Style CSS files) -->

    <!-- Font CSS -->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&amp;display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <!-- Vendor CSS (Bootstrap & Icon Font) -->
    <link rel="stylesheet" href="{{asset('newassets/assets/css/vendor/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/vendor/edumall-icon.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/vendor/bootstrap.min.css')}}">

    <!-- Plugins CSS (All Plugins Files) -->
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/aos.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/swiper-bundle.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/jquery.powertip.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/glightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/flatpickr.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/ion.rangeSlider.min.css')}}">
    <link rel="stylesheet" href="{{asset('newassets/assets/css/plugins/select2.min.css')}}">

    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('newassets/assets/css/style.css')}}">

</head>
<body class="dashboard-page dashboard-nav-fixed">
<!-- Dashboard Nav Start -->
<div class="dashboard-nav offcanvas offcanvas-start" id="offcanvasDashboard">

    <!-- Dashboard Nav Wrapper Start -->
    <div class="dashboard-nav__wrapper">
        <!-- Dashboard Nav Header Start -->
        <div class="offcanvas-header dashboard-nav__header dashboard-nav-header">

            <div class="dashboard-nav__toggle d-xl-none">
                <button class="toggle-close" data-bs-dismiss="offcanvas"><i class="fal fa-times"></i></button>
            </div>

            <div class="dashboard-nav__logo">
                <a class="logo" href="#"><img src="{{asset('img/logo.png')}}" alt="Logo" width="120" height="50"></a>
            </div>

        </div>
        <!-- Dashboard Nav Header End -->

        <!-- Dashboard Nav Content Start -->
        <div class="offcanvas-body dashboard-nav__content navScroll">

            <!-- Dashboard Nav Menu Start -->
            <div class="dashboard-nav__menu">
                <ul class="dashboard-nav__menu-list">
                    <li>

                        <a href="{{route('classroom')}}" activeRoute="{{ request()->routeIs('classroom')}}">
                            <i class="edumi edumi-layers"></i>
                            <span class="text">Dashboard</span>
                            </a>

                    </li>


                    <li class="{{ Route::currentRouteNamed('mycourses') || Route::currentRouteNamed('completedcourses') || Route::currentRouteNamed('lockedcourses') ? "active" : "" }}">
                        <a class="menu-item" href="{{route('mycourses')}}">
                            <i class="edumi edumi-open-book"></i>
                            <span class="text">My Courses</span>
                        </a>
                    </li>








                   <!-- <li>
                        <a href="dashboard-wishlist.html">
                            <i class="edumi edumi-heart"></i>
                            <span class="text">Wishlist</span>
                        </a>
                    </li>-->
                    <li>
                        <a href="{{route('mycertificates')}}" activeRoute="{{ request()->routeIs('mycertificates')}}">


                            <i class="edumi edumi-star"></i>
                            <span class="text">Certificate</span>
                        </a>
                    </li>
                    <!--<li>
                        <a href="dashboard-my-quiz-attempts.html">
                            <i class="edumi edumi-help"></i>
                            <span class="text">My Quiz Attempts</span>
                        </a>
                    </li>-->
                    <li>
                        <a href="{{route('orderhistory')}}" activeRoute="{{ request()->routeIs('orderhistory')}}">
                            <i class="edumi edumi-shopping-cart"></i>
                            <span class="text">Purchase History</span>
                        </a>
                    </li>
                </ul>
                <ul class="dashboard-nav__menu-list">

                    <li class="{{ Route::currentRouteNamed('profile') || Route::currentRouteNamed('changepassword') ? "active" : "" }}">
                        <a class="menu-item" href="{{route('profile')}}">
                            <i class="edumi edumi-settings"></i>
                            <span class="text">Settings</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('supports')}}">
                            <i class="edumi edumi-users"></i>
                            <span class="text">Support</span>
                        </a>
                    </li>


                    <li>

                        <a href="{{ route('logout') }}" class="dropdown-item"  onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                            <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            <i class="edumi edumi-sign-out"></i>
                            <span class="text">Logout</span>
                        </a>


                    </li>
                </ul>
            </div>
            <!-- Dashboard Nav Menu End -->

        </div>
        <!-- Dashboard Nav Content End -->

        <div class="offcanvas-footer"></div>
    </div>
    <!-- Dashboard Nav Wrapper End -->

</div>
<!-- Dashboard Nav End -->





 <!-- Dashboard Main Wrapper Start -->
 <main class="dashboard-main-wrapper">

    <!-- Dashboard Header Start -->
    <div class="dashboard-header">
        <div class="container">
            <!-- Dashboard Header Wrapper Start -->
            <div class="dashboard-header__wrap">

                <div class="dashboard-header__toggle-menu d-xl-none">
                    <button class="toggle-btn" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDashboard">
                        <svg width="20px" height="18px" viewBox="0 0 20 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path d="M18.7179688,2.60581293 L1.28207031,2.60581293 C0.573828125,2.60581293 0,2.02491559 0,1.30798783 C0,0.591060076 0.573828125,0.0101231939 1.28207031,0.0101231939 L18.7179688,0.0101231939 C19.4261719,0.0101231939 20,0.591020532 20,1.30798783 C20,2.02495513 19.4261719,2.60581293 18.7179688,2.60581293 Z"></path>
                            <path d="M11.5384766,10.5957293 L1.28207031,10.5957293 C0.573828125,10.5957293 2.91322522e-13,10.0147924 2.91322522e-13,9.29786464 C2.91322522e-13,8.58093688 0.573828125,8 1.28207031,8 L11.5384766,8 C12.2466797,8 12.8205469,8.58089734 12.8205469,9.29786464 C12.8205469,10.0148319 12.2466797,10.5957293 11.5384766,10.5957293 Z"></path>
                            <path d="M18.7179688,17.6 L1.28207031,17.6 C0.573828125,17.6 0,17.0628683 0,16.4 C0,15.7371317 0.573828125,15.2 1.28207031,15.2 L18.7179688,15.2 C19.4261719,15.2 20,15.7370952 20,16.4 C20,17.0628683 19.4261719,17.6 18.7179688,17.6 Z"></path>
                        </svg>
                    </button>
                </div>

                <div class="dashboard-header__user">
                    <div class="dashboard-header__user-avatar">

                        @if(!Auth::user())
                        {
                            <script>
                            window.location.href = "{{url('login')}}";
                            </script>
                        }
                        @else

                        @if(Auth::User()->photo == "")
                        <img src="{{asset('newassets/assets/images/avatar-placeholder.jpg')}}" alt="Avatar" width="80" height="80">
                        @else

                        <?php

                        $photo = Auth::User()->photo;
                        ?>

                        <img src="/{{Auth::User()->photo}}" alt="Avatar" width="80" height="80">
                        @endif

                        @endif



                    </div>
                    <div class="dashboard-header__user-info">
                        <h4 class="dashboard-header__user-name"><span class="welcome-text">Welcome back,</span>
                            @if(!Auth::user())
                            {
                                <script>
                                window.location.href = "{{url('login')}}";
                                </script>
                            }
                            @else

                              {{Auth::User()->name}}

                            @endif
                        </h4>
                    </div>
                </div>

                <div class="dashboard-header__btn">
                    <a class="btn btn-outline-primary" href="{{route('onlinecourses')}}"><i class="edumi edumi-user"></i> <span class="text">Add course</span></a>
                </div>



            </div>
            <!-- Dashboard Header Wrapper End -->
        </div>
    </div>
    <!-- Dashboard Header End -->



    @yield('content')



    </main>

     <!-- Vendors JS -->
   <script src="{{asset('newassets/assets/js/vendor/modernizr-3.11.7.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/vendor/jquery-3.6.0.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/vendor/jquery-migrate-3.3.2.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/vendor/bootstrap.bundle.min.js')}}"></script>

   <!-- Plugins JS -->
   <script src="{{asset('newassets/assets/js/plugins/aos.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/parallax.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/swiper-bundle.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/perfect-scrollbar.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/jquery.powertip.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/nice-select.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/glightbox.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/jquery.sticky-kit.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/imagesloaded.pkgd.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/masonry.pkgd.min.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/flatpickr.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/range-slider.js')}}"></script>
   <script src="{{asset('newassets/assets/js/plugins/select2.min.js')}}"></script>


   <!-- Activation JS -->
   <script src="{{asset('newassets/assets/js/main.js')}}"></script>


   <script>
    $("#pdo").submit(function() {

        $(".submit").attr("disabled", true);
        $(".submit").text("Please wait processing ...");
    });
    $("#pdo2").submit(function() {

    $(".submit2").attr("disabled", true);
    $(".submit2").text("Please wait processing ...");
    });

    $("#pdo3").submit(function() {

    $(".submit3").attr("disabled", true);
    $(".submit3").text("Please wait processing ...");
    });

    </script>
</body>
</html>
