<!DOCTYPE html>
<html lang="en">




<head>
    <meta charset="UTF-8">

    <!-- Title-->
    <title>@yield('title')</title>

    <!-- SEO Meta-->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <!-- viewport scale-->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!-- Favicon and Apple Icons-->
    <!--<link rel="icon" type="image/x-icon" href="{{asset('img/fidaslogo.png')}}">-->


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Favicon -->
     <!--<link rel="shortcut icon" href="https://htmlstream.com/preview/front-v3.2.2/favicon.ico">-->

<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;display=swap" rel="stylesheet">



<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="{{asset('webassets/vendor/hs-mega-menu/dist/hs-mega-menu.min.css')}}">

<!-- CSS Front Template -->
<link rel="stylesheet" href="{{asset('webassets/css/theme.css')}}">




<!-- Icon fonts -->
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />


<!-- stylesheet-->
<link rel="stylesheet" href="{{asset('webassets/css/vendors.bundle.css')}}">

<link rel="stylesheet" href="{{asset('webassets/css/responsive.css')}}" />
<link rel="stylesheet" href="{{asset('webassets/css/owl.theme.default.min.css')}}" />

<link rel="stylesheet" href="{{asset('webassets/css/style2.css')}}" />
<!--
<link rel="stylesheet" href="{{asset('webassets/css/style3.css')}}" />-->


<style>
    [data-dark-overlay],
    [data-primary-overlay] {
        position: relative;
        z-index: 1
    }

    [data-dark-overlay]:after,
    [data-primary-overlay]:after {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: -1
    }

    [data-dark-overlay="1"]:after {
        background: rgba(0, 0, 0, 0.1)
    }

    [data-dark-overlay="2"]:after {
        background: rgba(0, 0, 0, 0.2)
    }

    [data-dark-overlay="3"]:after {
        background: rgba(0, 0, 0, 0.3)
    }

    [data-dark-overlay="4"]:after {
        background: rgba(0, 0, 0, 0.4)
    }

    [data-dark-overlay="5"]:after {
        background: rgba(0, 0, 0, 0.5)
    }

    [data-dark-overlay="6"]:after {
        background: rgba(0, 0, 0, 0.6)
    }

    [data-dark-overlay="7"]:after {
        background: rgba(0, 0, 0, 0.7)
    }

    [data-dark-overlay="8"]:after {
        background: rgba(0, 0, 0, 0.8)
    }

    [data-dark-overlay="9"]:after {
        background: rgba(0, 0, 0, 0.9)
    }

    .homepage .educators {
        padding: 43px 0 40px;
    }

    .section--accent {
        background-color: #000;
        padding: 40px 0;
        color: #fdd76e;
    }

    .photo-overlay {
        border-radius: 0px;
        color: #fff;
        position: relative;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        padding-left: 20px;
        padding-right: 20px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        transform: scale(1);
        transition: all 0.3s ease-in-out;
        box-sizing: border-box;
        transition: opacity .3s, ease-in-out;
        visibility: visible;
        font-family: 'Libre Franklin', sans-serif;
        padding-top: 6.25rem;
        padding-bottom: 6.25rem;
        height: 500px;
    }

    .mainOverlay {
        /*  background-color: #000;*/
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }

    .overlayText {
        font-weight: bold;
        position: absolute;
        color: #fff;
        opacity: 1;
    }
</style>









</head>

<body>



@php

use App\Course;
$courses = Course::where('course_type', 1)->orderby('is_single', 'DESC')->get();
$single_courses = Course::where('is_single', 1)->where('course_type', 1)->get();
$full_courses = Course::where('is_single', 2)->where('course_type', 1)->get();

@endphp

<header id="header" class=" container header header-bg-transparent-lg header-white-nav-links-lg header-abs-top-lg" data-hs-header-options='{
            "fixMoment": 1000,
            "fixEffect": "slide"
          }'>



        <div class="header-section">



            <div id="logoAndNav" class="container">
                <!-- Nav -->
                <nav class=" js-mega-menu navbar navbar-expand-lg fixed-top container-fluid pl-5 pr-5 pt-0 pb-0">
                    <!-- White Logo -->
                    <div class="container mt-2">
                        <a class="navbar-brand navbar-brand-default" href="{{route('welcome')}}" aria-label="Front">
                            <!--<img src="{{asset('webassets/img/LOGO.png')}}" alt="Logo">-->
                            <h2 style="color: #fff; font-weight:900">Memdal</h2>
                        </a>
                        <!-- End White Logo -->

                        <!-- Default Logo -->
                        <a class="navbar-brand navbar-brand-collapsed" href="{{route('welcome')}}" aria-label="Front">
                            <!--<img src="{{asset('webassets/img/LOGO.png')}}" alt="Logo">-->
                            <h2 style="color: #fff; font-weight:900">Memdal</h2>
                           <!-- <h2 style="color: #fff;"><span style="color:#fdd76e; font-weight:bold">M</span>emdal</h2>-->
                        </a>
                        <!-- End Default Logo -->

                        <!-- Responsive Toggle Button -->
                        <button type="button" class="text-white navbar-toggler btn btn-icon btn-sm rounded-circle" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
            <span class="navbar-toggler-default">
                <i class="fas fa-bars fa-2x"></i>
            </span>
            <span class="navbar-toggler-toggled">
                <i class="far fa-times-circle fa-2x"></i>
            </span>
          </button>
                        <!-- End Responsive Toggle Button -->

                        <!-- Navigation -->
                        <div id="navBar" class="collapse navbar-collapse">

                            <ul class="navbar-nav">

                                <!-- Home -->
                                <!--   <li class="nav-item dropdown mr-3">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Bootcamp
                                </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="single-course.html">Data Science and Analytics Training</a>
                                        <a class="dropdown-item" href="single-course.html">Advanced Microsoft Excel Training</a>
                                        <a class="dropdown-item" href="single-course.html">Fullstack Development Training</a>
                                        <a class="dropdown-item" href="single-course.html">Web Development Training</a>
                                        <a class="dropdown-item" href="single-course.html">Laravel Training</a>
                                        <a class="dropdown-item" href="single-course.html">Vue js Training</a>
                                        <a class="dropdown-item" href="single-course.html">Python for Data Science Training</a>
                                        <a class="dropdown-item" href="single-course.html">Database Management Training</a>
                                        <a class="dropdown-item" href="single-course.html">Microsoft Power BI Training</a>


                                    </div>
                                </li>-->

                                <li class="nav-item dropdown mr-3">
                                    <a class="nav-link" href="{{route('welcome')}}" id="" role="button"  >
                                  Home
                                </a>
                                    <!--<div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Our Story</a>
                                        <a class="dropdown-item" href="#">Career@memdal</a>
                                        <a class="dropdown-item" href="#">Testimonials</a>
                                        <a class="dropdown-item" href="#">Media</a>

                                        <a class="dropdown-item" href="#">Affiliate Program</a>
                                        <a class="dropdown-item" href="#">Store</a>
                                        <a class="dropdown-item" href="#">Upcoming Webinars</a>
                                        <a class="dropdown-item" href="#">Career Guides</a>

                                    </div>-->
                                </li>

                                <li class="nav-item dropdown mr-3">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Courses
                                </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    @foreach($courses as $course)
                                        <a class="dropdown-item" href="{{route('details', $course->slug)}}">{{$course->course_title}}</a>
                                    @endforeach
                                        <!--<a class="dropdown-item" href="single-course.html">Advanced Microsoft Excel Training</a>
                                        <a class="dropdown-item" href="single-course.html">Fullstack Development Training</a>
                                        <a class="dropdown-item" href="single-course.html">Web Development Training</a>
                                        <a class="dropdown-item" href="single-course.html">Laravel Training</a>
                                        <a class="dropdown-item" href="single-course.html">Vue js Training</a>
                                        <a class="dropdown-item" href="single-course.html">Python for Data Science Training</a>
                                        <a class="dropdown-item" href="single-course.html">Database Management Training</a>
                                        <a class="dropdown-item" href="single-course.html">Microsoft Power BI Training</a>-->
                                    </div>
                                </li>
                               <!-- <li class="nav-item dropdown mr-3">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  For Companies
                                </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Corporate Training</a>
                                        <a class="dropdown-item" href="#">Hire Memdal Graduates</a>
                                        <a class="dropdown-item" href="#">Verify Certificate</a>

                                    </div>
                                </li>-->

                                <li class="nav-item mr-3">
                                    <a class="nav-link" href="#">Hire Graduates </a>
                                </li>


                                @if (Auth::check() && Auth::user()->role->id ==3)
                <li class="list-inline-item mr-0 p-md-3 p-2 login-li border-white-0_1">
                        <a href="{{route('classroom')}}" class="btn btn-primary" style="background-color:#0d0033; color: #fff; border-color:#0d0033">My Dashboard</a>
                    </li>
                @elseif(Auth::check() && Auth::user()->role->id ==1)
                <li class="list-inline-item mr-0 p-md-3 p-2 login-li border-white-0_1">

                <a href="{{ route('logout') }}" class="btn btn-primary" style="background-color:#0d0033; color: #fff; border-color:#0d0033" onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                                <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                <i class="ft-power"></i> Logout
                            </a>

                    </li>

                    @elseif(Auth::check() && Auth::user()->role->id ==2)
                <li class="list-inline-item mr-0 p-md-3 p-2 login-li border-white-0_1">
                <a href="{{ route('logout') }}" class="btn btn-primary" style="background-color:#0d0033; color: #fff; border-color:#0d0033" onclick="event.preventDefault(); document.getElementById('logout-fm').submit();">
                                <form id="logout-fm" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                <i class="ft-power"></i> Logout
                            </a>
                    </li>

                    @else
                    <li class="nav-item mr-3">
                        <a href="{{route('login')}}"  class="btn btn-primary" style="background-color:#0d0033; color: #fff; border-color:#0d0033">Log In</a>
                    </li>

                @endif


                               <!--<li class="nav-item mr-3">
                                    <a class="nav-link" href="login.html">Login </a>
                                </li>-->





                               <!-- <li class="navbar-nav-last-item">
                                    <a class="headBtn btn btn-md" href="{{route('apply')}}">Apply Now</a>

                                </li>-->








                                <!-- Button -->
                                <!--<li class="navbar-nav-last-item">
                                    <a class="headBtn btn btn-md" href="signup.html">
                                    Sign Up
                                </a>
                                </li>-->
                                <!-- End Button -->
                            </ul>
                        </div>
                        <!-- End Navigation -->

                    </div>
                </nav>
                <!-- End Nav -->
            </div>
        </div>
    </header>
    <div>
    @yield('content')
    </div>

    <footer style="background-color:#0d0033;" class="text-white text-center text-lg-start">
            <!-- Grid container -->

            <!-- Copyright -->
            <div class="text-center p-3 mt-5" style="background-color: #00407d">
                Copyright © Memdal Training:
                <a class="footer-link" href="#">All rights reserved</a>
            </div>
            <!-- Copyright -->
        </footer>
    </main>
    <!-- ========== END MAIN CONTENT ========== -->




    <!-- Go to Top -->
    <a class=" js-go-to go-to position-fixed " href="javascript:; " style="visibility: hidden; " data-hs-go-to-options='{
       "offsetTop ": 700,
       "position ": {
         "init ": {
           "right ": 15
         },
         "show ": {
           "bottom ": 15
         },
         "hide ": {
           "bottom ": -15
         }
       }
     }'>
        <i class="fas fa-angle-up "></i>
    </a>
    <!-- End Go to Top -->



<script src="{{asset('webassets/vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('webassets/vendor/jquery-migrate/dist/jquery-migrate.min.js')}}"></script>
    <!-- <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js "></script> -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <!-- JS Implementing Plugins -->
    <script src="{{asset('webassets/vendor/hs-header/dist/hs-header.min.js')}}"></script>
    <script src="{{asset('webassets/vendor/hs-go-to/dist/hs-go-to.min.js')}}"></script>
    <script src="{{asset('webassets/vendor/hs-mega-menu/dist/hs-mega-menu.min.js')}}"></script>



    <!-- JS Front -->
    <script src="{{asset('webassets/js/hs.core.js')}}"></script>




    <script src="{{asset('webassets/js/vendors.bundle.js')}}"></script>
    <script src="{{asset('webassets/js/scripts.js')}}"></script>

    <script src="{{asset('webassets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('webassets/js/scripts2.js')}}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<script>
    AOS.init();
</script>

<script>
    $(document).ready(function() {
        $('.li-icon a').mouseenter(function() {
            $(this).parent().addClass('animate__animated animate__headShake')
        });
        $('.li-icon a').mouseleave(function() {
            $(this).parent().removeClass('animate__animated animate__headShake')
        });
    });

    let interval = setInterval(function(){
        let loginBtn = document.querySelector('.login-li')
        loginBtn.classList.add('animate__animated', 'animate__tada')
        loginBtn.addEventListener('animationend', function() {
            loginBtn.classList.remove('animate__animated', 'animate__tada')
        })

    }, 20000)
</script>
    <script src="{{asset('webassets/js/custom.js')}}"></script>

</body>
</html>
