<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from eduport.webestica.com/docs/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Feb 2022 07:54:55 GMT -->
<head>
	<title>Doc - Eduport Theme</title>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Webestica.com">

	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<!-- Google Font -->
<link rel="preconnect" href="https://fonts.googleapis.com/">
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;700&amp;family=Roboto:wght@400;500;700&amp;display=swap" rel="stylesheet">

	<!-- Plugins CSS -->
	<link rel="stylesheet" type="text/css" href="assets/vendor/font-awesome/css/all.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="web2/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="web2/assets/css/doc.css">
	<link rel="stylesheet" type="text/css" href="web2/assets/css/prism.css">

    <style>
        .revamped_lecture_player .course-sidebar h2 {
    padding-left: 25px;
    padding-right: 25px;
    padding-top: 25px;
    margin-top: 0;
    font-size: var(--fs1);
    line-height: var(--lh1);
    font-weight: 600;
}
    </style>

</head>

<body>

<!-- **************** MAIN CONTENT START **************** -->
<main>
<!-- Navbar top START -->
<div class="navbar-top bg-dark px-3 px-sm-4 px-md-5">
	<div class="d-flex justify-content-between align-items-center">
		<!-- Logo START -->
			<a class="navbar-brand py-3" href="index.html">
				<img class="navbar-brand-item h-30px" src="assets/images/logo.svg" alt="logo">				
			</a>
			<!-- Logo END -->

		<!-- Navbar right -->
		<div>
			<ul class="text-end list-inline m-0">
        <li class="list-inline-item me-2 me-md-4"> <a class="text-white" href="../index-9.html">Live Preview</a> </li>
        <li class="list-inline-item me-2 me-md-4"> <a class="text-white" href="https://support.webestica.com/" target="_blank">Support</a> </li>
        <li class="list-inline-item text-white"> <span class="badge bg-danger text-white">v1.1.0</span></li>
      </ul>
		</div>
	</div>
</div>
<!-- Navbar top END -->


<div class="container-fluid px-0">
	<div class="page-wrapper">
		<!-- Left sidebar START -->
		<nav class="navbar navbar-expand-sm navbar-light bg-light px-3">
		  <a class="navbar-brand py-2 d-sm-none" href="#">Doc nav</a>
		  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#docNav" aria-controls="docNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="docNav">
		    <div class="left-sidebar bg-light">
					<div class="content h-100vh">



                    <div class="course-sidebar-head" style=" padding-left: 25px;
    padding-right: 25px;
    padding-top: 25px;
    padding-bottom: 10px;
    margin-top: 0;
    font-size: var(--fs1);
    line-height: var(--lh1);
    background-color:#fff;
    font-weight: 600;">
    <h4 style="padding-bottom: 0px; font-size: 18px;" class="text-center">Courese progress</h4>
    <!-- Course Progress -->
    
    <div class="course-progress lecture-page is-at-top" style="padding-left: 25px;
    padding-right: 25px;
    text-align: center;
    margin-top: 10px !important;
    margin-bottom: 25px;
    font-size: var(--fs0);
    line-height: var(--lh0);
    font-weight: 600;
    color: var(--obsidian);

  
    border-bottom: 1px solid var(--gravel);
}

">
      <div class="progressbar">
        <div class="progressbar-fill" style="min-width: 0%; width: 100%;
    border-radius: 99px;
    -moz-border-radius: 99px;
    -webkit-border-radius: 99px;
    margin-top: 15px;
    text-align: right;
    font-size: 0.7em;
    height: 8px;
    line-height: 40px;
    padding: 0px;
    background: silver" ></div>
      </div>
      <div class="small course-progress-percentage text-center mt-2" >
        <span class="percentage">
          0%
        </span>
        COMPLETE
      </div>
    </div>
    
  </div>




                  
						<div class="list-group list-group-borderless p-3 p-md-4">
							<b class="text-dark text-uppercase mb-2">Getting Started</b>
								<a class="list-group-item" href="index.html">Introduction Introduction Troduction</a>
								<a class="list-group-item" href="gulp.html">Gulp</a>
								<a class="list-group-item" href="customization.html">Customization</a>
				        <a class="list-group-item" href="color-scheme.html">Color Scheme</a>
								<a class="list-group-item" href="logo-settings.html">Logo Settings</a>
								<a class="list-group-item" href="page-loading.html">Page Loading</a>
								<a class="list-group-item" href="fonts.html">Fonts</a>
								<a class="list-group-item" href="dark-mode.html">Dark mode</a>
								<a class="list-group-item" href="rtl.html">RTL Version</a>
				        <a class="list-group-item" href="sources-and-credits.html">Sources and Credits</a>
				        <a class="list-group-item" href="changelog.html">Changelog</a>
				        <a class="list-group-item" href="updates.html">Updates</a>
				        <a class="list-group-item" href="support.html">Support</a>
							<b class="text-dark text-uppercase mt-3 mb-2">Components</b>
								<a class="list-group-item" href="alerts.html">Alerts</a>
								<a class="list-group-item" href="avatar.html">Avatar</a>
								<a class="list-group-item" href="badges.html">Badges</a>
								<a class="list-group-item" href="breadcrumb.html">Breadcrumb</a>
								<a class="list-group-item" href="buttons.html">Buttons</a>
								<a class="list-group-item" href="card.html">Card</a>
								<a class="list-group-item" href="font-icons.html">Font icons</a>
								<a class="list-group-item" href="google-map.html">Google Map</a>
								<a class="list-group-item" href="nav-and-tabs.html">Nav and Tabs</a>
								<a class="list-group-item" href="pagination.html">Pagination</a>
								<a class="list-group-item" href="progress.html">Progress</a>
				        <a class="list-group-item" href="sticky-element.html">Sticky element</a> 
				        <a class="list-group-item" href="utility-classes.html">Utility classes</a>
							<b class="text-dark text-uppercase mt-3 mb-2">Plugins</b>
				        <a class="list-group-item" href="tiny-slider.html">Tiny Slider</a>
                <a class="list-group-item" href="video-player.html">Video player</a>
						</div>
					</div>
				</div>
		  </div>
		</nav>		
		<!-- Left sidebar END -->

        @yield('content')


</main>



<script src="web2/assets/js/bootstrap.bundle.min.js"></script>

<script src="web2/assets/js/simple-scrollbar.min.js"></script>
<script src="web2/assets/js/prism.js"></script>
<script src="web2/assets/js/clipboard.min.js"></script>
 

<script src="web2/assets/js/functions.js"></script>

</body>

</html>