<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>NIALS Student Portal - @yield('title')</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('new/app-assets/images/ico/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <style>

    </style>

	@yield('css')
  </head>
   <body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <!-- fixed-top-->
   @include('layouts.top')


    @include('layouts.menu')

	<div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        	@yield('content')
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


	<footer class="footer footer-static footer-light navbar-border navbar-shadow">
	  <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="center d-block d-md-inline-block">&copy; 2021 © Copyright Nigerian Institute of Advanced Legal Studies Postgraduate Portal</span></p>
	</footer>

	@yield('script')
  <script>
    $(document).ready(function() {
        $("#pdo").submit(function() {
        $(".spinner").removeClass("hide");
        $(".submit").attr("disabled", true);
        $(".submit").text("Please wait processing ...");
        });
    });
</script>
  </body>
</html>
