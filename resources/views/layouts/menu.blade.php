
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">



            <li class="nav-item {{ Route::currentRouteNamed('') ? "active" : "" }}">
              <a href="#"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('') ? "active" : "" }}">
              <a href="#"><i class="la la-tablet"></i><span class="menu-title" data-i18n="nav.dash.main">Personal Data</span></a>
            </li>




                 <li class="nav-item {{ Route::currentRouteNamed('') ? "active" : "" || Route::currentRouteNamed('courseregistration') ? "active" : "" || Route::currentRouteNamed('courseregistration') ? "active" : ""  }}">
              <a href="#"><i class="la la-calendar-minus-o"></i><span class="menu-title" data-i18n="nav.dash.main">Courses</span></a>
            </li>

            <li class="nav-item {{ Route::currentRouteNamed('') ? "active" : "" || Route::currentRouteNamed('examregistration') ? "active" : "" || Route::currentRouteNamed('examregistration') ? "active" : ""  }}">
              <a href="#"><i class="la la-tablet"></i><span class="menu-title" data-i18n="nav.templates.main">Exams</span></a>
            </li>

            <li class="nav-item {{ Route::currentRouteNamed('') ? "active" : "" || Route::currentRouteNamed('checkresult') ? "active" : "" || Route::currentRouteNamed('checkresult') ? "active" : ""  }}">
              <a href="#"><i class="la la-tablet"></i><span class="menu-title" data-i18n="nav.templates.main">Check Result</span></a>
            </li>



                <li class="nav-item {{ Route::currentRouteNamed('') ? "active" : "" || Route::currentRouteNamed('acceptancepayment') ? "active" : "" || Route::currentRouteNamed('feespayment') ? "active" : "" }}">
                  <a href="#"><i class="la la-bank"></i><span class="menu-title" data-i18n="nav.dash.main">Payments</span></a>
                </li>




            <li class="nav-item {{ Route::currentRouteNamed('') ? "active" : "" }}">
              <a href="#"><i class="la la-bank"></i><span class="menu-title" data-i18n="nav.dash.main">Staffs</span></a>
            </li>




        <li class=" nav-item"><a href=href="{{ route('logout') }} " onclick="event.preventDefault();
           document.getElementById('logout-form').submit();"><i class="la la-power-off"></i><span class="menu-title" data-i18n="nav.login_register_pages.main">{{ __('Logout') }}</span></a>
           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>

        </li>


      </ul>
    </div>
  </div>
