<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from eduport.webestica.com/docs/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Feb 2022 07:54:55 GMT -->
<head>
	<title>Memdal School</title>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    	<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/memdalemblem.jpg')}}">




	<!-- Google Font -->
<link rel="preconnect" href="https://fonts.googleapis.com/">
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;700&amp;family=Roboto:wght@400;500;700&amp;display=swap" rel="stylesheet">

	<!-- Plugins CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/font-awesome/css/all.min.css')}}">

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('web2/assets/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('web2/assets/css/doc.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('web2/assets/css/prism.css')}}">

    <style>
        .revamped_lecture_player .course-sidebar h2 {
    padding-left: 25px;
    padding-right: 25px;
    padding-top: 25px;
    margin-top: 0;
    font-size: var(--fs1);
    line-height: var(--lh1);
    font-weight: 600;
}
    </style>

</head>

<body>

<!-- **************** MAIN CONTENT START **************** -->
<main>
<!-- Navbar top START -->
<div class="navbar-top  px-3 px-sm-4 px-md-5" style="background-color:#1358db">
	<div class="d-flex justify-content-between align-items-center">
		<!-- Logo START -->
			<a class="navbar-brand py-3" href="#">
				<span style="color:#fff">Memdal School</span>
				<!--<img class="navbar-brand-item h-30px" src="{{asset('assets/images/logo.svg')}}" alt="logo">-->
			</a>
			<!-- Logo END -->

		<!-- Navbar right -->
		<div>
			<ul class="text-end list-inline m-0">
        <li class="list-inline-item me-2 me-md-4"> <a class="text-white" href="{{route('supports')}}" target="_blank">Support</a> </li>
        <li class="list-inline-item me-2 me-md-4"> <a class="text-white" href="{{route('classroom')}}" target="_blank">Dashboard</a> </li>

      </ul>
		</div>
	</div>
</div>
<!-- Navbar top END -->


        @yield('content')


</main>


<script src="{{asset('newassets/assets/js/vendor/modernizr-3.11.7.min.js')}}"></script>
<script src="{{asset('newassets/assets/js/vendor/jquery-3.6.0.min.js')}}"></script>
<script src="{{asset('newassets/assets/js/vendor/jquery-migrate-3.3.2.min.js')}}"></script>
<script src="{{asset('newassets/assets/js/vendor/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('web2/assets/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('web2/assets/js/simple-scrollbar.min.js')}}"></script>
<script src="{{asset('web2/assets/js/prism.js')}}"></script>
<script src="{{asset('web2/assets/js/clipboard.min.js')}}"></script>


<script src="{{asset('web2/assets/js/functions.js')}}"></script>

<script>

$("#pdo").submit(function() {
$(".submit").attr("disabled", true);
$(".submit").text("Please wait processing ...");
});

</script>


</body>

</html>
