<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <!-- Title-->
    <title>@yield('title')</title>

    <!-- SEO Meta-->
    <meta name="description" content="Education theme by EchoTheme">
    <meta name="keywords" content="HTML5 Education theme, responsive HTML5 theme, bootstrap 4, Clean Theme">
    <meta name="author" content="education">

    <!-- viewport scale-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="/assets/img/logo.png">
    <link rel="shortcut icon" href="/assets/img/logo.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/img/logo.png">


    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700%7CWork+Sans:400,500">

 <!-- Icon fonts -->
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/themify-icons/css/themify-icons.css')}}">


    <!-- stylesheet-->
    <link rel="stylesheet" href="{{asset('assets/css/vendors.bundle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/style2.css')}}" />


    <style>
        .photo-overlay {
            border-radius: 0px;
            color: #fff;
            position: relative;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            padding-left: 20px;
            padding-right: 20px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            transform: scale(1);
            transition: all 0.3s ease-in-out;
            box-sizing: border-box;
            transition: opacity .3s, ease-in-out;
            visibility: visible;
            font-family: 'Libre Franklin', sans-serif;
            padding-top: 6.25rem;
            padding-bottom: 6.25rem;
            height: 500px;
        }

        .mainOverlay {
            /*  background-color: #000;*/
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
        }

        .mainOverlay img {
            /* opacity: 0.3;*/
        }

        .overlayText {
            font-weight: bold;
            position: absolute;
            color: #fff;
            opacity: 1;
        }
    </style>

</head>

<body>


    <header class="site-header text-white-0_5" style="background-color: darkgreen; color:#f4f4f4">
        <div class="container">
            <div class="row align-items-center justify-content-between mx-0">
                <ul class="list-inline d-none d-lg-block mb-0" style="color:#f4f4f4">
                    <li class="list-inline-item mr-3">
                        <div class="d-flex align-items-center">
                            <i class="ti-email mr-2"></i>
                            <a href="#">support@farmkonnect.com</a>
                        </div>
                    </li>

                </ul>
                <ul class="list-inline mb-0" style="color:#f4f4f4; border:none">
                    <li class="list-inline-item mr-0 p-3  border-white-0_1">
                        <a href="#"><i class="ti-facebook"></i></a>
                    </li>
                    <li class="list-inline-item mr-0 p-3  border-white-0_1">
                        <a href="#"><i class="ti-twitter"></i></a>
                    </li>
                    <li class="list-inline-item mr-0 p-3  border-white-0_1">
                        <a href="#"><i class="ti-vimeo"></i></a>
                    </li>
                    <li class="list-inline-item mr-0 p-3  border-white-0_1">
                        <a href="#"><i class="ti-linkedin"></i></a>
                    </li>
                </ul>
                <ul class="list-inline mb-0" style="color:#f4f4f4">

                    <li class="list-inline-item mr-0 p-md-3 p-2  border-white-0_1">
                        <a href="#" class="btn btn-primary" style="background-color:orange; color: #fff">Free Courses</a>
                    </li>
                </ul>
            </div>

        </div>

    </header>




    <nav class="ec-nav sticky-top bg-white" style="background-color: #fff;
    box-shadow: 0 2px 10px 0 rgba(0,0,0,.1);
    position: -webkit-sticky;
    position: sticky;
    z-index: 1100;
    left: 0;
    right: 0;
    top: 0;
    width: 100%;
    transition: background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1);">
        <div class="container">
            <div class="navbar p-0 navbar-expand-lg">
                <div class="navbar-brand">
                    <a class="logo-default" href="{{route('welcome')}}"><img alt="" src="{{asset('assets/img/logo.png')}}" width="10px"></a>
                </div>
                <span aria-expanded="false" class="navbar-toggler ml-auto collapsed" data-target="#ec-nav__collapsible" data-toggle="collapse">
        <div class="hamburger hamburger--spin js-hamburger">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
      </span>
                <div class="collapse navbar-collapse when-collapsed" id="ec-nav__collapsible">
                    <ul class="nav navbar-nav ec-nav__navbar ml-auto">

                        <li class="nav-item nav-item__has-megamenu megamenu-col-2">
                            <a class="nav-link" href="#" style="font-size: .92857143rem;

                            font-style: normal;
                            font-stretch: normal;
                            line-height: normal;




                            cursor: pointer">About</a>

                        </li>

                        <li class="nav-item nav-item__has-dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" style="font-size: .92857143rem;

                            font-style: normal;
                            font-stretch: normal;
                            line-height: normal;

                            cursor: pointer">Courses</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('onlinecourses')}}" class="nav-link__list">Online Courses</a></li>
                                <li><a href="#" class="nav-link__list">Classroom Courses</a></li>
                                <li><a href="#" class="nav-link__list">Free Courses</a></li>
                               <!-- <li><a href="{{route('classroomcourses')}}" class="nav-link__list">Classroom Courses</a></li>-->
                                <!--<li><a href="{{route('freecourses')}}" class="nav-link__list">Free Courses</a></li>-->

                            </ul>
                        </li>



                       <!-- <li class="nav-item nav-item__has-dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" style="font-size: .92857143rem;

                            font-style: normal;
                            font-stretch: normal;
                            line-height: normal;




                            cursor: pointer">Enterprise</a>
                            <ul class="dropdown-menu">
                                <li><a href="online-courses.html" class="nav-link__list">Online Courses</a></li>
                                <li><a href="classroom-courses.html" class="nav-link__list">Classroom Courses</a></li>

                            </ul>
                        </li>-->

                        <li class="nav-item nav-item__has-dropdown">
                            <a class="nav-link" data-toggle="dropdown" href="#" style="font-size: .92857143rem;

                            font-style: normal;
                            font-stretch: normal;
                            line-height: normal;




                            cursor: pointer">Career</a>

                        </li>

                       <!-- <li class="nav-item nav-item__has-dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" style="font-size: .92857143rem;

                            font-style: normal;
                            font-stretch: normal;
                            line-height: normal;


                            cursor: pointer"> Blog </a>

                        </li>-->




                    </ul>
                </div>

            </div>
        </div>
        <!-- END container-->
    </nav>

    <div>
    @yield('content')
    </div>

    <footer class="site-footer">
  <div class="footer-top bg-dark text-white-0_6 pt-5 paddingBottom-100">
    <div class="container">
      <div class="row">

        <div class="col-lg-4 col-md-6 mt-5">
         <img src="{{asset('assets/img/logo.png')}}" alt="Logo" width="100px">
         <div class="margin-y-40">
           <p>
           Learn practical courses in Agricultural value chain from experts who have tons of experience in Agricuture, Technology and Business.
          </p>
         </div>

        </div>

        <div class="col-lg-5 col-md-6 mt-5">
          <h4 class="h5 text-white">Contact Us</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-3"><i class="ti-headphone mr-3"></i><a href="tel:+8801740411513">080 </a></li>
            <li class="mb-3"><i class="ti-email mr-3"></i><a href="#">support@farmkonnect.com</a></li>
            <li class="mb-3">
             <div class="media">
              <i class="ti-location-pin mt-2 mr-3"></i>
              <div class="media-body">
                <span> FarmKonnect House, Ibadan, Nigeria</span>
              </div>
             </div>
            </li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 mt-5">
          <h4 class="h5 text-white">Quick links</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-2"><a href="{{route('about')}}">About Us</a></li>
            <li class="mb-2"><a href="page-contact.html">Contact Us</a></li>
            <li class="mb-2"><a href="page-sp-student-profile.html">Students</a></li>
            <li class="mb-2"><a href="page-sp-admission-apply.html">Admission</a></li>
            <li class="mb-2"><a href="page-events.html">Events</a></li>
            <li class="mb-2"><a href="blog-card.html">Latest News</a></li>
          </ul>
        </div>


        </div>


<div class="row mt-5">
<div class="col-md-5">
<ul class="list-inline">
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href="#"><i class="ti-facebook"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href="#"><i class="ti-twitter"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href="#"><i class="ti-linkedin"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href="#"><i class="ti-pinterest"></i></a></li>
          </ul>
</div>
<div class="col-md-7">
<p class="text-white-0_5 mb-0">© 2021 FarmKonnect Academy. All rights reserved. Developed by <a href="http://memdal.com/" target="_blunk">Memdal IT Services</a></p>
</div>
</div>
      </div> <!-- END row-->


    </div> <!-- END container-->
  </div> <!-- END footer-top-->


</footer>


    <div class="scroll-top">
        <i class="ti-angle-up"></i>
    </div>

    <script src="assets/js/vendors.bundle.js"></script>
    <script src="assets/js/scripts.js"></script>

    <script src="assets/js/jquery.scrollUp.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/scripts2.js"></script>
    <script src="assets/js/custom.js"></script>
</body>
</body>
</html>
