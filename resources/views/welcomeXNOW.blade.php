@extends('layouts.weblayouts')
@section('title', 'Digital school of technology')


@section('content')
<style>

.card-img-top {
    width: 100%;
    height: 10vw;
    object-fit: cover;
}

.small-heading {
    font-weight: 600;
    font-size: 1.640625rem;
}

.partnership-process-item.right::after{
    left: 0;
    right: unset;
}

.accelerate-hiring {
    background-image: url("../assets/images/hire/bg-5.svg");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    padding: 10rem 0;
}

.recruit {
    background-image: url("../assets/images/hire/bg-2.svg");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    padding: 10rem 0;
}

.why-hire .big-heading, .recruit .big-heading {
    max-width: 550px;
    margin-left: auto;
    margin-right: auto;
}

.no-bg {
    background: none;
    padding: 10rem 0;
}

.countries > * {
    position: relative;
    z-index: 999;
}

.countries .flag {
    width: 100%;
}

.flag-slider {
    margin-top: 4rem;
}

.company-wrapper {
    position: static;
    width: 100%;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    opacity: 1;
    transition: opacity 500ms ease;
}

.features {
    padding: 6rem 0;
    background-image: url("../assets/images/section-2.svg");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    position: relative;
    z-index: 999;
}

.feature-item-card {
    text-align: center;
    padding: 3rem 2rem;
    background-color: #ffffff;
    box-shadow: 0 0 10px rgba(0, 171, 251, 0.16);
    height: 100%;
    border-radius: 10px;
}

.feature-item-card img {
    display: block;
    margin: 0 auto 1.5rem;
}

.feature-item-card h6 {
    font-size: 1.171875rem;
    font-weight: 600;
}

.feature-item-card p {
    margin: 0;
    color: #343434;
}

.join-online-session , .courses-join-online-session {
    padding: 5rem 0;
    background-image: url("../assets/images/join-free-online%402x.png");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    color: #042343;
}

.course-detail {
    padding-top: 18rem;
    padding-bottom: 8rem;
    background-image: url("../assets/images/blockchain/3rd-bg.png");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: top;
    color: #000000;
}

.course-card {
    border: 1px solid #00FFBF;
    border-radius: 10px;
    height: 100%;
    padding: 3rem 1.5rem;
    box-shadow: 1px 1px 0 rgba(0, 171, 251, 0.16);
}

.course-card h5 {
    text-align: center;
    font-weight: 600;
    font-size: 1.640625rem;
    margin-bottom: 2rem;
}

.course-detail-item {
    display: flex;
    align-items: center;
}

.course-detail-item img {
    display: block;
    margin-right: 1rem;
    width: 4rem;
}

.course-detail-item p {
    margin: 0;
}

.course-card.half:first-child {
    margin-bottom: 20px;
}

.course-card.half {
    height: calc(50% - 10px);
}

.courses-join-online-session {
    background-image: url("../assets/images/s5-bg.png");
}

.btns-wrapper {
    max-width: 250px;
    margin: 0 0 0 auto;
}

.btns-wrapper a {
    width: 100%;
    margin: 10px;
}

.join-online-session > * {
    position: relative;
    z-index: 999;
}

.join-online-session p {
    margin-bottom: 0;
    max-width: 400px;
}

.testimonials {
    padding: 6rem 0;
    background-image: url("../assets/images/section-3.svg");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    position: relative;
    z-index: 999;
}

.quote-card {
    padding: 2rem;
    border-radius: 10px;
    color: #343434;
    background-color: #ffffff;
    height: 100%;
    border: 1px solid #06AEF4;
    box-shadow: 0 20px 6px rgba(142,204,230, 0.16);
    max-width: 600px;
    margin: 0 auto;
}

.quote-card .quote-flag {
    display: block;
    height: 2.2rem;
}

.quote-card .quote-stars {
    display: block;
    height: 1.5rem;
}

.quote-card .quote {
    font-size: 0.98rem;
    margin-bottom: 0.8rem;
}

.quote-card .quote-by {
    font-size: 0.9rem;
}

.courses-banner {
    min-height: 100vh;
    width: 100%;
    background-image: url("../assets/images/banner-bg.png");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: left;
    display: flex;
    color: #000000;
    align-items: center;
}

.courses-banner.blockchain-c {
    background: #042343 !important;
    color: #ffffff;
    overflow: hidden;
}

.courses-banner.blockchain-c .banner-content-wrapper {
    max-width: 550px;
}

.learning-outcomes.blockchain-c {
    padding: 8rem 0 !important;
}

.learning-outcomes.blockchain-c .outcome-item {
    max-width: 350px;
    margin: 0 auto;
}

.learning-outcomes.blockchain-c .outcome-item img {
    height: 5rem;
}

.middle-graphic {
    height: auto !important;
    width: 100%;
    transform: translateX(35px) scale(1.4);
    margin: 0 !important;
}

.curriculum {
    padding: 8rem 0;
    background-image: url("../assets/images/s3-bg.png");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: right;
}

.target {
    padding: 8rem 0 ;
}

.curriculum .btns-wrapper {
    margin: 0 auto;
}

.curriculum-feature, .target-feature {
    display: flex;
    align-items: center;
}

.curriculum-feature img, .target-feature img {
    width: 5.5rem;
    display: block;
    margin-right: 1.5rem;
}

.target-feature img {
    width: 9.5rem;
    margin-right: 3rem;
}

.curriculum-feature h5, .target-feature h5 {
    font-weight: 600;
    font-size: 1.171875rem;
    max-width: 350px;
}

.curriculum-feature p {
    color: #343434;
    margin: 0;
}

.target .feature-bg {
    position: absolute;
    top: 0;
    right: 0;
    left: 50%;
    bottom: 0;
    background-color: #F6F9FD;
    z-index: 1;
}

.target .d-flex {
    position: relative;
    z-index: 3;
}

.target .d-flex h5 {
    padding-left: 6rem;
}

.target-small-features-wrapper {
    background-color: #ffffff;
    padding: 5rem 6rem;
}

.target-small-features-wrapper .feature {
    position: relative;
    margin-bottom: 1rem;
}

.target-small-features-wrapper .feature::before {
    content: '';
    position: absolute;
    width: 8px;
    height: 8px;
    background-color: #08FABF;
    border-radius: 50%;
    left: -1.7rem;
    top: 50%;
    transform: translateY(-50%);
}

.target-small-features-wrapper .feature:last-child {
    margin-bottom: 0;
}

.mw-800 {
    max-width: 800px;
    margin-left: auto;
    margin-right: auto;
}


.join-online-session , .courses-join-online-session {
    padding: 5rem 0;
    background-image: url("../assets/images/join-free-online%402x.png");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    color: #042343;
}


</style>




<div class="jumbotron" style="border-radius: 0px; height: 550px; background-size: cover; margin-bottom:0px;background-image: url({{asset('webassets/img/1920x800/img3.jpg')}})">

            <div class="container">
               <!-- <p>Immersive 12-WeekData Science Course</p>-->
            <h1 class="text-white mb-5" style="
                    font-size: 2.65rem;
                    font-weight: 800;
                    font-style: normal;
                    font-stretch: normal;
                    line-height: 4rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff; margin-top:100px">Become a Certified Data <br/>Analyts in 8 weeks</h1>
           <p class="" style="font-size: 1.10rem;


                    font-style: normal;
                    font-stretch: normal;
                    line-height: 2rem;
                    letter-spacing: -.1px;
                    text-align: left;
                    color: #fff;
                    margin-top:-30px
                   ">
                         Join the most practical online data analytic training and <br/>learn in-demand skills with official Microsoft curriculum.
                        </p>
<br/>
                        <a href="{{route('details', 'data-analysis-with-power-bi')}}" class="btn btn-md headBtn" style="padding-left:40px;padding-right:40px; padding-top:10px;  padding-bottom:10px">Course Details</a>

            </div>
        </div>

        <section class="educators section section--accent">
            <div class="container">

                <div class="row">
                    <div class="col-md-2 " style="color: rgba(254,254, 2);">
                        Hands-on Projects
                    </div>
                    <div class="col-md-2 " style="color:  rgba(254,254, 2);">
                        Lab Practical
                    </div>

                    <div class="col-md-2 " style="color:  rgba(254,254, 2);">
                        Mentorship
                    </div>

                    <div class="col-md-2 " style="color:  rgba(254,254, 2);">
                     Internship
                    </div>
                    <div class="col-md-2 " style="color:  rgba(254,254, 2);">
                        Job Preparation
                    </div>

                    <div class="col-md-2 " style="color:  rgba(254,254, 2);">
                        Reading Materials
                    </div>
                </div>


            </div>

        </section>



        <section class="padding-y-100">
<!-- Features Section -->
<div class="container space-2">
  <div class="row justify-content-lg-between align-items-lg-center">
    <div class="col-lg-6 mb-9 mb-lg-0">
      <div class="mb-3" style="margin-top: -60px;">
      <h4 class="mb-4" style="color:#1e2022; font-weight: 800;">

       Emperical Training for the future of work
        </h4>
      </div>

      <p style="color:#333">Memdal Training offers practical training in data analysis using official Microsoft curriculumn. Our training is bent on research on the skills required by employers and the curriculum along with projects and cases are carefully curated to give you the skills you need to get your job done as a data analyst. Our trainers are certified with years of experience in both training and solutions implementation. </p>
      <p style="color:#333">Through out your learning journey you will never be alone. Memdal training primary focus is to build IT professionals through practical training bent on experience and research depth and offers expert mentorship for the future of work. </p>

     <!-- <p style="color:#333">To achieve this, Memdal offers the best training and accept students for virtual internship through our parent company for 6 months. After the six months internship, Memdal selects the very best performing students and add them to our job ready database and get them connected to our hiring partners.</p>
-->
      <div class="mt-4">
        <a class="btn btn-primary btn-wide transition-3d-hover" href="{{route('details', 'data-analysis-with-power-bi')}}" style="background-color:  rgba(254,254, 2); border-color:  rgba(254,254, 2); color:#333">Course Details</a>
      </div>
    </div>

    <div class="col-lg-5 col-xl-5">
      <!-- SVG Element -->
      <div class="position-relative min-h-500rem mx-auto" style="max-width: 28rem;">
        <figure class="position-absolute top-0 right-0 z-index-2 mr-11">
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 450 450" width="165" height="165">
            <g>
              <defs>
                <path id="circleImgID2" d="M225,448.7L225,448.7C101.4,448.7,1.3,348.5,1.3,225l0,0C1.2,101.4,101.4,1.3,225,1.3l0,0
                  c123.6,0,223.7,100.2,223.7,223.7l0,0C448.7,348.6,348.5,448.7,225,448.7z"/>
              </defs>
              <clipPath id="circleImgID1">
                <use xlink:href="#circleImgID2"/>
              </clipPath>
              <g clip-path="url(#circleImgID1)">
                <image width="450" height="450" xlink:href="img/img12.jpg" ></image>
              </g>
            </g>
          </svg>
        </figure>



        <figure class="d-none d-sm-block position-absolute top-0 left-0 mt-11">
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 515 515" width="200" height="200">
            <g>
              <defs>
                <path id="circleImgID4" d="M260,515h-5C114.2,515,0,400.8,0,260v-5C0,114.2,114.2,0,255,0h5c140.8,0,255,114.2,255,255v5
                  C515,400.9,400.8,515,260,515z"/>
              </defs>
              <clipPath id="circleImgID3">
                <use xlink:href="#circleImgID4"/>
              </clipPath>
              <g clip-path="url(#circleImgID3)">
                <image width="515" height="515" xlink:href="img/img1.jpg" transform="matrix(1 0 0 1 1.639390e-02 2.880859e-02)"></image>
              </g>
            </g>
          </svg>
        </figure>

        <figure class="position-absolute top-0 right-0" style="margin-top: 11rem; margin-right: 13rem;">
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 67 67" width="25" height="25">
            <circle fill=" rgba(254,254, 2)" cx="33.5" cy="33.5" r="33.5"/>
          </svg>
        </figure>

        <figure class="position-absolute top-0 right-0 mr-3" style="margin-top: 8rem;">
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 141 141" width="50" height="50">
            <circle fill=" rgba(254,254, 2)" cx="70.5" cy="70.5" r="70.5"/>
          </svg>
        </figure>

        <figure class="position-absolute bottom-0 right-0">
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 770.4 770.4" width="280" height="280">
            <g>
              <defs>
                <path id="circleImgID6" d="M385.2,770.4L385.2,770.4c212.7,0,385.2-172.5,385.2-385.2l0,0C770.4,172.5,597.9,0,385.2,0l0,0
                  C172.5,0,0,172.5,0,385.2l0,0C0,597.9,172.4,770.4,385.2,770.4z"/>
              </defs>
              <clipPath id="circleImgID5">
                <use xlink:href="#circleImgID6"/>
              </clipPath>
              <g clip-path="url(#circleImgID5)">
                <image width="900" height="900" xlink:href="img/img7.jpg" transform="matrix(1 0 0 1 -64.8123 -64.8055)"></image>
              </g>
            </g>
          </svg>
        </figure>
      </div>
      <!-- End SVG Element -->
    </div>
  </div>
</div>
<!-- End Features Section -->
</section>



<!--
        <section class="curriculum">
    <div class="container">
        <h5 class="small-heading mb-5">
            Memdal Data Analytic Training with Microsoft Curriculum
        </h5>
        <div class="row align-items-center">
            <div class="col-lg-6 mb-5 mb-lg-0">
                <div class="row align-items-center">
                    <div class="col-lg-6 mb-4">
                        <div class="curriculum-feature">
                            <img src="assets/images/s3-icon-1.svg" alt="Course Length">
                            <div>
                                <h5>
                                    Course Length
                                </h5>
                                <p>
                                    12 weeks
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="curriculum-feature">
                            <img src="assets/images/s3-icon-2.svg" alt="Tuition Fee">
                            <div>
                                <h5>
                                    Tuition Fee
                                </h5>
                                <p>
                                    $350 or 35,000 ksh
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4 mb-lg-0">
                        <div class="curriculum-feature">
                            <img src="assets/images/s3-icon-3.svg" alt="Mode of Delivery">
                            <div>
                                <h5>
                                    Mode of Delivery
                                </h5>
                                <p>
                                   Online
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="curriculum-feature">
                            <img src="assets/images/s3-icon-4.svg" alt="Start date">
                            <div>
                                <h5>
                                    Start date
                                </h5>
                                <p>
                                    September 6th 2021
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="btns-wrapper">
                    <a href="https://form.jotform.com/211992081625558" class="btn btn-primary mb-lg-5">Apply now!</a>
                    <a href="https://www.dropbox.com/s/r1wm5l9zh18hmai/Africa Data School.pdf?dl=0" class="btn btn-primary inverted">Download Syllabus</a>
                </div>
            </div>
        </div>
    </div>
</section>

-->
<!--
        <section class="course-lists mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="mt-5" style="
                        font-weight: 600;
                        font-style: normal;
                        font-stretch: normal;
                        letter-spacing: normal; font-size: 1.857rem;
        line-height: 2.711rem; color:#333">Are you ready for the future of work?</h3>
                        <h5 class="mb-5" style="color:#333">Our courses are practical with hands-on projects, Labs and Case studies</h5>
                    </div>
                    <div id="courses" class="owl-carousel owl-theme">
                @foreach($courses as $i => $c)
                    <div class="courses" data-aos="fade-up" data-aos-once="true" data-aos-delay="{{ $loop->iteration*100 }}">
                        <div class="courses-pack">

                            <div>
                                <div class="mainOverlay">

                                @if($c->image_url == "")
                        <img class="img-responsive card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
                        @else
							<img class="img-responsive card-img-top" src="{{$c->image_url}}" alt="course image"/>
                        @endif


                                </div>
                            </div>
                            <div class="content">
                                <a href="{{route('details', $c->id)}}" >
                                    <h6 style="font-size:13px">{{$c->course_title}}</h6>
                                </a>
                                <p>
                                    <span><i class="fas fa-chevron-right"></i></i></span> Course + Certificate <br>

                                </p>


                            </div>
                        </div>
                    </div>

                    @endforeach
                </div>

                    <div class="col-md-12 mt-3">
                        <a href="#" class="courses-all headBtn btn">Apply Now</a>
                    </div>
                </div>
            </div>
        </section>-->







<section class="padding-y-100 ">
            <div class="container">

                <div class="row">

                    <div class="col-12  mb-2 ">
                    <h4 class="" style="color:#1e2022; font-weight: 600;">
                           Why Memdal Training
                        </h4>

                    </div>

                    <div class="col-md-4 mt-2 text-center" data-aos="fade-right" data-aos-once="true">
                        <div class="card">
                            <img class="rounded" src="{{asset('webassets/img/labimg.jpg')}}" alt="">
                        </div>
                        <h5 class="my-4" style="margin-bottom: 1.14285714rem;
                        text-align: center;
                        font-size: 19px;
                        line-height: 1.2; font-family: AvenirNext,Helvetica,Arial,sans-serif;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal; color:#1e2022;">
                            Hands-On Projects & Labs
                        </h5>
                        <p style="color:#000">
                            Build more than 5 real-world projects in each course that you can showcase in your portfolio.
                        </p>
                    </div>
                    <div class="col-md-4 mt-2 text-center" data-aos="fade-up" data-aos-once="true">
                        <div class="card">
                            <img class="rounded" src="{{asset('webassets/img/climg.jpg')}}" alt="">
                        </div>
                        <h5 class="my-4" style="margin-bottom: 1.14285714rem;
                        text-align: center;
                        font-size: 19px;
                        line-height: 1.2; font-family: AvenirNext,Helvetica,Arial,sans-serif;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal; color:#1e2022;">
                            Comprehensive Curriculum
                        </h5>
                        <p style="color:#000">
                            Our curriculum is curated after years of research on the right skill needed in the industry.
                        </p>
                    </div>
                    <div class="col-md-4 mt-2 text-center" data-aos="fade-left" data-aos-once="true">
                        <div class="card">
                            <img class="rounded" src="{{asset('webassets/img/instructorimg.jpg')}}" alt="">
                        </div>
                        <h5 class="my-4" style="margin-bottom: 1.14285714rem;
                        text-align: center;
                        font-size: 19px;
                        line-height: 1.2; font-family: AvenirNext,Helvetica,Arial,sans-serif;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal; color:#1e2022;">
                            Experienced Trainer
                        </h5>
                        <p style="color:#000">
                            Our trainers are Professionals with 10+ years of experience in both training and solution implementation.
                        </p>
                    </div>
                </div>
                <!-- END row-->
            </div>
            <!-- END container-->
        </section>




<section class="certificate mb-5 mt-5" style="background-color: #fff; color:#333;">
        <div class="container mt-1">

        <div class="col-md-12 text-center mb-5" style="padding-top: 40px;">
                    <h3 class="mt-2" style="font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-weight: 600;
                    font-style: normal;
                    font-stretch: normal;
                    letter-spacing: normal; font-size: 2.23rem;
    line-height: 2.711rem;">Become a Data Analyst in 8 weeks</h3>
    <p class="lead" style="font-size: 16px;">Learn online at your own space and have access to expert 1 on 1 mentorship</p>

<div class=" text-center mb-5 mt-3">
                    <a href="{{route('details', 'data-analysis-with-power-bi')}}" class="btn btn-primary light-bg-hover" style="background-color:  rgba(254,254, 2); border-color:  rgba(254,254, 2); color:#333">Course Details</a>

                </div>
                </div>





        </div>

    </section>














<!--
<section class="padding-y-100" style="background-color: #f8f9fa! important;">
        <div class="container" style="max-width:1140px">

        <div class="row">
<div class="col-md-10">
<h4 class="mb-3 mt-3" style="color:#1e2022; font-weight: 600;">
      Courses
    </h4>
</div>


</div>
<p class="mb-3 lead" style="color:#333">Well curated courses for your career in software development and data analytics</p>
            <div class="row no-gutters">


                @foreach($courses as $course)
                <div class="col-md-3 pr-2 d-flex pb-3" data-aos="fade-left" data-aos-once="true" data-aos-delay="{{$loop->index*100}}">

                <div class="card  mb-1">

                            <div class="rounded mainOverlay">
                            @if($course->image_url == "")

                        <img class="rounded card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>

                        @else

							<img class="rounded card-img-top" src="{{$course->image_url}}" alt="course image"/>

                        @endif

                            </div>

                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-weight: 600;
    font-size: 1rem;
    line-height: 1.25rem;
    height: 40px;">
                                   {{$course->course_title}}
                                </a>


                            <div class="card-text d-flex bd-highlight mt-2">
                                <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($course->course_type == 1)
                                    Type: Online
                                    @elseif($course->course_type == 2)
                                    Type: Classroom-based
                                    @elseif($course->course_type == 3)
                                    Type: Free course
                                    @else
                                    Online and Offline
                                    @endif
                                     </p>


                            </div>

                        </div>

                        <div class="card-footer border-0 pt-0">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">


                                    <a class="btn btn-sm transition-3d-hover mt-2" href="{{route('details', $course->id)}}" style="background-color: rgba(254, 254, 2); color: #333; font-weight: bold;">Course details</a>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>

@endforeach

            </div>
        </div>
    </section>-->


<!--

        <section class="padding-y-100">



<div class="container">

    <h4 class="" style="color:#1e2022; font-weight: 600;">
      Courses
    </h4>

    <div class="mb-3 mt-3">


        <div class="row mx-n2">
        @foreach($courses as $course)
            <div class="col-sm-4 px-2 mb-3 mb-sm-0 mt-1" data-aos="fade-left" data-aos-once="true" data-aos-delay="100">

                <a href="{{route('details', $course->id)}}">
                    <div class="text-center rounded p-4 magicCategory" style="padding:42px 34px 42px 38px; box-shadow: 0 2px 18px 0 rgba(0,0,0,.08); background-color: #00407d; color:#ffdd00;">

                        <h2 class="h3 mb-0" style="flex-grow: 2;
                    margin: 0 9px 0 18px;
                    font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-size: 16px;
                    font-weight: 600;

                    letter-spacing: normal;
                 text-align: left;">{{$course->course_title}}


                </h2>

                    </div>
                </a>

            </div>

            @endforeach

        </div>

    </div>


</div>
</section>

-->




<!--
<div class="container" style="margin-top: -10px;">

<div class="row">
<div class="col-md-10">
<h4 class="mb-3" style="color:#1e2022; font-weight: 600;">
      Courses
    </h4>
</div>

<div class="col-md-2"><a href="#" style="color:#1e2022; font-weight: 600;">All courses</a></div>
</div>



    <p class="mb-3 lead" style="color:#333">Well curated courses for your career in software development and data analytics</p>
<div class="row ">
                    <div class="col-lg-4 mb-4" data-aos="fade-up" data-aos-once="true" data-aos-delay="100">
                        <div class="card" style="border: none;">
                            <img class="card-img-top" src="img/img2.png" alt="Card image cap">
                            <div class="card-body text-left">
                                <a style="text-decoration: none;" href="single-course.html">
                                    <h5 class="card-title text-left" style="font-weight: 600;">Data Processing</h5>
                                </a>
                                <p class="card-text" style="color: #faae2f; font-weight: 600;">5.0
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </p>
                                <div class="row">
                                    <div class="col-lg-7 col-7">
                                        <span style="font-size: 13px;"><i class="far fa-check-square"></i> Course + Certificate</span>
                                        <br>
                                        <span style="font-size: 13px;"><i class="fas fa-stopwatch"></i> Type: Online</span>
                                    </div>
                                    <div class="col-lg-5 col-5 text-right">
                                        <h6 class="my-0" style="color: #999999;"><del>&#x20A6;40,000</del></h6>
                                        <h5 class="my-0 text-right" style="font-weight: 600;">&#x20A6;20,000</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4" data-aos="fade-up" data-aos-once="true" data-aos-delay="200">
                        <div class="card" style="border: none;">
                            <img class="card-img-top" src="img/img2.png" alt="Card image cap">
                            <div class="card-body text-left">
                                <a style="text-decoration: none;" href="single-course.html">
                                    <h5 class="card-title text-left" style="font-weight: 600;">Data Processing</h5>
                                </a>
                                <p class="card-text" style="color: #faae2f; font-weight: 600;">5.0
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </p>
                                <div class="row">
                                    <div class="col-lg-7 col-7">
                                        <span style="font-size: 13px;"><i class="far fa-check-square"></i> Course + Certificate</span>
                                        <br>
                                        <span style="font-size: 13px;"><i class="fas fa-stopwatch"></i> Type: Online</span>
                                    </div>
                                    <div class="col-lg-5 col-5 text-right">
                                        <h6 class="my-0" style="color: #999999;"><del>&#x20A6;40,000</del></h6>
                                        <h5 class="my-0 text-right" style="font-weight: 600;">&#x20A6;20,000</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4" data-aos="fade-up" data-aos-once="true" data-aos-delay="300">
                        <div class="card" style="border: none;">
                            <img class="card-img-top" src="img/img2.png" alt="Card image cap">
                            <div class="card-body text-left">
                                <a style="text-decoration: none;" href="single-course.html">
                                    <h5 class="card-title text-left" style="font-weight: 600;">Data Processing</h5>
                                </a>
                                <p class="card-text" style="color: #faae2f; font-weight: 600;">5.0
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </p>
                                <div class="row">
                                    <div class="col-lg-7 col-7">
                                        <span style="font-size: 13px;"><i class="far fa-check-square"></i> Course + Certificate</span>
                                        <br>
                                        <span style="font-size: 13px;"><i class="fas fa-stopwatch"></i> Type: Online</span>
                                    </div>
                                    <div class="col-lg-5 col-5 text-right">
                                        <h6 class="my-0" style="color: #999999;"><del>&#x20A6;40,000</del></h6>
                                        <h5 class="my-0 text-right" style="font-weight: 600;">&#x20A6;20,000</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


</div>

</div>

-->




<!--
<section class="padding-y-100">


<div class="container space-2">

  <div class="col-12  mb-4">
                    <h4 class="" style="color:#1e2022; font-weight: 600;">
                           Success stories
                        </h4>

                    </div>

  <div class="card-deck d-block d-lg-flex card-lg-gutters-2">

    <div class="card shadow-none bg-transparent">
      <div class="card-body">
        <ul class="list-inline text-warning small">
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>

        </ul>
        <div class="mb-auto">
          <p class="mb-0">With Front Pay, you can check out across the web and in apps without having to enter any payment information.</p>
        </div>
      </div>

      <div class="card-footer border-0 bg-transparent pt-0 px-5 pb-5">
        <div class="media">
          <div class="avatar avatar-circle mr-3">
            <img class="avatar-img" src="img/avatar.jpg" alt="Image Description">
          </div>
          <div class="media-body">
            <h4 class="mb-1">Christina Kray</h4>
            <small class="d-block text-body">Business Manager</small>
          </div>
        </div>
      </div>
    </div>

    <div class="card bg-primary text-white" style="background-color: #00407d;">
      <div class="card-body">
        <ul class="list-inline text-warning small">
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
        </ul>
        <div class="mb-auto">
          <p class="text-white mb-0">From boarding passes to transit and movie tickets, there's pretty much nothing you can't store with Front Pay.</p>
        </div>
      </div>

      <div class="card-footer border-0 bg-primary text-white pt-0 px-5 pb-5">
        <div class="media">
          <div class="avatar avatar-circle mr-3">
            <img class="avatar-img" src="img/avatar.jpg" alt="Image Description">
          </div>
          <div class="media-body">
            <h4 class="text-white mb-1">Massalha Shady</h4>
            <small class="d-block text-light">CEO at Slack</small>
          </div>
        </div>
      </div>
    </div>

    <div class="card shadow-none bg-transparent">
      <div class="card-body">
        <ul class="list-inline text-warning small">
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
          <li class="list-inline-item mx-0">
            <i class="fas fa-star"></i>
          </li>
        </ul>
        <div class="mb-auto">
          <p class="mb-0">I love Front Pay for cash back, reward points and fraud protection – just like when you're swiping your card.</p>
        </div>
      </div>

      <div class="card-footer border-0 bg-transparent pt-0 px-5 pb-5">
        <div class="media">
          <div class="avatar avatar-circle mr-3">
            <img class="avatar-img" src="img/avatar.jpg" alt="Image Description">
          </div>
          <div class="media-body">
            <h4 class="mb-1">Mark McManus</h4>
            <small class="d-block text-body">Front Pay user</small>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>


</section>

-->




<!--

</section>-->



<!--<div class="container">SECTION: COURSE CERTIFICATE BATCH</div>

<div class="container">SECTION: PROJECTS STUDENT WILL BUILD WITH SLIDER</div>-->




@endsection
