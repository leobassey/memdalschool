@extends('layouts.weblayouts')
@section('title', 'Welcome to FarmKonnect Academy')
@section('content')

<section class="bg-cover" data-dark-overlay="7" style="background: url(assets/img/1920/550_2.jpg) no-repeat;">

        <div class="photo-overlay">
            <div class="container">
                <div class="row">
                <div class="col-lg-7 mr-auto text-white my-3">
                    <h1 class="line-height-normal mb-4" style="font-weight: bolder; font-size:50px">
                       FarmKonnect Academy
                    </h1>

                    <p>Learn practical courses in Agricultural value chain from experts who have tons of experience in Agricuture, Technology and Business.</p>
                    <a href="#" class="btn mt-4" style="background-color: Orange;">Get Started Now</a>
                </div>


                </div>
                <!-- END row-->
            </div>
        </div>
        <!-- END container-->
    </section>



    <section class="course-lists mt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mt-5" style="font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-weight: 600;
                    font-style: normal;
                    font-stretch: normal;
                    letter-spacing: normal; font-size: 1.857rem;
    line-height: 2.711rem;">Empirical Training from FarmKonnect Nigeria</h3>
                    <h5 class="mb-5" style="font-size:16px">Choose from more than 20 self-pace courses with case studies and course certificate</h5>
                </div>
                @foreach($courses as $c)
                <div id="courses" class="owl-carousel owl-theme">
                    <div class="courses">
                        <div class="courses-pack">

                            <div>
                                <div class="mainOverlay">
                                    <img src="assets/img/riskmanagement.jpg" class="img-responsive" alt="Courses" />

                                </div>
                            </div>
                            <div class="content">
                                <a href="">
                                    <h6>{{$c->course_title}}</h6>
                                </a>
                                <p>
                                    <span><i class="fas fa-chevron-right"></i></i></span> Course Certificate <br>
                                    <span><i class="fas fa-chevron-right"></i></i></span> 100% online
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

                @endforeach
                <div class="col-md-12 mt-3">
                    <a href="{{route('onlinecourses')}}" class="courses-all btn" style="background-color: orange;color:#fff">All Courses</a>
                </div>
            </div>
        </div>
    </section>




    <section class="certificate" style="background-color: #f8f9fa! important; color:#333">
        <div class="container mt-5">

        <div class="col-md-12 text-center" style="padding-top: 40px;">
                    <h3 class="mt-5" style="font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-weight: 600;
                    font-style: normal;
                    font-stretch: normal;
                    letter-spacing: normal; font-size: 1.857rem;
    line-height: 2.711rem;">Learn and Earn Certificates</h3>
                    <h5 class="mb-5" style="font-size:16px">Complete a course to get a course certificate or complete set of courses to get a program certificate</h5>
                </div>
            <div class="row" >

                <div class="col-md-12 col-lg-6 certificates mt-5" style="margin-left: -20px;">
                    <img src="assets/img/certificate-online-course-sample.png" alt="">
                </div>
                <div class="col-md-12 col-sm-12 col-lg-6 certificates" style="padding-bottom: 60px; margin-left: 20px;">
                    <div class="card-text mb-4 d-flex bd-highlight">
                        <img class="col-md-2 img-responsive" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAFyUlEQVR4nO2dXYhVVRTHfzNXaq4fNfdBoZmBGk2dnnzxRSiGqIcIYnrpaxAsMi0cKhhIBR2llwr6eIowSiXDKBwarYdqsi8lyCiDBkwnyMoUShiTkoKx28NahzMznnPn3rl37/Nx1w8O3rnn7L3X2f+799p7n32WYBiGYRiGYRiGYRiGYRiGYRiGYRhG89LSoHxKwN1AH9ADdAELGpR3WvkbOAOcAA4BI8BEohYBRWArcAEoN/kxAWwB2uqq0TroAL6eYtCHwHpgJflvHSD32IPc80eE9XAMqRuvdAC/qAE/ADf7NiCF3ILURRmpG2+iFAlbxqdAu6+CM0A78BlSN1/hqfvaqgWewMSIogScROpos4/CAgdu3VQ8vYSOvuSyoIcIHXglWoF+4D3gLDDJ3EcvY5rnWA3ng++SZBSxZZ3LQt7VQh6ucM1S4FukL70XcW4Fl0allEeQuhp2WUjQN66MOb8U+A3Y4NKIjNBDOAp1xkUtZFHEuVakZZgYwiKkri66LCToo6PoR7opI6RSfTkv4H3gHpeFZ5BEBTlLAksGKSdRQSZpztFUJRIVpJaC4+YUro6p85Jqy27EXCYzgjQLNQvS6sgQY46YICnDBEkZaRTEt8NPy2LknDCnXhvm1LOOCZIy8iDI1cgzh2H9nGmyLkgR2aB2WY8R/S6z1LpzMXBQUenKc8ivVvYAdwDjwI/ACuAn4EE9vxfoBk4BNwLLgQ+QR89JUKm+IsmaIKeQB2BlpMILwGvAf3q+Fdm4dhkRrAV4FREuCXItyHzgD+BaZGU5+O52RJx/gZdnpJkH/AksBi45tC2OmgXJkg9Zg7SQSeQGB4HTwADQCVyj378IPIHc26SmWePfXD8kMTHsBHYBvwNrkUp/E/gcWBZx/TLgKPCGXrtW0+7SvHySu+X3O4HzwLOEG84GETGuqpCuCDxPOOIqaR7nNU9f5E6QVci2omCP7Hzk1x7VMqJYQNh/t2leqxpp4CzkThCQucWAfr4L2fpfLW8TtogBzcsnuRRkNfAzMsQdRLqiahlEfEhB81jdcOsqU7Mg8xwZkhZe0H9zu/kia13WVKzLagBxTn35HPLKhFNP+8SwE1nB3YkMXS8BzwCvU3lltwg8x/Rh705N43su4pS0TAz3IZO/qDWqFXpuLzYxdMorwHH93AI8iVT0KLJc8hJwGFnvepyw9R/XtElQsyBZWVwcAu4DbmD64mIRuA3xKWVkWf4w8I+eDxYXTyNzkqcd2RdHzYuLWRj2DiFvYt0KHEHebaxl+f2Mpv1Er/EtilN8d1lDyBadJfr3HuAc8AWwG/gS8ScFPfbpd7v1mnOaBs1jTPP0Ra58yEwxZhJU8Djwjh7jVabxJUpuBNlOdRW7gys3OeyoMu32BtobR24EOYC8470fiTA01SlOFSOOKFFaNK/9mveBBtobR24EAXnsuhFxzn36XTViBMwUpU/z2qh5+yBXggRsRH7VS4DvqU6MgM1ICJDrgLfw/4ZwooK4eqVtMdLFVNsyZhK0lAnctIwC8F3MuUQFcfnS5zCwrY7023AXUaELWbSMIlFBDiGz6WajHwk5EkWigtyPbD5oNo4iKwlROBdkttAa3wCPujQgZWxCQvpFPcbwElpjtuAz3cja0WMujUgJm4BfgetjznsJPhOEZ1pf4ZpuJATgEaQb6yQfz7QLiAN/AOmmjiGrz3EEe5CdhmcKApjN9ly7FRHjIDICqSeAWVqOSb2XEcRnzPa09WNNt26W6+qihIzny0gUTiOaXqSOJvAQl3ILYd9oQTCvpIRs8C4DT/kosA3pP8tIfCynQR4zRokEwsTC9EDKJ5Em2uz0ErYMr4GUAzqQX0Hg9EaRkcVNwELfxiTAQuReNxA68KBlJBY3rA1ZUQ0cfTMfE4jPqOtN4EbtEmlHnjf0Ib+aLvLfSv4i/O8qDupxIVGLDMMwDMMwDMMwDMMwDMMwDMMwDMPIOP8Dl+u7jKFps9UAAAAASUVORK5CYII="
                        />
                        <div class="col-md-10">
                            <span class="aquisition-heading">Get Your Course Certificate</span> <br> You will receive a digital certificate with veriable ID from FarmKonnect in every course you complete.
                        </div>
                    </div>
                    <div class="card-text mb-4 d-flex bd-highlight">
                        <img class="col-md-2 img-responsive" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAOI0lEQVR4nO2beXBVVZ7HP+e+m5ddJAHZZZNNVGwElEUSsppNtjwd6UFhsF2qXKbabrvH0THqtN3jOHa1PVVtzVSXC11O40MQQliyQEIwiIgL0MgiS5BFJKzZyXv3N3+83Pvuy/aS9wL21PS3iqpz7m89v3fO+f3OOQH+hr/h/zWUvZOZO3+SVxyzlVKax8PGLRvdB34ox3oTGTnzbzHQ7xYRA1GVZRvc+0yaAkh2ueIcDfKegvkBkkrej1ZXflJYWNhwjX3uFeTl5cU0GZF/FPi7AILICs0Ts6y4eHm9DqDXsxzFvHYaRC1qlKjrgZxr4nEvo9GIfBu4rx1BqfslotEBuFR6tut2UfIFgKZp5Gal09TUTMnmCkSklZ+0knUry66l8+EiNW/BVGVonwIopUibMxuPx0N5ZZU1Lg01RRcld5tC2ZmpPPn4wwA4dAcbNvnGLIb8BPg/FQDN0JZJazsvJ5MnH1tm0bZs/RgAryJJU0pplpDDYTHdm53p16bU3JycRX2vrsu9h+kuV7R93edlpVu0qKhIq61ENE0Zxifmh8pt2/F6vQDcNHoko0aOsOSauPLAVfW6FxFbz3ygD8DYMaMZMfxGAFpaWti2/VM/o6Y+0YrXf/gp8A3AhYuX2LnrC4uekZpktZWoJVff9V6CYqnZzExNtj5XfbKT2to6AAQ5NuuOiVUaIALvmUzFZRWWQHpqErqutyqVqWnZrluvsuthIzlrwVCQOQC6rpM0e6ZFs49NKfVuQUGBoQGIGO8BBsD2HZ9x6XItANfFx3PnlMl+7UoevPpDCA8OTVsKOABmTp9Gn+viAd/s3vXFVyabGF7HcgANYPP6VdWiKAfweDxsqdhmKcxIS7brX3zHHY9EXNURhAelkIfMToZt+heXlVv7G1CxecOKwwC6JQnvAikAxaXlzMvLAuDOqZNJ6Hs95y9cBBiQOOjcPUBhKN4lZ7sGOpC5Sql7UTIaYWgr6VvgsAiFXtSa8vXu70LRn57nmi2GjAZITOjLlMmTLFrp5q1WW4l612z7U+CV6A+BWoBDh49w5OgxABwOBylJsyxhI4TNMDPTlZCak/+6ruSoUrwFko0wDoht/TceyFGKt3QlR9Ny8v89M9OV0FM7GGJtfmkpSWiab3j7DxziWPVxk1TfoDd/aHasABQXL68H3Ga/xLZhZKbPsZvJzcpy9e+uT5m58yd5dflCwTNAVDdEooCfeXXZnZ69cEp37WRkLI4VWGj120x/C8IHH69dW2t2dWzQxHjXUNo/AJSWV7Jsyd+j6w5GDL+RMTeN4tA3RwCcHgeLgN8FdSrLleIVKQRizG83jx9LemoSk26dSP9+/QA4W1PDl7v/QsnmCr7ef9BkHSJKVaTmLMgtK1q1JZgtw9l4H0KcaePGYUMAuHKlxar8AJTDP/3BNgMAitevqgQ5DHDx4iU+/exzi5aZ5p8FIrKMIEjPXTjG0MRtDr7PdfEUPP9zfvf6r8jNymDY0CFERUUSFRXJsKFDyMvO4M3Xf8WLz/2M+Pg4U02MQnOnZN0/Opg9xJ/7M2y+Vu3YSV1dfSuLHCspdG+1iwUEwMejlpsd+9RJSZpFRISVAG5Nz3bd3oU7SnwbTQLADf378fs3fs3Mu6YFHcesGXfyn2/8hv79Es1PiZpmvNuVTEaeayQwC8DpjCDp7ukWrbjUP3k0tLcBscu2DQAGxju01gQ7du7i0qXLAMTHxzF92h02Sf+G0xZpOfnzgekAkU4nL7/wCwYNHNDVGAIweNAAXnrhWZxOM+AyMy07f25n/IZhLKP1bmPWjDuJi40F4Nz5C+z6YrfJJoLxp7ay7QKwef2qalAVAB6Pl80BNYF9GfDjrKysyLbyPiJPmc35c7MZPWpEZ753ijGjRzE3N8uvUvPrtKOgoEADtdjyMdXvY0lZBYZh+OQVW0qLPjzSVr5dAHz++6fchmL/KXjK5EkkJliHwsQWLbbdRUnq/PmJKGaC734hf15exyPsBlwL7rVSmRJmd5QaP961Nw24EaB/v0R+NOkWi1ayudxqt9Y57dBhABwtMStprQmOHjvO4SPHfN8dDtLmzPYz2g4dFpod02nNLhMnjKNPn+s6H2EQ9L2+DxPGjzW7uldnelseMVhitjPSkq2Afb3/IMe/PWmS6jzRalVHNjoMQHHx8nqElVbfthlmpqfYrHNPZuaCQW3Eh5iN4TcOJVyMCNQxxN5JS3P1Aay9ITXZ/+ME5H74oNztrutIf4cBgMB8Wbp5Kx6PB4BhQwczftwYk6R7de3HbTRa0zQ+Lo5wcV18vNUWJYl2mooyHqA1zd46cQLDhg4GfLm/fGuVXw7e6Ux/pwHw5UtfTXC5tpYdATVBsp01YBkoUefM9uXaWsLFxcuX/c4anA0girLlfr9PH2/fQV19vdk9Wla0chudoNMAAIJSVtooLi23CHNmzyLS6TS7N6fkuqwELyKnzHb18RNdqO8ejtt1aH7dc+5xjROYBr5rrtmzbLnfVsYLtMv9dnQVAHSP/g5WTfC5eSIkNjaGGXdNtfllLLFkvKoK8ADs23+QixcvdWWiS1y4eIl9/tK4xSmR282OwyEPm+27Z9xFTHQ0ADXnzvP5l/7c79BUu9xvR5cB2Ljxz8eArQBerzfgniDwgKQWTXe5ogE2bXKfB7YBGIaBe/Xarkx0iQ9WfmRdYQNbi4revwCQnJysA9beY5/+JZv9uR9UWXGh+2hXNroMAASenTeW+MvKybffZi9X+8Q0iLUbK6XeNNsfFW7g0OF29UdQHPrmCGuKNvn9wK/TGZeYBQwCGDCgP5NunWjx2U+xiq5LaOhGAFpiWQnUARyr9tcESinSUuyXpjxmObHOvVpBFfh25BdfeY1Tp88EM2Xh5KnTvPivr9HS0mJ+qiwpcltTyRD1qNnOSE1GKd8T5/4Dh/j2hJX7L0dpVzrM/XYEDUC5212HktVm3360vCc9xTKOIik1J996ZNEc6iHgPMDZmnM88dNfsq1qR5e2RISt27bzxE//ibM1VjI5B7LE7GTkuO4AssF36XmPrS6x+was7M6bZtAA+DzTVpjN8sqPrXU5eNAA7pzqPyAp1L+Y7U1r3d8IRj5QD1BbW8dLr77Ok888x9p1G6k+foLGpiYam5qoPn6CNes28tQzz/HKb96wjq9APYp8ew1vKCmg9eCTkZrMDf19dwpm8CyXRf25O0PTg7PAhe/6FvcdeP48kHDmzFm+PnCIm1tL1KWL72fHzl2tQZG0tBzX46VF7j8AlBWt2pKZO3+mVxxrgOHgm6b7DxzqjtmTSmReSdGHn5kf0rPzHxYhF0DXHTxwn/8xe/fefdScO292z3obzga9RIFuzoBdu/6rRRBrDZbbptqokSPIyki1cctvU/MWWDly07rVX0Xi/BHwb0BjN8xdUYo3PRGeW0rW+wefeu/C20RhbYRzc7MYOOAGS6iissquw11eXu7pzti6twQAMdQHZrvqk50BtEcfftB+3o/UDO1tO72o6P0LpUUrfymOiJEoeUREFQL78W2udSBfi6hClDyitXiHlaxb+XT5Rx9dtOtQXu0/gGjwnTGWPhj4UmffXzQxVtBNdGsJAFz6PqG078Dz1cDwhITAd9KY6GhefuFZnv758zQ0NKI5tA5vdMvW/s8Z4L9b//UIMTHR9Q0NDcTHx/HS88/aK1EA+23V/hnTbttWvD5oAug5krMWDF209LEnLl2+7JEOcLT6uLhXF8rRY8fPiUhyb9kVkRmNTU3fVWzbLt+frenItHx74pSxaMljL6ZkLxjeW3a7cqiqQy8C0SwiL4lITHCNndqJEZGXW3UFw65QbKjgLB069iJQUH38BJ9/uZs9e/fR3NzcIW+Lx9t4tqbm6Pdnak61eP2VTVeIcEREDLghcUj//v1G6roeBaDrEYwdM8r/WAs4nU4mThjH2DGjAX6tlHqup2MJNQDJxWXlW377+7fweLzBBa4yHn34IfLn5WYppTb2VLbbm6Adf3S7dxetWv9XMXiA9/60gvO1F/YF52wPR3CW9rhS6xjQ1NL0j6HIXg14PB727T349pFD+77vqWy364BAg94L1hngrwQe8V4IRS6kAJSWui9FOp3ngnNeG0ToekP5hlUng3O2R0gBANAc2t5QZXsbEc6I/XRx7dUVQg5AY31j12fba4jm5uZPg3N1jJADgJI9Icv2MgzD+1Vwro4RUhoE0JSxxyshJREA4mJjrVfc8soq6uvD+Htsb+g/RugB8DbtFz3Oaxg9j0JcbCx/ePM16zh7f/48Hn/62VCDIC2Rxl9CEYQwlsCGDRuaHQ5HlzeunSF59syAs/yggQNIvntGSH5E6PqZtkfnniD0PQBoudIS0gFExGj3zZCQNnE8Xu/nwbk6R1gBQGkhrb2Kyu2c/s5/S3zq9JmA+7yeQDB2B+fqHCHvAQAK9oTyu9XV1/P408+SNMs37Su2hbEJGqH9CCbCC4DGng5mc7dQX9/A+k2l4Zg3EVYAwloCxYXuY0pTHb67XyO09I0lrP/YFd4e4LuO/wFLYnXA7XZfCUdDuAEA+SErwvBthx+AMNdgOBAVvu0eH+ozMhbHGs6mpUokFsBAxit+oP9NouR9JWo3gIgyBG1N2foVB+0sBQUFWuVne3+hIDJaNb/W9r2wx1lAnI2vIjxlpj8V2rVi70DUIoFFPkcEhedR4CY7S+XOPYsV6lWABon0Aq/Y6T1eAkaoJdsPBA1l/eIKVdOW3uMZ4PDE/LNXbziilBps/z5h/NhxCX2vHxiamz3DiZOnv6m2/REggEC9pmj3IlxStNKdnr1wkaGUlBW52z2Z/S8e/MD4vR9dTQAAAABJRU5ErkJggg=="
                        />
                        <div class="col-md-10">
                            <span class="aquisition-heading">Complete Courses in A Program</span> <br> Complte serveral courses to get a program certificate with a veriable ID from FarmKonnect.
                        </div>
                    </div>
                    <div class="card-text mb-4 d-flex bd-highlight">
                        <img class="col-md-2" src="assets/img/skills-400x400.png" />
                        <div class="col-md-10">
                            <span class="aquisition-heading">Get A Real-World Skills</span> <br> Get exposed to practical skills with case studies that you can implement immediately.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>






    <section class="padding-y-100">
        <div class="container">

            <div class="row">

                <div class="col-12 text-center mb-5" style="margin-top:-40px;">
                    <h2 class="">
                        FarmKonnect Proven Experience
                    </h2>

                </div>

                <div class="col-md-4 mt-5 text-center">
                    <div class="iconbox iconbox-xxl font-size-26 bg-primary-0_2">
                        <i class="ti-bookmark-alt text-primary"></i>
                    </div>
                    <h4 class="my-4">
                        Practical with case studies
                    </h4>
                    <p>
                        Build more than 5 real-world projects in each course that you can showcase in your portfolio.
                    </p>
                </div>
                <div class="col-md-4 mt-4 text-center">
                    <div class="iconbox iconbox-xxl font-size-26 bg-primary-0_2">
                        <i class="ti-user text-primary"></i>
                    </div>
                    <h4 class="my-4 mt-5">
                        Comprehensive Curriculum
                    </h4>
                    <p>
                        Our curriculum is curated after years of research on the right skill needed in the industry.
                    </p>
                </div>
                <div class="col-md-4 mt-4 text-center">
                    <div class="iconbox iconbox-xxl font-size-26 bg-primary-0_2">
                        <i class="ti-cup text-primary"></i>
                    </div>
                    <h4 class="my-4 mt-5">
                        Experienced Trainer
                    </h4>
                    <p>
                        Our trainers are Professionals with 10+ years of experience in both training and solution implementation.
                    </p>
                </div>
            </div>
            <!-- END row-->
        </div>
        <!-- END container-->
    </section>

    <section class="padding-y-100" style="background-color: #f8f9fa! important;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center" style="margin-top:-40px;">
                    <h2 class="">
                        Our Courses
                    </h2>

                </div>

                <!--  <div class="col-12 mt-3">
                    <div class="d-md-flex justify-content-between bg-white rounded shadow-v1 p-4">
                        <ul class="nav nav-pills nav-isotop-filter align-items-center my-2">

                            <a class="nav-link" href="online-courses.html" data-filter=".free-courses">Online Courses</a>
                            <a class="nav-link" href="classroom-courses.html" data-filter=".start-soon">In-Person Courses</a>
                            <a class="nav-link" href="topic-courses.html" data-filter=".free-courses">Topic-Based Courses</a>

                            <a class="nav-link" href="#" data-filter=".free-courses">Career Courses</a>
                            <a class="nav-link" href="#" data-filter=".free-courses">Free Courses</a>
                        </ul>


                    </div>
                </div>-->

                <!-- END row-->

                @foreach($courses as $course)
                <div class="col-lg-4 mt-3">

                <div class="card">
                        <a href="#">
                            <div class="rounded mainOverlay">
                                <img class="rounded" src="assets/img/marketing.jpg" alt="">
                              <!--<p class="text-center overlayText">View Details</p>-->
                            </div>
                        </a>
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 20px; font-weight: bold;">
                                   {{$course->course_title}}
                                </a>

                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>



                            </p>


                            <div class="card-text d-flex bd-highlight mt-2">
                                <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($course->course_type == 1)
                                    Type: Online
                                    @elseif($course->course_type == 2)
                                    Type: Classroom-based
                                    @else
                                    Type: Free course
                                    @endif
                                     </p>
                                <h4 class="h5 text-right ml-5">
                                    <span class="" style="font-weight: bold; margin-left:-40px">N{{$course->course_price}}</span>

                                </h4>

                            </div>
                            <a href="{{route('onlinecoursedetails', $c->id)}}" class="btn mt-2" style="background-color:orange; color:#fff">View course details</a>

                        </div>
                    </div>

                </div>

@endforeach

                <div class="col-md-12 mt-5">
                    <a href="{{route('onlinecourses')}}" class="courses-all btn" style="background-color:darkgreen; color:#f4f4f4">All Courses</a>
                </div>

            </div>
        </div>
    </section>



    <section class="accomplishment" style="height: 530px;">
        <div class="fill-screen fixed-attachment">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="color:#333" class="mt-3">Start your learning experience today</h2>
                        <p style="color:#333">Join thousands of students who have already enhanced their career with Memdal training</p>
                    </div>
                    <div class="col-md-4 digits">
                        <div class="col-md-12" style="color:#333"><span class="number" style="color:darkgreen">1000+</span> <br> <span></span>Trained Students</div>
                    </div>
                    <div class="col-md-4 digits">
                        <div class="col-md-12" style="color:#333"><span class="number" style="color:darkgreen">20+</span> <br> Certified Trainers</div>
                    </div>
                    <div class="col-md-4 digits">
                        <div class="col-md-12" style="color:#333"><span class="number" style="color:darkgreen">94%</span> <br> Satisfied Clients</div>
                    </div>
                    <div class="col-md-12 mt-5">
                        <a href="{{route('onlinecourses')}}" class="courses-all btn" style="background-color:darkgreen; color:#f4f4f4">Courses</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--<section class="padding-y-100 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>
                        What Students Said
                    </h2>

                </div>

                <div class="col-md-4 mt-5">
                    <div class="card height-100p text-center px-4 py-5 shadow-v2">
                        <p>
                            Investig tiones was demons trave wunt was ectores is legere lkurus quod legunt the saepiu clartas consectetur sicing elitsed.
                        </p>
                        <img class="iconbox iconbox-xxl mx-auto my-3" src="assets/img/avatar/4.jpg" alt="">
                        <h6 class="mb-0">
                            Kenelia Deshmukh
                        </h6>
                        <p class="font-size-14 text-gray mb-0">
                            Developer Instructor
                        </p>
                    </div>
                </div>

                <div class="col-md-4 mt-5">
                    <div class="card height-100p text-center px-4 py-5 shadow-v2">
                        <p>
                            Investig tiones was demons trave wunt was ectores is legere lkurus quod legunt the saepiu clartas consectetur sicing elitsed.
                        </p>
                        <img class="iconbox iconbox-xxl mx-auto my-3" src="assets/img/avatar/5.jpg" alt="">
                        <h6 class="mb-0">
                            Colt Steele
                        </h6>
                        <p class="font-size-14 text-gray mb-0">
                            Developer Instructor
                        </p>
                    </div>
                </div>

                <div class="col-md-4 mt-5">
                    <div class="card height-100p text-center px-4 py-5 shadow-v2">
                        <p>
                            Investig tiones was demons trave wunt was ectores is legere lkurus quod legunt the saepiu clartas consectetur sicing elitsed.
                        </p>
                        <img class="iconbox iconbox-xxl mx-auto my-3" src="assets/img/avatar/6.jpg" alt="">
                        <h6 class="mb-0">
                            Stephen Grider
                        </h6>
                        <p class="font-size-14 text-gray mb-0">
                            Developer Instructor
                        </p>
                    </div>
                </div>

            </div>

        </div>

    </section>-->
@endsection
