@extends('layouts.studentlanding')
@section('title', 'Change Profile')

@section('content')

<style>
	.btn-info {
    border-color: #069 !important;
    background-color: #069 !important;
    border-radius: 0.21rem;
}

.btn-info:hover {
    background-color: #0481c0 !important;
    border-color: #0481c0 !important;
}
.btn-warning {
	border: 1.5px solid #ff9149;
	}
	.btn-warning:hover {
		border-color: #fc6300 !important;
		color: #fff;
		background-color: #fc6300 !important;
	}
</style>

<div class="app-content container center-layout mt-2">
<section id="configuration">
<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header d-flex justify-content-center">
					<h4 class="">Change Profile </h4>
					<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

				</div>
				<div class="card-content collapse show">
					<div class="card-body">

                    @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        <form class=""  action="{{route('postupdateprofile')}}" enctype="multipart/form-data" method="POST" style="margin-top:-25px">
                                                @csrf
                                                <div class="row justify-content-md-center">

                              <div class="col-md-6">
                                  <div class="form-body">




                                      <div class="form-group">
                                          <label for="eventInput2">Fullname</label>


                                          <input type="text" class="form-control" id="eventInput2"
                                             placeholder="Fullname"  value="{{$userObject->name}}" name="fullname" required>

                                             <input type="hidden" class="form-control" id="eventInput2"
                                             placeholder="id"  value="{{$userObject->id}}" name="id" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="eventInput2">Address</label>


                                          <input type="text" class="form-control" id="eventInput2"
                                             placeholder="Address" value="{{ $userObject->address }}" name="address" required>
                                      </div>


                                      <div class="form-group">
                                          <label for="eventInput2">Phone</label>


                                          <input type="text" class="form-control" id="eventInput2"
                                             placeholder="Phone" name="phone" value="{{ $userObject->phone }}" required>
                                      </div>


                                  </div>
                              </div>
                          </div>

                          <div class="d-flex justify-content-center center mb-5">


                              <button type="submit" class="btn" style="background-color: #08193e; color:#fff">
                                  <i class="la la-check-square-o"></i> Submit
                              </button>


                              <a  href="{{route('classroom')}}" class="btn btn-warning mr-1 ml-1" style="background-color: #ffc221; color:#fff; border-color:#ffc221">
                                  <i class="ft-x"></i> Cancel</a>
                          </div>
                      </form>



                      <div class="card-header d-flex justify-content-center">
					<h4 class="">Change Password </h4>
					<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

				</div>
                        <form class=""  action="{{route('postupdatepassword')}}" enctype="multipart/form-data" method="POST" style="margin-top:-25px">
                                                @csrf
                                                <div class="row justify-content-md-center">

                              <div class="col-md-6">
                                  <div class="form-body">




                                      <div class="form-group">
                                          <label for="eventInput2">Current Password</label>


                                          <input type="password" class="form-control" id="eventInput2"
                                             placeholder="Current Password"  value="{{ old('cpassword') }}" name="cpassword" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="eventInput2">New Password</label>


                                          <input type="password" class="form-control" id="eventInput2"
                                             placeholder="New Password" value="{{ old('npassword') }}" name="npassword" required>
                                      </div>


                                      <div class="form-group">
                                          <label for="eventInput2">Confirm Password</label>


                                          <input type="password" class="form-control" id="eventInput2"
                                             placeholder="Confirm password" name="password_confirmation" value="{{ old('password_confirmation') }}" required>
                                      </div>


                                  </div>
                              </div>
                          </div>

                          <div class="form-actions center">


                              <button type="submit" class="btn"  style="background-color: #08193e; color:#fff">
                                  <i class="la la-check-square-o"></i> Submit
                              </button>


                              <a  href="{{route('classroom')}}" class="btn mr-1"  style="background-color: #ffc221; color:#fff; border-color:#ffc221">
                                  <i class="ft-x"></i> Cancel</a>
                          </div>
                      </form>

                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


@endsection
