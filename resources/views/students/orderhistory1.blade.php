@extends('layouts.studentlayout')
@section('title', 'Order history')

@section('content')


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
        <h4 class="dashboard-title">Order history</h4>


         <!-- Dashboard Purchase History Start -->
         <div class="dashboard-purchase-history">
            @if(count($studentPayments) == 0)
            <h4>No order history</h4>
            @else
            <div class="dashboard-table table-responsive">
                <table class="table">
                    <thead>
                        <tr>

                            <th class="courses">Course</th>
                                    <th class="amount">Amount paid</th>
                                    <th class="status">Payment reference</th>
                                    <th class="date">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($studentPayments as $payment)
                                <tr>
                                    <td>{{$payment->course_title}}</td>
                                    <td>{{number_format($payment->amount_paid, 2)}}</td>
                                    <td>{{$payment->tempref}}</td>

                                  <td>{{$payment->created_at}}</td>



                                </tr>

                            @endforeach


                    </tbody>
                </table>
            </div>
            @endif
        </div>
        <!-- Dashboard Purchase History End -->


    </div>


</div>


@endsection
