@extends('layouts.studentlanding')
<style>
    .dashboard-review-item {
    border-radius: 5px;
    border: 1px solid #ededed;
    background: #FFFFFF;
    padding: 20px;
    display: flex;


}
.dashboard-review-item__content {
    flex-grow: 1;
    padding-left: 30px;
}
.dashboard-review-item__title-wrap {
    display: flex;
}

.dashboard-review-item__title {
    flex-grow: 1;
    font-size: 18px;
    font-weight: 500;
    margin-bottom: 10px;
}
.dashboard-review-item__title span {
    font-weight: 400;
}

a, button, img, input {
    transition: all 0.25s cubic-bezier(0.645, 0.045, 0.355, 1);
}

.dashboard-review-item__review-links {
    flex-shrink: 0;
    min-width: 135px;
    margin-bottom: 5px;
}
*, *::after, *::before {
    box-sizing: border-box;
}
.dashboard-review-item__review-links a {
    color: #0071dc;
}
.dashboard-review-item__rating .rating-star {
    display: inline-block;
    position: relative;
}

.dashboard-review-item__text {
    margin-top: 14px;
}
</style>
@section('title', 'Support')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Feedback for course: {{$course->course_title}}</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">


                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">

                                                <div class="card-body collapse show">
                                                    <div class="card-body card-dashboard">
                                                    </div>

                                                kkk
                                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                </section>



<section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show" style="margin-top: -30px;">
                    <div class="card-body card-dashboard">
                    </div>
<form action="{{route('submitfeedback')}}" method="post">
    @csrf

                    <div class="contact-form__wrapper" data-aos="fade-up" data-aos-duration="1000" style="width: 100%" style="margin-top: -30px">
                        <div class="row gy-4">
                            <div class="col-md-12">

                                <div class="contact-form__input">
                                    <input class="form-control" type="hidden" placeholder="What is the subject of your support?" name="course_id" required value="{{$course->id}}">
                                </div>
                            </div><br/>
                            <div class="col-md-12 mt-1">
                                Body
                                <div class="contact-form__input">
                                    <textarea class="form-control" placeholder="What would you want to ask us?" name="body" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="contact-form__input">
                                    <br/>
                                    <button class="btn btn-primary btn-hover-secondary" style="background-color: #08193e;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>

</form>
                    <!-- Contact Form Wrapper End -->
                </div>
            </div>
        </div>

    </div>

</section>

<section id="patients-list">

    <div class="row">
        <div class="container">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show" style="background-color: #f4f5fa; margin-top: -25px" >

                    @foreach($feedbacks as $f)
                    <div class="dashboard-review-item mt-1" >
                        <div class="dashboard-review-item__thumbnail">
                            <img src="{{asset('img/avatar.jpg')}}" alt="Courses" width="50" height="50" style="border-radius: 50%;">
                        </div>
                        <div class="dashboard-review-item__content">
                            <div class="dashboard-review-item__title-wrap">
                                <h2 class="dashboard-review-item__title"><span>{{$f->name}}</span> <span style="font-size: 14px" class="ml-1">4 months ago</span></h2>
                                <!--<div class="dashboard-review-item__review-links">
                                    <a href="#"><i class="far fa-pencil"></i><span>Edit Feedback</span></a>
                                </div>-->
                            </div>
                            <div class="dashboard-review-item__rating">

                                <div class="rating-star">
                                    <div class="rating-label" style="width: 100%;"></div>
                                </div>

                               <!--<p>4 months ago</p>-->
                            </div>
                            <div class="dashboard-review-item__text" style="margin-top: -8px">
                                <p>{{$f->body}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- Dashboard Review Item End -->





                    </div>


                </div>
            </div>
        </div>

</section>






                    </div>


                </div>
            </div>
        </div>

</section>








                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
