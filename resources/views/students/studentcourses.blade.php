@extends('layouts.app')
@section('title', 'Student courses')
@section('css')
  <!-- BEGIN: Vendor CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset('bof/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bof/app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bof/app-assets/css/plugins/forms/switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bof/app-assets/css/core/colors/palette-switch.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('bof/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- END: Vendor CSS-->


 <!-- BEGIN VENDOR CSS-->
 <link rel="stylesheet" type="text/css" href="{{ asset('bof/app-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bof/app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bof/app-assets/css/core/menu/menu-types/vertical-menu-modern.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bof/app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bof/app-assets/css/core/colors/palette-callout.css') }}">
    <link rel="stylesheet" type="text/css" href="bio/app-assets/css/plugins/loaders/loaders.min.css">
    <link rel="stylesheet" type="text/css" href="bio/app-assets/css/core/colors/palette-loader.css">

    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('bof/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bof/css/custom.css') }}">
    <style>
        .buttons-collection,.buttons-colvis,.dataTables_filter{
            display: none;
        }
        .hide{
            display: none;
        }
    </style>


    <script src="{{asset('vue-files/vue.js')}}"></script>
  <script src="{{asset('vue-files/axios.js')}}"></script>
  <script src="http://www.remitademo.net/payment/v1/remita-pay-inline.bundle.js"></script>
    <!--<script src="https://login.remita.net/payment/v1/remita-pay-inline.bundle.js"></script>-->


    <style>
        div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: 350px;
            padding: 10px;
            font-size: 16px;
        }
    </style>

@endsection

@section('content')
<div class="row">


        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-basic">Your Registered Courses</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>



                    <div class="heading-elements">

                            <a style="padding: 10px;" href="#" class="round btn btn-primary btn-sm">Register Course</a>

                        </ul>
                    </div>



                </div>

                <div class="card-content collpase show">

                    <div class="card-body">

                        @include('layouts.message')


                       <h1>Welcome</h1>



                                </div>
                            </div>



                    </div>
                </div>
            </div>
        </div>

      </template>
    </div>

@endsection

@section('script')
<script src="{{ asset('bof/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('bof/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('bof/app-assets/js/core/app.js') }}"></script>
    <script  src="{{ asset('bof/js/close.js')}}"></script>



<script src="{{asset('bof/app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('bof/-assets/js/scripts/forms/switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('bof/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>


<script src="{{asset('bof/app-assets/js/scripts/customizer.min.js')}}" type="text/javascript"></script>
<script src="{{asset('bof/app-assets/vendors/js/jquery.sharrre.js')}}" type="text/javascript"></script>
<script src="{{asset('bof/app-assets/js/scripts/tables/datatables/datatable-basic.min.js')}}" type="text/javascript"></script>



@endsection
