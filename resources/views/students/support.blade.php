@extends('layouts.studentlanding')
@section('title', 'Support')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Support</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">


                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">

                                                <div class="card-body collapse show">
                                                    <div class="card-body card-dashboard">
                                                    </div>

                                                kkk
                                                    </div>


                                                </div>
                                            </div>
                                        </div>

</section>



<section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show" style="margin-top: -30px;">
                    <div class="card-body card-dashboard">
                    </div>
<form action="{{route('submitsupport')}}" method="post">
    @csrf

                    <div class="contact-form__wrapper" data-aos="fade-up" data-aos-duration="1000" style="width: 70%" style="margin-top: -30px">
                        <div class="row gy-4">
                            <div class="col-md-12">
                                Subject
                                <div class="contact-form__input">
                                    <input class="form-control" type="text" placeholder="What is the subject of your support?" name="subject" required>
                                </div>
                            </div><br/>
                            <div class="col-md-12 mt-1">
                                Body
                                <div class="contact-form__input">
                                    <textarea class="form-control" placeholder="What would you want to ask us?" name="body" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="contact-form__input">
                                    <br/>

                                    <button class="btn btn-primary btn-hover-secondary">Submit support</button>
                                </div>
                            </div>
                        </div>
                    </div>



</form>
                    <!-- Contact Form Wrapper End -->
                </div>
            </div>
        </div>

    </div>


                    </div>


                </div>
            </div>
        </div>

</section>




                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
