@extends('layouts.studentlayout')
@section('title', 'Exam Success')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <!--<h4 class="card-title">My Certificates</h4>-->
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">




                       <div class="row">
                           <div class="col-md-2">
                           <h3 class="ml-1"><img src="{{asset('img/exam.png')}}" width="100px"/> </h3>
                           </div>
                           <div class="col-md-10">
                           <p class="lead"><span style="color: green">Congratulations  {{$user_name}}!</span> You've passed this exam with {{$userMark}}% score.
                           <br/>
Click the button below to generate your certificate. You can also generate your certificate in <br/><a href="{{route('mycertificates')}}">In My certificate page</a></p>

<p>
<br/>
<form method="post" action="{{route('generatecertificate')}}">
                                    @csrf

                                    <input type="hidden" name="course_id" value="{{$course_id}}"/>
                                    <button class="btn btn-info mg-r-5" style="cursor:pointer"> <i class="la la-save"></i>Generate Certificate</button>

                                    </form>
</p>
                           </div>

                       </div>
                        <br><br>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection


