@extends('layouts.studentlanding')
@section('title', 'Order history')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Order history</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">


                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show">
                    <div class="card-body card-dashboard">
                    </div>

                    @if(count($studentPayments) == 0)
                    <h4>No order history</h4>
                    @else

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered patients-list">
                            <thead style="background-color: #ffc221; color:#333">
                                <tr>
                                    <th>Course</th>
                                    <th>Amount paid</th>
                                    <th>Payment reference</th>
                                    <th>Date</th>

                                </tr>
                            </thead>
                            <tbody>
                           @foreach($studentPayments as $payment)
                                <tr>
                                    <td>{{$payment->course_title}}</td>
                                    <td>{{number_format($payment->amount_paid, 2)}}</td>
                                    <td>{{$payment->tempref}}</td>

                                  <td>{{$payment->created_at}}</td>



                                </tr>

                            @endforeach


                            </tbody>

                        </table>
                    </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
</section>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
