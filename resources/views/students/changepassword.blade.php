@extends('layouts.studentlayout')
@section('title', 'My courses')

@section('content')


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="dashboard-title">Settings</h4>
            </div>

            <div class="col-md-6">
                @include('layouts.message')
                @if (count($errors) > 0)
                    <div class="col-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            </div>
        </div>

        <!-- Dashboard Settings Start -->
        <div class="dashboard-settings">

            <!-- Dashboard Tabs Start -->
            <div class="dashboard-tabs-menu">
                <ul>
                    <li><a  href="{{route('profile')}}">Profile</a></li>
                    <li><a class="active" href="{{'completedcourses'}}">Change Password</a></li>
                    <!--<li><a href="{{'lockedcourses'}}">Locked courses</a></li>-->
                </ul>
            </div>
            <!-- Dashboard Tabs End -->


        </div>
        <!-- Dashboard Settings End -->

        <form action="{{route('savepassword')}}" method="post" id="pdo">
            @csrf
            <div class="row">
                <div class="col-lg-6">

                    <!-- Dashboard Settings Info Start -->
                    <div class="dashboard-content-box dashboard-settings__info">


                        <div class="row gy-4">
                            <div class="col-md-12">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Current Password</label>
                                    <input type="password" class="form-control" name="cpassword" required  placeholder="Enter current password" >
                                </div>
                                <!-- Account Account details End -->
                            </div>
                            <div class="col-md-6">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">New Password</label>
                                    <input type="password" class="form-control" name="npassword" required  placeholder="Enter new password">
                                </div>
                                <!-- Account Account details End -->
                            </div>
                            <div class="col-md-6">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Confirm New Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Enter confirm password" required>
                                </div>
                                <!-- Account Account details End -->
                            </div>
                        </div>

                    </div>
                    <!-- Dashboard Settings Info End -->

                </div>
            </div>

            <div class="dashboard-settings__btn">
                <button class="btn btn-primary btn-hover-secondary submit">Reset Password</button>
            </div>
        </form>





                </div>
            </div>





    </div>


</div>
<!-- Dashboard Content End -->




@endsection
