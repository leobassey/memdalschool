
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate</title>
    <link rel="stylesheet" href=
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src=
    "https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js">
        </script>
        <script src=
    "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js">
        </script>
        <script src=
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
        </script>


</head>

<body>
    <div class="row mt-2 ml-2" id="closediv">
        <div class="col-md-6 mb-3"></div>
        <div class="col-md-6 mb-3">
        <span><a href="#" onclick="printcertificate()" >Print Certificate</a></span> <span class="ml-2"><a href="{{route('mycertificates')}}">Go to My Certificate page</a></span>

        </div>
        </div>


    <div>
        <div style="width:1100px; height:700px; padding:13px; text-align:center; border: 10px solid #08193e; background-color: #08193e;">
            <div style="width:1000px; height:650px; padding:20px; text-align:center; border: 5px solid #fff;backbackground-color: #fff; background-image: url('bg.png'); background-size: cover; margin-left: 25px;">


        <table width="100%" style="margin-top:-20px">
            <tr>
                <td align="left" style="width: 60%;">

                    <img style="width: 40%; margin-top:13px" src="{{asset('img/Memdalschool.png')}}" alt="">


                </td>

                <td align="right" style="width: 40%;">


                    <div class="text-left">
                        <span style="display: block;
                font-size: 20px;
                font-family: sans-serif;

                height: auto;
                margin: 0px 50px;

                font-family: sans-serif;">
                    Certificate ID: {{$certNo}}



                </span>

                <span style="margin-left: 50px">www.memdalschool.com/verify</span>

               <!-- <hr style="width: 62%; margin-left: 2px">

                        <span style="display: block;
                padding-top: 0;
                color: #08193e  ;
                font-family: serif;
                font-size: 19px;">www.memdalschool.com/verify</span>-->
                    </div>



                </td>
            </tr>

        </table>



            <div class="row" style="height: 130px;">



                <table width="100%">
                    <tr>
                        <td align="left" style="width: 100%;">
                            <h3 style="margin-left:10px; font-size: 40px;
                            font-family: sans-serif; font-weight: bolder; color:#08193e; margin-top:50px">{{ $course_name }} </h3>
                        </td>

                        <td align="right">
                        </td>
                    </tr>

                </table>


            </div>





            <table width="100%" style="margin-top:-20px">
                <tr>
                    <td align="left" style="width: 40%;">
                        <span style="color: #333;
                        font-size: 25px;
                        sans-serif;">Certifcate is presented to:</span>
                        <h3>
                            <span id="holder-name" style="width: 455px; display: block; font-size: 50px;
                            padding-bottom: 0px; color:#08193e; font-weight: bold;
                           ">{{$user_name}}</span>
                        </h3>

                        <span style="color: #333;
                        font-size: 20px;
                        font-family: serif; display: block; margin-top:25px"><!--Having completed course requirements on June 03, 2022 --> Having completed <strong>Memdal School Virtual Training Program on {{$cert_date}}.</strong> </span>
                                <span style="display: block;
                        padding-top: 10px;
                        font-size: 21px;
                        font-weight: 700;  font-family: sans-serif;"></span>

            </td>

                    <td  style="width: 40%;">

                        <ul style="list-style: none; font-size: 20px; sans-serif;text-align: left;margin-left: 50px; padding-top: 25px;">
                           {!! $course->course_objectives !!}
                           </ul>

                    </td>
                </tr>

            </table>





        <table width="100%" style="margin-top:-130px">
            <tr>
                <td align="left" style="width: 40%; ">

                    <div class="text-left" style="padding-top: 70px;">
                        <span style="display: block;
                font-size: 20px;
                font-family: sans-serif;

                height: auto;


                font-family: sans-serif;">
                    <!-- <img src="{{asset('signatures/signature697851611867846.png')}}"  width="150px;" height="86px"/>-->

                    <img src="{{$userObjectForSignature->signature_url}}"  width="150px;" height="86px"/>




                </span>

                <!--<hr style="width: 62%; margin-left: 2px; margin-top:-10px">-->

                        <span style="display: block;
                padding-top: 0;
                color: #08193e  ;
                font-family: serif;
                font-size: 19px; margin-top:-10px">

                Chairman - Memdal School</span>
                    </div>


                </td>



                <td>

                    <!--
                    <div class="text-left" style="padding-top: 70px;">
                        <span style="display: block;
                font-size: 20px;
                font-family: sans-serif;

                height: auto;


                font-family: sans-serif;">
                    <img src="{{asset('signatures/signature697851611867846.png')}}"  width="178px;" height="86px"/>



                </span>

                <hr style="width: 62%; margin-left: 2px">

                        <span style="display: block;
                padding-top: 0;
                color: #08193e  ;
                font-family: serif;
                font-size: 19px;">Chairman - Memdal School</span>
                    </div>-->


                </td>


            </tr>

        </table>






           <!-- <div class="row" style="margin-top: -70px">
                <div class="col-md-7 text-left">
                    <span style="color: #333;
            font-size: 20px;
            sans-serif;">Certifcate is presented to:</span>
                </div>-->

                <!--
                <div class="col-md-5 text-left">
                    <ul style="list-style: none; font-size: 18px; sans-serif;">
                        <li>Data analysis fundamental</li>
                        <li>Data Transformation with Power Query</li>
                        <li>Data Manipulation with DAX</li>
                        <li>Data Visualization with Charts</li>
                        <li>Advanced Power BI Concept</li>
                        <li>Power BI Service</li>
                        <li>Dasgboard design with PowerPoint</li>
                       </ul>
                </div>

            </div>-->
           <!-- <div class="row" style="margin-top: -190px">
                <div class="col-md-7 text-left mt-4" style="margin-top: 0px">
                    <span id="holder-name" style="width: 455px; display: block; font-size: 30px;
            padding-bottom: 0px; color:#08193e; font-weight: bold;
           ">Emmanuel Bassey</span>
                </div>-->
           <!-- </div>-->
           <!-- <div class="row">
                <div class="col-md-12 text-left" style="padding-top: 20px;">
                    <span style="color: #333;
            font-size: 20px;
            font-family: serif; display: block;">Having completed course requirements on June 03, 2022 </span>
                    <span style="display: block;
            padding-top: 10px;
            font-size: 21px;
            font-weight: 700;  font-family: sans-serif;"></span>
                </div>
            </div>-->

        </div>
    </div>
    </div>

</body>


<script>
    function printcertificate() {
      document.getElementById("closediv").style.display = "none";
      window.print();
      window.location.href="mycertificates";
    }
    </script>

</html>


