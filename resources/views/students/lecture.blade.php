@extends('layouts.layout1')
<style>
    .tcvpb-videoWrapper-vimeo .player .sidedock .share-button{
    display: none !important;
}
</style>

@section('content')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
<div class="container-fluid px-0">
	<div class="page-wrapper">
		<!-- Left sidebar START -->
		<nav class="navbar navbar-expand-sm navbar-light bg-light px-3">
		  <a class="navbar-brand py-2 d-sm-none" href="#"></a>
		  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#docNav" aria-controls="docNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="docNav">
		    <div class="left-sidebar bg-light">
					<div class="content h-100vh">



                    <div class="course-sidebar-head" style=" padding-left: 25px;
    padding-right: 25px;
    padding-top: 25px;
    padding-bottom: 10px;
    margin-top: 0;
    font-size: var(--fs1);
    line-height: var(--lh1);
    font-weight: 600;">
    <h4 style="padding-bottom: 0px; font-size: 18px;" class="text-center">Course progress</h4>
    <!-- Course Progress -->

    <div class="course-progress lecture-page is-at-top" style="padding-left: 25px;
    padding-right: 25px;
    text-align: center;
    margin-top: 10px !important;
    margin-bottom: 25px;
    font-size: var(--fs0);
    line-height: var(--lh0);
    font-weight: 600;
    color: var(--obsidian);


    border-bottom: 1px solid var(--gravel);
}

">
      <div class="progressbar">
	  <div class="progress">
  <div class="progress-bar" role="progressbar" style="width:{{$countIsCompleted}}%; background-color: #08193e;" aria-valuenow="25%" aria-valuemin="0" aria-valuemax="100"></div>

  <!--<div class="progress-bar"  role="progressbar" style="width: {{$countIsCompleted}}%" aria-valuenow="{{$countIsCompleted}}" aria-valuemin="0" aria-valuemax="100"><p class="" style="color:#fff; background-color:#08193e">{{$countIsCompleted}}% completed</p></div>-->
</div>
      </div>
      <div class="small course-progress-percentage text-center mt-2" >
        <span class="percentage" style="font-weight:bolder; font-size:17px; color:#333">
          {{$countIsCompleted}}%
        </span>
		<span style="font-weight:bolder; font-size:17px; color:#333">
        COMPLETE
		<span>
      </div>
    </div>

  </div>





						<div class="list-group list-group-borderless p-3 p-md-4" style="overflow: scroll; height:500px">

						@foreach($moduleArray as $key)
							<div class="mb-2 mt-2"><b class="text-dark mb-2">{{$key["name"]}}</b></div>
							@foreach ($key["lectures"] as $one)

								<a class="list-group-item" href="{{route('lecture',  [$one["id"], $one["course_id"]])}}">
									@if($one["is_completed"] == 1)
                                    <div class="row">
                                        <div class="col-md-10"><?php echo $one["lecture_title"] ?></div>
                                        <div class="col-md-2"><i class="bi bi-check-square" style="color:#08193e !important; font-size:20px"></i></div>
                                    </div>


                                </a>
									@else

									<?php echo $one["lecture_title"] ?></a>
									@endif

							@endforeach

								@endforeach


						</div>


					</div>
				</div>
		  </div>
		</nav>
		<!-- Left sidebar END -->
<div class="main-content">

			<div class="row">
				<div class="col-12">
<div class="row mt-2 mb-4">
  <div class="col-md-8 mb-2" style="margin-top: 37px;">
  <h2 style="
    font-size: 18px;
    line-height: var(--lh3);
    font-weight: 500; margin-top:10px">{{$LectureObject->lecture_title}}</h2>


  </div>

  <div class="col-md-4 mb-2" style="margin-top: 13px;  font-size: 1rem;">


    <!--<span><a href="#">Downloads</a> </span>-->


    <a href="{{route('completed', [$LectureObject->id, $courseId])}}" class="btn mt-4" style="background-color:#08193e; color:#fff; font-size: 15px">Mark as completed</a>

    <a href="#" class="btn mt-4" style="background-color:#08193e; color:#fff; font-size: 15px">3 Discussions</a>

  </div>

  <hr style="height: 5px; color: #08193e">
</div>


@if($LectureObject->url != "")
<div style="" class="mt-2">


    <div style="

        border-radius: 5px;
        background: #08193e;
        border: 1px solid #eee; border-radius:20px">
<iframe class="mt-4 mb-4" src="{{$LectureObject->url}}" width="1000" height="500" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="2. Page navigation with drill-through"></iframe>

        <!--<iframe src="{{$LectureObject->url}}" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:30;left:0;width:95%;height:85%;" title="3 understaining-power-bi-environment-part-two" ></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>-->

    </div>

</div>
@endif
@if($LectureObject->note != "")
    <div class="mt-3" style="

    border-radius: 5px;
    background: #ffff;
    border: 1px solid #eee; padding:38px; text-align: justify; border-radius:20px">
    {!!$LectureObject->note!!}
    </div>
@endif


    <div class="mt-1">
        <a href="{{route('completed', [$LectureObject->id, $courseId])}}" class="btn mt-4" style="background-color:#08193e; color:#fff; font-size: 15px">Mark as completed</a>
    </div>







			</div>

		</div>

	</div>


</div>


@endsection
