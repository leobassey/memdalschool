@extends('layouts.studentlayout')
@section('title', 'Order history')

@section('content')


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
        <h4 class="dashboard-title">Cerificate</h4>


         <!-- Dashboard Purchase History Start -->
         <div class="dashboard-purchase-history">
            @if(count($studentCertificates) == 0)
            <h4>No certificate found</h4>
            @else
            <div class="dashboard-table table-responsive">
                <table class="table">
                    <thead>
                        <tr>

                            <th class="courses">Certificate No</th>
                                    <th class="amount">Course</th>
                                    <!--<th class="status">Score</th>
                                    <th class="date">Status</th>-->
                                    <th class="date"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($studentCertificates as $sc)
                        <tr>
                            <td>{{$sc->certificate_no}}</td>
                            <td>{{$sc->course_title}}</td>
                           <!-- <td>{{$sc->score}}</td>
                            <td>
                            @if($sc->score >= 70)

                           Pass
                           @else
                           Fail
                           @endif

                            </td>-->
                          <!--  <td>{{$sc->created_at}}</td>-->
                            <td>


                            <form method="post" action="{{route('generatecertificate')}}">
                            @csrf

                            <input type="hidden" name="course_id" value="{{$sc->course_id}}"/>
                            <button class="btn btn-info mg-r-5 ml-3" style="cursor:pointer; background-color:#08193e; color:#fff; border-color#08193e"> <i class="la la-save"></i>Generate</button>

                            </form>



                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>
            @endif
        </div>
        <!-- Dashboard Purchase History End -->


    </div>


</div>


@endsection
