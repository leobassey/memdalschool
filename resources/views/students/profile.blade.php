@extends('layouts.studentlayout')
@section('title', 'My courses')

@section('content')


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">

        <div class="row">
            <div class="col-md-6">
                <h4 class="dashboard-title">Settings</h4>
            </div>

            <div class="col-md-6">
                @include('layouts.message')
                @if (count($errors) > 0)
                    <div class="col-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            </div>
        </div>


        <!-- Dashboard Settings Start -->
        <div class="dashboard-settings">

            <!-- Dashboard Tabs Start -->
            <div class="dashboard-tabs-menu">
                <ul>
                    <li><a class="active" href="dashboard-settings.html">Profile</a></li>
                    <li><a href="{{'changepassword'}}">Change Password</a></li>
                    <!--<li><a href="{{'lockedcourses'}}">Locked courses</a></li>-->
                </ul>
            </div>
            <!-- Dashboard Tabs End -->


        </div>
        <!-- Dashboard Settings End -->



        <form action="{{route('saveprofile')}}" method="post" id="pdo">
            @csrf
            <div class="row gy-6">
                <div class="col-lg-6">

                    <!-- Dashboard Settings Info Start -->
                    <div class="dashboard-content-box">

                        <h4 class="dashboard-content-box__title">Contact information</h4>
                        <p>Provide your details below to create your account profile. The information you provide will be used for Memdal job board if you enable this setting.</p>

                        <div class="row gy-4">
                            <div class="col-md-6">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">First name</label>
                                    <input type="text" class="form-control" name="firstname" value="{{$userObject->name}}">
                                    <input type="hidden" class="form-control" name="userId" value="{{$userObject->id}}">

                                </div>
                                <!-- Account Account details End -->
                            </div>
                            <div class="col-md-6">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Last name</label>
                                    <input type="text" class="form-control" name="lastname" value="{{$userObject->lastname}}">
                                </div>
                                <!-- Account Account details End -->
                            </div>
                           <!-- <div class="col-md-6">

                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Job Title</label>
                                    <input type="text" class="form-control">
                                </div>

                            </div>-->
                            <div class="col-md-12">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Phone Number</label>
                                    <input type="text" class="form-control" name="phone" value="{{$userObject->phone}}">
                                </div>
                                <!-- Account Account details End -->
                            </div>
                            <div class="col-md-12">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Bio</label>
                                    <textarea class="form-control" name="bio"> {{$userObject->bio}}</textarea>
                                </div>
                                <!-- Account Account details End -->
                            </div>
                            <!--
                            <div class="col-md-6">

                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Twitter</label>
                                    <input type="text" class="form-control" name="twitter" value="{{$userObject->twitter}}">
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Facebook</label>
                                    <input type="text" class="form-control" name="facebook" value="{{$userObject->facebook}}">
                                </div>

                            </div>-->
                           <!-- <div class="col-md-6">

                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Instagram</label>
                                    <input type="text" class="form-control" name="instagram" value="{{$userObject->instagram}}">
                                </div>

                            </div>-->
                            <div class="col-md-12">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Linkedin</label>
                                    <input type="text" class="form-control" name="linkedin" value="{{$userObject->linkedin}}">
                                </div>
                                <!-- Account Account details End -->
                            </div>

                            <div class="col-md-12">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <label class="form-label-02">Github</label>
                                    <input type="text" class="form-control" name="github" value="{{$userObject->github}}">
                                </div>
                                <!-- Account Account details End -->
                            </div>
                        </div>

                    </div>
                    <!-- Dashboard Settings Info End -->

                    <div class="dashboard-settings__btn">
                        <button class="btn btn-primary btn-hover-secondary submit">Update Profile</button>
                    </div>
                </form>

                </div>
                <div class="col-lg-6">

                    <!-- Dashboard Settings Info Start -->
                    <div class="dashboard-content-box">

                        <form action="{{route('savecertname')}}" method="post" id="pdo2">
                            @csrf
                        <div class="dashboard-settings-profile-info">
                            <label class="dashboard-settings-profile-info__display-name">Display name on certificate  as</label>
                            <div class="col-md-12">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <input type="text" class="form-control" name="certname" value="{{$userObject->certname}}">
                                    <input type="hidden" class="form-control" name="user_id" value="{{$userObject->id}}">
                                </div>
                                <!-- Account Account details End -->
                            </div>

                            <div class="dashboard-settings__btn">
                                <button class="btn btn-primary btn-hover-secondary submit2">Update name</button>
                            </div>
                        </div>

                        </form>
                        <br/>

<hr/>



                        <h4 class="dashboard-content-box__title mt-5" >Photo</h4>
                        <p>Upload your profile photo. <span>Profile Photo Size: <strong>200x200</strong> pixels,</span></p>


                        <form action="{{route('savephoto')}}" method="post"  enctype="multipart/form-data" id="pdo3">
                            @csrf
                        <div class="dashboard-settings-profile-info">

                            <div class="col-md-12">
                                <!-- Account Account details Start -->
                                <div class="dashboard-content__input">
                                    <input type="file" class="form-control" name="photo" required>
                                    <input type="hidden" class="form-control" name="user_id" value="{{$userObject->id}}">
                                </div>
                                <!-- Account Account details End -->
                            </div>

                            <div class="dashboard-settings__btn">
                                <button class="btn btn-primary btn-hover-secondary submit3">Save photo</button>
                            </div>
                        </div>

                        </form>
                    </div>
                    <!-- Dashboard Settings Info End -->

                </div>
            </div>





    </div>


</div>
<!-- Dashboard Content End -->




@endsection
