@extends('layouts.studentlayout')
@section('title', 'Support')

@section('content')


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="dashboard-title">Support form</h4>
            </div>

            <div class="col-md-6">
                @include('layouts.message')
                @if (count($errors) > 0)
                    <div class="col-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            </div>
        </div>



        <!-- Dashboard Announcement Start -->
        <div class="dashboard-announcement">


           <!-- <div class="dashboard-content-box">


                <div class="dashboard-announcement-add">

                    <div class="dashboard-announcement-add__content-wrap">
                        <div class="dashboard-announcement-add__icon">
                            <i class="far fa-bell"></i>
                        </div>
                        <div class="dashboard-announcement-add__content">
                            <small>Create Announcement</small>
                            <p><strong>Notify all students of your course</strong></p>
                        </div>
                    </div>



                </div>

            </div>-->
            <!-- Dashboard Announcement Box End -->

            <!-- Dashboard Announcement Box Start -->
            <div class="dashboard-content-box">
                <form action="{{route('submitsupport')}}" method="post" id="pdo">
                    @csrf
                <div class="row">
                    <div class="col-md-10">
                        <div class="dashboard-content__input">
                            <label class="form-label-02">Subjects</label>

                            <div class="contact-form__input">
                                <input class="form-control" type="text" placeholder="What is the subject of your support ?" name="subject" required>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="row mt-5">
                    <div class="col-md-10">
                        <div class="dashboard-content__input">
                            <label class="form-label-02">Body</label>

                            <div class="contact-form__input">
                                <textarea col-3 class="form-control" type="text" placeholder="How can we help you ?" name="body" required></textarea>

                            </div>

                        </div>
                    </div>

                </div>


                <div class="dashboard-settings__btn">
                    <button class="btn btn-primary btn-hover-secondary submit">Submit support</button>
                </div>
            </div>
            <!-- Dashboard Announcement Box End -->


        </form>





    </div>


</div>


@endsection
