@extends('layouts.studentlayout')
@section('title', 'Exam')

@section('content')


<!-- Dashboard Content Start -->
<div class="dashboard-content">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                    <h3 class="ml-1"><img src="{{asset('img/exam.png')}}" width="100px"/> </h3>
                    </div>
                    <div class="col-md-10">
                    <p class="lead">Welcome to the course exam!
Now it’s time to test your knowledge and get your course certificate. You will be able to download your certificate after reaching a minimum score of 70%.</p>

<a class="btn btn-primary" href="{{route('coursesec', $id)}}" style="margin-top:20px; background-color:#02b159; border-color:#02b159;"><< Back to course modules</a>

                    </div>

                </div>
            </div>

            <div class="col-md-12">

                @include('layouts.message')
                @if (count($errors) > 0)
                    <div class="col-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            </div>
        </div>





        <div class="dashboard-announcement">


            <form action="{{ route('submitExam') }}" method="POST">
                @csrf
                <input type="hidden" value="{{ $id }}" name="course_id" />
                @foreach ($questions as $question)

                <div class="card-deck-wrapper">


                <div class="card" style="margin-top:30px">
                    <div class="card-header">



                        <p class="lead " style="font-size: 16px">{{ $question->question }}</p>

                    <div class="mb-3" style="">

                        <fieldset id="{{ $question->id }}">
                            <div class="form-control" style="height:65px">
                            <div class="ml-2 mt-1" >
                            <input class="form-check-input" type="radio" name="{{ $question->id }}" value="{{ $question->option1 }}" id="{{ $question->option1 }}">
                                <label class="form-check-label" for="{{ $question->option1 }}">
                                    {{ $question->option1 }}
                                </label>
                            </div>
                            </div>
                            <div class="form-control" style="height:65px">
                            <div class="ml-2 mt-1" >
                                <input class="form-check-input" type="radio" name="{{ $question->id }}" value="{{ $question->option2 }}" id="{{ $question->option2 }}">
                                <label class="form-check-label" for="{{ $question->option2 }}">
                                    {{ $question->option2 }}
                                </label>
                                </div>
                            </div>
                            <div class="form-control" style="height:65px">
                            <div class="ml-2 mt-1" >
                                <input class="form-check-input" type="radio" name="{{ $question->id }}" value="{{ $question->option3 }}" id="{{ $question->option3 }}">
                                <label class="form-check-label" for="{{ $question->option3 }}">
                                {{$question->option3}}
                                </label>
                            </div>
                            </div>
                            <div class="form-control" style="height:65px">
                            <div class="ml-2 mt-1" >
                                <input class="form-check-input" type="radio" name="{{ $question->id }}" value="{{ $question->option4 }}" id="{{ $question->option4 }}">
                                <label class="form-check-label" for="{{ $question->option4 }}">
                                    {{ $question->option4 }}
                                </label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                </div>
                @endforeach
                <button class="btn btn-primary ml-2 mt-5" type="submit" style="margin-top:-20px">Finish Exam</button>
            </form>


        </div>














@endsection


