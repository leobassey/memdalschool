@extends('layouts.studentlayout')
@section('title', 'Support')

@section('content')

<style>
    .rating {
    float:left;
}
.rating:not(:checked) > input {
    position:absolute;
   /* top:-9999px;*/
    clip:rect(0,0,0,0);
}
    .rating:not(:checked) > label {
    float:right;
    width:1em;
    padding:0 .1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:200%;
    line-height:1.2;
    color:#ddd;
    text-shadow:1px 1px #bbb, 2px 2px #666, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:before {
    content: '★ ';
}

.rating > input:checked ~ label {
    color: #f70;
    text-shadow:1px 1px #c60, 2px 2px #940, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:hover,
.rating:not(:checked) > label:hover ~ label {
    color: gold;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > input:checked + label:hover,
.rating > input:checked + label:hover ~ label,
.rating > input:checked ~ label:hover,
.rating > input:checked ~ label:hover ~ label,
.rating > label:hover ~ input:checked ~ label {
    color: #ea0;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > label:active {
    position:relative;
    top:2px;
    left:2px;
}
</style>


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="dashboard-title">Course feedback</h4>
            </div>

            <div class="col-md-6">
                <span class="dashboard-header__btn">
                    <a class="btn btn-outline-primary" href="{{route('coursesec', $course->id)}}"><< Back</a>
                </span>
                @include('layouts.message')
                @if (count($errors) > 0)
                    <div class="col-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif


            </div>
        </div>


        <div class="dashboard-announcement">

            <div class="dashboard-content-box">
               Thank you for being a part of Memdal School community. We are constantly improving our platform and services. We do this using the feeback our students give us.


               Please rate this course to help others take a better decision. Your feedback is absolutely welcome.

                <br/><br/>

               <p>Note: This is a feedback form only, if you require help, please visit our <a href="{{route('supports')}}"><u>Support Page</u></a></p>
            </div>

        </div>



        <!-- Dashboard Announcement Start -->
        <div class="dashboard-announcement">



            <!-- Dashboard Announcement Box Start -->
            <div class="dashboard-content-box">
                <form action="{{route('submitfeedback')}}" method="post" id="pdo">
                    @csrf


                <div class="row mt-5">
                    <div class="col-md-10">
                        <div class="dashboard-content__input" style="margin-top:-20px">
                            <label class="form-label-02">Body</label>

                            <div class="contact-form__input">
                                <textarea col-3 class="form-control" type="text" placeholder="What is your review for this course ?" name="body" required></textarea>
                                <input class="form-control" type="hidden" name="course_id" required value="{{$course->id}}">
                            </div>

                        </div>



                    </div>



                </div>

                <br/> <br/>


                <fieldset class="rating" style="margin-top:-30px">

                    <input type="radio" id="star5" name="rating" value="5" required/><label for="star5" title="Excellent">5 stars</label>
                    <input type="radio" id="star4" name="rating" value="4" required/><label for="star4" title="Very good">4 stars</label>
                    <input type="radio" id="star3" name="rating" value="3" required/><label for="star3" title="Good">3 stars</label>
                    <input type="radio" id="star2" name="rating" value="2" required/><label for="star2" title="Somehow good">2 stars</label>
                    <input type="radio" id="star1" name="rating" value="1" required/><label for="star1" title="Bad">1 star</label>
                    </fieldset>



                <div class="dashboard-settings__btn">
                    <button class="btn btn-primary btn-hover-secondary submit">Submit feedback</button>
                </div>
            </div>
            <!-- Dashboard Announcement Box End -->


        </form>





    </div>

    @foreach($feedbacks as $f)
    <div class="dashboard-announcement mt-5">



            <div class="dashboard-review-item mt-1" >
                <div class="dashboard-review-item__thumbnail">
                    <img src="{{asset('img/avatar.jpg')}}" alt="Courses" width="50px" height="50px" style="border-radius: 50%; height:50px; width:50px">
                </div>
                <div class="dashboard-review-item__content">
                    <div class="dashboard-review-item__title-wrap">
                        <h2 class="dashboard-review-item__title"><span>{{$f->name}}</span> <span style="font-size: 14px" class="ml-1">4 months ago</span></h2>
                        <!--<div class="dashboard-review-item__review-links">
                            <a href="#"><i class="far fa-pencil"></i><span>Edit Feedback</span></a>
                        </div>-->
                    </div>

                    <div class="dashboard-review-item__text" style="margin-top: -8px">
                        <p>{{$f->reviews}}</p>
                    </div>
                </div>
            </div>



    </div>

    @endforeach


</div>


@endsection
