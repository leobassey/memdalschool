@extends('layouts.studentlanding')
@section('title', 'My Courses')

@section('content')
<style>

.card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: cover;
}

</style>

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">

                            <div class="row">
                            <div class="col-md-10">
                            <h4 class="card-title">Locked Courses - courses whose duration has expired</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="col-md-2">
                            <a class="btn btn-info" href="{{route('onlinecourses')}}" style="color:#fff; background-color:#08193e; border-color:#08193e"><i class="bi bi-plus"></i> Add Course</a>
                            </div>
                            </div>


                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">


                                <div class="row" >


		<div class="col-12">
			<div class="card-deck-wrapper">
				<div class="card-deck">
                @foreach($studentCourses as $courses)
                <div class="col-md-4 mt-1">
					<div class="card">
						<div class="card-content">
                            <a href="#">
                        @if($courses->image_url == "")

                        <img class="card-img-top img-fluid card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="Card image cap"/>
                        @else
							<img class="card-img-top img-fluid card-img-top" src="{{$courses->image_url}}" alt="Card image cap"/>
                        @endif
                        </a>
							<div class="card-body">
                            @if($courses->course_type == 1)
								<h4 class="card-title" ><a href="#" style="color:#000; font-size:18px">{{$courses->course_title}}</a>

                                <div class="mt-1">Status: Locked</<div>
                                <!-- <div class="progress" style="margin-top:5px" style="height: 1px;">
                                     Enrolled
                                <div class="progress-bar" role="progressbar" style="width: {{$countIsCompleted}}%" aria-valuenow="{{$countIsCompleted}}" aria-valuemin="0" aria-valuemax="100">{{$countIsCompleted}}%</div>
                                </div>-->
                            </h4>
                        @elseif($courses->course_type == 3)
                        <h4 class="card-title"><a href="{{route('coursesec', $courses->id)}}">{{$courses->course_title}}</a></h4>
                        @else
                        <h4 class="card-title">{{$courses->course_title}}</h4>
                        @endif


						@if($courses->course_type == 1)

                        <p class="card-text"  style="font-size:18px; margin-top:-20px"><small class="text-muted">Course type: <span style="font-weight:bold">Online</span></small></p>
                        @elseif($courses->course_type == 3)

                        <p class="card-text" style="font-size:18px; margin-top:-20px"><small class="text-muted">Course type: <span style="font-weight:bold">Free</span></small></p>
                        @else
                        <p class="card-text" style="font-size:18px; margin-top:-20px"><small class="text-muted">Course type: <span style="font-weight:bold">Campus</span></small></p>
                        @endif



                        <a class="btn btn-info" href="#" style="color:#fff; background-color:#08193e; border-color:#08193e; margin-top:11px"><i class="bi bi-plus"></i> Unlock Course</a>

							</div>
						</div>
                    </div>

</div>
                @endforeach

				</div>
			</div>
		</div>



        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
