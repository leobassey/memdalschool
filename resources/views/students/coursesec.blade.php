@extends('layouts.studentlayout')
@section('title', 'Course section')

@section('content')

 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
<div class="mb-1">
    @include('layouts.message')
                @if (count($errors) > 0)
                    <div class="col-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
</div>
        <div class="row">
            <div class="col-md-6">
                <h4 class="dashboard-title">Course details</h4>
            </div>

            <div class="col-md-6">
                <span class="dashboard-header__btn">
                    <a class="btn btn-outline-primary" href="{{route('feedback', $courseObect->id)}}"><span class="text">Rate this Course</span></a>
                </span>

                @if($studentCourseObj->is_active == 0)

                <span class="dashboard-header__btn">
                    <a class="btn btn-outline-primary" href="{{route('unlock', $studentCourseObj->id)}}"><span class="text">Unlock this course</span></a>
                </span>

                @endif

            </div>


        </div>

        @if($studentCourseObj->is_completed == 1)

        <div class="col-md-12 dashboard-content-box mb-4">

            <div class="row">
                <div class="col-md-8">

                    <img class="" src="{{asset('img/trophy.png')}}" alt="trophy" width="60px"/>
                    <span style="margin-left: 10px; font-weight:bold; font-size:18px">Congratulations on completing your course!</span><span style="margin-left: 5px; font-size:15px"><a href="{{route('mycertificates')}}" style="color:#1358db; "><u>Generate Certificate</u></a></span>

                </div>

            </div>
         </div>
        @endif


        <div class="row gy-6 mb-5">
            <div class="col-lg-6">

                <!-- Dashboard Settings Info Start -->
                <div class="dashboard-content-box">


                    <div class="card">
                        <div class="card-header">
                        <div class="card-content">
                                        @if($courseObect->image_url == "")
                                        <img class="card-img-top img-fluid card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="Card image cap"/>
                                        @else
                                            <img class="card-img-top img-fluid card-img-top" src="{{asset('/'.$courseObect->image_url)}}" alt="Card image cap"/>
                                        @endif

                </div>
                <div class="card-body">
                   <h5> {{$courseObect->course_title}}</h5><br/>

                     <div class="progress" style="margin-top:5px" style="height: 1px;">

                    <div class="progress-bar"  role="progressbar" style="width: {{$countIsCompleted}}%" aria-valuenow="{{$countIsCompleted}}" aria-valuemin="0" aria-valuemax="100"><p class="" style="color:#fff; background-color:#08193e">{{$countIsCompleted}} %  completed</p></div>
                    </div>



                    <div class="row mt-5">

                        <div class="col-md-5"><a href="{{route('lecture', [$firstLecture->id, $firstLecture->course_id])}}" class="btn" style="background-color:#08193e"><p class="" style="color:#fff">Start Lecture</p></a></div>
                        <div class="col-md-7"><p class="lead">{{$countStudents}} students enrolled</p></div>
                    </div>


                </div>
                        </div>
                        </div>





                </div>

            </div>

            <div class="col-lg-6">

                <!-- Dashboard Settings Info Start -->
                <div class="dashboard-content-box" style="height: 280px">



                           <h4>Course resources </h4>
                           <hr>

                           @if($studentCourseObj->is_active == 0)
                           <div>
                            <p>Course is locked! Unlock course to have access to resources</p>
                           </div>
                           @else
                           <div>

                           <ul style="list-style-type:none; margin-left: -30px;">
                               @foreach($courseResources as $r)
                               <li class="mt-1"><a href="{{$r->resource_link}}" target="_blank" style="color:#08193e; font-size:16px"> {{$r->resource_name}} </a><li>
                               @endforeach
                           </ul>

                        </div>
                        @endif


                </div>


                <div class="dashboard-content-box">



                    <h4>Exam (Optional and ungraded)</h4>
                    <hr>

                    @if($studentCourseObj->is_active == 0)

                    <div>
                        <p>Course is locked! Unlock course to write exam</p>
                    </div>

                    @else

                    <div>

                        @if($courseObect->is_exam == 0)
                        <p class="lead" style="font-size:17px">No exam for this course</p>
                        @else
                        <div class="">
                        <img src="{{asset('img/exam.png')}}" width="100px"/>
                        <p class="lead mt-1"><a href="{{route('exam', $id)}}" style="color:#1358db; font-size:16px"> <i class="bi bi-pencil-square"></i> Write Exam</a></p>
                        </div>
                        @endif

                 </div>
                 @endif


         </div>


        </div>






        <div class="row gy-6" style="margin-top: -15px">
            <div class="col-lg-6">

                <!-- Dashboard Settings Info Start -->
                <div class="dashboard-content-box">

                    <h4>Student success advisor <h4>
                        <hr>

                        <div>
                            <p class="lead" style="font-size:17px">studentsuccess@memdalschool.com</p>
                        </div>

                </div>

            </div>


            <div class="col-lg-6">

                <!-- Dashboard Settings Info Start -->
                <div class="dashboard-content-box">

                    <h4>Student mentor<h4>
                        <hr>

                        <div>
                            <p class="lead" style="font-size:17px">studentsuccess@memdalschool.com</p>
                        </div>

                </div>

            </div>


        </div>


        <div class="row gy-6" style="margin-top: -15px">
            <div class="col-lg-12">
                <div class="dashboard-content-box">


                    <div class="row">
                    <div class="col-lg-10">


                        <h4 class="ml-2"><b>Course Curriculum</b></h4> <p class="ml-2 lead" style="font-size:16px">Click on any lesson under module to start learning</p>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        </div>


                        <div class="col-lg-2">
                            <a href="{{route('lecture', [$firstLecture->id, $firstLecture->course_id])}}" style="color:#08193e; font-weight:bold">Start lecture</a>
                        </div>


                    </div>



                </div>

            </div>



        </div>







     <div class="row gy-6" style="margin-top: -15px">

        <div class="col-lg-12">

            <!-- Dashboard Settings Info Start -->


                @foreach($moduleArray as $key)

                                <div class="col-12 mt-2">
			<div class="card-deck-wrapper">
				<div class="card-deck">

                <div class="card">
                            <div class="card-header">

                            <div class="row">
                            <div class="col-md-12">
                            <h4 class="card-title mb-1" style="font-size:17px"><strong>{{$key["name"]}}</strong></h4>
                            @foreach ($key["lectures"] as $one)
                               <a class="list-group-item" style="border:none; margin-top:0px; color:#333; font-size:15px" href="{{route('lecture',  [$one["id"], $one["course_id"] ])}}">
									@if($one["is_completed"] == 1)
									<i class="bi bi-check-square mr-1" style="color:#1358db !important; font-size:20px"></i>
									{{$one["lecture_title"]}}</a>
									@else

									{{$one["lecture_title"]}}</a>
									@endif

                                    @endforeach

                            </div>

                            </div>


                            </div>




                            </div>
							</div>
						</div>




								@endforeach




            </div>

        </div>

     </div>









        <br/>










<!--


    <div class="conatiner">
    <div class="row gy-6">
        <div class="col-md-7" >
        <div class="card">
        <div class="card-header">
        <div class="card-content">
                        @if($courseObect->image_url == "")
                        <img class="card-img-top img-fluid card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="Card image cap"/>
                        @else
							<img class="card-img-top img-fluid card-img-top" src="{{asset('/'.$courseObect->image_url)}}" alt="Card image cap"/>
                        @endif

</div>
<div class="card-body">
   <h2> {{$courseObect->course_title}}</h2><br/>

     <div class="progress" style="margin-top:5px" style="height: 1px;">

                                <div class="progress-bar"  role="progressbar" style="width: {{$countIsCompleted}}%" aria-valuenow="{{$countIsCompleted}}" aria-valuemin="0" aria-valuemax="100"><p class="" style="color:#fff; background-color:#08193e">{{$countIsCompleted}}% completed</p></div>
                                </div>

    <div class="row">


        <div class="col-md-4"><a href="{{route('lecture', [$firstLecture->id, $firstLecture->course_id])}}" class="btn" style="background-color:#08193e"><p class="" style="color:#fff">Start Lecture</p></a></div>
        <div class="col-md-8"><p class="lead">{{$countStudents}} students currently enrolled in this course</p></div>
    </div>


</div>
        </div>
        </div>


        </div>
        <div class="col-md-5">


            <div class="card">
        <div class="card-header">
           <h4>Course resources </h4>
           <hr>
           <div>

           <ul>
               @foreach($courseResources as $r)
               <li><a href="{{$r->resource_link}}" target="_blank" style="color:#08193e; font-size:16px"> {{$r->resource_name}} </a><li>
               @endforeach
           </ul>

        </div>
        </div>
        </div>



        <div class="card" style="margin-top:-20px">
        <div class="card-header">
          <h4> Course Exam<h4>
              <hr>

              @if($courseObect->is_exam == 0)
              <p class="lead" style="font-size:17px">No exam for this course</p>
              @else
              <div class="text-center">
              <img src="{{asset('img/exam.png')}}" width="100px"/>
              <p class="lead mt-1"><a href="{{route('exam', $id)}}" style="color:#1358db; font-size:16px"> <i class="bi bi-pencil-square"></i> Write Exam</a></p>
              </div>
              @endif
        </div>
        </div>


        <div class="card" style="margin-top:-20px">
        <div class="card-header">
           <h4>Student success advisor <h4>
               <hr>

               <div>
                   <p class="lead" style="font-size:17px">studentsuccess@memdalschool.com</p>
               </div>
        </div>
        </div>




    </div>
    </div>

    </div>





			            <div class="card-deck-wrapper">
				        <div class="card-deck">

</div>
</div>
</div>















                        <div class="col-12" style="margin-top:-45px">
			<div class="card-deck-wrapper">
				<div class="card-deck">
<div class="card">

<div class="card-header">


<div class="row gy-6">

                            <div class="col-md-10">
                            <h4 class="ml-2"><b>Course Curriculum</b></h4> <p class="ml-2 lead" style="font-size:16px">Click on any lesson under module to start learning</p>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="col-md-2">
                            <a href="{{route('mycourses')}}" style="color:#08193e; font-weight:bold">My Courses</a>
                            </div>
                            </div>


</div>

</div>



</div>
</div>







                                <div class="row" >

                                @foreach($moduleArray as $key)

                                <div class="col-12">
			<div class="card-deck-wrapper">
				<div class="card-deck">

                <div class="card">
                            <div class="card-header">

                            <div class="row">
                            <div class="col-md-10">
                            <h4 class="card-title mb-1" style="font-size:17px"><strong>{{$key["name"]}}</strong></h4>
                            @foreach ($key["lectures"] as $one)
                               <a class="list-group-item" style="border:none; margin-top:0px; color:#333; font-size:15px" href="{{route('lecture',  [$one["id"], $one["course_id"] ])}}">
									@if($one["is_completed"] == 1)
									<i class="bi bi-check-square mr-1" style="color:#1358db !important; font-size:20px"></i>
									{{$one["lecture_title"]}}</a>
									@else

									{{$one["lecture_title"]}}</a>
									@endif

                                    @endforeach

                            </div>

                            </div>


                            </div>




                            </div>
							</div>
						</div>
					</div>



								@endforeach








</div>
</div>
</div>

				</div>
			</div>
		</div>



        </div>
    -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
