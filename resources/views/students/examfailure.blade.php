@extends('layouts.studentlayout')
@section('title', 'Exam Success')

@section('content')

<div class="dashboard-content">



    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                    <h3 class="ml-1"><img src="{{asset('img/exam.png')}}" width="100px"/> </h3>
                    </div>

                    <div class="col-md-9">
                        <p class="lead"><span style="color: red">Sorry  {{$user_name}}!</span> You've failed this exam with {{$userMark}}% score. The passing score for this exam is 70% and above. You can attempt this exam as many times as possible. <p class="lead">Please note, you must complete a course 100% and  pass course exam to be elegible for course certificate. Good luck!</p>
                        <br/>
                        <span class="dashboard-header__btn">


<a  class="btn btn-outline-primary" href="{{route('exam', $course_id)}}" style="font-size: 18px">Retake exam</a></p>
</span>

                        </div>

                </div>
            </div>





            <!--
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">

                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">




                       <div class="row">
                           <div class="col-md-2">
                           <h3 class="ml-1"><img src="{{asset('img/exam.png')}}" width="100px"/> </h3>
                           </div>
                           <div class="col-md-10">
                           <p class="lead"><span style="color: red">Sorry  {{$user_name}}!</span> You've failed this exam with {{$userMark}}% score. The passing score for this exam is 70% and above. You can attempt this exam as many times as possible. <p class="lead">Please note, you must complete a course 100% and  pass course exam to be elegible for course certificate. Good luck!</p>
                           <br/>
 <br/><a href="{{route('exam', $course_id)}}" style="font-size: 18px">Retry this exam</a></p>


                           </div>

                       </div>
                        <br><br>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>-->


        </div>


    </div>

@endsection


