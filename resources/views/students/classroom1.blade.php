@extends('layouts.studentlayout')
@section('title', 'dashboard')

@section('content')


        <!-- Dashboard Content Start -->
        <div class="dashboard-content">

            <div class="container">
                <h4 class="dashboard-title">Dashboard</h4>

                <!-- Dashboard Info Start -->
                <div class="dashboard-info">
                    <div class="row gy-2 gy-sm-6">
                        <div class="col-md-4 col-sm-6">
                            <!-- Dashboard Info Card Start -->
                            <div class="dashboard-info__card">
                                <a class="dashboard-info__card-box" href="{{route('mycourses')}}">
                                    <div class="dashboard-info__card-icon icon-color-01">
                                        <i class="edumi edumi-open-book"></i>
                                    </div>
                                    <div class="dashboard-info__card-content">
                                        <div class="dashboard-info__card-value">{{$enrolledcourses}}</div>
                                        <div class="dashboard-info__card-heading">My Courses</div>
                                    </div>
                                </a>
                            </div>
                            <!-- Dashboard Info Card End -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Dashboard Info Card Start -->
                            <div class="dashboard-info__card">
                                <a class="dashboard-info__card-box" href="{{route('lockedcourses')}}">
                                    <div class="dashboard-info__card-icon icon-color-02">
                                        <i class="edumi edumi-streaming"></i>
                                    </div>
                                    <div class="dashboard-info__card-content">
                                        <div class="dashboard-info__card-value">{{$lockedcourses}}</div>
                                        <div class="dashboard-info__card-heading">Locked Courses</div>
                                    </div>
                                </a>
                            </div>
                            <!-- Dashboard Info Card End -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- Dashboard Info Card Start -->
                            <div class="dashboard-info__card">
                                <a class="dashboard-info__card-box" href="{{route('completedcourses')}}">
                                    <div class="dashboard-info__card-icon icon-color-03">
                                        <i class="edumi edumi-correct"></i>
                                    </div>
                                    <div class="dashboard-info__card-content">
                                        <div class="dashboard-info__card-value">{{$completedcourses}}</div>
                                        <div class="dashboard-info__card-heading">Completed Courses</div>
                                    </div>
                                </a>
                            </div>
                            <!-- Dashboard Info Card End -->
                        </div>



                        <div class="col-md-4 col-sm-6">
                            <!-- Dashboard Info Card Start -->
                            <div class="dashboard-info__card">
                                <a class="dashboard-info__card-box" href="{{route('mycertificates')}}">

                                    <div class="dashboard-info__card-icon icon-color-04">
                                        <i class="edumi edumi-group"></i>
                                    </div>
                                    <div class="dashboard-info__card-content">
                                        <div class="dashboard-info__card-value">{{$certificatecount}}</div>
                                        <div class="dashboard-info__card-heading">Certificates</div>
                                    </div>

                            </a>
                            </div>
                            <!-- Dashboard Info Card End -->
                        </div>
                        <!--<div class="col-md-4 col-sm-6">

                            <div class="dashboard-info__card">
                                <div class="dashboard-info__card-box">
                                    <div class="dashboard-info__card-icon icon-color-05">
                                        <i class="edumi edumi-user-support"></i>
                                    </div>
                                    <div class="dashboard-info__card-content">
                                        <div class="dashboard-info__card-value">{{$lockedcourses}}</div>
                                        <div class="dashboard-info__card-heading">Locked Courses</div>
                                    </div>
                                </div>
                            </div>

                        </div>-->
                        <!--
                        <div class="col-md-4 col-sm-6">

                            <div class="dashboard-info__card">
                                <div class="dashboard-info__card-box">
                                    <div class="dashboard-info__card-icon icon-color-06">
                                        <i class="edumi edumi-coin"></i>
                                    </div>
                                    <div class="dashboard-info__card-content">
                                        <div class="dashboard-info__card-value"><span class="sale-price">$383.<small class="separator">01</small></span></div>
                                        <div class="dashboard-info__card-heading">Total Earnings</div>
                                    </div>
                                </div>
                            </div>

                        </div>-->
                    </div>
                </div>
                <!-- Dashboard Info End -->
            </div>


        </div>
        <!-- Dashboard Content End -->


@endsection
