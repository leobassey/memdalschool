@extends('layouts.studentlanding')
@section('title', 'dashboard')

@section('content')



<!-- BEGIN: Content-->
<div id="app2">

        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">

            <!-- Hospital Info cards -->
            <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-center">
                                            <i class="la la-ship font-large-2 success"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h5 class="text-muted text-bold-500"><a href="{{route('mycourses')}}" style="color:#0071dc">Active courses</a></h5>
                                            <h3 class="text-bold-600" style="color:#08193e">{{$activecourses}}</h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-center">
                                            <i class="la la-street-view font-large-2 warning"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h5 class="text-muted text-bold-500"><a href="{{route('lockedcourses')}}" style="color:#0071dc">Locked Courses</a></h5>
                                             <h3 class="text-bold-600" style="color:#08193e">{{$lockedcourses}}</h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-center">
                                            <i class="la la-database font-large-2 info"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h5 class="text-muted text-bold-500"><a href="mycertificates" style="color:#0071dc">Certificates</a></h5>
                                              <h3 class="text-bold-600" style="color:#08193e">{{$certificatecount}}</h3>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"></h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                              <div class="card-content collapse show">
                                <div class="card-body" style="height:500px">
                                    <div id="chartContainer" class="text-center" >

                                        <img src="{{asset('assets/img/flat-graphic-4.png')}}" width="40%" />
                                    </div>
</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>



    <!-- END: Content-->


</div>


@endsection
