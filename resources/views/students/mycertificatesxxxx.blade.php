@extends('layouts.studentlanding')
@section('title', 'My Certificates')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">My Certificates</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">


                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show">
                    <div class="card-body card-dashboard">
                    </div>

                    @if(count($studentCertificates) == 0)
                    <h4>No certificate found</h4>
                    @else
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered patients-list">
                            <thead style="background-color: #ffc221; color:#333">
                                <tr>
                                    <th>Certificate No</th>
                                    <th>Course</th>
                                    <th>Score</th>
                                    <th>Status</th>
                                   <!-- <th>Date</th>-->

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                           @foreach($studentCertificates as $sc)
                                <tr>
                                    <td>{{$sc->certificate_no}}</td>
                                    <td>{{$sc->course_title}}</td>
                                    <td>{{$sc->score}}</td>
                                    <td>
                                    @if($sc->score >= 70)

                                   Pass
                                   @else
                                   Fail
                                   @endif

                                    </td>
                                  <!--  <td>{{$sc->created_at}}</td>-->
                                    <td>

                                    @if($sc->score >= 70)
                                    <form method="post" action="{{route('generatecertificate')}}">
                                    @csrf

                                    <input type="hidden" name="course_id" value="{{$sc->course_id}}"/>
                                    <button class="btn btn-info mg-r-5 ml-3" style="cursor:pointer; background-color:#08193e; color:#fff; border-color#08193e"> <i class="la la-save"></i>Generate Certificate</button>

                                    </form>

                                    @else
                                    No Certificate
                                    @endif


                                    </td>
                                </tr>

                            @endforeach


                            </tbody>

                        </table>
                    </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
</section>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
