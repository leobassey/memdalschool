@extends('layouts.studentlayout')
@section('title', 'My courses')

@section('content')


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
        <h4 class="dashboard-title">My courses</h4>

        <!-- Dashboard Settings Start -->
        <div class="dashboard-settings">

            <!-- Dashboard Tabs Start -->
            <div class="dashboard-tabs-menu">
                <ul>
                    <li><a class="active" href="{{route('mycourses')}}">Active courses</a></li>
                    <li><a href="{{'completedcourses'}}">Completed courses</a></li>
                    <li><a href="{{'lockedcourses'}}">Locked courses</a></li>
                </ul>
            </div>
            <!-- Dashboard Tabs End -->


              <!-- Dashboard Course List Start -->
              <div class="dashboard-course-list">





                @foreach($studentCourses as $courses)
                <!-- Dashboard Course Item Start -->
                <div class="dashboard-course-item">
                    <a class="dashboard-course-item__link" href="{{route('coursesec', $courses->id)}}">

                        @if($courses->image_url == "")
                        <div class="dashboard-course-item__thumbnail">
                            <img class="card-img-top img-fluid card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="Card image cap" width="260" height="160"/>

                        </div>

                        @else

                        <div class="dashboard-course-item__thumbnail">
                            <img class="card-img-top img-fluid card-img-top" src="{{$courses->image_url}}" alt="Card image cap" width="260" height="160"/>

                        </div>
                        @endif

                        <div class="dashboard-course-item__content">
                            <div class="dashboard-course-item__rating">
                                <!--<div class="rating-star">
                                    <div class="rating-label" style="width: 80%;"></div>
                                </div>-->
                            </div>
                            <h3 class="dashboard-course-item__title">{{$courses->course_title}}</h3>
                            <div class="dashboard-course-item__meta">
                                <ul class="dashboard-course-item__meta-list">
                                    <li> <span class="meta-label">Course due date: </span>
                                        <span class="meta-value"> {{ \Carbon\Carbon::parse($courses->expire_date)->format('M d, Y')}}</span>
                                    </li>
                                    <li>

                                        <span class="meta-label">Course type:</span>
                                        <span class="meta-value">
                                            @if($courses->course_type == 1)

                         Online
                        @elseif($courses->course_type == 3)

                       Free
                        @else
                       Campus
                        @endif
                                        </span>
                                    </li>
                                    <!--<li>
                                        <span class="meta-label">Lessons:</span>
                                        <span class="meta-value">14</span>
                                    </li>-->
                                </ul>
                            </div>
                           <!-- <div class="dashboard-course-item__progress-bar-wrap">
                                <div class="dashboard-course-item__progress-bar">
                                    <div class="dashboard-course-item__progress-bar-line" style="width: 22%;"></div>
                                </div>
                                <div class="dashboard-course-item__progress-bar-text">22% Complete</div>
                            </div>-->
                        </div>
                    </a>
                </div>
              @endforeach









        </div>
        <!-- Dashboard Settings End -->
    </div>


</div>
<!-- Dashboard Content End -->




@endsection
