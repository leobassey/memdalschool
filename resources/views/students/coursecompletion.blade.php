@extends('layouts.studentlayout')
@section('title', 'Course completion')

@section('content')

<style>
    .rating {
    float:left;
}
.rating:not(:checked) > input {
    position:absolute;
   /* top:-9999px;*/
    clip:rect(0,0,0,0);
}
    .rating:not(:checked) > label {
    float:right;
    width:1em;
    padding:0 .1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:200%;
    line-height:1.2;
    color:#ddd;
    text-shadow:1px 1px #bbb, 2px 2px #666, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:before {
    content: '★ ';
}

.rating > input:checked ~ label {
    color: #f70;
    text-shadow:1px 1px #c60, 2px 2px #940, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:hover,
.rating:not(:checked) > label:hover ~ label {
    color: gold;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > input:checked + label:hover,
.rating > input:checked + label:hover ~ label,
.rating > input:checked ~ label:hover,
.rating > input:checked ~ label:hover ~ label,
.rating > label:hover ~ input:checked ~ label {
    color: #ea0;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > label:active {
    position:relative;
    top:2px;
    left:2px;
}
</style>


 <!-- Dashboard Content Start -->
 <div class="dashboard-content">

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="dashboard-title">Course complete</h4>
            </div>

            <div class="col-md-6">
               <!-- <span class="dashboard-header__btn">
                    <a class="btn btn-outline-primary" href="#"><< Back</a>
                </span>-->
                @include('layouts.message')
                @if (count($errors) > 0)
                    <div class="col-12 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif


            </div>
        </div>


        <div class="dashboard-announcement">

            <div class="dashboard-content-box">

                <h3 style="color:darkseagreen">Success !</h3>
                <br/>
               Congratulations on completing <span style="font-weight: bold">{{$course->course_title}} course.</span> The following name will appear in your certificate of completion.

               <h5 class="mt-2">{{$certname}}</h5>


               <p class="mt-2">If this is not correct, <a href="{{route('profile')}}"><u>update your name here</u></a></p>

               <h5 class="mt-2"><a href="{{route('mycertificates')}}"><u>Get Certificate</u></a></h5>
            </div>

        </div>



        <!-- Dashboard Announcement Start -->
        <div class="dashboard-announcement">

            <!-- Dashboard Announcement Box Start -->
            <div class="dashboard-content-box">



    </div>


</div>


@endsection
