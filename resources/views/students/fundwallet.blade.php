@extends('layouts.studentlanding')
@section('title', 'Classroom')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <!--<h4 class="card-title">My Certificates</h4>-->
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">




                                <div class="col-lg-12 space-2 space-lg-0">
                <div class="pl-lg-9 pl-xl-8">
                    <div class="d-lg-flex flex-lg-column w-100 hs-snippets-main-img-height">
                        <div class="d-lg-flex justify-content-lg-center align-content-lg-center flex-lg-column text-center h-100">
                            <div class="w-md-75 w-lg-50 mx-auto">
                                <img class="img-fluid" src="{{asset('nextassets/images/classroom.png')}}" alt="Image Description">
                            </div>
                            <div class="w-md-75 mx-auto mt-5 mb-5">
                                <h2>Provide the Amount </h2>

                                <input type="number" id="amount" style="width:300px; border:1px solid #ccc"><br><br>

                                <button onclick="initiateWalletFunding()"> Submit </button>


                            </div>
                        </div>
                    </div>
                </div>
            </div>







                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection


<input  type="hidden" id="t_" value="{{ csrf_token()}}">
<script>
var site = "<?php echo url('/');?>"
</script>
  <script src="https://checkout.flutterwave.com/v3.js"></script>






<script>

function initiateWalletFunding()
{

	var url = site + "/initiateWalletFunding";
	var xml = new XMLHttpRequest();
	var t = document.getElementById('t_').value;
  var amount = Number(document.getElementById('amount').value);
  if(isNaN(amount))
  {
    return;
  }

	var xml = new XMLHttpRequest();
	xml.open("POST", url, true);
	fd = new FormData();

	fd.append("amount", amount);


		 xml.setRequestHeader("X-CSRF-TOKEN", t);
			xml.onreadystatechange = function()
			{
				 if(xml.readyState == 4 && xml.status == 200)
				 {
					 	if(xml.responseText.includes("nsima"))
						{
              var resdata =   JSON.parse(xml.responseText);
						  callFlutter(amount, resdata.consumer_id, resdata.consumer_mac, resdata.username, resdata.ref, resdata.email)
						}
						else{
              console.log(xml.responseText)
						//	ReportError(xml.responseText,'errorzone') ; return;
						}
				 }
				 if(xml.status == 419)
				 {
           	location.reload();
				 }
			}
		xml.send(fd);
}


function callFlutter(amount, consumer_id, consumer_mac, username, ref, email)
{
    FlutterwaveCheckout({
      public_key: "FLWPUBK_TEST-d4d87be296ca20a5bef4cfe860322639-X",
      tx_ref: ref,
      amount: amount,
      currency: "NGN",
      country: "NG",
      payment_options: "card",
      meta: {
        consumer_id: consumer_id,
        consumer_mac: consumer_mac,
      },
      customer: {
        email: email,
        phone_number: "",
        name: username,
      },
      callback: function (data) {
        console.log(data);
        	verifyReference(data.tx_ref, data.amount,data.flw_ref, data.transaction_id);
      },
      onclose: function() {
      location.reload();
      },
      customizations: {
        title: "LMSA",
        description: "Funding of wallet",
        logo: "https://assets.piedpiper.com/logo.png",
      },
    });
  }//end function


  function verifyReference(tx_ref, amount, flw_ref,  transaction_id)
  {
    var url = site + "/verifyFundWalletReference";
    var xml = new XMLHttpRequest();
    var t = document.getElementById('t_').value;
    var xml = new XMLHttpRequest();
    xml.open("POST", url, true);
    fd = new FormData();

    fd.append("tx_ref", tx_ref);
    fd.append("amount", amount);
    fd.append("flw_ref", flw_ref);
    fd.append("transaction_id", transaction_id);

  		 xml.setRequestHeader("X-CSRF-TOKEN", t);
  			xml.onreadystatechange = function()
  			{
  				 if(xml.readyState == 4 && xml.status == 200)
  				 {
  					 	if(xml.responseText.includes("nsima"))
  						{
                //var resdata =   JSON.parse(xml.responseText);

               location.reload();
               console.log(xml.responseText)
  						}
  						else{
                console.log(xml.responseText)
  						//	ReportError(xml.responseText,'errorzone') ; return;
  						}
  				 }
  				 if(xml.status == 419)
  				 {
             	location.reload();
  				 }
  			}
  		xml.send(fd);
  }

</script>
