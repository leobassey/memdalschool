@extends('layouts.weblayouts')
@section('title', 'About')

@section('content')

<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url(assets/img/1920/background.jpg) no-repeat">

            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mx-auto text-white my-5 text-center" style="margin-bottom: 0px !important;">
                        <h1 style="font-weight: bolder; font-size: 35px;">
                          About Us
                        </h1>


                    </div>

                </div>
                <!-- END row-->
            </div>
        </div>
        <!-- END container-->
    </div>



<section class="padding-y-100">
  <div class="container" style="max-width:1140px">
    <div class="row">
     <div class="col-12 text-center">
        <h2>
        The leader in Agric and Data Training
        </h2>
        <div class="width-4rem height-4 bg-primary my-2 mx-auto rounded"></div>
      </div>
      <div class="col-lg-6 mt-5">
        <p style="font-family:AvenirNext,Helvetica,Arial,sans-serif; font-size: 16px;">
        Established in 2020, FIDAS- FarmKonnect Institute for Data and Agricbusiness Studies offers training in Agricbusiness. Our comprehensive curriculums are developed to give learners the skills to succeed in Agric related business.
        </p>
        <p  style="font-family:AvenirNext,Helvetica,Arial,sans-serif; font-size: 16px;">
         Our trainers are pracademics with years of business in Agriculture, entrepreneurship and technology. At FIDAS, you can decide to study online at your convenience or join our classroom training in a conducive learning environment
        </p>


        <div class="col-md-12" style="background-color:#eee; padding:10px; font-size:18px"><span style="color:#00CB54">Mission</span> - To help learns gain practical skills and business their own business</div>
        <div class="col-md-12 mt-2" style="background-color:#eee; padding:10px; font-size:18px"><span style="color:#00CB54">Vision</span> - Remain the best training provider in the field of Agric, Technology and Data</div>
      </div>


      <div class="col-lg-6">
         <img class="w-100" src="assets/img/flat-graphic-3.png" alt="">

      </div>
    </div> <!-- END row-->
  </div> <!-- END container-->
</section>






<hr class="mb-5 mt-5">
















@endsection
