@extends('layouts.weblayouts')
@section('title', 'Online Course Details')


@section('content')
<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url({{asset('assets/img/1920/background.jpg')}}) no-repeat">

<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-8 my-2 mx-auto text-white text-center">
            <h2 class="h4" style="font-size:35px">
                Course enrollment
            </h2>
        </div>

    </div>
</div>

</div>


<section class="paddingTop-50 paddingBottom-100 bg-light-v2">
  <div class="container" style="max-width:1140px">
    <div class="list-card align-items-center shadow-v1 marginTop-30">
      <div class="col-lg-4 px-lg-4 my-4">


        @if($course->image_url == "")
            <img class="w-100" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="w-100" src="../{{$course->image_url}}" alt="course image"/>
           @endif
      </div>
      <div class="col-lg-8 paddingRight-30 my-4">
       <div class="media justify-content-between">
         <div class="group">
          <a href="#" class="h4">
          {{$course->course_title}}
          </a>
          <ul class="list-inline mt-3">
            <li class="list-inline-item mr-2">
              <i class="ti-user mr-2"></i>
             Course ratings
            </li>
            <li class="list-inline-item mr-2">
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <span class="text-dark">5</span>

            </li>
          </ul>
         </div>

       </div>
       <p>
         You can pay for this course using your credit card or pay via your FIDAS wallet
       </p>
       <div class="d-md-flex justify-content-between align-items-center">
         <ul class="list-inline mb-md-0">
           <li class="list-inline-item mr-3">
             <span class="h4 d-inline ">N{{$course->course_price}}</span>
             <!--<span class="h6 d-inline small text-gray"><s>$249</s></span>-->
           </li>
           <li class="list-inline-item mr-3">
             <i class="ti-headphone small mr-2"></i>
             Well curated contents
           </li>
           <li class="list-inline-item mr-3">
             <i class="ti-time small mr-2"></i>
             Learn with case studies
           </li>
         </ul>

       </div>
      </div>

      <hr>



<div class="col-md-7 mb-5 animate__animated animate__pulse">
  <?php if($hasEnrolled == false){?>

  <?php if($course->course_type == 3){?>
    <a href="#" onclick="EnrollFree('<?php echo $course->id?>')" class="btn btn-primary" style="background-color:orange; color:#fff">Enroll Free</a>
  <?php } else {?>
  <p class="lead">Pay via card</p>
  <a href="#" onclick="paywithFlutter('<?php echo $course->id?>')" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N{{$course->course_price}}</a>
   <?php } ?>
<?php } else{
  echo '<h6>You are already enrolled in this course</h6>';
}
?>
</div>




<div class="col-md-5 animate__animated animate__pulse mb-5">
  <?php if($hasEnrolled == false)
  {?>

              <?php if($course->course_type == 3){?>
                    <a href="#" onclick="EnrollFree('<?php echo $course->id?>')" class="btn btn-primary" style="background-color:orange; color:#fff">Enroll Free</a>
              <?php } else {?>

                           <p class="lead">Pay via your FIDAS Wallet (Balance: N<?php echo $balance ?>)</p>
                           <?php if($balance > $course->course_price){?>
                          <a href="#" onclick="payViaWallet('<?php echo $course->id?>')" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N <?php echo $course->course_price ?></a>
                        <?php }  else { echo "Insufficient Balance"; }?>

            <?php } ?>

  <?php } ?>
</div>

</div>




  </div> <!-- END container-->
</section>




<!--<section class="courseinfo mt-5 " >
<div class="container" style="max-width:1140px">
    <div class="row">

        <div class="col-md-6 details">
          <p class="lead">Pay via card</p>
          <a href="#" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N{{$course->course_price}}</a>
        </div>

        <div class="col-md-6 animate__animated animate__pulse">
            <p class="lead">Pay via your FIDAS Wallet (Balance: N45000)</p>
            <a href="#" class="btn btn-primary" style="background-color:orange; color:#fff">Pay N{{$course->course_price}}</a>
        </div>
    </div>
</div>
</section>-->



<section class="padding-y-100 mt-5 " style="background-color: #f8f9fa! important;">
        <div class="container" style="max-width:1140px">
            <div class="row">
                <div class="col-12 text-center" style="margin-top:-40px;">
                  <h2 class="text-left">
                      Students also enrolled
                      <a href="{{route('onlinecourses')}}" style="float: right; font-size: initial; margin-top: 13px;">All Courses</a>
                  </h2>

                </div>

                <!--  <div class="col-12 mt-3">
                    <div class="d-md-flex justify-content-between bg-white rounded shadow-v1 p-4">
                        <ul class="nav nav-pills nav-isotop-filter align-items-center my-2">

                            <a class="nav-link" href="online-courses.html" data-filter=".free-courses">Online Courses</a>
                            <a class="nav-link" href="classroom-courses.html" data-filter=".start-soon">In-Person Courses</a>
                            <a class="nav-link" href="topic-courses.html" data-filter=".free-courses">Topic-Based Courses</a>

                            <a class="nav-link" href="#" data-filter=".free-courses">Career Courses</a>
                            <a class="nav-link" href="#" data-filter=".free-courses">Free Courses</a>
                        </ul>


                    </div>
                </div>-->

                <!-- END row-->

                @foreach($courses as $c)
                <div class="col-lg-4 mt-3  mb-4" data-aos="fade-left" data-aos-once="true" data-aos-delay="{{$loop->index*100}}">

                <div class="card">
                        <a href="#">
                            <div class="rounded mainOverlay">
                            @if($c->image_url == "")
                            <img class="rounded" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
                        @else
							<img class="rounded" src="../{{$c->image_url}}" alt="course image"/>
                        @endif

                              <!--<p class="text-center overlayText">View Details</p>-->
                            </div>
                        </a>
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 20px; font-weight: bold;">
                                   {{$c->course_title}}
                                </a>

                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>



                            </p>


                            <div class="card-text d-flex bd-highlight mt-2">
                                <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($c->course_type == 1)
                                    Type: Online
                                    @elseif($c->course_type == 2)
                                    Type: Classroom-based
                                    @else
                                    Type: Free course
                                    @endif
                                     </p>
                                <h4 class="h5 text-right ml-5">
                                    <span class="" style="font-weight: bold; margin-left:-40px">N{{$c->course_price}}</span>

                                </h4>

                            </div>
                            <a href="{{route('onlinecoursedetails', $c->id)}}" class="btn btn-primary mt-3" style="background-color:orange; color:#fff">View course details</a>

                        </div>
                    </div>

                </div>

@endforeach


            </div>
        </div>
    </section>

@endsection

<input  type="hidden" id="t_" value="{{ csrf_token()}}">
<script>
var site = "<?php echo url('/');?>"
</script>
  <script src="https://checkout.flutterwave.com/v3.js"></script>






<script>

function paywithFlutter(course_id)
{

	var url = site + "/initiatepaywithflutter";
	var xml = new XMLHttpRequest();
	var t = document.getElementById('t_').value;
	var xml = new XMLHttpRequest();
	xml.open("POST", url, true);
	fd = new FormData();

	fd.append("course_id", course_id);


		 xml.setRequestHeader("X-CSRF-TOKEN", t);
			xml.onreadystatechange = function()
			{
				 if(xml.readyState == 4 && xml.status == 200)
				 {
					 	if(xml.responseText.includes("nsima"))
						{
              var resdata =   JSON.parse(xml.responseText);
						  callFlutter(resdata.amount, resdata.consumer_id, resdata.consumer_mac, resdata.username, resdata.ref, resdata.email, course_id)
						}
						else{
              console.log(xml.responseText)
						//	ReportError(xml.responseText,'errorzone') ; return;
						}
				 }
				 if(xml.status == 419)
				 {
           	location.reload();
				 }
			}
		xml.send(fd);
}


function callFlutter(amount, consumer_id, consumer_mac, username, ref, email, course_id)
{
    FlutterwaveCheckout({
      public_key: "FLWPUBK_TEST-d4d87be296ca20a5bef4cfe860322639-X",
      tx_ref: ref,
      amount: amount,
      currency: "NGN",
      country: "NG",
      payment_options: "card",
      meta: {
        consumer_id: consumer_id,
        consumer_mac: consumer_mac,
      },
      customer: {
        email: email,
        phone_number: "",
        name: username,
      },
      callback: function (data) {
        console.log(data);
        	verifyReference(data.tx_ref, data.amount,data.flw_ref, course_id, data.transaction_id);
      },
      onclose: function() {
      location.reload();
      },
      customizations: {
        title: "LMSA",
        description: "Payment for course",
        logo: "https://assets.piedpiper.com/logo.png",
      },
    });
  }//end function


  function verifyReference(tx_ref, amount, flw_ref, course_id, transaction_id)
  {
    var url = site + "/verifyReference";
    var xml = new XMLHttpRequest();
    var t = document.getElementById('t_').value;
    var xml = new XMLHttpRequest();
    xml.open("POST", url, true);
    fd = new FormData();

    fd.append("tx_ref", tx_ref);
    fd.append("amount", amount);
    fd.append("flw_ref", flw_ref);
    fd.append("course_id", course_id);
    fd.append("transaction_id", transaction_id);

  		 xml.setRequestHeader("X-CSRF-TOKEN", t);
  			xml.onreadystatechange = function()
  			{
  				 if(xml.readyState == 4 && xml.status == 200)
  				 {
  					 	if(xml.responseText.includes("nsima"))
  						{
                //var resdata =   JSON.parse(xml.responseText);

               location.reload();
               console.log(xml.responseText)
  						}
  						else{
                console.log(xml.responseText)
  						//	ReportError(xml.responseText,'errorzone') ; return;
  						}
  				 }
  				 if(xml.status == 419)
  				 {
             	location.reload();
  				 }
  			}
  		xml.send(fd);
  }


function EnrollFree(course_id)
{
  var url = site + "/enrollfree";
  var xml = new XMLHttpRequest();
  var t = document.getElementById('t_').value;
  var xml = new XMLHttpRequest();
  xml.open("POST", url, true);
  fd = new FormData();


  fd.append("course_id", course_id);


     xml.setRequestHeader("X-CSRF-TOKEN", t);
      xml.onreadystatechange = function()
      {
         if(xml.readyState == 4 && xml.status == 200)
         {
            if(xml.responseText.includes("nsima"))
            {
              //var resdata =   JSON.parse(xml.responseText);

             location.reload();
             console.log(xml.responseText)
            }
            else{
              console.log(xml.responseText)
            //	ReportError(xml.responseText,'errorzone') ; return;
            }
         }
         if(xml.status == 419)
         {
            location.reload();
         }
      }
    xml.send(fd);
}

function payViaWallet(course_id)
{
  var url = site + "/payViaWallet";
  var xml = new XMLHttpRequest();
  var t = document.getElementById('t_').value;
  var xml = new XMLHttpRequest();
  xml.open("POST", url, true);
  fd = new FormData();


  fd.append("course_id", course_id);


     xml.setRequestHeader("X-CSRF-TOKEN", t);
      xml.onreadystatechange = function()
      {
         if(xml.readyState == 4 && xml.status == 200)
         {
            if(xml.responseText.includes("nsima"))
            {
              //var resdata =   JSON.parse(xml.responseText);

             location.reload();
             console.log(xml.responseText)
            }
            else{
              console.log(xml.responseText)
            //	ReportError(xml.responseText,'errorzone') ; return;
            }
         }
         if(xml.status == 419)
         {
            location.reload();
         }
      }
    xml.send(fd);
}
</script>
