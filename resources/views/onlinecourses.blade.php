

@extends('layouts.frontlayout')
@section('title', 'Digital school of technology')


@section('content')

<style>
    .mainOverlay {
              background-color: #000;
              display: flex;
              justify-content: center;
              flex-direction: column;
              align-items: center;
          }

          .mainOverlay img {
              opacity: 0.3;
          }

          .overlayText {
              font-weight: bold;
              position: absolute;
              color: #fff;
              opacity: 1;
          }
          .regular-price {
    font-size: 15px;
    font-weight: 500;
    color: #666666;
    text-decoration: line-through;
}
</style>

  <!-- Page Banner Section Start -->
  <div class="page-banner bg-color-05">
    <div class="page-banner__wrapper">
        <div class="container">

            <!-- Page Breadcrumb Start -->
            <div class="page-breadcrumb">
                <ul class="breadcrumb">

                </ul>
            </div>
            <!-- Page Breadcrumb End -->

            <!-- Page Banner Caption Start -->
            <div class="page-banner__caption text-center">
                <h2 class="page-banner__main-title">Online Courses</h2>
            </div>
            <!-- Page Banner Caption End -->

        </div>
    </div>
</div>
<!-- Page Banner Section End -->


  <!-- Event Start -->
  <div class="event-section" style="margin-top: -40px">
    <div class="container">




        <div class="row align-items-center">
            <div class="col-lg-6">

                <!-- Section Title Start -->
                <div class="section-title">
                    <h2 class="section-title__title-03"></h2>

                </div>
                <!-- Section Title End -->

            </div>
            <div class="col-lg-6">

                <!-- Section button Start -->
                <div class="section-btn-02 text-lg-end" data-aos="fade-up" data-aos-duration="1000">
                   <!--<input type="text">-->
                </div>
                <!-- Section button End -->

            </div>
        </div>

        <div class="row gy-6">


            @foreach ($programArray as $key)

            <div class="container" style="margin-bottom: 20px">
            <div class="row">
            <div class="col-md-10"><span style="font-size: 19px; color:#000; font-weight:bold"><strong>{{$key["name"]}}</strong></span></div>
            <div class="col-md-2"><span style="font-size: 150%; color:#000;">
                {{ count($key["courses"]) }}

                @if (count($key["courses"]) > 1)
                Courses

                @else
                Course

                @endif

            </span></div>
            </div>
            </div>

            <hr style="margin-top:-6px">

            <section class="padding-y-100" style="background-color: #fff! important;">



                    <div class="row">

                     @foreach ($key["courses"] as $course)

                        <div class="col-lg-4 mt-1">
                            <div class="card card-price mh-100" style="height: 420px">
                                <div class="card-img">
                                    <a href="{{route('details', $course->slug)}}">

                                      <div class="mainOverlay">

                                        @if($course->image_url)
                                        <img class="rounded card-img-top" src="{{$course->image_url}}" alt="course image" width="359px" height="240px">
                                        <p class="text-center overlayText">View Details</p>
                                    @else
                                        <img src="{{asset('assets/img/course-image.jpg')}}" class="img-responsive" width="359px" height="240px">
                                        <p class="text-center overlayText">View Details</p>
                                    @endif

                                      </div>
                                    </a>
                                  </div>


                                  <a href="{{route('details', $course->slug)}}">
                                  <div class="card-body" style="background-color: #fff;S">
                                        <p><span style="color: #000000;"><span style="font-size: 120%; font-weight:bold"><strong>{{ $course->course_title }}</strong></span></span></p>

                                        <div class="course-info__rating mb-1">
                                            <div class="rating-star">
                                                @if($course->average_ratings > 4.5)
                                                <div class="rating-label" style="width: 100%;"></div>
                                                @else
                                                <div class="rating-label" style="width: 80%;"></div>
                                                @endif
                                            </div>
                                            <span>


                                                @if($course->average_ratings > 5)
                                                4.8
                                                @else

                                                {{number_format($course->average_ratings, 1)}}
                                                @endif

                                                ({{$course->reviews}} reviews)</span>

                                        </div>

                                        <!--<span style="color: #ffcc00;"><strong></strong>&nbsp; <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> {{number_format($course->average_ratings, 1)}} ({{$course->reviews}} reviews)</span>-->








                                        <div class="row" style="">
                                            <div class="col-md-7"><p style="margin-top:3px"><i class="fa fa-check-square-o"></i> Course + Certificate </p></div>
                                            @if($course->promo_price == 0.00)
                                            <!--<div class="col-md-5" style="color: #000000; font-size: 110%; font-weight:bold"><strong>&#x20A6; {{number_format($course->course_price, 2)}} </strong></div>-->
                                            @else

                                            <div class="col-md-5 regular-price" style=""><strong>&#x20A6; {{number_format($course->course_price,2)}} </strong></div>

                                            @endif

                                        </div>

                                        <p class="lead" style="text-align:left">
                                            <div class="row" style="margin-top: -18px">
                                                <div class="col-md-7"><i class="fa fa-clock-o"></i><span style="font-size: 90%"> Duration: {{ $course->duration }} / online</span></div>

                                                @if($course->promo_price == 0.00)
                                                <div class="col-md-5" style="color: #000000; font-size: 110%; font-weight:bold"><strong>&#x20A6; {{number_format($course->course_price, 2)}} </strong></div>

                                                @else
                                                <div class="col-md-5" style="color: #000000; font-size: 110%; font-weight:bold"><strong>&#x20A6; {{number_format($course->promo_price, 2)}} </strong></div>

                                                @endif
                                            </div>
                                        </p>
                                    </div>
                                  </a>
                           </div>
                        </div>
                    @endforeach <!-- End of course foreach loop-->
                    </div>


            <br>
            </section>

       @endforeach <!-- End of category foreach loop-->

            <!--@foreach($courses as $course)
            <div class="col-lg-4 col-sm-6">


                <div class="course-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="course-header">
                        <div class="course-header__thumbnail ">
                            <a href="{{route('details', $course->slug)}}"><img src="{{$course->image_url}}" alt="event" width="370" height="209"></a>

                        </div>

                    </div>
                    <div class="course-info event-item__content">

                        <a href="{{route('details', $course->slug)}}" class="course-info__instructor">
                        <h3 class="course-info__title"><a href="{{route('details', $course->slug)}}">{{$course->course_title}}</a></h3>
                      </a>
                        <div class="course-info__price">

                            <span class="sale-price">&#x20A6;{{ number_format($course->course_price, 2)}}</span>

                        </div>



                    </div>
                </div>

            </div>

            @endforeach-->

        </div>
    </div>
  </div>


















    @endsection


