@extends('layouts.adminlayout')
@section('title', 'Course lists')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">


                        <div class="card-header">
                                <h4 class="card-title">Course lists </h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="" href="{{route('createcourse')}}">+ Create New Course</a></li>

                                        <li><a data-action="" href="{{route('teachercourselists')}}">+ View Teacher Coures</a></li>

                                        <li><a data-action="" href="{{route('allcourses')}}">+ View All Courses</a></li>

                                        <li><a data-action="" href="{{route('courses')}}">+ View My Courses</a></li>

                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">

                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show">
                    <div class="card-body card-dashboard">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered patients-list dataex-html5-export">
                            <thead>
                            <tr>
                            <th class="wd-15p">Added By</th>
                            <th class="wd-15p">Title</th>
                            <th class="wd-25p">Price</th>
                            <th class="wd-25p">Promo Price</th>
                            <th class="wd-25p">Status</th>

                            <th class="wd-25p">Actions</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($courses as $c)
                <tr>
                <td >{{$c->name}}</td>
                  <td >{{$c->course_title}}</td>
                  <td>{{$c->course_price}}</td>
                  <td>{{$c->promo_price}}</td>
                  <td>
                  @if($c->course_status == 0)
                  Inactive
                  @else
                  Active
                  @endif
                  </td>
                  <td><a href="{{route('coursesettings', $c->id)}}" title="View course settings">
                      <i class="la la-cog" ></i></a>
                      <a href="{{route('editcourse', $c->id)}}" title="Edit course"><i class="ft-edit text-success ml-1"></i></a>
                      <a href="{{route('disablecourse', $c->id)}}" title="Disable this course"><i class="fa fa-lock ml-1 text-warning"></i></a>
                      <a href="{{route('enablecourse', $c->id)}}" title="Enable this course"><i class="fa fa-unlock-alt ml-1 text-warning"></i></a>

                      <a href="{{route('disableenrollment', $c->id)}}" title="Turn OFF enrollment"><i class="fa fa-lock ml-1 text-warning"></i></a>
                      <a href="{{route('acceptenrollment', $c->id)}}" title="Turn ON enrollment"><i class="fa fa-unlock-alt ml-1 text-warning"></i></a>

                    </td>


                </tr>
                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
