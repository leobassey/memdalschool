@extends('layouts.adminlayout')
@section('title', 'Edit lecture note')

@section('content')

<style>
.form-group-sm .form-control {
    height: 30px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
.fa-border {
    padding: .2em .25em .15em;
    border: solid .08em #eee;
    border-radius: .1em;
}
.text-small{
    height:32px;
    font-size:13px;
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.label-small
{
    font-size: 12px;
}

.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #6b6f82;
}
.table th, .table td {
    padding: 0.7rem;
    font-size:12px;
}
.table th
{
    background-color:#02b159 ;
    color:#fff;
}

.la {
    position: relative;
    top: 2px;
}

.form-group-sm {
	padding-bottom: 5px;
}

.btn-info {
    border-color: #02b159 !important;
    background-color: #02b159 !important;
    border-radius: 0.21rem;
}

.btn-info:hover {
    background-color: #02b159 !important;
    border-color: #02b159 !important;
}

.input-group-addon {
	background-color: #fff;
	padding-top: 0.6rem;
}

</style>

<script src="https://cdn.tiny.cloud/1/nnd7pakaxqr7isf3oqefsdlew1jsidgl78umfeus6tg21ng0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


<!--<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>-->
<!--<script src="https://cdn.tiny.cloud/1/nnd7pakaxqr7isf3oqefsdlew1jsidgl78umfeus6tg21ng0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>-->



<script>

/*
tinymce.init({
        selector: 'textarea',
        path_absolute: "/",
        image_class_list: [
        {title: 'img-responsive', value: 'img-responsive'},
        ],
        height: 500,
        setup: function (editor) {
            editor.on('init change', function () {
                editor.save();
            });
        },
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",

        image_title: true,
        automatic_uploads: true,
       // images_upload_url: '/upload',
        file_picker_types: 'image',
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
            };
            input.click();
        }
    });*/





    /*

 var editor_config = {
    selector:'textarea',

    directionality: document.dir,
            path_absolute: "/",


            paste_data_images: true,
          plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
          ],
          toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
          toolbar2: "print preview media | forecolor backcolor emoticons",




    menu: {
      favs: { title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons' }
    },
    menubar: 'favs file edit view insert format tools table help',

            image_title: true,
            automatic_uploads: true,
            relative_urls: false,
            language: document.documentElement.lang,
            height: 300,
            file_browser_callback : function (field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.querySelector('body').clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document.querySelector('body').clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
                } else {
                cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no"
                });
            },
        }
            tinymce.init(editor_config);

            */





/*

            tinymce.init({
            selector: 'textarea',

            image_class_list: [
            {title: 'img-responsive', value: 'img-responsive'},
            ],
            height: 500,
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",

            image_title: true,
            automatic_uploads: true,
            images_upload_url: './upload',
            file_picker_types: 'image',
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                };
                input.click();
            }
        });*/







        tinymce.init({
       selector: 'textarea',
       image_class_list: [
            {title: 'img-responsive', value: 'img-responsive'},
            ],
            height: 500,
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",
            toolbar2: "print preview media | forecolor backcolor emoticons",
            image_title: true,
            automatic_uploads: true,
       images_upload_handler: function (blobInfo, success, failure) {
           var xhr, formData;
           xhr = new XMLHttpRequest();
           xhr.withCredentials = false;
           xhr.open('POST', '/upload');
           var token = '{{ csrf_token() }}';
           xhr.setRequestHeader("X-CSRF-Token", token);
           xhr.onload = function() {
               var json;
               if (xhr.status != 200) {
                   failure('HTTP Error: ' + xhr.status);
                   return;
               }
               json = JSON.parse(xhr.responseText);

               if (!json || typeof json.location != 'string') {
                   failure('Invalid JSON: ' + xhr.responseText);
                   return;
               }
               success(json.location);

           };
           formData = new FormData();
           formData.append('file', blobInfo.blob(), blobInfo.filename());
           xhr.send(formData);
       }
     });

</script>


<!-- BEGIN: Content-->


<div class="content-overlay"></div>
    <div class="content-wrapper" style="margin-top:-25px">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                    </div>
                    <div class="card-content collapse show" style="padding: 20px;">
                        <div class="card-content">


                    </div>
                    </div>


                </div>
            </div>
        </div>




            <div class="content-body" style="margin-top:-20px">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div id="accordion" class="accordion fa-border">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">
                                        <a class="card-title" href="#" style="font-size:13px; font-weight:bold"><i class="la la-ship"></i> Edit lecture</a>
                                    </div>
            <div id="collapseOne" class="card-body collapse show" data-parent="#accordion">

            <div id="app2">



            <div class="sl-pagebody mt-1">



<div class="row mb-3">
<div class="col-md-10"> @foreach($errors->all() as $error)
<p class="alert alert-danger">{{$error}}
<button type="button" class="close" data-dismiss="alert">×</button>
</p>
@endforeach

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif
</div>


</div>
</div>


<form action="{{route('postededitlecture')}}" method="post" enctype="multipart/form-data">
@csrf


<div class="row mg-b-25">

    <div class="col-lg-4 ml-2">
        <div class="form-group">
          <label class="form-control-label">Module: <span class="tx-danger">*</span></label>
          <select class="form-control select2" name="module" required>
            <option label="Select Module" value=""></option>
            @foreach ( $modules as $m)


            <option value="{{$m->id}}" {{$lectureObject->section_id == $m->id ? "selected" : "" }}>{!! $m->section_title  !!}</option>

            @endforeach
          </select>
        </div>
      </div><!-- col-4 -->


    <div class="col-lg-4 ">
      <div class="form-group">
        <label class="form-control-label">Title: <span class="tx-danger">*</span></label>
        <input class="form-control" type="text" name="title" required value="{{{$lectureObject->lecture_title}}}">
        <input class="form-control" type="hidden" name="lecturenote_id"  value="{{$lectureObject->id}}">
      </div>
    </div><!-- col-4 -->


    <div class="col-lg-8 ml-2">
        <div class="form-group">
          <label class="form-control-label">URL: <span class="tx-danger"></span></label>
          <input class="form-control" type="text" name="url" value="{{$lectureObject->url}}">
        </div>
      </div><!-- col-4 -->

</div>


<div class="row col-lg-12">

<div class="col-md-12 col-sm-12 form-group-sm">

<br/>
 Lecture

 <div class="form-group">
<textarea name="lecture">{{$lectureObject->note}}</textarea>

 </div>

<br/>

</div>

</div>



</div>
<!--
                <div class="row col-lg-12">


                    <div class="col-md-2 col-sm-12 form-group-sm"></div>
                    <div class="col-md-12 col-sm-12 form-group-sm">
                    Lecture
                    <input class="form-control" type="text" name="note"  placeholder="Enter resource" disabled value="{{$lectureObject->lecture_title}}">
                    <input class="form-control" type="hidden" name="lecturenote_id"  value="{{$lectureObject->id}}">

                    <br/>
                     Lecture Note


                    <textarea name="note">{{$lectureObject->note}}</textarea>

                    <br/>

                    Enter text Lecture

                    <textarea name="text_lecture">{{$lectureObject->url}}</textarea>
                    </div>

                    </div>



</div>-->

                <button class="btn btn-info mg-r-5 mt-3" style="cursor:pointer margin-left:40px"> <i class="la la-save"></i>Save Changes</button>

                <a class="btn btn-secondary mt-3" href="{{route('courselectures', $lectureObject->course_id)}}"><< Back</a>
</div>
                </form>
        </div>





    </div>



</div>


            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>













@endsection
