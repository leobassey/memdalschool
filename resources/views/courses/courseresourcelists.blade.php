@extends('layouts.adminlayout')
@section('title', 'Course resources')

@section('content')

        <div class="content-overlay"></div>
        <div class="content-wrapper" style="margin-top:-25px">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">


                        <div class="card-header">
                                <h4 class="card-title">Course resources</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                       <!-- <li><a data-action="" href="{{route('createcourse')}}">+ Enroll students</a></li>
-->
                                        <li><a data-action="" href="{{route('courses')}}">+ Course Lists</a></li>


                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="background-color:#f4f5fa">

                                @if(count($errors) >0)
<ul style="color:red; font-weight:bold" class="mb-3">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif

@if (session('error'))
                            <div class="alert alert-danger mb-4">

                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success mb-3">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert">×</button>
                            </div>
                        @endif




                                <section id="patients-list">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body collapse show">
                    <div class="card-body card-dashboard">
                    
                    <a href="{{route('addcourseresource', $courseId)}}" class="btn btn-primary">Add course resources</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered patients-list dataex-html5-export">
                            <thead>
                            <tr>
                            <th class="wd-15p">Name</th>

                            <th class="wd-15p">Link</th>

                            <th class="wd-25p">Edit</th>

                            <th class="wd-25p">Delete</th>

                            </tr>
                            </thead>
                            <tbody>

                @foreach($courseresources as $c)
                <tr>
                <td >{{$c->resource_name}}</td>

                <td >{{$c->resource_link}}</td>

                  <td>

                      <a href="{{route('editcourseresources', $c->id)}}" title="Edit course resources"><i class="ft-edit text-success ml-1"></i></a>


                    </td>

                    <td> <form method="post" action="{{route('deletecourseresource')}}">
                                    @csrf

                                    <input type="hidden" name="resource_id" value="{{$c->id}}"/>
                                    <button class="btn btn-info mg-r-5 ml-3" style="cursor:pointer; background-color:#02b159; border-color:#02b159">Delete</button>

                                    </form></td>


                </tr>
                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection
