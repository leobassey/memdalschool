@extends('layouts.adminlayout')
@section('title', 'Create Exam')

@section('content')

<style>
.form-group-sm .form-control {
    height: 30px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
.fa-border {
    padding: .2em .25em .15em;
    border: solid .08em #eee;
    border-radius: .1em;
}
.text-small{
    height:32px;
    font-size:13px;
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.label-small
{
    font-size: 12px;
}

.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #6b6f82;
}
.table th, .table td {
    padding: 0.7rem;
    font-size:12px;
}
.table th
{
    background-color:#069;
    color:#fff;
}

.la {
    position: relative;
    top: 2px;
}

.form-group-sm {
	padding-bottom: 5px;
}

.btn-info {
    border-color: #02b159 !important;
    background-color: #02b159 !important;
    border-radius: 0.21rem;
}

.btn-info:hover {
    background-color: #02b159 !important;
    border-color: #02b159 !important;
}

.input-group-addon {
	background-color: #fff;
	padding-top: 0.6rem;
}

</style>




<!-- BEGIN: Content-->


<div class="content-overlay"></div>
    <div class="content-wrapper" style="margin-top:-25px">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic">INSTRUCTION</h4>
                    </div>
                    <div class="card-content collapse show" style="padding: 20px;">
                        <div class="card-content">

                        <h4>Course name: {{$courseObject->course_title}}</h4>
                    </div>
                    </div>

                </div>
            </div>
        </div>




            <div class="content-body" style="margin-top:-20px">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div id="accordion" class="accordion fa-border">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">
                                        <a class="card-title" href="#" style="font-size:13px; font-weight:bold"><i class="la la-ship"></i> Create Exam </a>
                                    </div>
            <div id="collapseOne" class="card-body collapse show" data-parent="#accordion">

            <div id="app2">



            <div class="sl-pagebody mt-1">



<div class="row mb-3">
<div class="col-md-10"> @foreach($errors->all() as $error)
<p class="alert alert-danger">{{$error}}
<button type="button" class="close" data-dismiss="alert">×</button>
</p>
@endforeach

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif
</div>


</div>
</div>


<form action="{{route('postexam')}}" method="post" style="margin-top:-40px">
@csrf
                <div class="row col-lg-12">

                <div class="col-md-12">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Exam Question</label>
                  <input class="form-control" type="text" name="question" placeholder="Example: Enter question" v-model="form.course_requirements" required>
                  <input class="form-control" type="hidden" name="courseId"  value="{{$courseObject->id}}" required>
                </div>
              </div>

                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Option 1</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="option1" placeholder="Enter option 1" v-model="form.title" required>
                    </div>
                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Option 2</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                   <!-- <select class="form-control select2" data-placeholder="Select category" v-model="form.program" name="program" required>
                    <option label="Select category"></option>

                  </select>-->
                  <input class="form-control" type="text" name="option2" placeholder="Enter option 2" v-model="form.title" required>
                    </div>

                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Option 3</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="option3"  placeholder="Enter option 3" v-model="form.promo_price">
                   <!-- <select class="form-control select2" data-placeholder="Choose country" v-model="form.privacy" name="privacy" required>
                    <option label="Choose Privacy"></option>
                    <option value="1">Public only</option>
                    <option value="2">Private (staff only)</option>
                    <option value="3">Public and private for staff</option>
                  </select>-->
                    </div>

                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Option 4</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">

                    <input class="form-control" type="text" name="option4" placeholder="Enter option 4" v-model="form.title" required>

                    </div>
                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Correct option</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                   <select class="form-control select2" data-placeholder="Choose correct option" v-model="form.course_type" name="correct_option" required>
                    <option label="Choose Type"></option>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="3">Option 3</option>
                    <option value="4">Option 4</option>
                  </select>
                    </div>
                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Mark</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="number" name="mark"  placeholder="Enter Mark" v-model="form.promo_price">
                    </div>

                    <!--<div class="col-md-2 col-sm-12 form-group-sm"><small>Course Duration</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="duration"  placeholder="Enter course duration - 3 months etc" v-model="form.promo_price">
                    </div>-->






            <!--  <div class="col-md-12">
            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Course Objectives - What will students learn in this course?: <span class="tx-danger">*</span></label>

                  <textarea  name="course_objectives" required id="summernote" v-model="form.course_objectives" required>

                  </textarea>

                </div>
            </div>-->



           <!-- <div class="col-md-12">

            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Full Course Description - The description you write will help studenst know if the course is for them: <span class="tx-danger">*</span></label>

                  <textarea  name="course_full_description" required id="summernote-desc" v-model="form.course_full_description" required>

</textarea>

            </div>

                </div>-->

</div>



                <div class=""  style="float:left; margin-top:10px;">

                   <!-- <button type="button" class="btn btn-info mr-1">

                        <i class="la la-save"></i> Create Course
                    </button>

                    <a class="btn btn-secondary" href="{{route('courses')}}">Cancel</a>-->


                <button class="btn btn-info mg-r-5 mt-3" style="cursor:pointer"> <i class="la la-save"></i> Submit Form</button>
                <a class="btn btn-secondary mt-3" href="{{route('courses')}}">Cancel</a>
</div>
                </form>
        </div>
    </div>



</div>


            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>













@endsection
