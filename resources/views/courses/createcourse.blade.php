@extends('layouts.adminlayout')
@section('title', 'Create Course')

@section('content')

<style>
.form-group-sm .form-control {
    height: 30px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
.fa-border {
    padding: .2em .25em .15em;
    border: solid .08em #eee;
    border-radius: .1em;
}
.text-small{
    height:32px;
    font-size:13px;
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.label-small
{
    font-size: 12px;
}

.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #6b6f82;
}
.table th, .table td {
    padding: 0.7rem;
    font-size:12px;
}
.table th
{
    background-color:#069;
    color:#fff;
}

.la {
    position: relative;
    top: 2px;
}

.form-group-sm {
	padding-bottom: 5px;
}

.btn-info {
    border-color: #02b159 !important;
    background-color: #02b159 !important;
    border-radius: 0.21rem;
}

.btn-info:hover {
    background-color: #02b159 !important;
    border-color: #02b159 !important;
}

.input-group-addon {
	background-color: #fff;
	padding-top: 0.6rem;
}

</style>

<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>


<script>tinymce.init({
    selector:'textarea',
    plugins: 'link, image',
    plugins: 'lists',
  toolbar: 'numlist bullist'


}
);
</script>




<!-- BEGIN: Content-->


<div class="content-overlay"></div>
    <div class="content-wrapper" style="margin-top:-25px">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic">INSTRUCTION</h4>
                    </div>
                    <div class="card-content collapse show" style="padding: 20px;">
                        <div class="card-content">

                        <p>See a course as a book, sections as chapters in a book and lectures as contents in a book grouped by sections. After creating a course, you will be redirected to the course settings page where you can create sections and add lectures to the course.</p>
                    </div>
                    </div>

                </div>
            </div>
        </div>




            <div class="content-body" style="margin-top:-20px">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div id="accordion" class="accordion fa-border">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">
                                        <a class="card-title" href="#" style="font-size:13px; font-weight:bold"><i class="la la-ship"></i> Course details </a>
                                    </div>
            <div id="collapseOne" class="card-body collapse show" data-parent="#accordion">

            <div id="app2">



            <div class="sl-pagebody mt-1">



<div class="row mb-3">
<div class="col-md-10"> @foreach($errors->all() as $error)
<p class="alert alert-danger">{{$error}}
<button type="button" class="close" data-dismiss="alert">×</button>
</p>
@endforeach

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif
</div>


</div>
</div>


<form action="{{route('postcourse')}}" method="post">
@csrf
                <div class="row col-lg-12">

                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Course Title</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="title" placeholder="Enter course title" v-model="form.title" required>
                    </div>
                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Course Category</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <select class="form-control select2" data-placeholder="Select category" v-model="form.program" name="program" required>
                    <option label="Select category"></option>
                    @foreach($programs as $p)
                    <option value="{{$p->id}}">{{$p->program_name}}</option>
                  @endforeach
                  </select>
                    </div>

                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Course Duration</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="duration"  placeholder="Enter course duration - 3 months etc" v-model="form.promo_price">
                   <!-- <select class="form-control select2" data-placeholder="Choose country" v-model="form.privacy" name="privacy" required>
                    <option label="Choose Privacy"></option>
                    <option value="1">Public only</option>
                    <option value="2">Private (staff only)</option>
                    <option value="3">Public and private for staff</option>
                  </select>-->
                    </div>

                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Course Type</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <select class="form-control select2" data-placeholder="Choose country" v-model="form.course_type" name="course_type" required>
                    <option label="Choose Type"></option>
                    <option value="1">Online Course</option>
                    <option value="2">On Campus Course</option>
                    <option value="3">Free Course</option>
                  </select>
                    </div>
                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Course Price</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="price"  placeholder="Enter price" v-model="form.price">
                    </div>
                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Promo Price (Optional)</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="promo_price"  placeholder="Enter promo price" v-model="form.promo_price">
                    </div>

                    <div class="col-md-2 col-sm-12 form-group-sm"><small>Select Single or Full Course</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <select class="form-control select2" data-placeholder="Choose country" v-model="form.course_type" name="is_single" required>
                    <option label="Choose Type"></option>
                    <option value="1">Single Course</option>
                    <option value="2">Full Campus Course</option>

                  </select>
                    </div>

                    <!--<div class="col-md-2 col-sm-12 form-group-sm"><small>Course Duration</small></div>
                    <div class="col-md-4 col-sm-12 form-group-sm">
                    <input class="form-control" type="text" name="duration"  placeholder="Enter course duration - 3 months etc" v-model="form.promo_price">
                    </div>-->


                    <div class="col-md-12 mt-2">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Are there any course requirements?:</label>
                  <input class="form-control" type="text" name="course_requirements" placeholder="Example: Student should be able to read and write" v-model="form.course_requirements">
                </div>
              </div>



              <div class="col-md-12">
            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Course Objectives - What will students learn in this course?: <span class="tx-danger">*</span></label>



                  <textarea name="course_objectives"></textarea>

                </div>
            </div>





            <div class="col-md-12">

                <div class="form-group mg-b-10-force">
                      <label class="form-control-label">Biref Course Description  <span class="tx-danger">*</span></label>

                      <textarea name="course_brief_description"></textarea>



                </div>

                    </div>





            <div class="col-md-12">

            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Full Course Description - The description you write will help studenst know if the course is for them: <span class="tx-danger">*</span></label>


<textarea name="course_full_description"></textarea>

            </div>

                </div>

</div>



                <div class="form-actions ml-2"  style="float:left; margin-top:10px;">

                   <!-- <button type="button" class="btn btn-info mr-1">

                        <i class="la la-save"></i> Create Course
                    </button>

                    <a class="btn btn-secondary" href="{{route('courses')}}">Cancel</a>-->


                <button class="btn btn-info mg-r-5 ml-3" style="cursor:pointer"> <i class="la la-save"></i> Create Course</button>
                <a class="btn btn-secondary" href="{{route('courses')}}">Cancel</a>
</div>
                </form>
        </div>
    </div>



</div>


            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>













@endsection
