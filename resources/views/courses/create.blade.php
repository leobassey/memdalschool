@extends('layouts.main')
@section('title', 'Courses')
@section('content')


<div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">

        <h6 class="">Create Course</h6>
      </nav>

      <div class="sl-pagebody">


        <div class="card pd-20 pd-sm-40">
        <div class="row mb-3">
        <div class="col-md-10"></div>
        <div class="col-md-2"><a class="btn btn-primary" href="{{route('courses.index')}}">Course Lists</a></div>

        </div>



        <div class="card pd-20 pd-sm-40">

          <p class="mg-b-20 mg-sm-b-30">INSTRUCTIONS: Create a course and add lectures to the course after course creation under course settings in the course lists section.</p>
<form action="{{route('courses.store')}}" method="post">
@csrf
          <div class="form-layout">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Course Title: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="title" placeholder="Enter course title" v-model="form.title">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Course Category: <span class="tx-danger">*</span></label>
                  <select class="form-control select2" data-placeholder="Select category" v-model="form.program" name="program">
                    <option label="Select category"></option>
                    <option value="1">United States of America</option>
                    <option value="2">United Kingdom</option>
                    <option value="3">China</option>
                    <option value="4">Japan</option>
                  </select>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Course Privacy: <span class="tx-danger">*</span></label>
                  <select class="form-control select2" data-placeholder="Choose country" v-model="form.privacy" name="privacy">
                    <option label="Choose country"></option>
                    <option value="1">Public only</option>
                    <option value="2">Private (staff only)</option>
                    <option value="3">Public and private for staff</option>
                  </select>
                </div>
              </div><!-- col-4 -->

              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Course Price: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="price"  placeholder="Enter price" v-model="form.price">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Promo Price: (optional)</label>
                  <input class="form-control" type="text" name="promo_price"  placeholder="Enter promo price" v-model="form.promo_price">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Course Type: <span class="tx-danger">*</span></label>
                  <select class="form-control select2" data-placeholder="Choose country" v-model="form.course_type" name="course_type">
                    <option label="Choose country"></option>
                    <option value="1">Online Course</option>
                    <option value="2">On Campus Course</option>
                    <option value="3">Free Course</option>
                  </select>
                </div>
              </div><!-- col-4 -->



              <div class="col-md-12">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Are there any course requirements?:</label>
                  <input class="form-control" type="text" name="course_requirements" placeholder="Example: Student should be able to read and write" v-model="form.course_requirements">
                </div>
              </div><!-- col-8 -->


             <div class="col-md-12">
            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Course Objectives - What will students learn in this course?: <span class="tx-danger">*</span></label>

                  <textarea  name="course_objectives" required id="summernote" v-model="form.course_objectives">

                  </textarea>

                </div>
            </div>

            <div class="col-md-12">
            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Brief Course Description - Not more than 30 charactures: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="course_brief_description" placeholder="Example: Student should be able to read and write" v-model="form.course_brief_description">
                </div>
            </div>

            <div class="col-md-12">

            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Full Course Description - The description you write will help studenst know if the course is for them: <span class="tx-danger">*</span></label>

                  <textarea  name="course_full_description" required id="summernote-desc" v-model="form.course_full_description">

</textarea>

            </div>


         <!--   <div class="col-md-12">

<div class="form-group mg-b-10-force">
      <label class="form-control-label">Course Message - Write a message to students (optional) that will be sent automatically when they join the course to encourage them to engage with the course content: </label>
      <div id="summernote-welcome-message">Hello, universe!</div>
    </div>
</div>


<div class="col-md-12">

<div class="form-group mg-b-10-force">
      <label class="form-control-label">Congratulations Message - Write a congratulations message to students (optional) that will be sent automatically when they complete the course:</label>
      <div id="summernote-congratulation-message">Hello, universe!</div>
    </div>
</div>-->


<!--<div class="col-md-6 mt-5">jhsdjd</div>
<div class="col-md-6 mt-5">Upload quality course cover image. Important guides: 750*422 pixels; .jpg, .jpeg, .gif or .png</div>
-->
</div><!-- row -->



            <div class="form-layout-footer">
              <button class="btn btn-info mg-r-5">Submit Form</button>
              <a class="btn btn-secondary" href="{{route('courses.index')}}">Cancel</a>
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->


</form>
        </div>






</div>


      <script>

var vm = new Vue({

    el:"#app2",
    data:{

    programs:[],

    form:{'title':'','program':'', 'privacy':'', 'price':'', 'promo_price':'', 'course_type':'', 'course_requirements':'', 'course_objectives':'', 'course_brief_description':'', 'course_full_description':''},

    },
    methods:{

       /* deleteApproval:function(id)
        {

            var c = confirm("Are you sure you want to delete this record ?");
            if(c)
            {

                this.form.id = id;
                var _this = this;
                var input = this.form;

                axios.post('DeleteApproval', input).then(function(response){
                    alert('Approval deleted successfully');
                    _this.loadApproval();

                }).catch(function(error){
                    alert('There was error deleting approval. Please try again');

                })

        }

        },*/


       /* selectApproval:function(id)
        {

                var _this = this;
				this.ApprovalObject.id =  id;
				var input = this.ApprovalObject;

            axios.post('getApproval', input).then(function(response){
                _this.ApprovalObject = response.data;

            }).catch(function(error){
                alert('There was error getting this record. Please try again');

            })

        },*/



        createcourse:function()
        {

            var _this = this;
            var input = this.form;
            document.getElementById('btnAdd').disabled = true;
            axios.post('/c', input).then(function(response){
                alert('Approval created successfully');

                document.getElementById('btnAdd').disabled = false
                _this.form = {'title':'','program':'', 'privacy':'', 'price':'', 'promo_price':'', 'course_type':'', 'course_requirements':'', 'course_objectives':'', 'course_brief_description':'', 'course_full_description':''};
            }).catch(function(error){
                alert('There was error creating record. Please try again');
                document.getElementById('btnAdd').disabled = false;
            })

        },

       /* updateApproval:function()
        {

            var _this = this;
            var input = this.ApprovalObject;
            document.getElementById('btnUpdate').disabled = true;
            axios.post('UpdateApproval', input).then(function(response){

                alert('Changes saved successfully');
                _this.loadApproval();
                document.getElementById('btnUpdate').disabled = false
                _this.showEditApprovalModal = false;
            }).catch(function(error){
                alert('There was error updating approval. Please try again');
                document.getElementById('btnUpdate').disabled = false;
            })

        },*/

                loadprograms:function()
                   {

                     var _this = this;

                     axios.get('loadprograms').then(function(response){
                    _this.programs = response.data;

                    console.log(_this.programs)

                  });

           },



    },

    created:function()
    {
        this.loadprograms();
    }

})

</script>


@endsection
