@extends('layouts.adminlayout')
@section('title', 'Manage section')

<style>
.user-tab {
    position: relative;
    cursor: pointer;
    display: block;
    border-radius: 8px;
    padding: 15px;
    margin: -5px;
    margin-bottom: 20px;
    height: 150px;
    /* background: #2980AC; */
    background: #d9dfe2;
}

.user-tab div {
    position: absolute;
    bottom: 10px;
    left: 0;
    width: 100%;
    padding: 5px;
    background-color: #fff;
    text-align: center;
}

h2 {
    font-size: 16px;
    line-height: 16px;
    font-weight: 400;
}

h1 {
    font-size: 24px;
    line-height: 24px;
    font-weight: 400;
}



        .choose::-webkit-file-upload-button {
  color: white;
  display: inline-block;
  background: #1CB6E0;
  border: none;
  padding: 7px 15px;
  font-weight: 700;
  border-radius: 3px;
  white-space: nowrap;
  cursor: pointer;
  font-size: 10pt;
}







.has-search .form-control {
    padding-left: 1.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;

}

.text-small{
    height:32px;
    font-size:13px;
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.label-small
{
    font-size: 12px;
}



.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #6b6f82;
}
.table th, .table td {
    padding: 0.50rem;
    font-size:12px;
}
.table th
{
    background-color:#02b159;
    color:#fff;
}
#app2 {
    font-family: TheRoboto;
}

.btn-info {
    border-color: #02b159 !important;
    background-color: #02b159 !important;
    border-radius: 0.21rem;
}

.btn-info:hover {
    background-color: #02b159 !important;
    border-color: #02b159 !important;
}

#overlay
		{
			position: fixed;
			top: 40;
			bottom: 1003;
			left: 100;
			right: 2;
			background:rgba(0,0,0,0, 0.6);
		}

        .row {
    display: flex;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}


</style>

@section('content')

<div id="app2">
</template>


<div class="sl-mainpanel">

<div class="container">


      <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header" style="">


                                <div class="row">
                                <div class="col-md-8">
                                <h5 class="" style="color:#333">Add Sections to Course: {{$courseObject->course_title}}</h5>
                                </div>
                                <div class="col-md-4">
                               <a class="btn " style="background-color:#02b159; color:#fff" a href="#" id="btnAdd" data-toggle="modal" data-target=".bd-example-modal-lg">Create Sections</a>
                                <a class="btn " style="background-color:#02b159; color:#fff" a href="{{route('coursesettings', $courseObject->id)}}"><< Course Settings</a>
                                </div>
                                </div>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">


                                <form>





                            </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">

                                <div class="" style="margin-top:-15px">
                                <!--<p class="mt-1 mb-1" style="color:#333">The table below lists all the lectures added to the course: <strong>{{$courseObject->course_title}}.</strong> You can click on the pencil icon to edit a lecture or click on the bin icon to delete a lecture.</p>-->

            <table class="table table-bordered">
       <tr>

            <th>Section Name</th>
		    <th>Section Title</th>

            <th>Action</th>

     </tr>

     <tr v-for="s in sections">

    <td style="color:#333">@{{s.section_name}}</td>
    <td style="color:#333">@{{s.section_title}}</td>

    <td><a href="#" @click="showEditModal=true; SelectSection(s.id)"><img src="{{asset('img/edit.png')}}" width="20px"/></a>
    |
    <a href="#" @click="DeleteSection(s.id)"><img src="{{asset('img/delete.png')}}" width="20px"/></a>
    </td>


     </tr>



   </table>
               </div>




                            </div>

                        </div>
                    </div>



     <!--  Large Modal -->
     <div class="modal fade bd-example-modal-lg mt-5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">

                           <h5 class="modal-title ml-3" id="exampleModalCenterTitle" style="margin-left:-10px">Create Course Section</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                            <div class="container ml-2" style="color:#333">See a course as a book, a section as chapters in a book and lectures as contents grouped by chapter <br/>(section in this case).</div>
                            <div class="d-flex justify-content-center">


                            </div>
                            <form enctype="multipart/form-data" id="menuform">
                            <div class="row" style="margin:25px">

<div class="col-md-6" >
    <div class="form-group">


    </div>
</div>

<div class="col-md-6">
    <div class="form-group">

    </div>
</div>


</div>

<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">
        <label>Section Name</label>
<input type="text" class="form-control"  placeholder="Example: Section 2" v-model="form.section_name" style="height:50px">

</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Section Title</label>
<input type="text" class="form-control"  placeholder="Example: Business Model Canvas" v-model="form.section_title" style="height:50px">

</div>
</div>

</div>




<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">


<img src="{{asset('img/loading.gif')}}" width="50px" v-show="loader"/><span v-show="loader">Creating section please wait...</span>



<div class="alert alert-success alert-dismissible fade show" role="alert"  v-show="success" style="height:40px">

<p v-show="success" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>

<div class="alert alert-danger alert-dismissible fade show" role="alert"  v-show="error" style="height:40px">

<p v-show="error" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>



</div>



</div>


</div>
                                <button class="btn" type="button"  style="background-color:#02b159; color:#fff; cursor:pointer" @click.prevent() = "postsection" id="btnpost">Submit</button>
                            </div>

                            </form>
                        </div>
                    </div>



                    <!-- EDIT-->
                    <!--  Large Modal -->
    <div id="overlay" class="overlay" v-if="showEditModal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">

                                <h5 class="modal-title ml-5" id="exampleModalCenterTitle">Edit Course Section</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.prevent="showEditModal=false"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">

                            <div class="d-flex justify-content-center">




                            </div>

                            <form enctype="multipart/form-data" id="menuform">
                            <div class="row" style="margin:25px">

<div class="col-md-6" >
    <div class="form-group">


    </div>
</div>

<div class="col-md-6">
    <div class="form-group">

    </div>
</div>


</div>

<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">
<div class="form-group">
        <label>Section Name</label>
<input type="text" class="form-control"  placeholder="Example: Section 2" v-model="SectionObject.section_name" style="height:50px">
<input type="hidden" class="form-control text-small"  v-model="SectionObject.id">
</div>
</div>


<div class="col-md-12">
<div class="form-group">
        <label>Section Title</label>
<input type="text" class="form-control"  placeholder="Example: Business Model Canvas" v-model="SectionObject.section_title" style="height:50px">

</div>
</div>

</div>




<div class="row" style="margin:25px; margin-top:-28px">

<div class="col-md-12">


<img src="{{asset('img/loading.gif')}}" width="50px" v-show="loader"/><span v-show="loader">updating section please wait...</span>



<div class="alert alert-success alert-dismissible fade show" role="alert"  v-show="success" style="height:40px">

<p v-show="success" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>

<div class="alert alert-danger alert-dismissible fade show" role="alert"  v-show="error" style="height:40px">

<p v-show="error" class="lead" style="margin-top:-10px">@{{message}}</p>

</div>



</div>



</div>


</div>
    <button class="btn" type="button"  style="background-color:#02b159; color:#fff; cursor:pointer" @click.prevent() = "updatesection" id="btnupdate">Save Changes</button>
    </div>

</form>

                        </div>
                    </div>





                </div>





                </div>

</div>



</div>


</template>
</div>

      <script>

var vm = new Vue({

    el:"#app2",
    data:{

    programs:[],
    file:'',
    loader:false,
    success:false,
    error:false,
    message:'',
    image: '',

    courses:[],
    sections:[],
    lectures:[],

    showEditModal:false,

    form:{'id':'', 'course_id':{{$courseObject->id}}, 'section':0, 'section_title':'', 'section_name':''},
    SectionObject:{'id':'', 'course_id':{{$courseObject->id}}, 'section':0, 'section_title':'', 'section_name':''},



    showAddCourseImageModal:true
    //showEditCurrencyModal:false,

    },
    methods:{
        onFileChange(e)
        {
         this.file = e.target.files[0];

        },

        DeleteSection:function(id)
        {

            var c = confirm("Are you sure you want to delete this record ?");
            if(c)
            {

                this.form.id = id;
                var _this = this;
                var input = this.form;

                axios.post('../DeleteSection', input).then(function(response){
                    alert('Section deleted successfully');
                  //  _this.loadsections();
                  location.reload();

                }).catch(function(error){
                    alert('There was error deleting record. Please try again');

                })

        }

        },

        createcourse:function()
        {

            var _this = this;
            var input = this.form;
            document.getElementById('btnAdd').disabled = true;
            axios.post('/c', input).then(function(response){
                alert('Approval created successfully');

                document.getElementById('btnAdd').disabled = false
                _this.form = {'title':'','program':'', 'privacy':'', 'price':'', 'promo_price':'', 'course_type':'', 'course_requirements':'', 'course_objectives':'', 'course_brief_description':'', 'course_full_description':''};
            }).catch(function(error){
                alert('There was error creating record. Please try again');
                document.getElementById('btnAdd').disabled = false;
            })

        },


        loadprograms:function()
                   {

                     var _this = this;

                     axios.get('loadprograms').then(function(response){
                    _this.programs = response.data;

                    console.log(_this.programs)

                  });

           },

           AddImage:function(id)
           {
               this.form.course_id = id;
           },

           loadlectures:function()
                   {

                    var _this = this;
                    var input = this.form;

                    axios.post('/load_lectures', input).then(function(response){
                    _this.lectures = response.data;




                  });

         },

         loadcourses:function()
                   {

                     var _this = this;

                     axios.get('load_courses').then(function(response){
                    _this.courses = response.data;

                  });


                   },

                   loadsections:function()
                   {

                     var _this = this;
                     var input =  this.form;



                     axios.post('../load_sections', input).then(function(response){
                    _this.sections = response.data;



                  });


                   },

                   loadlecturesbysectionid:function()
                    {

		            	var _this = this;
                        var input =  this.form;

                        axios.post('../loadlecturesbysectionid', input).then(function(response){
                       _this.lectures = response.data;

                    });

                    },




                postsection:function()
                 {




                    var  _this = this;

                    if(this.form.section_name == "")
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please enter section name";
                        return;
                    }
                    else if(this.form.section_title == "")
                    {
                        _this.success = false;
                        _this.error = true;
                        _this.message = "Please enter section title";
                        return;
                    }

                    else
                    {


                var _this = this;
                let currentObj = this;

                let formData = new FormData();
                formData.append('file', this.file);
                formData.append('course_id', this.form.course_id);
                formData.append('section_name', this.form.section_name);
                formData.append('section_title', this.form.section_title);

             document.getElementById('btnpost').disabled = true
            _this.loader = true;
            _this.error = false;
            _this.success = false;
           // axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
         //   axios.defaults.xsrfCookieName = 'csrftoken'
          //  axios.defaults.xsrfHeaderName = 'X-CSRFToken'

            //'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            axios.post('../postsection', formData).then(function(response){

                _this.loadsections();
                _this.error = false;
                _this.success = true;
               // _this.message = "Lecture uploaded successfully";
                 _this.message = response.data;
                _this.form = { 'course_id':{{$courseObject->id}},'section_name':'', 'section_title':'', 'course_id':0},

                _this.loader = false;
                document.getElementById('btnpost').disabled = false

            }).catch(function(error){
                document.getElementById('btnpost').disabled = false

                _this.success = false;
                _this.loader = false;
                _this.error = true;
                _this.message = error;

                document.getElementById('btnpost').disabled = false;

            })


        }

  },







                 SelectSection:function(id)
                   {

                        var _this = this;
                        this.SectionObject.id =  id;
                        var input = this.SectionObject;
                        axios.post('../getSectionById', input).then(function(response){
                        _this.SectionObject = response.data;

                    }).catch(function(error){
                        alert('Could not load record. Please try again');

                    })

                 },

                 updatesection:function()
                 {


                       var _this = this;
                        var input = this.SectionObject;

                        document.getElementById('btnupdate').disabled = true
                        _this.loader = true;
                        _this.error = false;
                        _this.success = false;
                        axios.post('../updatesection', input).then(function(response){


                _this.error = false;
                _this.success = true;
               // _this.message = "Lecture uploaded successfully";
                 _this.message = response.data;

                _this.loader = false;
                document.getElementById('btnupdate').disabled = false

                        _this.loadsections();
                        _this.showEditModal = false;

                    }).catch(function(error){
                        alert('Could not update record. Please try again');

                        _this.success = false;
                _this.loader = false;
                _this.error = true;
                _this.message = error;

                document.getElementById('btnupdate').disabled = false;

                    })
                 }



    },

    created:function()
    {

        this.loadsections();
        this.loadlectures();

        //this.form.course_id = {{$courseObject->id}}
    }

})

</script>


@endsection
