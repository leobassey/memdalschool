@extends('layouts.weblayouts')

@section('content')

<div class="jumbotron" style="border-radius: 0px; height: 300px; background-size: cover; background-image: url({{asset('webassets/img/1920x800/jumbo.jpg')}})">
            <h1 class="text-white text-center mt-10"> Forgot Password </h1>
        </div>


<section class="padding-y-100 bg-light mb-5" style="">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mx-auto" >
        <div class="card shadow-v2 ml-5" style="width:550px">
         <div class="card-header border-bottom">
          <h4 class="mt-4">

          </h4>
         </div>
          <div class="card-body">
          @if ($message = Session::get('success'))
<div class="alert alert-success alert-block mb-3" style="width: 100%; margin-left: 7px;">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <strong style="color:green"> {{ session()->get('success') }}</strong>
</div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block mb-3" style="width: 100%; margin-left: 7px;">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <strong style="color:red"> {{ session()->get('error') }}</strong>
</div>
@endif

@if(count($errors) >0)
<ul style="color:red; font-weight:bold; list-style:none">
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
</ul>
@endif
           <!-- <div class="row">
              <div class="col my-2">
                <button class="btn btn-block btn-facebook">
                 <i class="ti-facebook mr-1"></i>
                 <span>Facebook Sign in</span>
               </button>
              </div>
              <div class="col my-2">
                <button class="btn btn-block btn-google-plus">
                 <i class="ti-google mr-1"></i>
                 <span>Google Sign in</span>
               </button>
              </div>
            </div>-->
          <!--  <p class="text-center my-4">
              OR
            </p>-->

            <form method="POST" action="{{ route('postforgotpassword') }}" class="px-lg-4">
                        @csrf
              <div class="input-group input-group--focus mb-3">


                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter email address associated with your account">

@error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
              </div>


              <button class="btn btn-block btn-primary">Send Password Reset Link</button>




            </form>
          </div>
        </div>
      </div>
    </div> <!-- END row-->
  </div> <!-- END container-->
</section>


<!--
<section class="padding-y-100 bg-light">
<div class="container">
    <div class="row">
      <div class="col-lg-6 mx-auto">
        <div class="card shadow-v2">
         <div class="card-header border-bottom">
          <h4 class="mt-4">
            Log In to Your EchoTheme Account!
          </h4>
         </div>
                <div class="card-body">
                <div class="row">
              <div class="col my-2">
                <button class="btn btn-block btn-facebook">
                 <i class="ti-facebook mr-1"></i>
                 <span>Facebook Sign in</span>
               </button>
              </div>
              <div class="col my-2">
                <button class="btn btn-block btn-google-plus">
                 <i class="ti-google mr-1"></i>
                 <span>Google Sign in</span>
               </button>
              </div>
            </div>
            <p class="text-center my-4">
              OR
            </p>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
