@extends('layouts.weblayouts')
@section('title', 'Online Course Details')


@section('content')



<div class="jumbotron" style="border-radius: 0px; height: 300px; background-size: cover; background-image: url({{asset('webassets/img/1920x800/jumbo.jpg')}})">
            <h2 class="text-white text-center mt-10" style="font-weight: bold;"> {{$course->course_title}} Training</h2>
        </div>
<!--<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url({{asset('assets/img/1920/background.jpg')}}) no-repeat">

<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-8 my-2 mx-auto text-white text-center">
            <h2 class="h4" style="font-size:35px">
                {{$course->course_title}} course
            </h2>
        </div>

    </div>
</div>

</div>-->

<section class="course-lists mt-10">
            <div class="container mb-5">
                <div class="row container">
                    <div class="col-lg-7 mb-4">



                    <h4 style="color: #000;"><span style="font-weight: 600;"> {{$course->course_title}}</h3>
                    <p class="card-text">
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i> (5.0 user rating)
                        </p>
            <p class="mb-4">


            <span>{!! $course->course_full_description !!}</span>
            </p>
            <h4>Skills you will learn</h4>

                <!--<i class="fas fa-check"></i> Basics of leadership & team development. <br />
                <i class="fas fa-check"></i> Strategic decision making & negotiation skills. <br />
                <i class="fas fa-check"></i> Essentials of project, crisis and time management. <br />-->

                <ul class="pl-0 mt-3" style="list-style-type: none;">
                <li>

               {!! $course->course_objectives !!}

                </li>

           <!-- <h4>About This Course</h4>
            <p class="mb-5">
                In today’s business world, management and leadership skills are essential for long-term success. This course teaches you how to manage organisations and empower people. You will learn various leadership models, discover different leadership styles and
                understand how teams develop.
                <br /> You will also learn the basics of project management, change management and time management. Furthermore, this course will teach you how to handle crises, how to make better decisions and how to become a superior, more effective
                negotiator. <br /><br /> You will also receive short case studies of Apple, Microsoft and the University of Oxford that summarize the key takeaways of this course.
            </p>-->



            <!--<div class="card padding-30 shadow-v3">
          <h4>
            Course Features Include:
          </h4>
          <ul class="list-inline mb-0 mt-2">
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">246 lectures</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">27.5 Hours</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">98,250 students entrolled</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">Lifetime access</span>
            </li>
            <li class="list-inline-item my-2 pr-md-4">
              <i class="fas fa-check"></i>
              <span class="ml-2">Certificate of Completion</span>
            </li>

          </ul>
        </div>-->



        <!--<div class="col-12 mt-4">
         <ul class="nav tab-line tab-line tab-line--3x border-bottom mb-5" role="tablist">
           <li class="nav-item">
            <a class="nav-link active show" data-toggle="tab" href="#tabDescription" role="tab" aria-selected="true">
              Description
            </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabCurriculum" role="tab" aria-selected="true">
              Curriculum
            </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabInstructors" role="tab" aria-selected="true">
              Instructors
            </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabReviews" role="tab" aria-selected="true">
              Reviews
            </a>
           </li>
         </ul>
          <div class="tab-content">
            <div class="tab-pane fade  active show" id="tabDescription" role="tabpanel">
              <h4>
                Course Description
              </h4>
              <p>
                Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius claritas est conctetur adip sicing. Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standad dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it make type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting remaining essentially unchanged.
              </p>
              <p>
                Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimen tum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi.
              </p>

              <div class="row mt-5">
               <div class="col-12">
                 <h4>
                  What Will I Learn?
                </h4>
               </div>
                <div class="col-md-12 my-2">
                  <ul class="list-unstyled list-style-icon list-icon-check">
                    <li>Learn how to captivate your audience</li>
                    <li>Get rid of negative self talk that brings you down before your presentations</li>
                    <li>Take your business / job to the next level</li>
                    <li>Overcome the fear of public speaking</li>
                  </ul>
                </div>


                <div class="col-12">
                 <h4>
                 Course Requirements
                </h4>
               </div>
                <div class="col-md-12 my-2">
                  <ul class="list-unstyled list-style-icon list-icon-check">
                    <li>Learn how to captivate your audience</li>
                    <li>Get rid of negative self talk that brings you down before your presentations</li>
                    <li>Take your business / job to the next level</li>
                    <li>Overcome the fear of public speaking</li>
                  </ul>
                </div>

                <div class="col-12">
                 <h4>
                 Target Audience
                </h4>
               </div>
                <div class="col-md-12 my-2">
                  <ul class="list-unstyled list-style-icon list-icon-check">
                    <li>Learn how to captivate your audience</li>
                    <li>Get rid of negative self talk that brings you down before your presentations</li>
                    <li>Take your business / job to the next level</li>
                    <li>Overcome the fear of public speaking</li>
                  </ul>
                </div>





              </div>
            </div> -->

            <!--<div class="tab-pane fade" id="tabCurriculum" role="tabpanel">
            <div id="accordionCurriculum">

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_1" data-toggle="collapse" aria-expanded="false">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                    <span class="h6 d-inline">Getting Started</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     6 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     20:20
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_1" class="collapse" data-parent="#accordionCurriculum" style="">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_2" data-toggle="collapse" aria-expanded="true">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                    <span class="h6 d-inline">HTML 5</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     19 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     2:37:10
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_2" class="collapse" data-parent="#accordionCurriculum">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_3" data-toggle="collapse" aria-expanded="true">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                    <span class="h6 d-inline">Bootstrap 4</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     8 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     40:40
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_3" class="collapse" data-parent="#accordionCurriculum">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>

              <div class="accordion-item list-group mb-3">

                <div class="list-group-item bg-light">
                 <a class="row collapsed" href="#accordionCurriculum_4" data-toggle="collapse" aria-expanded="true">
                   <span class="col-9 col-md-8">
                     <span class="accordion__icon text-primary mr-2">
                      <i class="ti-plus"></i>
                      <i class="ti-minus"></i>
                    </span>
                      <span class="h6 d-inline">JavaScript: The Tricky Stuff</span>
                   </span>
                   <span class="col-2 d-none d-md-block text-right">
                     12 Lectures
                   </span>
                   <span class="col-3 col-md-2 text-right">
                     1:20:40
                   </span>
                 </a>
                </div>

                <div id="accordionCurriculum_4" class="collapse" data-parent="#accordionCurriculum">

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Introduction To Getting Started Section
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">00:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <a class="col-9 col-md-8" href="#">
                         <i class="ti-file text-primary small mr-1"></i>
                        Your First Webpage
                      </a>
                      <span class="col-2 d-none d-md-block text-right">
                        <a href="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" data-fancybox="" class="text-success">Preview</a>
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">2:36</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Creating A Full Webpage
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">7:12</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-files small mr-1 text-primary"></i>
                        Accessing Elements - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Responding To A Click
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                  <div class="list-group-item">
                    <span class="row">
                      <span class="col-9 col-md-8" href="#">
                        <i class="ti-control-play small mr-1 text-primary"></i>
                        Changing Website Content - Files
                      </span>
                      <span class="col-3 col-md-2 ml-auto text-right">4:07</span>
                    </span>
                  </div>

                </div>
              </div>


              </div>
            </div>-->


           <!-- <div class="tab-pane fade" id="tabInstructors" role="tabpanel">
              <h4 class="mb-4">
                About Instructors
              </h4>

              <div class="border-bottom mb-4 pb-4">
                <div class="d-md-flex mb-4">
                  <a href="#">
                    <img class="iconbox iconbox-xxxl" src="assets/img/262x230/5.jpg" alt="">
                  </a>
                  <div class="media-body ml-md-4 mt-4 mt-md-0">
                    <h6>
                      Hasan Jubayer
                    </h6>
                    <p class="mb-2">
                      <i class="ti-world mr-2"></i> Web Developer and Instructor
                    </p>
                    <ul class="list-inline">
                      <li class="list-inline-item mb-2">
                        <i class="ti-user mr-1"></i>
                        147570 studends
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-book mr-1"></i>
                        20 Courses
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-star text-warning mr-1"></i>
                        4.9 Average Rating (4578)
                      </li>
                    </ul>
                    <a href="#" class="btn btn-outline-primary btn-pill btn-sm">View Profile</a>
                  </div>
                </div>
                <h6>
                  Experience as Web Developer
                </h6>
                <p>
                  Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius claritas est conctetur adip sicing. Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standad dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it make type specimen book. It has survived not only five centuries.
                </p>
              </div>

              <div class="border-bottom mb-4 pb-5">
                <div class="d-md-flex mb-4">
                  <a href="#">
                    <img class="iconbox iconbox-xxxl" src="assets/img/262x230/6.jpg" alt="">
                  </a>
                  <div class="media-body ml-md-4 mt-4 mt-md-0">
                    <h4>
                      Hasan Jubayer
                    </h4>
                    <p class="mb-2">
                      <i class="ti-world mr-2"></i> Web Developer and Instructor
                    </p>
                    <ul class="list-inline">
                      <li class="list-inline-item mb-2">
                        <i class="ti-user mr-1"></i>
                        147570 studends
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-book mr-1"></i>
                        20 Courses
                      </li>
                      <li class="list-inline-item mb-2">
                        <i class="ti-star text-warning mr-1"></i>
                        4.7 Average Rating (4578)
                      </li>
                    </ul>
                    <a href="#" class="btn btn-outline-primary btn-pill btn-sm">View Profile</a>
                  </div>
                </div>
              </div>

            </div>-->

           <!-- <div class="tab-pane fade" id="tabReviews" role="tabpanel">
             <h4 class="mb-4">
               Students Feedback
             </h4>

              <div class="row px-0 align-items-center border p-4">
               <div class="col-md-4 text-center">
                  <h1 class="display-4 text-primary mb-0">
                    4.70
                  </h1>
                  <p class="my-2">
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="mb-0">
                    Average Rating
                  </p>
               </div>
               <div class="col-md-8">
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:100%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>90%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:80%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>87%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:60%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>34%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:40%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>12%</span>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                       <div class="width-7rem text-light d-none d-sm-block mr-3">
                        <i class="fas fa-star text-warning"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                        <div class="progress flex-auto mr-3" style="height:10px">
                          <div class="progress-bar bg-primary" style="width:10%" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100"></div>
                        </div>
                       <span>2%</span>
                    </div>
               </div>
              </div>

              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <img class="iconbox iconbox-xl" src="assets/img/avatar/4.jpg" alt="">
                  <div class="media-body ml-4">
                   <small class="text-gray">7 min ago</small>
                   <h6>
                     Anthony Forsey
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="font-size-18">
                    Awesome course
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                  </p>
                </div>
              </div>

              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <img class="iconbox iconbox-xl" src="assets/img/avatar/5.jpg" alt="">
                  <div class="media-body ml-4">
                   <small class="text-gray">1 mon ago</small>
                   <h6>
                     Justin Nam
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p class="text-light">
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                  </p>
                  <p class="font-size-18">
                    Test review lol
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty.
                  </p>
                </div>
              </div>
              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <div class="iconbox iconbox-xl border">
                    MD
                  </div>
                  <div class="media-body ml-4">
                   <small class="text-gray">3 Mon ago</small>
                   <h6>
                     Murir Dokan
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="font-size-18">
                    This is a title of review. the developer suffer from lot's of vitamin. what about you?
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                  </p>
                </div>
              </div>


              <div class="row border-bottom mx-0 py-4 mt-4">
                <div class="col-md-4 my-2 media">
                  <img class="iconbox iconbox-xl" src="assets/img/avatar/6.jpg" alt="">
                  <div class="media-body ml-4">
                   <small class="text-gray">1 year ago</small>
                   <h6>
                     John Doe
                   </h6>
                  </div>
                </div>
                <div class="col-md-8 my-2">
                  <p>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                    <i class="fas fa-star text-warning"></i>
                  </p>
                  <p class="font-size-18">
                    Best course ever
                  </p>
                  <p>
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                    Investig ationes demons travge vunt lectores legee lrus quodk legunt saepius was claritas kesty they conctetur they kedadip lectores legee sicing.
                  </p>
                </div>
              </div>
              <div class="text-center mt-5">
                <a href="#" class="btn btn-primary btn-icon">
                  <i class="ti-reload mr-2"></i>
                  Load More
                </a>
              </div>
            </div>
          </div>
      </div>-->
        </div>


        <div class="col-lg-5" data-aos="fade-up" data-aos-once="true" data-aos-delay="100">
                        <div class="card" style="border: none;">
                        @if($course->image_url == "")
            <img class="card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="card-img-top" src="../{{$course->image_url}}" alt="course image"/>
           @endif
                            <div class="card-body text-left">

                                <div class="row">
                                    <div class="col-lg-4 col-4 text-left">
                                        <h6 class="my-0" style="color: #999999;"><del>&#x20A6; {{$course->promo_price}}</del></h6>
                                        <h5 class="my-0 text-left" style="font-weight: 600;">&#x20A6;{{$course->course_price}}</h5>
                                    </div>
                                    <div class="col-lg-8 col-4 text-right">
                                        @if($course->course_status == 2)
                                        <a href="#" class="w-100 btn-lg courses-all headBtn btn">Enrollment CLOSED</a>

                                        @else
                                        <a href="{{route('enrollment', $course->id)}}" class="w-100 btn-lg courses-all headBtn btn">Enroll Now</a>
                                        @endif
                                    </div>
                                </div>

                                <hr>

                                <div class="card-text d-flex bd-highlight">
                        <p class="flex-fill bd-highlight mb-2">
                            <i class="fas fa-laptop "></i> Type: <br />
                            <i class="fas fa-stopwatch"></i> Duration: <br />
                            <i class="fas fa-star"></i> Award: <br />


                        </p>
                        <p class="flex-fill bd-highlight mb-2">
                          <i class="fas fa-laptop mr-2" style="visibility: hidden;"></i>

                          @if($course->course_status == 1)
                          Online
                          @elseif($course->course_status == 2)
                          Classroom
                          @else
                          Free
                          @endif

                          <br />
                          <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem; visibility: hidden;"></i> {{$course->duration}} <br />
                          <i class="fas fa-star mr-2" style="margin-left: 0.06rem; visibility: hidden;"></i> Certificate <br />


                      </p>


                    </div>

                </div>
                @if($course->content_url == "")
                <a href="#" class="ml-3 mb-4 email-top2" style="color: #0d0033; text-decoration: none; font-weight: bold; font-size:19px">Download course contents</a>
                @else
                <a href="../{{$course->content_url}}" target = "_blank" class="ml-3 mb-4 email-top2" style="color: #0d0033; text-decoration: none; font-weight: bold; font-size:19px">Download course contents</a>
                @endif
                <a href="#" class="ml-3 mb-4 email-top2" style="color: #0d0033; text-decoration: none; font-weight: bold; font-size:19px; margin-top:-20px">Enquires call: 08102555913</a>

            </div>
                            </div>
                        </div>
                    </div>

      <!--  <div class="col-md-4 animate__animated animate__pulse">
            <div class="card mb-3" style="border: 1px solid rgba(0,0,0,.125) !important;">
            @if($course->image_url == "")
            <img class="card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="card-img-top" src="../{{$course->image_url}}" alt="course image"/>
           @endif

                <div class="card-body pt-0">
                    <div class="cards bd-highlight">
                        <h5 class="card-title flex-fill bd-highlight">

                            <br />
                            <span class="current flex-fill bd-highlight enroll-span">Price: &#8358;{{$course->course_price}}</span>
                            <a class="btn btn-primary ml-2 enroll" href="{{route('enrollment', $course->id)}}" style="color:#ffff">Enroll Now</a>
                        </h5>

                    </div>
                    <div class="card-text d-flex bd-highlight">
                        <p class="flex-fill bd-highlight mb-2">
                            <i class="fas fa-laptop mr-2"></i> Type: <br />
                            <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem;"></i> Duration: <br />
                            <i class="fas fa-star mr-2"></i> Award: <br />


                        </p>
                        <p class="flex-fill bd-highlight mb-2">
                          <i class="fas fa-laptop mr-2" style="visibility: hidden;"></i> Online <br />
                          <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem; visibility: hidden;"></i> {{$course->duration}} <br />
                          <i class="fas fa-star mr-2" style="margin-left: 0.06rem; visibility: hidden;"></i> Certificate <br />


                      </p>


                    </div>

                </div>
                <a href="#" class="ml-5 mb-4 email-top2" style="color: #00b249; text-decoration: none; font-weight: bold;">Download course contents</a>

            </div>
        </div>-->
    </div>
</div>
</section>



<div class="container">SECTION: COURSE CERTIFICATE BATCH</div>

<div class="container">SECTION: PROJECTS STUDENT WILL BUILD WITH SLIDER</div>




@endsection
