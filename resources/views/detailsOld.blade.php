@extends('layouts.weblayouts')
@section('title', 'Course Details')


@section('content')



<div class="jumbotron" style="border-radius: 0px; height: 300px; background-size: cover; background-image: url({{asset('webassets/img/1920x800/jumbo.jpg')}})">
            <h2 class="text-white text-center mt-10" style="font-weight: bold;"> {{$course->course_title}} Training</h2>
<h3 class="text-center text-white" style="font-weight: bold;">Location: Online</h3>

        </div>


<section class="course-lists mt-10">
            <div class="container mb-5">
                <div class="row container">
                    <div class="col-lg-7 mb-4">



                    <h4 style="color: #000;"><span style="font-weight: 600;"> {{$course->course_title}}</h3>
                    <p class="card-text">
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i>
                            <i class="fas fa-star" style="color: #faae2f; font-weight: 600;"></i> (5.0 user rating)
                        </p>
            <p class="mb-4">


            <span>{!! $course->course_full_description !!}</span>
            </p>

            <h4 style="color: #000;"><span style="font-weight: 600;">Skills you will learn</h3>

                <ul class="pl-0 mt-3" style="list-style-type: none;">
                <li>

               {!! $course->course_objectives !!}

                </li>


        </div>


        <div class="col-lg-5 " data-aos="fade-up" data-aos-once="true" data-aos-delay="100">
                        <div class="card" style="border: none;">
                        @if($course->image_url == "")
            <img class="card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="card-img-top" src="../{{$course->image_url}}" alt="course image"/>
           @endif
                            <div class="card-body text-left card">

                                <div class="row">
                                    <div class="col-lg-4 col-4 text-left">
                                        <h6 class="my-0" style="color: #999999;"><del>&#x20A6; {{$course->promo_price}}</del></h6>
                                        <h5 class="my-0 text-left" style="font-weight: 400;">&#x20A6;{{number_format($course->course_price,2)}}</h5>
                                    </div>
                                    <div class="col-lg-8 col-4 text-right">
                                        @if($course->course_status == 2)
                                        <a href="#" class="w-100 btn-lg courses-all headBtn btn">Enrollment CLOSED</a>

                                        @else
                                       <!-- <a href="{{route('apply')}}" class="w-100 btn-lg courses-all headBtn btn">Apply Now</a>-->
                                         <a href="{{route('enrollment', $course->id)}}" class="w-100 btn-lg courses-all headBtn btn">Apply Now</a>
                                        @endif


                                    </div>
                                </div>

                                <hr>

                                <div class="card-text d-flex bd-highlight">
                        <p class="flex-fill bd-highlight mb-2">
                            <i class="fas fa-laptop "></i> Type: <br />
                            <i class="fas fa-stopwatch"></i> Duration: <br />
                            <i class="fas fa-star"></i> Award: <br />


                        </p>
                        <p class="flex-fill bd-highlight mb-2">
                          <i class="fas fa-laptop mr-2" style="visibility: hidden;"></i>

                          @if($course->course_type == 1)
                          Online
                          @elseif($course->course_type == 2)
                          Classroom
                          @else
                          Free
                          @endif

                          <br />
                          <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem; visibility: hidden;"></i> {{$course->duration}} <br />
                          <i class="fas fa-star mr-2" style="margin-left: 0.06rem; visibility: hidden;"></i> Certificate <br />


                      </p>


                    </div>

                </div>
                @if($course->content_url == "")
                <a href="#" class="ml-3 mb-4 email-top2 mt-4" style="color: #0d0033; text-decoration: none; font-weight: bold; font-size:19px">Download course contents</a>
                @else
                <a href="../{{$course->content_url}}" target = "_blank" class="ml-3 mb-4 email-top2 mt-5" style="color: #0d0033; text-decoration: none; font-weight: bold; font-size:19px">Download course contents</a>
                @endif
                <a href="#" class="ml-3 mb-4 email-top2" style="color: #0d0033; text-decoration: none; font-weight: bold; font-size:19px; margin-top:-20px">Enquires call: 08102555913</a>

            </div>
                            </div>
                        </div>
                    </div>

      <!--  <div class="col-md-4 animate__animated animate__pulse">
            <div class="card mb-3" style="border: 1px solid rgba(0,0,0,.125) !important;">
            @if($course->image_url == "")
            <img class="card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="card-img-top" src="../{{$course->image_url}}" alt="course image"/>
           @endif

                <div class="card-body pt-0">
                    <div class="cards bd-highlight">
                        <h5 class="card-title flex-fill bd-highlight">

                            <br />
                            <span class="current flex-fill bd-highlight enroll-span">Price: &#8358;{{$course->course_price}}</span>
                            <a class="btn btn-primary ml-2 enroll" href="{{route('enrollment', $course->id)}}" style="color:#ffff">Enroll Now</a>
                        </h5>

                    </div>
                    <div class="card-text d-flex bd-highlight">
                        <p class="flex-fill bd-highlight mb-2">
                            <i class="fas fa-laptop mr-2"></i> Type: <br />
                            <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem;"></i> Duration: <br />
                            <i class="fas fa-star mr-2"></i> Award: <br />


                        </p>
                        <p class="flex-fill bd-highlight mb-2">
                          <i class="fas fa-laptop mr-2" style="visibility: hidden;"></i> Online <br />
                          <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem; visibility: hidden;"></i> {{$course->duration}} <br />
                          <i class="fas fa-star mr-2" style="margin-left: 0.06rem; visibility: hidden;"></i> Certificate <br />


                      </p>


                    </div>

                </div>
                <a href="#" class="ml-5 mb-4 email-top2" style="color: #00b249; text-decoration: none; font-weight: bold;">Download course contents</a>

            </div>
        </div>-->
    </div>

</div>
</section>

<section class=" mb-5" style="background-color: rgba(254, 254, 2); color:#333;">
<div class="col-md-12 text-center mb-3" style="padding-top: 40px;">
                    <h3 class="mt-2" style="font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-weight: 600;
                    font-style: normal;
                    font-stretch: normal;
                    letter-spacing: normal; font-size: 1.857rem;
    line-height: 2.711rem;">Projects you will build</h3>
    <p class="lead" style="margin-left: auto; margin-right: auto; width:40em">When training ends, lab begins - is our popular mantra. The hands-on projects is to deepen your horizon with the skills you need to succeed as a data analyst.</p>
                </div>


            <div class="row">
<div class="container">
                <div class="col-md-12 certificates mt-5 mb-5" style=""
                data-aos="fade-up" data-aos-once="true">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{asset('img/pos.png')}}" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('img/dashboard.png')}}" alt="Second slide">
    </div>

    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('img/uddrinks.png')}}" alt="Second slide">
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
                   <!-- <img src="{{asset('img/pos.png')}}" alt="Certificate">-->
                </div>

</div>
</section>
<!--
<section class="certificate mb-5" style="background-color: rgba(254, 254, 2); color:#333;">
        <div class="container mt-1">

        <div class="col-md-12 text-center mb-3" style="padding-top: 40px;">
                    <h3 class="mt-2" style="font-family: AvenirNext,Helvetica,Arial,sans-serif;
                    font-weight: 600;
                    font-style: normal;
                    font-stretch: normal;
                    letter-spacing: normal; font-size: 1.857rem;
    line-height: 2.711rem;">Projects you will build</h3>
                </div>


            <div class="row">

                <div class="col-md-12 col-lg-6 certificates mt-5 mb-5" style="margin-left: -20px;"
                data-aos="fade-up" data-aos-once="true">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{asset('img/pos.png')}}" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('img/dashboard.png')}}" alt="Second slide">
    </div>

    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('img/uddrinks.png')}}" alt="Second slide">
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

                </div>
                <div class="col-md-12 col-sm-12 col-lg-6 certificates" style="padding-bottom: 60px; margin-left: 20px;">
                    <div class="card-text mb-4 d-flex bd-highlight"
                    data-aos="fade-up" data-aos-once="true" data-aos-delay="100" style="margin-top:-50px">


                    </div>
                    <div class="card-text mb-2 d-flex bd-highlight"
                    data-aos="fade-up" data-aos-once="true" data-aos-delay="200">


                        <div class="col-md-10 mb-5" style="margin-top: -20px; color:#333">

<p class="lead">
                         In this project, you will learn the best practices in developing a software application from start to finish. You will analize the project, model database. You will use GIT and apply all the things taught in this course to development a complete POS software and deploy to the server
                         <br/><br/>
                        In the data analytics project, you will carry out the ETL process on your dataset and create a sale analysis dashboard from start to finish.
</p>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </section>
-->


<!--<div class="container">SECTION: COURSE CERTIFICATE BATCH</div>

<div class="container">SECTION: PROJECTS STUDENT WILL BUILD WITH SLIDER</div>-->




@endsection
