@extends('layouts.weblayouts')
@section('title', 'Online Course Details')


@section('content')

<style>
.card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: cover;
}
</style>

<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url({{asset('assets/img/1920/background.jpg')}}) no-repeat">

<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-8 my-2 mx-auto text-white text-center">
            <h2 class="h4" style="font-size:35px">
                {{$course->course_title}} course
            </h2>
        </div>

    </div>
</div>

</div>



<section class="courseinfo mt-5">
<div class="container" style="max-width:1140px">
    <div class="row">

        <div class="col-md-8 details">
            <h3 style="font-weight: bolder;font-size:30px"> {{$course->course_title}}</h3>
            <p class="mb-4">

                <i class="fas fa-star text-warning"></i>
                <i class="fas fa-star text-warning"></i>
                <i class="fas fa-star text-warning"></i>
                <i class="fas fa-star text-warning"></i>
                <span class="text-dark ml-1">(4.5 user rating)</span>


            </p>
            <p class="mb-4">


            <span>{!! $course->course_full_description !!}</span>
            </p>
            <h4>Skills you will learn</h4>
            <p>
                <!--<i class="fas fa-check"></i> Basics of leadership & team development. <br />
                <i class="fas fa-check"></i> Strategic decision making & negotiation skills. <br />
                <i class="fas fa-check"></i> Essentials of project, crisis and time management. <br />-->



                {!! $course->course_objectives !!}
            </p>

        </div>

        <div class="col-md-4 animate__animated animate__pulse">
            <div class="card mb-3" style="border: 1px solid rgba(0,0,0,.125) !important;">
            @if($course->image_url == "")
            <img class="card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
             @else
				<img class="card-img-top" src="../{{$course->image_url}}" alt="course image"/>
           @endif

                <div class="card-body pt-0">
                    <div class="cards bd-highlight">
                        <h5 class="card-title flex-fill bd-highlight">
                            <!--<span class="former">
                                &#8358;70,000.00
                            </span>-->
                            <br />
                            <span class="current flex-fill bd-highlight enroll-span">Price: &#8358;{{$course->course_price}}</span>
                            <a class="btn btn-primary ml-2 enroll" href="{{route('enrollment', $course->id)}}" style="color:#ffff">Enroll Now</a>
                        </h5>

                    </div>
                    <div class="card-text d-flex bd-highlight">
                        <p class="flex-fill bd-highlight mb-2">
                            <i class="fas fa-laptop mr-2"></i> Type: <br />
                            <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem;"></i> Duration: <br />
                            <i class="fas fa-star mr-2"></i> Award: <br />


                        </p>
                        <p class="flex-fill bd-highlight mb-2">
                          <i class="fas fa-laptop mr-2" style="visibility: hidden;"></i> Online <br />
                          <i class="fas fa-stopwatch mr-2" style="margin-right: 0.6rem !important; margin-left: 0.15rem; visibility: hidden;"></i> {{$course->duration}} <br />
                          <i class="fas fa-star mr-2" style="margin-left: 0.06rem; visibility: hidden;"></i> Certificate <br />


                      </p>


                    </div>

                </div>
               <!-- <a href="#" class="ml-5 mb-4 email-top2" style="color: #00b249; text-decoration: none; font-weight: bold;">Download course contents</a>-->

            </div>
        </div>
    </div>
</div>
</section>



<section class="padding-y-100 mt-5" style="background-color: #f8f9fa! important;">
        <div class="container" style="max-width:1140px">
            <div class="row">
                <div class="col-12 text-center" style="margin-top:-40px;">
                  <h2 class="text-left">
                      Students also enrolled
                      <a href="{{route('onlinecourses')}}" style="float: right; font-size: initial; margin-top: 13px;">All Courses</a>
                  </h2>

                </div>

                <!--  <div class="col-12 mt-3">
                    <div class="d-md-flex justify-content-between bg-white rounded shadow-v1 p-4">
                        <ul class="nav nav-pills nav-isotop-filter align-items-center my-2">

                            <a class="nav-link" href="online-courses.html" data-filter=".free-courses">Online Courses</a>
                            <a class="nav-link" href="classroom-courses.html" data-filter=".start-soon">In-Person Courses</a>
                            <a class="nav-link" href="topic-courses.html" data-filter=".free-courses">Topic-Based Courses</a>

                            <a class="nav-link" href="#" data-filter=".free-courses">Career Courses</a>
                            <a class="nav-link" href="#" data-filter=".free-courses">Free Courses</a>
                        </ul>


                    </div>
                </div>-->

                <!-- END row-->

                @foreach($courses as $c)
                <div class="col-lg-4 mt-3 mb-4" data-aos="fade-left" data-aos-once="true" data-aos-delay="{{$loop->index*100}}">

                <div class="card">
                        <a href="#">
                            <div class="rounded mainOverlay ">
                            @if($c->image_url == "")
                            <img class="rounded card-img-top" src="{{asset('assets/img/sample-course-img.png')}}" alt="course image"/>
                        @else
							<img class="rounded card-img-top" src="../{{$c->image_url}}" alt="course image"/>
                        @endif

                              <!--<p class="text-center overlayText">View Details</p>-->
                            </div>
                        </a>
                        <div class="card-body px-0 pl-3">

                            <a href="#" class="h4 my-2" style="font-size: 20px; font-weight: bold;">
                                   {{$c->course_title}}
                                </a>

                            <p class="mb-0">
                                <span class="text-dark ml-1">5.8</span>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>



                            </p>


                            <div class="card-text d-flex bd-highlight mt-2">
                                <p class="flex-fill bd-highlight"><i class="fas fa-check-square"></i> Course + Certificate <br>
                                    <i class="fas fa-stopwatch"></i>
                                    @if($c->course_type == 1)
                                    Type: Online
                                    @elseif($c->course_type == 2)
                                    Type: Classroom-based
                                    @else
                                    Type: Free course
                                    @endif
                                     </p>
                                <h4 class="h5 text-right ml-5">
                                    <span class="" style="font-weight: bold; margin-left:-40px">N{{$c->course_price}}</span>

                                </h4>

                            </div>
                            <a href="{{route('onlinecoursedetails', $c->id)}}" class="btn btn-primary mt-3" style="background-color:orange; color:#fff">View course details</a>

                        </div>
                    </div>

                </div>

@endforeach


            </div>
        </div>
    </section>

@endsection
